<?php
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>
  
 UPGRADE NOTE: 
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.  
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' )) {
  header ( 'Location: static_pages/' );
}
final class ADB {
    /**
     * @var MySql|AMySQLi
     */
    private $driver;
    public $error='';
    public $registry;

    /**
     * @param string $driver
     * @param string $hostname
     * @param string $username
     * @param string $password
     * @param string $database
     * @throws AException
     */
    public function __construct($driver, $hostname, $username, $password, $database) {
        $filename = DIR_DATABASE . $driver . '.php';
    if (file_exists($filename)) {
            /** @noinspection PhpIncludeInspection */
            require_once($filename);
    } else {
      throw new AException(AC_ERR_MYSQL, 'Error: Could not load database file ' . $driver . '!');
    }
        
    $this->driver = new $driver($hostname, $username, $password, $database);
    
    $this->registry = Registry::getInstance();
  }

    /**
     * @param string $sql
     * @param bool $noexcept
     * @return bool|stdClass
     */
    public function query($sql,$noexcept=false) {
      
        if ( $this->registry->has('extensions') ) {
          $result = $this->registry->get('extensions')->hk_query($this, $sql,$noexcept);
        } else {
          $result = $this->_query($sql,$noexcept);
        }
    
        if($noexcept && $result===false){
          $this->error = $this->driver->error;
        }
        return $result;
    }

    /**
     * @param string $table_name
     * @return string
     */
    public function table($table_name){
    //detect if encryption is enabled
    $postfix = '';
    if ( is_object($this->registry->get('dcrypt')) ) {
      $postfix = $this->registry->get('dcrypt')->posfix($table_name);
    }
    return DB_PREFIX . $table_name . $postfix;
  }

    /**
     * @param string $sql
     * @param bool $noexcept
     * @return bool|stdClass
     */
    public function _query($sql, $noexcept=false) {
    return $this->driver->query($sql,$noexcept);
    }
    
  public function escape($value) {
    return $this->driver->escape($value);
  }

    /**
     * @return int
     */
    public function countAffected() {
    return $this->driver->countAffected();
    }

    /**
     * @return int
     */
    public function getLastId() {
    return $this->driver->getLastId();
    }

    /**
     * @param $file
     * @return null
     */
  public function performSql($file){

    if ($sql = file($file)) {
      $query = '';
      foreach($sql as $line) {
        $tsl = trim($line);
        if (($sql != '') && (substr($tsl, 0, 2) != "--") && (substr($tsl, 0, 1) != '#')) {
          $query .= $line;
          if (preg_match('/;\s*$/', $line)) {
            $query = str_replace("`ac_", "`" . DB_PREFIX, $query);
            $result = $this->_query($query);
            if (!$result) {
              $this->error = mysql_error();
                            return null;
            }
            $query = '';
          }
        }
      }
    }
  }

  public function insert_with_trail($table_name,$set,$audit_log = false){
    
    $key = array_keys($set);
    $values = array_values($set);
    $sql = $this->_insert($table_name,$key,$set);

    // if($audit_log) $this->audit_log($data_before,$set,$audit_log);
    foreach ($set as $key => $value) {

        $string = preg_replace('/\'/', '', $value);
        $set[$key] = $string;
      
    }

    if($audit_log) $this->save_audit(false,false,$set,$audit_log);

    $result=$this->_query($sql,$noexcept);
    if($noexcept && $result===false){
      $this->error = $this->driver->error;
    }
    return $result;
  }

  private function _insert($table, $keys, $values){
    return "INSERT INTO ".$table." (".implode(', ', $keys).") VALUES (".implode(', ', $values).")";
  }

  
  public function update_with_trail($table, $values,$where,$audit_log = false){
    
    $sql = $this->_update_with_trail($table, $values,$where);
    $data_before = $this->_get_data_before_array($table, $values,$where);

   
    foreach ($values as $key => $value) {

        $string = preg_replace('/\'/', '', $value);
        $data_after[$key] = $string;
      
    }

    $data_before_compare = array_diff_assoc($data_before, $data_after);
    $data_after_compare = array_diff_assoc($data_after, $data_before);

    // var_dump($data_after_compare);exit();
    if (!is_null($data_after_compare['domicile_country'])||!is_null($data_after_compare['domicile_city'])||!is_null($data_after_compare['correspondence_country'])||!is_null($data_after_compare['correspondence_city'])) {
      $data_before_compare['domicile_country'] = $data_before['domicile_country'];
      $data_before_compare['domicile_city'] = $data_before['domicile_city'];
      $data_before_compare['correspondence_country'] = $data_before['correspondence_country'];
      $data_before_compare['correspondence_city'] = $data_before['correspondence_city'];

      $data_after_compare['domicile_country'] = $data_after['domicile_country'];
      $data_after_compare['domicile_city'] = $data_after['domicile_city'];
      $data_after_compare['correspondence_country'] = $data_after['correspondence_country'];
      $data_after_compare['correspondence_city'] = $data_after['correspondence_city'];
    
    }
    
    

    $data_before = $this->convert_array_to_string($data_before_compare); 
    if (!empty($data_before)){
      $data_before .= $where;  
    }

    //var_dump($data_before);exit();
    // if($audit_log) $this->audit_log($data_before,$data_after_compare,$audit_log);

    if($data_before && $data_after) $this->save_audit($data_before,$data_before_compare,$data_after_compare,$audit_log,$where);
    $result=$this->_query($sql,$noexcept);
    if($noexcept && $result===false){
      $this->error = $this->driver->error;
    }
    return $result;
  }

  public function _update_with_trail($table, $values,$where){
    foreach ($values as $key => $val){
      $valstr[] = $key . ' = '.$val;
    }
    $sql = "UPDATE ".$table." SET ".implode(', ', $valstr);
    $sql .= ($where != '' AND count($where) >=1) ? " WHERE ".$where : '';
    return $sql;
  }

  public function delete_with_trail($table,$where,$audit_log = false){
    $sql = "DELETE FROM ".$table ." WHERE ". $where ;
    $data_before = $this->_get_data_before($table,array("*"=>"*"),$where);
    if($audit_log) $this->audit_log($data_before, " " ,$audit_log);
    // if($audit_log) $this->save_audit($data_before, " " ,$audit_log);
    return $this->_query($sql,$noexcept);
  }

  private function _get_data_before($table, $values, $where){
    $sql = "SELECT ".implode(array_keys($values),",")." FROM ".$table ." WHERE ". $where ;
    $result=$this->_query($sql,$noexcept);
    return $this->convert_array_to_string($result->row);
  }

  private function _get_data_before_array($table, $values, $where){
    $sql = "SELECT ".implode(array_keys($values),",")." FROM ".$table ." WHERE ". $where ;
   /* if($table == 'abmmi_url_aliases'){
      echo "==>".$sql;
      exit();
    }*/
    $result=$this->query($sql);
    return $result->row;
  }

  private function audit_log($data_before , $data_after ,$audit_log){
    if (class_exists('ALog')) {
      $log_name = "audit-log-".date ( "Y-m-d" ).".txt";
      $log = new ALog(DIR_SYSTEM . 'logs/'.$log_name);
      // ******** FORMAT LOG PARAMS ******************
      // | user_login | IP | Login date | Logout date | Activity | Data Before | Data After
      // echo "<pre>";print_r($audit_log);exit;
      $data_after=$this->convert_array_to_string($data_after);
      $log->write('|'.$audit_log["user_login"]."|".$audit_log["IP"]."|".$audit_log["login"]." |".$audit_log["logout"]."|".$audit_log["Activity"]."| ".$data_before." |".$data_after);
      //log now saved into table 
    }
  }

  /* added by fazrin - create log only, not save any data */
  public function audit_log_only($table, $values,$where,$audit_log = false){
    $data_before = $this->_get_data_before_array($table, $values,$where);
    
    /*if($audit_log == 'test'){
      echo "<pre>";
      var_dump($data_before);
      exit();
    }
    */
    foreach ($values as $key => $value) {

        $string = preg_replace('/\'/', '', $value);
        $data_after[$key] = $string;
      
    }
    $data_before_compare = array_diff_assoc($data_before, $data_after);
    $data_after_compare = array_diff_assoc($data_after, $data_before);

    if ($audit_log) {
      $this->save_audit($data_before,$data_before_compare,$data_after_compare,$audit_log,$where);
    }
   
  }

  private function convert_array_to_string($data){
    $string = "";
    foreach ($data as $key => $value) {
      if($key=='password') $value = "***";
      $string .= $key.":".$value."; ";
    }
    return $string;
  }

  private function lookup_code($item_code){
    $select = "SELECT DISTINCT d.item_name from ".DB_PREFIX."lookup l JOIN ".DB_PREFIX."lookup_descriptions d ON l.lookup_id = d.lookup_id WHERE l.item_code='".$item_code."' AND language_id='".$_SESSION['content_language_id']."'";
    $val = $this->query($select)->row['item_name'];
    return $val;
  }
  private function location($country,$city){
    $select = "SELECT cd.name as country,zd.name as city FROM ".DB_PREFIX."countries c JOIN ".DB_PREFIX."country_descriptions cd ON c.country_id = cd.country_id JOIN ".DB_PREFIX."zones z ON cd.country_id = z.country_id JOIN ".DB_PREFIX."zone_descriptions zd ON z.zone_id = zd.zone_id WHERE cd.language_id = 1 AND zd.language_id=1 AND c.iso_code_2 = '".$country."' AND z.code='".$city."'";
    $data = $this->query($select)->row;
    return $data;
  }

  private function save_audit($data_before ,$data_before_array, $data_after ,$audit_log,$where=''){
    
    // insert into auditrails: audit_id, audited_datetime, audited_user, audited_ipaddress, login_datetime, logout_datetime, activity_name
    $sql = "INSERT INTO ".$this->table('audittrails').
           " (audited_datetime, audited_user, audited_ipaddress, login_datetime, logout_datetime, activity_name) VALUES 
             (NOW(), ".$audit_log['user_login'].",'".$audit_log['IP']."',DATE_FORMAT('".$audit_log['login']."','%Y-%m-%d %H:%i:%s'),DATE('".$audit_log['logout']."'),'".$audit_log['Activity']."')";

    $code_lists = array(
      'user_group_id' => array(
        'table' => 'user_groups',
        'field' => 'name'
      ),
      'tax_class_id' => array(
        'table' => 'tax_class_descriptions',
        'field' => 'title'
      ),
      'location_id' => array(
        'table' => 'locations',
        'field' => 'name'
      )
    );
    // lookup code list    
    $item_code_arr = $this->query("select item_code from ".DB_PREFIX."lookup GROUP BY item_code")->rows;
    foreach ($item_code_arr as $key => $value) {
      $lookup_code[] = $value['item_code'];  
    }
    $swap_lookup_key = array_flip($lookup_code);

    // zone code begin
    $code_zone = array(
      'domicile_country' => array(
        'country' => 'domicile_country',
        'city' => 'domicile_city'
      ),
      'domicile_city' => array(
         'country' => 'domicile_country',
        'city' => 'domicile_city'
      ),
      'correspondence_country' => array(
         'country' => 'correspondence_country',
        'city' => 'correspondence_city'
      ),
      'correspondence_city' => array(
         'country' => 'correspondence_country',
        'city' => 'correspondence_city'
      )
    );

    $where_field = $where_value = $values_after = $values_before = "";

    // date after begin
    foreach($data_after as $key => $val){
      // check item code lookup
      if (array_key_exists($val,$swap_lookup_key)) {
        $val = $this->lookup_code($val);
      }
      
      if (array_key_exists($key,$code_lists)) {
        $select = "SELECT ".$code_lists[$key]['field']." from ".DB_PREFIX.$code_lists[$key]['table']." WHERE ".$key."='".$val."' ";
        $val = $this->query($select)->row[$code_lists[$key]['field']];
      }
      // check zone code
      if (array_key_exists($key,$code_zone)) {
        $zone = $this->location($data_after[$code_zone[$key]['country']],$data_after[$code_zone[$key]['city']]);
        list($adress,$location) = explode('_',$key);
        $val = $zone[$location];
      }

      if (!($data_after[$key]==$data_before_array[$key])) {
        $fields .= $key." | ";
        $values_after .= $val." | ";
      }
    }
    
    
    // data before begin
    foreach($data_before_array as $key => $val){
      // check item code lookup
      if (array_key_exists($val,$swap_lookup_key)) {
        $val = $this->lookup_code($val);
      }

      // check code
      if (array_key_exists($key,$code_lists)) {
        $select = "SELECT ".$code_lists[$key]['field']." from ".DB_PREFIX.$code_lists[$key]['table']." WHERE ".$key."='".$val."' ";
        $val = $this->query($select)->row[$code_lists[$key]['field']];
      }

      if (array_key_exists($key,$code_zone)) {
        $zone = $this->location($data_before_array[$code_zone[$key]['country']],$data_before_array[$code_zone[$key]['city']]);
        list($adress,$location) = explode('_',$key);
        $val = $zone[$location];
      }
      if (!($data_after[$key]==$data_before_array[$key])) {
        $values_before .= $val." | ";
      }  
    }

    list($where_field, $where_value) = explode("=",$where);
    $where_field = trim($where_field);
    $where_value = trim(preg_replace('/\'/', '', $where_value));
    

    if(!strpos($where_value,'language_id')) {

      $fields .= $where_field." | ";

      $values_after .= $where_value." | ";
      $values_before .= $where_value." | ";
      // var_dump($values_before);exit();
    }else{

      list($search_params,$lang) = explode('AND',$where);
      list($where_key,$where_val) = explode('=',$search_params);
      list($lang_key,$lang_val) = explode('=',$lang);
      
      $lang_key = trim($lang_key);
      $lang_val = trim(preg_replace('/\'/', '', $lang_val));
      
      $fields .= $where_key." | ";
      $fields .= $lang_key." | ";
      $values_after .= $where_val." | ";
      $values_before .= $where_val." | ";

      // languages id to text
      $lang_sql = "SELECT name FROM ".DB_PREFIX."languages where ".$lang_key." ='".$lang_val."' ";    
      $lang_val = $this->query($lang_sql)->row['name'];
  
      $values_after .= $lang_val.' | ';
      $values_before .= $lang_val.' | ';
    }
    
    $fields = substr($fields, 0,-2);
    $values_after = substr($values_after, 0,-2);
    $values_before = substr($values_before, 0,-2);
    

    $this->query($sql);
     
     
    //insert into auditrails_dtl: audit_detail_id, audit_id, field_name, data_before, data_after
    $sql_dtl = "INSERT INTO ".$this->table('audittrail_dtl').
           " (audit_id, field_name, data_before, data_after) VALUES 
             (".$this->getLastId().",'".$fields."','".$this->escape($values_before)."','".$this->escape($values_after)."')";

    $this->query($sql_dtl);    
  }
}
