<?php 

$path = 'lib/phpseclib';
set_include_path(DIR_CORE.$path);
include_once('Crypt/RSA.php');

function publicKeyToHex($privatekey) {
	
	$rsa = new Crypt_RSA();

	$rsa->loadKey($privatekey);
	$raw = $rsa->getPublicKey(CRYPT_RSA_PUBLIC_FORMAT_RAW);
	return $raw['n']->toHex();			
}
