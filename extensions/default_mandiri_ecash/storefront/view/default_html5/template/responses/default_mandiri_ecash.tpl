<form action="<?php echo str_replace('&', '&amp;', $action); ?>" method="post" id="checkout">
	<input type="hidden" name="cmd" value="_cart"/>
	<input type="hidden" name="upload" value="1" />
	<input type="hidden" name="business" value="<?php echo $business; ?>"/>


	<?php if ( $logoimg ): ?>
		<input type="hidden" name="image_url" value="<?php echo $logoimg; ?>"/>
	<?php endif; ?>

	<?php if ( $cartbordercolor ): ?>
		<input type="hidden" name="cpp_cart_border_color" value="<?php echo $cartbordercolor; ?>"/>
	<?php endif; ?>

	<div class="control-group">
	   <div class="controls">
	   	<button class="btn btn-orange pull-right" title="<?php echo $button_confirm->name ?>" type="submit">
	   	    <i class="icon-ok icon-white"></i>
	   	    <?php echo $button_confirm->name?>
	   	</button>
	   	<a id="<?php echo $back->name ?>" href="<?php echo $back->href; ?>" class="btn mr10" title="<?php echo $back->text ?>">
	   	    <i class="icon-arrow-left"></i>
	   	    <?php echo $back->text ?>
	   	</a>
	    </div>
	</div>

</form>