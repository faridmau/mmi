<div class="heading">
  <div class="container">
    <?php if($text_finish) {?>
    <h2 class="animated fadeInUp delayp1"><?php echo $text_finish; ?></h2>
    <?php }else{?>
    <h2 class="animated fadeInUp delayp1"><?php echo $heading_title; ?></h2>
    <?php } ?>
    <p class="animated fadeInUp delayp1"></p>
  </div>
</div><!--.heading-->
<div class="container">

  <div class="row">
    <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">

      <div class="thumb">
        <div class="content p_20">

          <p class="text-center m_b_30">
            <img src="<?php echo $this->templateResource("/img/ic_finish.png"); ?>" width="75" style="display:block; margin:20px auto 0"><?php echo $text_message; ?>
          </p>
          <div class="m_t_20">
            <a href="<?php echo $continue ?>" class="btn btn-primary" style="width: 100px; display:block; margin: auto">&nbsp;<?php echo $button_continue ?></a>
          </div>

        </div><!--.content-->
      </div><!--.thumb-->

    </div><!--.col-->
  </div><!--.row-->
