<?php
/* ------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2013 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  Lincence details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

  UPGRADE NOTE:
  Do not edit or add to this file if you wish to upgrade AbanteCart to newer
  versions in the future. If you wish to customize AbanteCart for your
  needs please refer to http://www.AbanteCart.com for more information.
  ------------------------------------------------------------------------------ */
if (!defined('DIR_CORE')) {
    header('Location: static_pages/');
}

class ControllerResponsesExtensionDefaultMandiriEcash extends AController {

    public $data = array();

    public function main() {
        $this->data['button_confirm'] = 'Konfirmasi';
        $this->data['button_back'] = 'Kembali';
        $this->load->model('checkout/order');
        $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);

        $this->data['action'] = $this->html->getSecureURL('extension/default_mandiri_ecash/initiatepayment');

        if ($this->request->get['rt'] != 'checkout/guest_step_3') {
            $this->data['cancel_return'] = $this->html->getSecureURL('checkout/payment');
        } else {
            $this->data['cancel_return'] = $this->html->getSecureURL('checkout/guest_step_2');
        }

        if ($this->request->get['rt'] != 'checkout/guest_step_3') {
            $this->data['back'] = $this->html->getSecureURL('checkout/payment');
        } else {
            $this->data['back'] = $this->html->getSecureURL('checkout/guest_step_2');
        }

        $back = $this->request->get['rt'] != 'checkout/guest_step_3' ? $this->html->getSecureURL('checkout/payment') : $this->html->getSecureURL('checkout/guest_step_2');
        $this->data['back'] = HtmlElementFactory::create(array('type' => 'button',
                    'name' => 'back',
                    'text' => $this->language->get('button_back'),
                    'style' => 'button',
                    'href' => $back));
        $this->data['button_confirm'] = HtmlElementFactory::create(
                        array('type' => 'submit',
                            'name' => $this->language->get('button_confirm'),
                            'style' => 'button',
        ));

        $this->view->batchAssign($this->data);
        $this->processTemplate('responses/default_mandiri_ecash.tpl');
    }

    public function initiatepayment() {
        
        $this->loadLanguage('default_mandiri_ecash/default_mandiri_ecash');
        $this->extensions->hk_InitData($this, __FUNCTION__);
        if (!$this->customer->isLogged()) {
            $this->redirect($this->html->getSecureURL('account/login'));
        } else {
            $this->load->model('checkout/order');
            $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
            $ip_client = $_SERVER['REMOTE_ADDR']; //Alamat IP client ( pengguna)
            $ip_server = $_SERVER['SERVER_ADDR']; //ALamat IP Merchant
            $ecashID = $this->config->get('mandiri_ecash_mid'); // Digantikan Merchant ID 
            $amount = $order_info['total']; // amount tanpa separator, nomor sesuai jumlah barang yang dijual
            $amount = number_format($amount, 0, '', '');
            
            $products = $this->cart->getProducts();
            
            //added by fazrin, filter product yg sudah di proses agar tidak di proses lagi
            $current_order = array();
            // $this->session->data['products_filter'] = array();

            if(!$products){
                $this->session->data['products_filter'] = array();
                $this->session->data['curr_product_key'] = array();
                $this->session->data['curr_product_data'] = array();
            }
            if($this->session->data['products_filter']) $products = array_diff_key($products,$this->session->data['products_filter']);


            $numItems = count($products);

            /*foreach($products as $key => $p){
                $this->session->data['cart'][$key]['order_status'] = 'in_process';
            }*/

            foreach ($products as $key => $product) {
                 if(++$i === $numItems) {
//                      $description =  $description.$product['name'].' Qty. '.$product['quantity'].' ';
                      $description =  'Transaksi Topup ' . $product['name'];//;$description.$product['name'];
                 }else{
//                      $description =  $description.$product['name'].' Qty. '.$product['quantity'].' '. $this->language->get('text_add').' ';
                      $description =  'Transaksi Topup ' . $product['name'];//$description.$product['name'];
                 }
                 $current_order = array_slice($products,0,1,true);

                 $this->session->data['curr_product_key'] = $key;
                 $this->session->data['curr_product_data'] = $product;
                 // $this->session->data['products_filter'][$key] = $product;
                 break;
            }

            //fazrin - get 1 product untuk di proses
            $product_to_process = array_slice($products,0,1,true);
            foreach($product_to_process as $key => $value){
                $payment_method_pos = strpos($value['location'],$this->session->data['payment_method']['id']);
                $double_colon_pos = strpos($value['location'],":",$payment_method_pos)+1;
                $coma_pos = strpos($value['location'],",",$payment_method_pos);
                $merchant_id = trim(substr($value['location'],$double_colon_pos,($coma_pos-$double_colon_pos)));
                // echo $payment_method_pos." | ".$double_colon_pos." | ".$coma_pos."<hr>";
                // echo $value['location']."<hr>".$merchant_id_text;
                $amount = number_format($value['total']+$value['fee'],0,'','');
            }
            // $this->showDebug($merchant_id,false);

            $curr_product_id = $this->session->data['curr_product_key'];

            $clientAddress = $ip_client;
            $memberAddress = $ip_server;
            $toUsername = $merchant_id;//$ecashID;

            $trxid = $this->session->data['order_ids'][$curr_product_id]; // Digantikan nomor transaksi/Kode transaksi Merchant
            $returnUrl = $this->html->getURL('extension/default_mandiri_ecash/callback'); // Digantikan parameter return URL merchant
            $hash = sha1(strtoupper($toUsername) . $amount . $clientAddress);

            if($this->config->get('mandiri_ecash_server')==0){
                $soapUrl = "https://mandiri-ecash.com/ecommgateway/services/ecommgwws?wsdl";
                $redirectUrl = "https://mandiri-ecash.com/ecommgateway/payment.html?id=";
            }else if($this->config->get('mandiri_ecash_server')==1){
                $soapUrl = "https://www.mandiriecash.com/ecommgateway/services/ecommgwws?wsdl";
                $redirectUrl = "https://www.mandiriecash.com/ecommgateway/payment.html?id=";
            }

            $soapUser = $merchant_id; //$ecashID;  //  username ( Change to your MID )
            $soapPassword = $this->config->get('mandiri_ecash_pwd'); //p4ssw0rd00 //password sesuai yang dikirimkan melalui email 
            // $soapUser = 'reksadanamipu';
            // xml post structure

            //test untuk grant IP
            //$clientAddress = '202.138.229.149';
            //$memberAddress = '202.138.229.149';

            $xml_post_string = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://ws.service.gateway.ecomm.ptdam.com/">
                    <soapenv:Header/>
                        <soapenv:Body>
                            <ws:generate>
                                <params>
                                    <amount>' . $amount . '</amount>
                                    <clientAddress>' . $clientAddress . '</clientAddress>
                                    <description>' . $description . '</description>
                                    <memberAddress>' . $memberAddress . '</memberAddress>
                                    <returnUrl>' . $returnUrl . '</returnUrl>
                                    <toUsername>' . $toUsername . '</toUsername>
                                    <hash>' . $hash . '</hash>
                                    <trxid>' . $trxid . '</trxid>
                                </params>
                            </ws:generate>
                        </soapenv:Body>
                 </soapenv:Envelope>';

            $headers = array(
                "Content-type: text/xml;charset=\"utf-8\"",
                "Accept: text/xml",
                "Cache-Control: no-cache",
                "Pragma: no-cache",
                "Content-length: " . strlen($xml_post_string),
            );
            //SOAPAction: your op URL
            // $this->showDebug($xml_post_string);
            $url = $soapUrl;

            // PHP cURL  for https connection with auth
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
            curl_setopt($ch, CURLOPT_SSLVERSION, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
            curl_setopt($ch, CURLOPT_USERPWD, $soapUser . ":" . $soapPassword); // username and password - declared at the top of the doc  
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            // converting

            $data = curl_exec($ch);
            /*$this->showDebug($data,false);
            echo "<hr/>";
            $this->showDebug($soapUser." | ".$soapPassword." | ".$soapUrl,false);
            echo "<hr/>";
            $this->showDebug($xml_post_string,false);
            echo "<hr/>";
            $this->showDebug($products,false);*/
            // $this->showDebug($soapUser." | ".$soapPassword);
            $product_key = $this->session->data['curr_product_key'];
            if ($data === false || empty($data)) {
                $this->session->data['products_filter'][$product_key] = $this->session->data['curr_product_data'];
                $products = $this->cart->getProducts();
                $this->session->data['cart'][$key]['order_status'] = 'FAILED';
                foreach($products as $key => $p){
                  if(!$this->session->data['cart'][$key]['order_status']){
                    $this->session->data['cart'][$key]['order_status'] = 'in_process';
                  }
                }
                $this->redirect($this->html->getSecureURL('default_mandiri_ecash/failed_not_connect'));
            } else {
                curl_close($ch);
                $xml = simplexml_load_string($data);
                $id = $xml->xpath('//return[1]');
                if (isset($id[0]) && strcmp($id[0], 'INVALID DATA') != 0) {
                    $this->redirect($redirectUrl . $id[0]);
                } else {
                    $this->session->data['products_filter'][$product_key] = $this->session->data['curr_product_data'];
                    $products = $this->cart->getProducts();
                    $this->session->data['cart'][$key]['order_status'] = 'FAILED';
                    foreach($products as $key => $p){
                      if(!$this->session->data['cart'][$key]['order_status']){
                        $this->session->data['cart'][$key]['order_status'] = 'in_process';
                      }
                    }
                    $this->redirect($this->html->getSecureURL('default_mandiri_ecash/failed_not_connect'));
                }
            }
        }
    }

    public function callback() {
    $this->loadLanguage('default_mandiri_ecash/default_mandiri_ecash');
    $this->extensions->hk_InitData($this, __FUNCTION__);

     if ($this->request->get['id']==null) {
            $body = file_get_contents('php://input'); 
            $initdata = explode(",", $body);
            $initticket = trim($initdata[0]);
            $initphonenmbr = trim($initdata[1]);
            $inittracenmbr = trim($initdata[2]);
            $initorder_id = trim($initdata[3]);
            $initpstatus = trim($validation[4]);
            
            if($this->config->get('mandiri_ecash_server')==0){
                $validationUrl = "https://mandiri-ecash.com/ecommgateway/validation.html?id=";
            }else if($this->config->get('mandiri_ecash_server')==1){
                $validationUrl = "https://mandiriecash.com/ecommgateway/validation.html?id=";
            }

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_FAILONERROR, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_URL, $validationUrl.$initticket);
            $result = curl_exec($ch);
            curl_close($ch);

            if($result)
            $validation = explode(",", $result);
            $ticket = trim($validation[0]);
            $phonenmbr = trim($validation[1]);
            $tracenmbr = trim($validation[2]);
            $order_id = trim($validation[3]);
            $pstatus = trim($validation[4]);
            $this->load->model('checkout/order');
            $this->load->model('account/reminder');
            $order_info = $this->model_checkout_order->getOrder($order_id);
            // $this->showDebug($order_info);
            if ($order_info) {
                if (strcmp($pstatus, 'SUCCESS') == 0) {
                    if($order_info['payment_method_data']!=null && $order_info['order_status_id']==$this->config->get('mandiri_ecash_order_status_id')){
                          header('', '', 200);
                          die();
                    }else{
                          $product_key = $this->session->data['curr_product_id'];
                          $this->model_checkout_order->confirm($order_id, $this->config->get('mandiri_ecash_order_status_id'));
                          $this->model_account_reminder->addReminderSetup($product_key, $this->session->data['cart'][$product_key],$pstatus);
                          $this->model_checkout_order->updatePaymentMethodData($this->session->data['order_id'], $ticket);
                          header('', '', 200);
                          die();
                    }
                } else {
                          header('', '', 200);
                          die();
                }
            } else {
                          header('', '', 200);
                          die();
            }
        } else {
            $this->load->language('default_mandiri_ecash/default_mandiri_ecash');
            $returnid = $_GET["id"];
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_FAILONERROR, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            if($this->config->get('mandiri_ecash_server')==0){
                $validationUrl = "https://mandiri-ecash.com/ecommgateway/validation.html?id=";
            }else if($this->config->get('mandiri_ecash_server')==1){
                $validationUrl = "https://mandiriecash.com/ecommgateway/validation.html?id=";
            }
            curl_setopt($ch, CURLOPT_URL, $validationUrl. $returnid);
            $result = curl_exec($ch);
            curl_close($ch);

            $validation = explode(",", $result);
            $ticket = $validation[0];
            $phonenmbr = $validation[1];
            $tracenmbr = $validation[2];
            $order_id = trim($validation[3]);
            $pstatus = trim($validation[4]);

            $this->load->model('checkout/order');
            $this->load->model('account/reminder');
            $this->load->model('account/customer');
            $this->load->model('catalog/product');

            $this->session->data['cart'][$this->session->data['curr_product_key']]['order_status'] = $pstatus;
            
            $order_info = $this->model_checkout_order->getOrder($order_id);
            // $this->showDebug($this->session->data['cart'][$this->session->data['curr_product_key']]['reminder']);
            if ($order_info) {
                $product_key = $this->session->data['curr_product_key'];
                if (strcmp($pstatus, 'SUCCESS') == 0) {
                    $products = $this->cart->getProducts();
                    foreach($products as $key => $p){
                      if(!$this->session->data['cart'][$key]['order_status']){
                        $this->session->data['cart'][$key]['order_status'] = 'in_process';
                      }
                    }
                    $this->session->data['products_filter'][$product_key] = $this->session->data['curr_product_data'];
                    if($order_info['payment_method_data']!=null && $order_info['order_status_id']==$this->config->get('mandiri_ecash_order_status_id')){
                        $this->redirect($this->html->getSecureURL('checkout/confirm')); //checkout/success
                    }else{
                        $this->model_checkout_order->confirm($order_id, $this->config->get('mandiri_ecash_order_status_id'));
                        
                        //Fazrin ~> save into table reminder if exists
                        if($this->session->data['cart'][$product_key]['reminder']){
                          $this->model_account_reminder->addReminderSetup($product_key, $this->session->data['cart'][$product_key],$pstatus);
                        }
                        //Fazrin ~> save into table transaction History
                        //$this->model_checkout_order->addTrxHistory($order_info,$pstatus);
                        //add order status to session with current id

                        $this->model_checkout_order->updatePaymentMethodData($this->session->data['order_id'], $ticket);
                        $this->redirect($this->html->getSecureURL('checkout/confirm')); //checkout/success
                    }
                  
                } else {
                    $this->session->data['products_filter'] = array();
                    foreach($this->session->data['cart'] as $key => $val){
                        if(strcmp($this->session->data['cart'][$key]['order_status'],'SUCCESS')==0){
                            unset($this->session->data['cart'][$key]);
                        }else{
                            unset($this->session->data['cart'][$key]['order_status']);
                        }
                    }
                    // $this->session->data['cart'][$this->session->data['curr_product_key']]['order_status'] = 'in_process';
                    // $this->session->data['products_filter'][$product_key] = $this->session->data['curr_product_data'];
                    //$this->redirect($this->html->getSecureURL('default_mandiri_ecash/failed_not_successful'));
                    $this->redirect($this->html->getSecureURL('checkout/confirm'));
                }
            } else {
                $this->redirect($this->html->getSecureURL('checkout/confirm'));
                // $this->redirect($this->html->getSecureURL('default_mandiri_ecash/failed_not_connect'));
            }
        }
    }
    
}
