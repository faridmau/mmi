<?php
/* ------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2013 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  Lincence details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

  UPGRADE NOTE:
  Do not edit or add to this file if you wish to upgrade AbanteCart to newer
  versions in the future. If you wish to customize AbanteCart for your
  needs please refer to http://www.AbanteCart.com for more information.
  ------------------------------------------------------------------------------ */
if (!defined('DIR_CORE')) {
    header('Location: static_pages/');
}

class ControllerPagesDefaultMandiriEcash extends AController {
    public $data = array();
    
    public function failed_unable_to_validate(){
        $this->loadLanguage('default_mandiri_ecash/default_mandiri_ecash');
        $this->view->assign($this->language->getASet());
        $this->extensions->hk_InitData($this, __FUNCTION__);
        $this->view->assign('heading_title',$this->language->get('text_ecash_failed'));
        $this->view->assign('text_message',sprintf($this->language->get('text_failed_message'),$this->language->get('text_unable_to_validate'), $this->html->getURL('content/contact')));
        $this->view->assign('button_continue',$this->language->get('button_continue'));
        $this->view->assign('continue',$this->html->getURL('checkout/confirm')); //index/home
        $this->processTemplate('responses/mandiri_ecash_failed.tpl');
        $this->extensions->hk_InitData($this, __FUNCTION__);
    }
    public function failed_not_connect(){
        $this->loadLanguage('default_mandiri_ecash/default_mandiri_ecash');
        $this->view->assign($this->language->getASet());
        $this->extensions->hk_InitData($this, __FUNCTION__);
        $this->view->assign('heading_title',$this->language->get('text_ecash_failed'));
        $this->view->assign('text_message',sprintf($this->language->get('text_failed_message'),$this->language->get('text_unable_to_connect'), $this->html->getURL('content/contact')));
        $this->view->assign('button_continue',$this->language->get('button_continue'));
        $this->view->assign('continue',$this->html->getURL('checkout/confirm'));
        $this->processTemplate('responses/mandiri_ecash_failed.tpl');
        $this->extensions->hk_InitData($this, __FUNCTION__);
    }
    public function failed_not_successful(){
        $this->loadLanguage('default_mandiri_ecash/default_mandiri_ecash');
        $this->view->assign($this->language->getASet());
        $this->extensions->hk_InitData($this, __FUNCTION__);
        $this->view->assign('heading_title',$this->language->get('text_ecash_failed'));
        $this->view->assign('text_message',sprintf($this->language->get('text_failed_message'),$this->language->get('text_unable_to_pay'), $this->html->getURL('content/contact')));
        $this->view->assign('button_continue',$this->language->get('button_continue'));
        $this->view->assign('continue',$this->html->getURL('checkout/confirm'));
        $this->processTemplate('responses/mandiri_ecash_failed.tpl');
        $this->extensions->hk_InitData($this, __FUNCTION__);
    }
}
