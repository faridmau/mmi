<!--  -->
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
        <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Mandiri Clickpay</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="<?php echo $this->templateResource('/stylesheet/main.css'); ?>" rel="stylesheet">
    <link href="<?php echo $this->templateResource('/stylesheet/ecash.css'); ?>" rel="stylesheet">

    <script src="<?php echo $this->templateResource('/javascript/jquery.js'); ?>"></script>
    <script src="<?php echo $this->templateResource('/javascript/modernizr-2.6.1.min.js'); ?>"></script>
    <script type="text/javascript">
      var typingTimer;                //timer identifier
      var doneTypingInterval = 1000;

      var getTenDigit = function(){
          var debit_number = $("#debit_no").val();
          clearTimeout(typingTimer);
          typingTimer = setTimeout(
          function(){
            $('#ten_digit').val(debit_number.substr(-10));
          },doneTypingInterval);
      }

    </script>
</head>
<!-- -->
<body>

<div class="container m_b_30">
      <div class="main-content">
        
        <div class="row-5">
          <div class="col-sm-3 hidden-xs">
            <div class="box clearfix">
              <div class="invoice">
                <div class="content">
                  <h3 style="margin-top: 0"><?php echo $pay_to; ?></h3>
                  <h2><?php echo $product['name']?></h2>

                  <h3><?php echo $description; ?></h3>
                  <p><?php echo $product['description']; ?></p>

                  <h3><?php echo $total_payment; ?></h3>
                  <p>Rp <?php echo number_format($product['total_payment'], 2, '.', ','); ?></p>
                </div><!--.content-->
              </div><!--.invoice-->
            </div><!--.box-->
          </div><!--.col-->
          <div class="box col-sm-9">
            <div class="login">
              <div class="head">
                <div class="logo"><img src="<?php echo $this->templateResource('/img/clickpaylogo.png'); ?>"></div>
              </div>
              <div class="main">

                <div class="visible-xs payment-info">
                  <span class="typo-light cl-aaa"><?php echo $pay_to; ?></span><br>
                  <?php echo $product['name']?><br>
                  <br>
                  <span class="typo-light cl-aaa"><?php echo $description; ?>  </span><br>
                  <?php echo $product['description']; ?><br>
                  <br>
                  <span class="typo-light cl-aaa"><?php echo $total_payment; ?></span><br>
                  Rp <?php echo $product['total_payment']; ?><br>
                </div>

                <form action="<?php echo $callback; ?>" method="POST" class="form-horizontal" role="form">
                  <div class="form-group">
                    <label class="col-sm-5 control-label" style="padding-top: 0; margin-top: -2px"><?php echo $insert_debit_number; ?></label>
                    <div class="col-sm-7">
                      <input id="debit_no" type="text" name="debit_no" class="form-control" onkeyup="getTenDigit()">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label"><?php echo $token_response; ?></label>
                    <div class="col-sm-7">
                      <input type="text" name="response_token" class="form-control">
                    </div>
                  </div>

                  <p style="margin: 30px 0 20px"><?php echo $get_token_steps; ?></p>
                  <div class="form-group">
                    <label class="col-sm-5 control-label" style="padding-top: 0; margin-top: -2px"><?php echo $token_step_1; ?></label>
                    <div class="col-sm-7">
                      <input type="text" class="form-control" disabled value="3">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label" style="padding-top: 0; margin-top: -2px"><?php echo $token_step_2; ?></label>
                    <div class="col-sm-7">
                      <input id="ten_digit" type="text" class="form-control" disabled value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label" style="padding-top: 0; margin-top: -2px"><?php echo $token_step_3; ?></label>
                    <div class="col-sm-7">
                      <input type="text" class="form-control" disabled value="4291823">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label" style="padding-top: 0; margin-top: -2px"><?php echo $token_step_4; ?></label>
                    <div class="col-sm-7">
                      <input type="text" class="form-control" disabled value="8617">
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                      <a href="#">
                      <button type="submit" class="btn btn-primary gradient pull-right"><?php echo $button_send; ?></button>
                      </a>
                      <a href="<?php echo $link_confirm; ?>" class="btn btn-default gradient pull-right m_r_10"><?php echo $button_cancel; ?></a>
                    </div>
                  </div>
                </form>

              </div><!--.main-->
            </div><!--.login-->

          </div><!--.col-->


        </div>

      </div>
    </div>
    <script src="<?php echo $this->templateResource('/javascript/bootstrap.js'); ?>"></script>
    <script src="<?php echo $this->templateResource('/javascript/holder.js'); ?>"></script>
    <script src="<?php echo $this->templateResource('/javascript/main.js'); ?>"></script>
    <script type="text/javascript">
      $(document).ready(function(){
        var typingTimer;                //timer identifier
        var doneTypingInterval = 1000; 

        // var getTenDigit = function(){
        //   clearTimeout(typingTimer);
        //   typingTimer = setTimeout(
        //   function(){
        //     $('#ten_digit').val("234567890");
        //   },doneTypingInterval);

        // }
      });
    </script>
  </body>
</html>