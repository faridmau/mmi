<?php
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2013 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  Lincence details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if ( !defined ( 'DIR_CORE' )) {
	header ( 'Location: static_pages/' );
}

class ControllerResponsesExtensionDefaultBanktransfer extends AController {

	private $error = array();
  public $data = array();

	public function main() {
		$this->loadLanguage('default_banktransfer/default_banktransfer');

		$this->view->assign('text_instructions', $this->language->get('text_instructions'));
		$this->view->assign('text_payment', $this->language->get('text_payment'));
		
    $this->view->batchAssign(  $this->language->getASet() );
		$this->view->assign('instructions', nl2br($this->config->get('default_banktransfer_instructions')));
		// $this->view->assign('continue', $this->html->getSecureURL('checkout/success'));
		$this->view->assign('continue', $this->html->getSecureURL('extension/default_banktransfer/payment'));		

		if ($this->request->get['rt'] != 'checkout/guest_step_3') {
			$this->view->assign('back', $this->html->getSecureURL('checkout/payment'));
		} else {
			$this->view->assign('back', $this->html->getSecureURL('checkout/guest_step_2'));
		}
		
		//check total for to meat min requirement 
		if ( has_value($this->config->get('default_banktransfer_order_min')) ) {
			if ($this->cart->getTotal() < $this->config->get('default_banktransfer_order_min') ) {
				$this->view->assign('minimum_notmet', $this->language->get('text_minimum_notmet'));	
			}
		}
		
    $this->processTemplate('responses/default_banktransfer.tpl' );
	}

	public function confirm() {

		$this->loadLanguage('default_banktransfer/default_banktransfer');
		$this->load->model('checkout/order');
		
		// $comment  = $this->language->get('text_instructions') . "\n";
		// $comment .= $this->config->get('default_banktransfer_instructions') . "\n\n";
		// $comment .= $this->language->get('text_payment') . "\n";
    
    //jangan di confirm dulu,, tapi redirect ke halaman clickpay
		// $this->model_checkout_order->confirm($this->session->data['order_id'], $this->config->get('default_banktransfer_order_status_id'), $comment);
		// $this->redirect($this->html->getSecureURL('checkout/clickpay'));
		return true;
	}

	public function payment() {
    //init controller data
    $this->extensions->hk_InitData($this,__FUNCTION__);
    
    $this->loadLanguage('checkout/clickpay');

    if(!$this->session->data['payment_process']) $this->session->data['payment_process']=1;
    $products = $this->cart->getProducts();

    //added by fazrin, filter product yg sudah di proses agar tidak di proses lagi
    $current_order = array();
    // $this->session->data['products_filter'] = array();
    if(!$products){
        $this->session->data['products_filter'] = array();
        $this->session->data['curr_product_key'] = array();
        $this->session->data['curr_product_data'] = array();
    }

    if($this->session->data['products_filter']) $products = array_diff_key($products,$this->session->data['products_filter']);
    
    $numItems = count($products);

    foreach ($products as $key => $product) {
       if(++$i === $numItems) {
          $description =  'Transaksi Topup ' . $product['name'];//;$description.$product['name'];
       }else{
          $description =  'Transaksi Topup ' . $product['name'];//$description.$product['name'];
       }
       $current_order = array_slice($products,0,1,true);
       $this->session->data['curr_product_key'] = $key;
       $this->session->data['curr_product_data'] = $product;
       // $this->session->data['products_filter'][$key] = $product;
       break;
    }

    $this->data['product'] = $this->session->data['curr_product_data'];
    $this->data['product']['description'] = $description;
    $this->data['product']['total_payment'] = $this->data['product']['quantity']+$this->data['product']['fee'];
    // if($this->session->data['payment_method']['id']=="default_banktransfer"){
    //   $this->session->data['payment_method']['id'] = 'default_mandiri_clickpay';
    // }

    // $this->showDebug($product_to_process);
    $this->view->batchAssign( $this->data );
    $this->view->assign('link_confirm',$this->html->getSecureURL('checkout/confirm'));
    $this->view->assign('callback',$this->html->getSecureURL('extension/default_banktransfer/callback'));
    $this->processTemplate('responses/clickpay_step_1.tpl' );

    //init controller data
    $this->extensions->hk_UpdateData($this,__FUNCTION__);
  }

  public function callback(){

    $this->loadLanguage('default_banktransfer/default_banktransfer');
  	$this->loadLanguage('checkout/clickpay');
    $this->load->model('checkout/order');
    $this->load->model('account/reminder');
    $this->load->model('account/customer');
    $this->load->model('catalog/product');

    if($this->request->server['REQUEST_METHOD'] == 'POST'){

      $products = $this->cart->getProducts();
      //fazrin - get 1 product untuk di proses
      $product_to_process = array_slice($products,0,1,true);
      foreach($product_to_process as $key => $value){
          $payment_method_pos = strpos($value['location'],'default_mandiri_clickpay');
          $double_colon_pos = strpos($value['location'],":",$payment_method_pos)+1;
          $coma_pos = strpos($value['location'],",",$payment_method_pos);
          if(!$coma_pos) $coma_pos = strpos($value['location'],"}",$payment_method_pos);
          $merchant_id = trim(substr($value['location'],$double_colon_pos,($coma_pos-$double_colon_pos)));
          // echo $payment_method_pos." | ".$double_colon_pos." | ".$coma_pos."\n";
          // echo "-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789-123456789\n";
          // echo $value['location']."\n".$merchant_id."~".$this->session->data['payment_method']['id'];
          $amount = number_format($value['total']+$value['fee'],0,'','');
      }


      foreach($products as $key => $p){
          if(!$this->session->data['cart'][$key]['order_status']){
            $this->session->data['cart'][$key]['order_status'] = 'in_process';
          }
      }

      $curr_product_id = $this->session->data['curr_product_key'];
      $this->model_checkout_order->confirm($this->session->data['order_ids'][$curr_product_id], $this->config->get('default_banktransfer_order_status_id'), $comment);
      // echo "1. Send Request (cURL) with data ke Mandiri Clickpay<br/>";
      // echo "2. if success then change the order status payment with finish</br>";
      // echo "3. redirect to confirm page</br>";
      // echo "4. IF there still product(s) left to process then redirect to clickpay payment ELSE redirect to Success page with delay";  
      if($fazrin){
        $this->sendDataToAPI($data,$merchant_id);
      }

      $pstatus = 'SUCCESS';
      $this->session->data['cart'][$curr_product_id]['order_status'] = $pstatus;
      if(strcmp($pstatus,'SUCCESS')==0){
        $product_key = $this->session->data['curr_product_key'];
        //Fazrin ~> save into table reminder if exists
        if($this->session->data['cart'][$product_key]['reminder']){
          $this->model_account_reminder->addReminderSetup($product_key, $this->session->data['cart'][$product_key],$pstatus);
        }

        //add current key into session
        $this->session->data['products_filter'][$product_key] = $this->session->data['curr_product_data'];
        // $this->redirect($this->html->getSecureURL('checkout/confirm'));  

        $description =  'Transaksi Topup ' . $this->data['product']['name'];
        $this->data['product'] = $this->session->data['curr_product_data'];
        $this->data['product']['description'] = $description;
        $this->data['product']['total_payment'] = $this->data['product']['quantity']+$this->data['product']['fee'];
        $this->view->batchAssign( $this->data );

        $this->view->assign('finish_button',$this->html->getSecureURL('checkout/confirm'));
        $this->processTemplate('responses/clickpay_step_2.tpl' );

        // $this->redirect($this->html->getSecureURL('checkout/payment/finish'));        
      }
    }

  }

}
?>
