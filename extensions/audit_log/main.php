<?php
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright (c) 2011 Belavier Commerce LLC

  Released under the GNU General Public License
  Lincence details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.gnu.org/licenses/>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if (!defined('DIR_CORE')) {
	header('Location: static_pages/');
}

if(!class_exists('ExtensionAuditLog')){
	include ('core/audit_log.php');
}
$controllers = array(
    'storefront' => array( ),
    'admin' => array(
    	'pages/system/audit_log'),
);

$models = array(
    'storefront' => array(),
    'admin' => array('system/audit_log'),
);

$languages = array(
    'storefront' => array(),
    'admin' => array(
        'audit_log/audit_log',
    ),
);

$templates = array(
    'storefront' => array(),
    'admin' => array('pages/system/audit_log.tpl', 'pages/system/audit_log_list.tpl'),
);