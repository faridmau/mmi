<?php
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright (c) 2011 Belavier Commerce LLC

  Released under the GNU General Public License
  Lincence details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.gnu.org/licenses/>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if (!defined('DIR_CORE')) {
	header('Location: static_pages/');
}

class ExtensionAuditLog extends Extension {
	public function afterADb_query($sql) {
		$registry = Registry::getInstance ();
		if (stripos ( $sql, "select " ) === false) {
			$pattern = "/ (login|password) = '([0-9a-z].*)'(,).* /";
			$sql = preg_replace ( $pattern, ' [***secret data***] ', $sql );
			$sql = str_replace("\n",'',$sql);
			$string = $registry->get ( 'session' )->data ['user_id'] . " - " . $registry->get ( 'request' )->server ['REMOTE_ADDR'] . " - " . $sql;
			$log = new ALog ( DIR_LOGS."audit_log_" . date ( "Y-m-d" ) . ".txt" );
			$log->write ( $string );
		}
	}

}