<?php if (!empty($success)) { ?>
<div class="success"><?php echo $success; ?></div>
<?php } ?>
<?php if (!empty($error_warning)) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php if (!empty($error)) { ?>
<div class="warning"><?php echo $error; ?></div>
<?php } ?>
<div class="content">
  <div class="cbox_tl"><div class="cbox_tr"><div class="cbox_tc">
    	<div class="heading"><?php echo $heading_title; ?></div>    
  </div></div></div>
  <div class="cbox_cc">
  <div class="cbox_cl"><div class="cbox_cr"></div></div>    
      <table class="table_list">
        <thead>
          <tr>
          	<td width="10px">#</td>
			      <td width="15%"><?php echo $column_date_added; ?></td>
            <td><?php echo $column_username; ?></td>
            <td><?php echo $column_ip; ?></td>           
            <!-- <td width="50%"><?php //echo $column_sql; ?></td> -->
            <td>login</td>
            <td>logout</td>
            <td>activity</td>
            <td>data before</td>
            <td>data after</td>
          </tr>
        </thead>
        <tbody>
          <?php if ($log_rows) { ?>
          <?php foreach ($log_rows as $log_row) { ?>
          <tr>
            <td class="center"><?php echo $log_row['rownum']; ?></td>
            <td class="center"><?php echo $log_row['date_add']; ?></td>            
            <td class="center"><?php echo $log_row['user_id']; ?></td>
            <td class="center"><?php echo $log_row['ip']; ?></td>
            <td><?php echo $log_row['login']; ?></td>
            <td><?php echo $log_row['logout']; ?></td>
            <td><?php echo $log_row['activity']; ?></td>
            <td><?php echo $log_row['data_before']; ?></td>
            <td><?php echo $log_row['data_after']; ?></td>        
            <!-- <td class="left" style="text-align:left !important;"><?php //echo $log_row['sql']; ?></td> -->
          </tr>
          <?php } ?>
          <?php } else { ?>
          <tr>
            <td class="center" colspan="5"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>    
  <?php if($pagination){ ?>   
  <div class="pagination"><?php echo $pagination; ?></div>
  <?php } ?>
  </div>
    <div class="cbox_bl"><div class="cbox_br"><div class="cbox_bc"></div></div></div>
</div>