<?php if (!empty($success)) { ?>
<div class="success"><?php echo $success; ?></div>
<?php } ?>
<?php if ($warning) { 
	foreach($warning as $warn){?>
		<div class="warning"><?php echo $warn; ?></div>
	<?php }} ?>
<?php if ($error) { 
	foreach($error as $err){?>
		<div class="warning"><?php echo $err; ?></div>
	<?php }} ?>
<div class="content">
  <div class="cbox_tl"><div class="cbox_tr"><div class="cbox_tc">
    	<div class="heading"><?php echo $heading_title; ?></div>    
  </div></div></div>  
  <div class="cbox_cl"><div class="cbox_cr">
     <div class="cbox_cc">
  		<?php echo $form_open; ?>
	      <div class="filter">	      	         
	        <span class="search_keyword">&nbsp;<?php echo $text_date_from; ?><?php echo $date1; ?>
	        </span>
	        <span class="search_keyword">&nbsp;
	        	<?php echo $text_date_to; ?> 
	        	<?php echo $date2; ?>
	        </span>
	        <input type="hidden" name="page" value="<?php echo $page;?>">
	        <input type="hidden" name="limit" value="<?php echo $limit;?>">
	        <input type="hidden" name="rt" value="<?php echo $rt;?>">
	        <input type="hidden" name="s" value="<?php echo $s;?>">
	        <input type="hidden" name="token" value="<?php echo $token;?>">
		    &nbsp;
	        <?php echo $submit;  ?>
	        <div class="clr_both"></div>	        
	      </div>
	      </form>
      <table class="table_list">
        <thead>
          <tr>
          	<td class="center">#</td>			
            <td class="center"><?php echo $column_filename; ?></td>                   
            <!-- <td class="center"><?php //echo $column_action; ?></td> -->            
          </tr>
        </thead>
        <tbody>
          <?php if ($logs) { ?>
          <?php foreach ($logs as $logfile) { ?>
          <tr>
            <td class="col1"><?php echo $logfile['rownum']; ?></td>            
            <td class="col2"><a href="<?php echo $logfile['href']; ?>"><?php echo $logfile['filename']; ?></a></td>            
    <!--<td class="col3">
          		<a href="<?php //echo $logfile['action']['href']; ?>" class="btn_action">
          			<span class="icon_s_delete">
          				<span class="btn_text">
          					<?php //echo $logfile['action']['text']; ?>
          				</span>
          			</span>
          		</a>  
           </td> -->
          </tr>
          <?php } ?>
          <?php } else { ?>
          <tr>
            <td class="center" colspan="3"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>  
    <?php if($pagination){ ?>   
    <div class="pagination"><?php echo $pagination; ?></div>
    <?php } ?>
    </div></div></div>
    <div class="cbox_bl"><div class="cbox_br"><div class="cbox_bc"></div></div></div>
    </div>

<script type="text/javascript" src="admin/view/default/javascript/jquery/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
<!--
$(function() {
	var dates = $( "#log_search_date1, #log_search_date2" ).datepicker({
		defaultDate: "+1w",
		changeMonth: true,
		numberOfMonths: 1,
		dateFormat: 'yy-mm-dd',
		onSelect: function( selectedDate ) {
			var option = this.id == "log_search_date1" ? "minDate" : "maxDate",
				instance = $( this ).data( "datepicker" ),
				date = $.datepicker.parseDate(
					instance.settings.dateFormat ||
					$.datepicker._defaults.dateFormat,
					selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		}
	});
});

//-----------------------------------------
// Confirm Actions (delete)
//-----------------------------------------
$(document).ready(function(){
	    
    // Confirm Delete
    $('a').click(function(){
        if ($(this).attr('href') != null && $(this).attr('href').indexOf('delete',1) != -1) {
            if (!confirm ('<?php echo $text_confirm; ?>')) {
                return false;
            }
        }
    });
});

$('#log_search input').keydown(function(e) {
	if (e.keyCode == 13) {
		$('#log_search').submit();
	}
});
//--></script>

