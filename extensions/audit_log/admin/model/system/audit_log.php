<?php
if ( !defined ( 'DIR_CORE' )) {
	header ( 'Location: static_pages/' );
}

class ModelSystemAuditLog extends Model {
	
	private $rows_totals;
	public $error_warning = array ();
	public $success = '';

	public function getLogList($date1 = null, $date2 = null) {
		
		$date1 = ! $date1 && $date2 ? $date2 : $date1;
		$date2 = ! $date2 && $date1 ? $date1 : $date2;
		
		$this->error_warning = array ();
		$file_list = array_reverse ( scandir ( DIR_LOGS ) );
		
		$result = array ();
		$i = 0;
		foreach ( $file_list as $filename ) {
			//echo DIR_LOGS.$filename."<br/>";
			if (is_dir ( DIR_LOGS . $filename ) || strpos ( $filename, "audit-log-" ) === false) {
				continue;
			}
			$file_date = strtotime ( str_replace ( array ("audit-log-", ".txt" ), "", $filename ) );
			// skip files not in range
			if (($file_date < $date1 && $date1) || ($file_date > $date2 && $date2)) {
				continue;
			}
			// check is file changed manually			
			$file_mtime = filemtime ( DIR_LOGS . $filename );
			if (date ( "Y-m-d", $file_mtime ) > date ( "Y-m-d", $file_date )) {
				$this->error_warning [] = "Attention! Log file " . $filename . " was modified at " . date ( "Y-m-d H:i", $file_mtime );
			}
			
			$result [$i] ["rownum"] = $i + 1;
			$result [$i] ["filename"] = $filename;
			$i ++;
		}
		
		$this->rows_total = sizeof ( $result );

		// we need to know what lines are needed. 
		$limit = $this->registry->get ( 'request' )->get ['limit'];
		$limit = ! $limit ? $this->config->get ( 'config_admin_limit' ) : $limit;
		$page = $this->registry->get ( 'request' )->get ['page'];
		$page = ! $page ? 1 : $page;
		$page = ceil ( $this->rows_total / $limit ) > $page ? $page : ceil ( $this->rows_total / $limit );
		$start = $page * $limit - $limit;
		$stop = $page * $limit;
		$stop = $stop > $this->rows_total ? $this->rows_total : $stop;
			
/*		echo "<pre>";
		print_r($result);
		echo $start." | ".($stop);
		echo "</pre>";*/
	
		$result = array_slice ( $result, $start, ($stop - $start) );
		
		return $result;
	}
	public function getLog($log_name) {
		$this->warning = array ();
		$file = DIR_LOGS . $log_name;
		$contents = file ( $file );
		$this->rows_total = $contents !== false ? sizeof ( $contents ) : 0;
		// we need to know what lines are needed. 
		$limit = $this->registry->get ( 'request' )->get ['limit'];
		$limit = ! $limit ? $this->config->get ( 'config_admin_limit' ) : $limit;
		$page = $this->registry->get ( 'request' )->get ['page'];
		$page = ! $page ? 1 : $page;
		$page = ceil ( $this->rows_total / $limit ) > $page ? $page : ceil ( $this->rows_total / $limit );
		$start = $page * $limit - $limit;
		$stop = $page * $limit;
		$stop = $stop > $this->rows_total ? $this->rows_total : $stop;
		//well, let's fill table
		$table = array ();
		if ($contents) {
			$row_num = 0;
			$users = array (); // array for repeating usernames (preserve repeats of sql query)
			for($i = $start; $i < $stop; $i ++) {
				$row = explode ( "|", $contents [$i] ); // - 
				$table [$row_num] ['date_add'] = $row [0];
				$row [1] = ( int ) $row [1];
				if ($row [1]) {
					if (! isset ( $users [$row [1]] )) {
						$query = "SELECT u.username FROM `" . DB_PREFIX . "users` u WHERE user_id=" . ($row [1]);
						$query = $this->db->query ( $query );
						$users [$row [1]] = $table [$row_num] ['user_id'] = $query->row ['username'];
					} else {
						$table [$row_num] ['user_id'] = $users [$row [1]];
					}
				}
				$table [$row_num]['ip'] = $row [2];
				// $table [$row_num]['sql'] = htmlentities ( $row [3], ENT_QUOTES, 'UTF-8' );
				$table [$row_num]['rownum'] = $i + 1;
				$table [$row_num]['login'] = $row[3];
				$table [$row_num]['logout'] = $row[4];
				$table [$row_num]['activity'] = $row[5];
				$table [$row_num]['data_before'] = $row[6];
				$table [$row_num]['data_after'] = $row[7];
				$row_num ++;
			}
		}
		return $table;
	}
	/*
	 * returns number of rows for pagination 
	 */
	public function getTotalRows() {
		return $this->rows_total;
	}
}

?>
