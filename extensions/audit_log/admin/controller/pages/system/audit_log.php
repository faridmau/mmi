<?php
if ( !defined ( 'DIR_CORE' )) {
	header ( 'Location: static_pages/' );
}

class ControllerPagesSystemAuditLog extends AController {
	private $error = array ();
	
	public function main() {
		
		$this->loadLanguage( 'audit_log/audit_log' );
		$this->document->setTitle( $this->language->get ( 'audit_log_name' ) );
		$this->load->model ( 'system/audit_log' );
		
		if (isset ( $this->request->get ['filename'] )) {
			// show log-file with pagination
			$log_name = $this->request->get ['filename'];
			$log_name = ! $log_name ? "audit-log-" . date ( "Y-m-d" ) . ".txt" : $log_name;
			// $this->showDebug($log_name);
			$this->showLog ( $log_name );
		} else {
			//show log list by daterange			
			$this->showLogList ();
		}
	}
	
	private function showLogList() {
		// lets prepare dates
		if (isset ( $this->request->get ['date1'] )) {
			$date1 = strtotime ( $this->request->get ['date1'] );
		} else {
			$date1 = mktime ( 0, 0, 0, date ( "m" ), date ( "d" ) - 15, date ( "Y" ) );
		}
		if (isset ( $this->request->get ['date2'] )) {
			$date2 = strtotime ( $this->request->get ['date2'] );
		} else {
			$date2 = mktime ( 0, 0, 0, date ( "m" ), date ( "d" ), date ( "Y" ) );
		}
		$results = $this->model_system_audit_log->getLogList ( $date1, $date2 );
		//get number of files
		$rows_total = $this->model_system_audit_log->getTotalRows();
		// $this->showDebug($this);
		// $rows_total = 11;

		$logs = array ();
		foreach ( $results as $result ) {
			$logs [] = array ('rownum' => $result ['rownum'], 'filename' => $result ['filename'], 'href' => $this->html->getSecureURL ( 'system/audit_log', '&filename=' . $result ['filename'] ), 'action' => array ('text' => $this->language->get ( 'text_delete' ), 'href' => $this->html->getSecureURL ( 'system/audit_log/deletelog', '&filename=' . $result ['filename'] ) ) );
		}
		
		$page = isset ( $this->request->get ['page'] ) ? $this->request->get ['page'] : 1;
		$limit = isset ( $this->request->get ['limit'] ) ? $this->request->get ['limit'] : $this->config->get ( 'config_admin_limit' );
		$url = '&page=' . $page . "&limit=" . $limit . "&date1=" . date ( "Y-m-d", $date1 ) . "&date2=" . date ( "Y-m-d", $date2 );
		
		$this->document->resetBreadcrumbs();
		$this->document->addBreadcrumb( array ('href' => $this->html->getSecureURL ( 'common/home' ), 'text' => $this->language->get ( 'text_home' ), 'separator' => FALSE ));
		
		$this->document->addBreadcrumb( array ('href' => $this->html->getSecureURL ( 'system/audit_log', $url ), 'text' => $this->language->get ( 'audit_log_name' ), 'separator' => ' :: ' ));
		
		if (! $this->model_system_audit_log->error_warning) {
			$this->model_system_audit_log->error_warning = array ();
		}
		if (! $this->session->data ['warning']) {
			$this->session->data ['warning'] = array ();
		}
		$warning = array_merge ( $this->model_system_audit_log->error_warning, $this->session->data ['warning'] );
		$this->model_system_audit_log->error_warning = $this->session->data ['warning'] = array ();
		
		if (! $this->model_system_audit_log->error) {
			$this->model_system_audit_log->error = array ();
		}
		if (! $this->session->data ['audit_log_error']) {
			$this->session->data ['audit_log_error'] = array ();
		}
		$error = array_merge ( $this->model_system_audit_log->error, $this->session->data ['audit_log_error'] );
		$this->model_system_audit_log->error = $this->session->data ['audit_log_error'] = array ();
		
		if (! $this->model_system_audit_log->error) {
			$this->model_system_audit_log->error = '';
		}
		if (! $this->session->data ['audit_log_error']) {
			$this->session->data ['audit_log_error'] = '';
		}
		$success = $this->model_system_audit_log->success . $this->session->data ['audit_log_success'];
		$this->model_system_audit_log->success = $this->session->data ['audit_log_success'] = '';
		
		$this->view->assign ( 'warning', $warning );
		$this->view->assign ( 'success', $success );
		$this->view->assign ( 'error', $error );
		
		$this->view->assign ( 'heading_title', $this->language->get ( 'audit_log_name' ) );
		$this->view->assign ( 'text_date_from', $this->language->get ( 'text_date_from' ) );
		$this->view->assign ( 'text_date_to', $this->language->get ( 'text_date_to' ) );

		$this->view->assign ( 'page', $page );
		$this->view->assign ( 'limit', $limit );
		$this->view->assign ( 'rt', "system/audit_log" );
		$this->view->assign ( 's', ADMIN_PATH );
		$this->view->assign ( 'token', $this->request->get ['token'] );
		
		$this->view->assign ( 'logs', $logs );
		$this->view->assign ( 'audit_log_name', $this->language->get ( 'audit_log_name' ) );
		$this->view->assign ( 'text_no_results', $this->language->get ( 'text_no_results' ) );
		$this->view->assign ( 'column_filename', $this->language->get ( 'column_filename' ) );
		$this->view->assign ( 'column_action', $this->language->get ( 'column_action' ) );
		$this->view->assign ( 'text_confirm', $this->language->get ( 'text_confirm_delete_log' ) );
		$this->view->assign ( 'html', $this->html );
		
		$url = "&date1=" . date ( "Y-m-d", $date1 ) . "&date2=" . date ( "Y-m-d", $date2 );

		$form = new AForm('ST');
		$form->setForm(array(
		    'form_name' => 'log_search',
	    ));
		$data[ 'form_open' ] = $form->getFieldHtml(array(
		                                                 'type' => 'form',
		                                                 'name' => 'log_search',
			                                             'method' =>'GET',
		                                                 'action' => '',
		                                                 ));
		$data[ 'date1' ] = $form->getFieldHtml(array(
		                                              'type' => 'input',
		                                              'name' => 'date1',
		                                              'value' => date ( "Y-m-d", $date1 )
		                                              ));
		$data[ 'date2' ] = $form->getFieldHtml(array(
		                                              'type' => 'input',
		                                              'name' => 'date2',
		                                              'value' => date ( "Y-m-d", $date2 )
		                                              ));

		$data[ 'submit' ] = $form->getFieldHtml(array(
		                                              'type' => 'submit',
		                                              'name' => 'submit',
		                                              'text' => $this->language->get('button_go'),
		                                              'style' => 'button1',
		                                              ));
		$this->view->batchAssign($data);

		if ($rows_total) {
			$pagination = new APagination ();
			$pagination->total = $rows_total;
			$pagination->page = $page;
			$pagination->limit = $limit;
			$pagination->text = $this->language->get ( 'text_pagination' );
			$pagination->url = $this->html->getSecureURL ( 'system/audit_log' . $url, '&page={page}&limit={limit}' );
			$this->view->assign ( 'pagination', $pagination->render () );
		}
		$this->processTemplate ( 'pages/system/audit_log_list.tpl' );
	}
	
	private function showLog($log_name = null) {
		$log_name = $this->checkLogName ( $log_name );
		//get log as array
		$results = $this->model_system_audit_log->getLog( $log_name );
		//get number or lines in logfile
		$rows_total = $this->model_system_audit_log->getTotalRows ();
		$log_rows = array ();
		foreach ( $results as $result ) {
			$log_rows [] = array (
				'rownum' => $result ['rownum'], 
				'user_id' => $result ['user_id'], 
				'ip' => $result ['ip'], 
				'date_add' => $result ['date_add'],
				'login' => $result ['login'],
				'logout' => $result ['logout'],
				'activity' => $result ['activity'],
				'data_before' => $result ['data_before'],
				'data_after' => $result ['data_after'], 
				'sql' => $result ['sql'],
				);
		}
		
		$page = isset ( $this->request->get ['page'] ) ? $this->request->get ['page'] : 1;
		$limit = isset ( $this->request->get ['limit'] ) ? $this->request->get ['limit'] : $this->config->get ( 'config_admin_limit' );
		$url = '&page=' . $page . "&limit=" . $limit;
		
		$this->document->resetBreadcrumbs();
		$this->document->addBreadcrumb ( array ('href' => $this->html->getSecureURL ( 'common/home' ), 'text' => $this->language->get ( 'text_home' ), 'separator' => FALSE ));
		$this->document->addBreadcrumb ( array ('href' => $this->html->getSecureURL ( 'system/audit_log', $url ), 'text' => $this->language->get ( 'audit_log_name' ), 'separator' => ' :: ' ));
		$url .= '&filename=' . $log_name;
		$this->view->assign ( 'heading_title', $this->language->get ( 'audit_log_filename' ) . $log_name );
		$this->view->assign ( 'log_rows', $log_rows );
		$this->view->assign ( 'audit_log_name', $this->language->get ( 'audit_log_name' ) );
		$this->view->assign ( 'text_no_results', $this->language->get ( 'text_no_results' ) );
		$this->view->assign ( 'column_username', $this->language->get ( 'column_username' ) );
		$this->view->assign ( 'column_ip', $this->language->get ( 'column_ip' ) );
		$this->view->assign ( 'column_date_added', $this->language->get ( 'column_date_added' ) );
		$this->view->assign ( 'column_sql', $this->language->get ( 'column_sql' ) );
		
		if (! $this->model_system_audit_log->error_warning) {
			$this->model_system_audit_log->error_warning = array ();
		}
		if (! $this->session->data ['warning']) {
			$this->session->data ['warning'] = array ();
		}
		$warning = array_merge ( $this->model_system_audit_log->error_warning, $this->session->data ['warning'] );
		$this->model_system_audit_log->error_warning = $this->session->data ['warning'] = array ();
		
		if (! $this->model_system_audit_log->error) {
			$this->model_system_audit_log->error = array ();
		}
		if (! $this->session->data ['audit_log_error']) {
			$this->session->data ['audit_log_error'] = array ();
		}
		$error = array_merge ( $this->model_system_audit_log->error, $this->session->data ['audit_log_error'] );
		$this->model_system_audit_log->error = $this->session->data ['audit_log_error'] = array ();
		
		if (! $this->model_system_audit_log->error) {
			$this->model_system_audit_log->error = '';
		}
		if (! $this->session->data ['audit_log_error']) {
			$this->session->data ['audit_log_error'] = '';
		}
		$success = $this->model_system_audit_log->success . $this->session->data ['audit_log_success'];
		$this->model_system_audit_log->success = $this->session->data ['audit_log_success'] = '';
		
		$this->view->assign ( 'warning', $warning );
		$this->view->assign ( 'success', $success );
		$this->view->assign ( 'error', $error );
		
		if ($rows_total) {
			$pagination = new APagination ();
			$pagination->total = $rows_total;
			$pagination->page = $page;
			$pagination->limit = $limit;
			$pagination->text = $this->language->get ( 'text_pagination' );
			$pagination->url = $this->html->getSecureURL ( 'system/audit_log' . $url, '&page={page}&limit={limit}' );
			$this->view->assign ( 'pagination', $pagination->render () );
		}
		$this->processTemplate ( 'pages/system/audit_log.tpl' );
	}
	
	public function deleteLog($log_name = null) {
		$this->loadLanguage( 'audit_log/audit_log' );
		if(!$this->validateModify()){
			$url = '&page=' . $page . "&limit=" . $limit;
			$this->redirect ( $this->html->getSecureURL ( 'system/audit_log', $url ) );
			
		}
		
		
		$log_name = $this->request->get ['filename'];
		$log_name = $this->checkLogName ( $log_name );
		if ($log_name) {
			if (file_exists ( DIR_LOGS . $log_name )) {
				if (is_writable ( DIR_LOGS . $log_name )) {
					unlink ( DIR_LOGS . $log_name );
					$this->session->data ['audit_log_success'] = $this->language->get ( 'text_success' );
				} else {
					$this->session->data ['audit_log_error'] [] = "Can't to delete. Permission denied. File: " . $log_name;
				}
			} else {
				$this->session->data ['audit_log_error'] [] = "File does not exists";
			}
		}
		$page = isset ( $this->request->get ['page'] ) ? $this->request->get ['page'] : 1;
		$limit = isset ( $this->request->get ['limit'] ) ? $this->request->get ['limit'] : $this->config->get ( 'config_admin_limit' );
		$url = '&page=' . $page . "&limit=" . $limit;
		$this->redirect ( $this->html->getSecureURL ( 'system/audit_log', $url ) );
	
	}
	
	private function checkLogName($filename = '') {
		$filename = stripos ( $filename, "audit-log-" ) === false ? '' : $filename;
		return $filename;
	}
	private function validateModify() {
		if (! $this->user->hasPermission ( 'modify', 'system/audit_log' )) {
			$this->error ['warning'] = $this->language->get ( 'error_permission' );
		}
		
		if (! $this->error) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

}
?>