<?php
if ( !defined ( 'DIR_CORE' )) {
	header ( 'Location: static_pages/' );
}

// add new menu item
$menu = new AMenu ( "admin" );
$menu->insertMenuItem ( array ("item_id" => "audit_log", 
										"parent_id"=>"system",
										"item_text" => "audit_log_name", 
										"item_url" => "system/audit_log",
										"item_type"=>"extension",
										"sort_order"=>"" ));
