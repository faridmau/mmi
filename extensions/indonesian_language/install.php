<?php
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/

if (! defined ( 'DIR_CORE' )) {
	header ( 'Location: static_pages/' );
}

//before install validate it is unique 
$lng_code = 'id';
$lng_name = 'Indonesia';
$lng_directory = 'indonesia';
$lng_locale = 'id_ID.UTF-8,id_ID,indonesia';
$lng_flag_path = 'extensions/indonesian_language/storefront/language/indonesia/flag.png';
$lng_sort = 2; // sorting order with other langauges
$lng_status = 0; // Status on installation of extension

$query = $this->db->query("SELECT language_id FROM ".DB_PREFIX."languages WHERE code='".$lng_code."'");
if ($query->row['language_id']) {
	$this->session->data['error'] = "Error: Language with $lng_code code is already installed! Can not install duplicate languages! Uninstall this extension before attempting again.";
	$error = new AError ($this->session->data['error']);
	$error->toLog()->toDebug();
	return false;
}

$this->db->query("INSERT INTO ".DB_PREFIX."languages (`name`,`code`,`locale`,`image`,`directory`,`filename`,`sort_order`, `status`)
				  VALUES ('".$lng_name."', '".$lng_code."', '".$lng_locale."', '".$lng_flag_path."','".$lng_directory."','".$lng_directory."','".$lng_sort."',".$lng_status.");");
				 				  				  
$new_language_id = $this->db->getLastId();
$xml = simplexml_load_file(DIR_EXT.'indonesian_language/menu.xml');

$routes = array(
			'text_index_home_menu'=>'index/home',
			'text_account_login_menu'=>'account/login',
			'text_account_logout_menu'=>'account/logout',
			'text_account_account_menu'=>'account/account',
			'text_checkout_cart_menu'=>'checkout/cart',
			'text_checkout_shipping_menu'=>'checkout/shipping'
);

if($xml){
	foreach($xml->definition as $item){
		$translates[$routes[(string)$item->key]] = (string)$item->value;
	}

	$storefront_menu = new AMenu_Storefront();
	$storefront_menu->addLanguage($new_language_id,$translates);
}

//Load core content


$this->db->query("INSERT INTO ".DB_PREFIX."stock_statuses 
(`stock_status_id`,`language_id`, `name`) VALUES  
(1, $new_language_id, 'Ada'),
(2, $new_language_id, 'Kosong'),
(3, $new_language_id, 'Pre-Order');
");

$this->db->query("INSERT INTO ".DB_PREFIX."length_class_descriptions 
(`length_class_id`, `language_id`, `title`, `unit`) VALUES 
(1, $new_language_id, 'Centímeter', 'cm'),
(2, $new_language_id, 'Milímeter', 'mm'),
(3, $new_language_id, 'Inch', 'in');
");

$this->db->query("INSERT INTO ".DB_PREFIX."weight_class_descriptions 
(`weight_class_id`, `language_id`, `title`, `unit`) VALUES 
(1, $new_language_id, 'Kilogram', 'kg'),
(2, $new_language_id, 'Gram', 'g'),
(5, $new_language_id, 'Pons ', 'lb'),
(6, $new_language_id, 'Ons', 'oz');
");

$this->db->query("INSERT INTO ".DB_PREFIX."order_statuses 
(`order_status_id`, `language_id`, `name`) VALUES 
(1, $new_language_id, 'Pending'),
(2, $new_language_id, 'Dalam Proses'),
(3, $new_language_id, 'Sudah Dikirim'),
(7, $new_language_id, 'Batal'),
(5, $new_language_id, 'Selesai'),
(8, $new_language_id, 'Ditolak'),
(9, $new_language_id, 'Batal Pengembalian'),
(10, $new_language_id, 'Gagal'),
(11, $new_language_id, 'Dikembalikan'),
(12, $new_language_id, 'Terbalik'),
(13, $new_language_id, 'Tagihan');
");

$this->db->query("INSERT INTO ".DB_PREFIX."page_descriptions 
(`page_id`, `language_id`, `name`, `title`, `seo_url`, `keywords`, `description`, `content`, `created`) VALUES 
(1, $new_language_id, 'Halaman Lain', '', '', '', '', '', now() ),
(2, $new_language_id, 'Halaman Utama', '', '', '', '', '', now() ),
(3, $new_language_id, 'Halaman Checkout', '', '', '', '', '', now() ),
(4, $new_language_id, 'Halaman Login', '', '', '', '', '', now() ),
(5, $new_language_id, 'Halaman Produk Default ', '', '', '', '', '', now() ),
(10, $new_language_id, 'Halaman Perbaikan.', '', '', '', '', '', now() ),
(11, $new_language_id, 'Halaman Akun Pelanggan', '', '', '', '', '', now() );
");

$this->db->query("INSERT INTO ".DB_PREFIX."global_attributes_type_descriptions 
(`attribute_type_id`, `language_id`, `type_name`, `create_date`) VALUES 
(1, $new_language_id, 'Opsi Produk', NOW()),
(2, $new_language_id, 'Attribut Unduh', NOW());
");

$this->db->query("INSERT INTO ".DB_PREFIX."form_descriptions 
(`form_id`, `language_id`, `description`) VALUES 
(2,$new_language_id,'Halaman Kontak Kami');
");

$this->db->query("INSERT INTO ".DB_PREFIX."field_descriptions 
(`field_id`, `name`, `error_text`, `language_id`) VALUES 
(14,'Masukkan kode sesuai kode dalam kotak dibawah ini :','Kode verifikasi tidak sesuai dengan gambar!',$new_language_id),
(13,'Enquiry:','Enquiry harus antara 10 dan 3000 karakter!',$new_language_id),
(12,'Email:','Alamat Email tidak valid!',$new_language_id),
(11,'Nama pertama:','Nama harus anatara 3 dan 32 karakter!',$new_language_id);
");

$this->cache->delete('language');
$this->cache->delete('lang.id');
