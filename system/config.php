<?php
/*
	AbanteCart, Ideal OpenSource Ecommerce Solution
	http://www.AbanteCart.com
	Copyright © 2011-2013 Belavier Commerce LLC

	Released under the Open Software License (OSL 3.0)
*/
// Admin Section Configuration. You can change this value to any name. Will use ?s=name to access the admin
define('ADMIN_PATH', 'mmi');

// Database Configuration
define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'root');
define('DB_DATABASE', 'mmi');
define('DB_PREFIX', 'abmmi_');
define('SALT', 'LL7b');
define('UNIQUE_ID', '04c1933d83a73df476eab98b0ac96199');
?>