<section id="featured" class="container">
	
		<?php if ( $block_framed ) { ?>
		<div class="block_frame block_frame_<?php echo $block_details['block_txt_id']; ?>"
			 id="block_frame_<?php echo $block_details['block_txt_id'] . '_' . $block_details['instance_id'] ?>">

			<div class="heading col-xs-12">
			  <div class="content">
			    <h2 class="animated fadeInUp delayp1"><?php echo $heading_title; ?></h2>
			    <p class="animated fadeInUp delayp1"><?php echo $heading_subtitle; ?></p>
			  </div>
			</div>			
			<?php } ?>

			<?php include($this->templateResource('/template/blocks/product_list.tpl')) ?>

			<?php if ( $block_framed ) { ?>
	
	<?php } ?>
	<div class="col-xs-12">
        <p class="disclaimer"><?php echo $text_nav_note; ?></p>
      </div>
	</div>
</section>


