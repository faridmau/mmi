<?php if ( count($languages) > 1 ) { ?>  
<a class="pull-right dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" href="">
    <?php foreach ($languages as $language) { ?>
    <?php if ($language[ 'code' ] == $language_code) { ?>  
        
        <?php if($language[ 'image' ]) { ?>
            
        <?php } else { echo '&nbsp;'; } ?><?php echo $language[ 'name' ]; ?>
       
    <?php } ?>
    <?php } ?>
<span class="caret"></span></a>
<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">

<?php if ($languages) { ?>
  <li class="dropdown hover">
<?php foreach ($languages as $language) { ?>
      <li>
      <a href="<?php echo $language[ 'href' ]; ?>">
      <?php echo $language[ 'name' ]; ?>
      </a>
<?php } ?>
  </li>
<?php } ?>  
</ul>
<?php } ?>