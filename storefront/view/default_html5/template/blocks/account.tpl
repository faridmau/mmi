<?php $rt =  $_GET['rt'];?>
<div class="navbar navbar-bottom" role="navigation">
  <div class="container">

    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <!--<span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>-->
        <span class="glyphicon glyphicon-chevron-down"></span>
      </button>

      <h2 class="visible-xs" id="navbar-title-mobile">
        <?php if ( $rt == 'account/account') echo 'Dasboard'; ?>
        <?php if ( $rt == 'account/history' || $rt == 'account/history/detail') echo $text_history; ?>
        <?php if ( $rt == 'account/transactions/history') echo $text_transactions; ?>
        <?php if ( $rt == 'account/consolidate') echo $text_statments; ?>
        <?php if ( $rt == 'account/newsletter') echo $text_my_newsletter; ?>
        <?php if ( $rt == 'account/edit'||$rt == 'account/password') echo $text_edit; ?>
      </h2>
    </div>

    <div class="navbar-collapse collapse navbar-left" role="navigation">
      <ul class="nav navbar-nav custom-account" role="navigation">
        <li id="navbar-acc-dashboard" <?php if ( $rt == 'account/account') echo 'class="active"'; ?>>
          <a href="<?php echo $this->html->getSecureURL('account/account'); ?>"><?php echo $text_dasboard_main; ?></a>
        </li>
        <li id="navbar-acc-order" <?php if ( $rt == 'account/history' || $rt == 'account/history/detail') echo 'class="active"'; ?>>
          <a href="<?php echo $this->html->getSecureURL('account/history'); ?>"><?php echo $text_history; ?></a>
        </li>
        <li id="navbar-acc-transaction" <?php if ( $rt == 'account/transactions/history') echo 'class="active"'; ?>>
          <a href="<?php echo $this->html->getSecureURL('account/transactions/history'); ?>"><?php echo $text_transactions; ?></a>
        </li>
        <li id="navbar-acc-consolidated" <?php if ( $rt == 'account/consolidate') echo 'class="active"'; ?>>
          <a href="<?php echo $this->html->getSecureURL('account/consolidate');?>">
            <?php echo $text_statments; ?>
          </a>
        </li>
        <li id="navbar-acc-newsletter" <?php if ( $rt == 'account/newsletter') echo 'class="active"'; ?>>
          <a href="<?php echo $this->html->getSecureURL('account/newsletter'); ?>">
            <?php echo $text_my_newsletter; ?>
          </a>
        </li>
        <li id="navbar-acc-account" <?php if ( $rt == 'account/edit'||$rt == 'account/password') echo 'class="active"'; ?>>
          <a href="" data-toggle="modal" data-target="#editAccount">
            <?php echo $text_edit; ?>
          </a>
        </li>
        <li id="navbar-acc-logoff">
          <a href="<?php echo $this->html->getSecureURL('account/logout'); ?>">
            <span class="glyphicon glyphicon glyphicon-log-out hidden-sm"></span>
            &nbsp;<?php echo $text_logout; ?>
          </a>
        </li>

      </ul>
    </div>

  </div>
</div><!--.navbar-bottom-->

<!--EDIT ACCOUNT MODAL-->
<div class="modal fade editaccount" id="editAccount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 class="h4"><?php echo $text_edit; ?></h3>
      </div>
      <div class="modal-body clearfix">
        <a href="<?php echo $this->html->getSecureURL('account/edit'); ?>" class="font14 m_b_10 text-center" style="display:block"><?php echo $text_information; ?></a>
        <a href="<?php echo $this->html->getSecureURL('account/password');?>" class="font14 text-center" style="display:block"><?php echo $text_password; ?></a>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
	var line = $( "a.menu_account" ).parent().addClass( "active" );
</script>