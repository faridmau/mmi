<div class="row-5">

	<?php
	if ($products) {
		foreach ($products as $product) {

			$item = array();
			$item['image'] = $product['thumb']['thumb_html'];
			$item['title'] = $product['name'];
			$item['description'] = $product['model'];
			$item['rating'] = ($product['rating']) ? "<img width='100%' src='" . $this->templateResource('/image/stars_' . $product['rating'] . '.png') . "' alt='" . $product['stars'] . "' />" : '';

			$item['info_url'] = $product['href'];
			$item['buy_url'] = $product['add'];

			if (!$display_price) {
				$item['price'] = '';
			}

			$review = $button_write;
			if ($item['rating']) {
				$review = $item['rating'];
			}

			?>
			
			<div class="col-xs-6 col-sm-4 col-md-3">
	          <!-- <a href="<?php echo $item['info_url'] ?>" title="<?php echo $item['title'] ?>"> -->
	            <div class="thumb">
	              <div class="content">
	              	<a href="<?php echo $item['info_url'] ?>" title="<?php echo $item['title'] ?>">
		                <div class="image feature">
		                  <p class="title"><?php echo $item['title'] ?></p>
		                  <div class="pic-overlayer"></div>
		                  <?php echo $item['image'] ?>
		                </div>
		            </a>    
	                <div class="info clearfix">
	                	<?php if ($display_price) { ?>
							
		                	<?php if ($product['special']) { ?>
								<!-- <p class="pricenew"><?php echo $product['special'] ?></p> -->
								<p class="price pull-left"><?php echo $product['price'] ?><span class="unit">/ unit *</span></p>
							<?php } else { ?>
								<p class="price pull-left">
									<?php echo $product['navpu'];?>
									<span class="unit">/ unit *</span>
								</p>
							<?php }?>

							<?php if(!$product['call_to_order']){ ?>
							
							<button data-id="<?php echo $product['product_id'] ?>" onClick="location.href='<?php echo $item['buy_url'] ?>'" class="btn btn-sm btn-primary btn-subscribe visible-xs" style="width: 100%">
								<?php echo $button_subscribe; ?> &nbsp;<span class="glyphicon glyphicon-shopping-cart"></span>
							</button>
							
							<button class="btn btn-sm btn-primary btn-subscribe pull-right hidden-xs" onClick="location.href='<?php echo $item['buy_url'] ?>'"><?php echo $button_subscribe; ?> &nbsp;<span class="glyphicon glyphicon-shopping-cart"></span></button>
							
							<?php }else{ ?>
							
							<button data-id="<?php echo $product['product_id'] ?>" href="#"
							   class="btn btn-sm btn-primary btn-subscribe pull-right hidden-xs"><?php echo $text_call_to_order?>, Call To Order &nbsp;<span class="glyphicon glyphicon-shopping-cart"></span></button>
							
							<?php } 
							// echo $this->getHookVar('product_price_hook_var_' . $product['product_id']);
							?>
							
	                    <?php }?>
	                </div>

	              </div><!--.content-->
	            </div><!--.thumb-->
	          <!-- </a> -->
	        </div>

		<?php
		}
	}
	?>
</div>