<ul class="nav navbar-nav custom-nav" role="navigation">
	<?php
		foreach ($storemenu as $i => $menu_item) {
	 	   if ($menu_item['id'] == 'home') {
	    		unset($storemenu[$i]);
	    		break;
	    	}
		}?>
		<?php 
			//NOTE:
			//HTML tree builded in helper/html.php
			//To controll look and style of the menu use CSS in styles.css
		?>
	    <?php echo buildStoreFrontMenuTree( $storemenu ); ?>
</ul>
<script type="text/javascript">
	var cart = '<?php echo count($this->cart->getProducts()); ?>';
	var rt='<?php echo $_GET["rt"] ?>'
	$(document).ready(function(){
		if (cart!=0) {
			$('.menu_cart').append('  <span class="cart-count">'+cart+'</span>');	
		};
		if(rt=="checkout/cart"){
			$('.menu_cart').parent().addClass('active');
		}
		else if(rt=='account/login'){
			$('.menu_login').parent().addClass('active');	
		}
	});
</script>   