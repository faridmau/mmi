<div class="heading"><?php echo $heading_title; ?></div>
<!-- <form class="form-inline" role="form"> -->
<?php echo $form_open;?>
  <div class="clearfix" id="letter">
  	<?php foreach($form_fields as $field_name=>$field_value){?>
	<input type="hidden" name="<?php echo $field_name?>" value="<?php echo $field_value; ?>">
	<?php } ?>

    <input type="email" name="email" class="form-control custom pull-left" placeholder="<?php echo $text_subscribe; ?>" id="appendedInputButton">
    <input type="submit" class="btn btn-primary btn-sm pull-left" value="GO">
  </div>
</form>
<p class="text-left"><?php echo $text_signup; ?></p>
<script type="text/javascript">
	$(document).ready(function(){
		$('#subscribeFrm').addClass('form-inline');
	});
</script>