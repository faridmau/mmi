<div class="heading">
  <div class="container">
    <?php if($text_shopping_cart) { ?>
    <div class="row order-steps">
      <div class="col-xs-3">
        <div class="step-item clearfix">
          <div class="ic-step"><span class="glyphicon glyphicon-ok"></span></div>
          <p class="hidden-xs"><?php echo $text_shopping_cart; ?></p>
        </div>
      </div>
      <div class="col-xs-3">
        <div class="step-item clearfix">
          <div class="ic-step"><span class="glyphicon glyphicon-ok"></span></div>
          <p class="hidden-xs"><?php echo $text_payment_method; ?></p>
        </div>
      </div>
      <div class="col-xs-3">
        <div class="step-item clearfix">
          <div class="ic-step"><span class="glyphicon glyphicon-ok"></div>
          <p class="hidden-xs"><?php echo $text_confirmation; ?></p>
        </div>
      </div>
      <div class="col-xs-3">
        <div class="step-item clearfix">
          <div class="ic-step"><span class="glyphicon glyphicon-ok"></div>
          <p class="hidden-xs"><?php echo $text_payment_process; ?></p>
        </div>
      </div>
      <div class="col-xs-3">
        <div class="step-item active clearfix">
          <div class="ic-step">5</div>
          <p class="hidden-xs"><?php echo $text_finish; ?></p>
        </div>
      </div>
    </div>
    <?php } ?>
    <?php if($text_finish) {?>
    <h2 class="animated fadeInUp delayp1"><?php echo $text_finish; ?></h2>
    <?php }else{?>
    <h2 class="animated fadeInUp delayp1"><?php echo $heading_title; ?></h2>
    <?php } ?>
    <p class="animated fadeInUp delayp1"></p>
  </div>
</div><!--.heading-->
<div class="container">

  <div class="row">
    <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">

      <div class="thumb">
        <div class="content p_20">

          <p class="text-center m_b_30">
            <img src="<?php echo $this->templateResource("/img/ic_finish.png"); ?>" width="75" style="display:block; margin:20px auto 0"><?php echo $text_message; ?>
          </p>
          <div class="m_t_20">
            <a href="<?php echo $continue ?>" class="btn btn-primary" style="width: 100px; display:block; margin: auto">&nbsp;<?php echo $continue_button->text ?></a>
          </div>

        </div><!--.content-->
      </div><!--.thumb-->

    </div><!--.col-->
  </div><!--.row-->
