<!-- Header Start -->
<div class="navbar-top">
  <div class="container">
    <div class="dropdown">
   
      <?php echo ${$children_blocks[3]}; ?>
    </div>
  </div><!--.container-->
</div><!--.navbar-top-->

<div class="navbar navbar-static-top" role="navigation">
  <div class="container">

    <div class="navbar-header">
      <!--<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>-->

      <div class="nav-icons visible-xs clearfix">
        <!--<a href="<?php echo $prefix;?>home">
          <img src="<?php echo $prefix;?>assets/img/ic_nav_home-active.png" width="40" alt="Home" id="image-nav-home">
        </a>-->
        <?php if (is_null($this->customer->getId())): ?>
          <a href="<?php echo $login;?>">
            <p class="pull-left" style="color: #fff; padding: 10px 15px">Login</p>
          </a>
        <?php endif ?>
        
        <a href="<?php echo $dasboard;?>">
          <img src="<?php echo $this->templateResource('/img/ic_nav_account.png') ?>" width="40" alt="My Account" id="image-nav-account">
        </a>
        <a href="<?php echo $cart;?>">
          <img src="<?php echo $this->templateResource('/img/ic_nav_cart.png') ?>" width="40" alt="Shopping Cart" id="image-nav-cart">
          
        </a>
      </div>

      <?php if (is_file(DIR_RESOURCE . $logo)) { ?>
    	<a class="navbar-brand" style="background-color: transparent;" href="<?php echo $homepage; ?>"><img id="logo-main" src="resources/<?php echo $logo; ?>"
    																   title="<?php echo $store; ?>"
    																   alt="<?php echo $store; ?>" style="width: 80px;"/></a>
    	<?php } else if (!empty($logo)) { ?>
    	<a class="logo pull-left" style="background-color: transparent;" href="<?php echo $homepage; ?>"><?php echo $logo; ?></a>
      <?php } ?>
    </div>

    <div class="navbar-right hidden-xs" role="navigation">
        <?php echo ${$children_blocks[7]}; ?>
    </div>

  </div>
</div><!--.navbar-->

<!-- Header End -->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?2STjftJvPIiDzJRKn1KOFdvbAaEO4yp1';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>