<div class="footer">
	<div class="container">

		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<?php echo ${$children_blocks[0]}; ?>
			</div>
			<div class="col-xs-12 col-sm-3">
				<?php echo ${$children_blocks[2]}; ?>
				
			</div>
			<div class="col-xs-12 col-sm-3">
				<?php echo ${$children_blocks[3]}; ?>
			</div>
		</div>

		<div class="footer footer-bottom">
			<p class="pull-left">
				&copy; 2014 Mandiri Investasi. <?php echo $text_copy_right; ?>
				<a data-toggle="modal" href="#privacypolicy"><?php echo $text_privacy_police; ?></a> 
				&nbsp; <span class="cl-ccc">|</span> &nbsp;
				<a data-toggle="modal" href="#termsconditions"><?php echo $text_terms; ?></a>
			</p>
			<div class="foot-social">
				<?php echo ${$children_blocks[7]}; ?>
			</div>
		</div>

  </div><!--.container-->
</div><!--.footer-->


<!-- Footer -->


<!-- pop up -->
<div class="modal fade" id="privacypolicy" role="dialog" aria-labelledby="privacypolicy" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body" style="position: relative">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute; top: 10px; right: 15px">&times;</button>

          <h2 class="h2"><?php echo $text_agree_href_text; ?></h2>
	      <p class="subp"><?php echo $text_last_update ?> <?php echo $text_privacy_update; ?></p>
	      <b>
	        <?php echo $text_privacy_desc; ?>
	      </b> 
	      <br>
	      <br> 
	      <?php echo $text_privacy_content; ?>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

  <div class="modal fade" id="termsconditions" role="dialog" aria-labelledby="termsconditions" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body" style="position: relative">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute; top: 10px; right: 15px">&times;</button>

          <h2 class="h2"><?php echo $text_terms_text; ?></h2>
          <p class="subp"><?php echo $text_last_update ?> <?php echo $text_terms_update; ?></p>
          <b>
	        <?php echo $text_terms_desc; ?>
	      </b> 
	      <br>
	      <br> 
	      <?php echo $text_terms_content; ?> 
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<!--
AbanteCart is open source software and you are free to remove the Powered By AbanteCart if you want, but its generally accepted practise to make a small donatation.
Please donate via PayPal to donate@abantecart.com
//-->

<!-- Placed at the end of the document so the pages load faster -->

 <!-- Begin call custom JS -->

<script type="text/javascript"src="<?php echo $this->templateResource('/javascript/bootstrap.js'); ?>"></script>
<script type="text/javascript"src="<?php echo $this->templateResource('/javascript/holder.js'); ?>"></script>

<script>
//$('#navbar-login').removeClass('hidden');
</script>

<!-- END call custom JS -->	

<?php if ($google_analytics) {
	$ga_data = $this->registry->get('google_analytics_data');
	?>
	<script type="text/javascript">

		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', '<?php echo $google_analytics;?>']);
		_gaq.push(['_trackPageview']);

		<?php if($ga_data){?>
		_gaq.push(['_set', 'currencyCode', '<?php echo $ga_data['currency_code'];?>']);
		_gaq.push(['_addTrans',
			'<?php echo $ga_data['transaction_id'];?>',
			'<?php echo $ga_data['store_name'];?>',
			'<?php echo $ga_data['total'];?>',
			'<?php echo $ga_data['tax'];?>',
			'<?php echo $ga_data['shipping'];?>',
			'<?php echo $ga_data['city'];?>',
			'<?php echo $ga_data['state'];?>',
			'<?php echo $ga_data['country'];?>'
		]);
		_gaq.push(['_trackTrans']);
		<?php }?>

		(function () {
			var ga = document.createElement('script');
			ga.type = 'text/javascript';
			ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(ga, s);
		})();

	</script>

<?php } ?>

<?php foreach ($scripts_bottom as $script) { ?>
	<script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php } ?>