<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?php echo $text_confirmation_letter; ?></title>
</head>

<body bgcolor="#f9f9f9" style="font-family: Lucida Grande, Arial, Helvetica, sans-serif; font-size: 11px; line-height: 1.4; color:#333333" topmargin="0">
  <br>
  <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="-webkit-border-radius: 4px 4px 0px 0px; border-radius: 4px 4px 0px 0px; overflow: hidden; background: #033a67;">
    <tbody>
        <tr>
          <td style="padding-left: 25px">
            <img src="http://www.antikode.com/mmibeta/assets/img/img_logo_mmi.png" height="40">
          </td>
          <td style="font-size: 18px; color: #fff; padding: 20px; text-align: right">
            <?php echo $text_confirmation_letter; ?>
          </td>
        </tr>
    </tbody>
  </table>
  <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="color:#333333; border-width:1px; border-color:#eee; border-style:solid; -webkit-border-radius: 0px 0px 5px 5px; border-radius: 0px 0px 5px 5px; overflow: hidden; background: #fff; padding-top: 30px">
    <tbody>
      <tr>
        <td>
          <table width="550" border="0" cellspacing="0" cellpadding="0" align="center" style="margin-bottom: 20px">
            <tbody>
              <tr>
                <td style="padding: 2px 8px; color: #999999" width="60"><?php echo $text_trx; ?></td>
                <td style="padding: 2px 8px" width="215"><?php echo $trx_id; ?></td>
                <td style="padding: 2px 8px; color: #999999" width="60"><?php echo $text_date; ?></td>
                <td style="padding: 2px 8px" width="215"><?php echo $trx_date; ?></td>
              </tr>
              <tr>
                <td style="padding: 2px 8px; color: #999999" width="60"><?php echo $text_name; ?></td>
                <td style="padding: 2px 8px" width="215"><?php echo $name; ?></td>
                <td style="padding: 2px 8px; color: #999999" width="60"><?php echo $text_payment; ?></td>
                <td style="padding: 2px 8px" width="215"><?php echo $payment_method; ?></td>
              </tr>
              <tr>
                <td style="padding: 2px 8px; color: #999999" width="60"><?php echo $text_email; ?></td>
                <td style="padding: 2px 8px" width="215"><?php echo $email; ?></td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
      <tr>
        <td>
          <table width="550" border="0" cellspacing="0" cellpadding="0" align="center">
            <thead>
              <tr style="text-align: left;">
                <th style="border-bottom: 1px solid #eee; padding: 8px" width="280"><?php echo $text_product; ?></th>
                <th style="border-bottom: 1px solid #eee; padding: 8px; text-align: right" width="90"><?php echo $text_subs_amount; ?></th>
                <!--th style="border-bottom: 1px solid #eee; padding: 8px; text-align: right" width="80">Fee Amount</th-->
                <th style="border-bottom: 1px solid #eee; padding: 8px; text-align: right" width="80">NAVPU</th>
                <th style="border-bottom: 1px solid #eee; padding: 8px; text-align: right" width="80">Units</th>
              </tr>
            </thead>
            <tbody style="line-height: 16px">
              <tr>
                <td style="border-bottom: 1px solid #eee; padding: 8px">
                  <table border="0" cellspacing="0" cellpadding="0">
                    <tr><td><b><?php echo $product_name; ?></b></td></tr>
                  </table>
                </td>
                <td style="border-bottom: 1px solid #eee; text-align: right; padding: 8px"><?php echo $trx_amount; ?></td>
                <!--td style="border-bottom: 1px solid #eee; text-align: right; padding: 8px">Rp2.000,00</td-->
                <td style="border-bottom: 1px solid #eee; text-align: right; padding: 8px"><?php echo $nav_value; ?></td>
                <td style="border-bottom: 1px solid #eee; text-align: right; padding: 8px"><?php echo $trx_units; ?></td>
              </tr>
            <tbody>
            <!--tfoot>
              <tr>
                <td colspan="3" style="text-align: right; padding: 8px"><b>Total</b></td>
                <td style="text-align: right; padding: 8px; font-size: 13px;"><b>Rp306,000.00</b></td>
              </tr>
              <tr></tr>
            </tfoot-->
          </table>
        </td>
      </tr>
      <tr>
        <td>
          <table width="550" border="0" cellspacing="0" cellpadding="0" align="center" style="padding: 20px 0">
            <tbody>
              <tr>
                <td>
                  <?php echo $text_thanks; ?><br>
                  <br>
                  <em style="color: #666;"><b><?php echo $text_note ?></b><br>
                  <?php echo $text_note_notification ?></em>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>





  <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="font-size:11px; color: #999; margin-top:15px">
    <tbody>
      <tr>
        <td style="padding-left:20px; padding-right:20px">
          © 2014 <?php echo $text_reserved ?><br><br>
        </td>
      </tr>
    </tbody>
  </table>

</body>
</html>
