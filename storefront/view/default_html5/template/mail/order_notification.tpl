<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0058)http://mmiux.mercatocapitale.com/email/notification-1.html -->
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><?php echo $title; ?></title>
</head>

<body bgcolor="#f9f9f9" style="font-family: Lucida Grande, Arial, Helvetica, sans-serif; font-size: 10px; line-height: 1.4; color:#333333" topmargin="0">
  <br>
  <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="-webkit-border-radius: 4px 4px 0px 0px; border-radius: 4px 4px 0px 0px; overflow: hidden; background: #033a67;">
    <tbody>
        <tr>
          <td style="padding-left: 25px">
            <a href="<?php echo $store_url; ?>" title="<?php echo $store_name; ?>">
              <img src="<?php echo $logo; ?>" alt="<?php echo $store_name; ?>" style="border: none;"height="40">
            </a>

          </td>
          <td style="font-size: 18px; color: #fff; padding: 20px; text-align: right">
            <?php echo $text_order_detail; ?>
          </td>
        </tr>
    </tbody>
  </table>
  <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="color:#333333; border-width:1px; border-color:#eee; border-style:solid; -webkit-border-radius: 0px 0px 5px 5px; border-radius: 0px 0px 5px 5px; overflow: hidden; background: #fff; padding-top: 30px">
    <tbody>
      <tr>
        <td>
          <table width="550" border="0" cellspacing="0" cellpadding="0" align="center" style="margin-bottom: 20px">
            <tbody>
              <tr>
                <td style="padding: 2px 8px; color: #999999" width="60"><?php echo $text_trx ?></td>
                <td style="padding: 2px 8px" width="215"><?php echo $order_id; ?></td>
                <td style="padding: 2px 8px; color: #999999" width="60"><?php echo $text_date_added; ?></td>
                <td style="padding: 2px 8px" width="215"><?php echo $date_added; ?></td>
              </tr>
              <tr>
                <td style="padding: 2px 8px; color: #999999" width="60"><?php echo $text_name; ?></td>
                <td style="padding: 2px 8px" width="215"><?php echo $firstname ?> <?php echo $lastname ?></td>
                <td style="padding: 2px 8px; color: #999999" width="60"><?php echo $text_payment_method; ?></td>
                <td style="padding: 2px 8px" width="215"><?php echo $payment_method; ?></td>
              </tr>
              <tr>
                <td style="padding: 2px 8px; color: #999999" width="60"><?php echo $text_email; ?></td>
                <td style="padding: 2px 8px" width="215"><?php echo $customer_email; ?></td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
      <tr>
        <td>
          <table width="550" border="0" cellspacing="0" cellpadding="0" align="center">
            <thead>
              <tr style="text-align: left;">
                <th style="border-bottom: 1px solid #eee; padding: 8px" width="250"><?php echo $column_product; ?></th>
                <th style="border-bottom: 1px solid #eee; padding: 8px; text-align:right" width="150"><?php echo $text_subs_amount; ?></th>
                <th style="border-bottom: 1px solid #eee; padding: 8px; text-align: right" width="150"><?php echo $text_fee_amount; ?></th>
              </tr>
            </thead>
            <tbody style="line-height: 16px">
             
              <?php foreach ($products as $product) { ?>
              <tr >
                <td style="border-bottom: 1px solid #eee; padding: 8px"><?php echo $product[ 'name' ]; ?>
                  <?php foreach ($product[ 'option' ] as $option) { ?>
                    <br/>
                    &nbsp;&nbsp;- <?php echo $option[ 'name' ]; ?>: <?php echo $option[ 'value' ]; ?>
                    <?php } ?></td>
                
                <td style="border-bottom: 1px solid #eee; text-align: right; padding: 8px">Rp <?php echo number_format($product[ 'quantity' ], 2, '.', ','); ?></td>
                <td style="border-bottom: 1px solid #eee; text-align: right; padding: 8px">Rp <?php echo number_format($product[ 'tax' ], 2, '.', ','); ?></td>
              </tr>
              <?php } ?>
              <!-- <tr>
                <td style="border-bottom: 1px solid #eee; padding: 8px">
                  <table border="0" cellspacing="0" cellpadding="0">
                    <tbody><tr><td><b>Reksa Dana Mandiri Investa</b></td></tr>
                  </tbody></table>
                </td>
                <td style="border-bottom: 1px solid #eee; text-align: right; padding: 8px">Rp100,000.00</td>
                <td style="border-bottom: 1px solid #eee; text-align: right; padding: 8px">Rp2,000.00</td>
              </tr> -->
            </tbody><tbody>
            </tbody><tfoot>
              <!--tr>
                <td colspan="2" style="text-align: right; padding: 8px"><b>Total Subscription</b></td>
                <td style="text-align: right; padding: 8px; font-size: 13px;"><b>Rp300,000.00</b></td>
              </tr>
              <tr>
                <td colspan="2" style="text-align: right; padding: 8px"><b>Total Fee</b></td>
                <td style="text-align: right; padding: 8px; font-size: 13px;"><b>Rp6,000.00</b></td>
              </tr-->
              <tr>
                <td colspan="2" style="text-align: right; padding: 8px"><b><?php echo $text_total; ?></b></td>
                <td style="text-align: right; padding: 8px; font-size: 13px;"><b><?php echo $product['total']?></b></td>
              </tr>
              <tr></tr>
            </tfoot>
          </table>
        </td>
      </tr>
      <tr>
        <td>
          <table width="550" border="0" cellspacing="0" cellpadding="0" align="center" style="padding: 20px 0">
            <tbody>
              <tr>
                <td>
                  <?php echo $text_thanks; ?><br>
                  <br>
                  <em style="color: #666;"><b><?php echo $text_note ?></b><br>
                  <?php echo $text_note_notification ?></em>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>

  <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="font-size:11px; color: #999; margin-top:15px">
    <tbody>
      <tr>
        <td style="padding-left:20px; padding-right:20px">
          © 2014 <?php echo $text_reserved ?><br><br>
        </td>
      </tr>
    </tbody>
  </table>

</body></html>