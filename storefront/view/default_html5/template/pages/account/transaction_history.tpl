<?php include($this->templateResource('/template/blocks/account.tpl')) ?>
<div class="heading hidden-xs">
  <div class="container">
    <h2 class="animated fadeInUp delayp1"><?php echo $text_transactions; ?></h2>
    <p class="animated fadeInUp delayp1"></p>
  </div>
</div><!--.heading-->
<div class="container">

  <div class="alert alert-info alert-dismissible visible-xs visible-sm" role="alert">
    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <span class="glyphicon glyphicon-phone cl-blue-info"></span> <?php echo $text_mobile_note; ?>
  </div>

  <div class="thumb">
    <div class="content p_15">

      <!--ACTION BAR-->
      <div class="clearfix m_b_30">
        <button data-href="<?php echo $export_url; ?>" class="btn btn-default btn-print pull-left export-pdf"><span class="glyphicon glyphicon-file"></span>&nbsp;<?php echo $text_pdf; ?></button>

        <form id="filter_transaction" action="<?php echo $filter_url; ?>" method="post" class="pull-right form-inline form-period hidden-xs" role="form" accept-charset="/">
          <div class="form-group">
            <label class="m_r_10"><?php echo $text_transaction_date; ?></label>
            <?php echo $date_start; ?>
          </div>
          <div class="form-group">
            <label class="m_rl_5 typo-light"><?php echo $text_to; ?></label>
            <?php echo $date_end; ?>
          </div>
          <div class="form-group">
            <?php echo $product_type; ?>
          </div>
          <button type="submit" class="btn btn-primary"><?php echo $button_go; ?></button>
          <button type="reset" class="btn btn-default btn-reset"><?php echo $button_reset; ?></button>
        </form>
      </div><!--.clearfix-->

      <!--PERIOD INFO ON MOBILE-->
      <div class="m_b_30 visible-xs" style="margin-top: -15px">
        <p class="m_b_10">
          <?php if($date_start_value && $date_end_value): ?>
            <span class="cl-aaa typo-light"><?php echo $text_transaction_date; ?>&nbsp;&nbsp;</span> <?php echo $date_start_value; ?> &nbsp;<span class="typo-light cl-aaa"><?php echo $text_to; ?></span>&nbsp; <?php echo $date_end_value; ?>
          <?php endif; ?>
          <br><a href="" data-toggle="modal" data-target="#searchFilter"><?php echo $text_change; ?></a>
        </p>
      </div>

      <!--CHANGE DATE MODAL-->
      <div class="modal fade" id="searchFilter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h3 class="h4"><?php echo $text_change; ?></h3>
            </div>
            <div class="modal-body clearfix">
              <form id="filter_transaction_mobile" action="<?php echo $filter_url; ?>" method="post" class="form-inline visible-xs" role="form">
                <div class="form-group">
                  <label class="m_r_10"><?php echo $text_transaction_date; ?></label>
                  <?php echo $date_start_mobile; ?>
                </div>
                <div class="form-group">
                  <label class="m_rl_5 typo-light"><?php echo $text_to; ?></label>
                  <?php echo $date_end_mobile; ?>
                </div>
                <div class="form-group">
                  <?php echo $product_type; ?>
                </div>
                <button type="submit" class="btn btn-primary btn-full m_b_10"><?php echo $button_go; ?></button>
                <button type="reset" class="btn btn-default btn-reset btn-full"><?php echo $button_reset; ?></button>
              </form>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->

      <!--BODY WITH RESULT-->
      <div class="table-responsive">
        <table id="data-table" class="table table-striped"> 
          <thead>
            <tr> 
              <td><?php echo $text_column_date; ?></td>
              <td><?php echo $text_column_name; ?></td>
              <td><?php echo $text_column_type; ?></td>
              <td class="text-right"><?php echo $text_column_nav_date; ?></td>
              <td class="text-right"><?php echo $text_column_navpu; ?></td>
              <td class="text-right"><?php echo $text_column_unit; ?></td>
              <td class="text-right"><?php echo $text_column_amount; ?></td>
              <td class="text-right"><?php echo $text_column_fee; ?></td>
              <td class="text-right"><?php echo $text_column_total_amount; ?></td>
              <td class="text-right"><?php echo $text_column_saldo; ?></td>
            </tr>
          </thead>
          <tbody>
            <?php
              /*if(count($transactions)){
                foreach($transactions as $trn): ?>

              <tr class="<?php if(!empty($trn['frontend_trx_id'])) echo 'highlight'; ?>"> 
                <td><?php echo $trn['transaction_date']; ?></td>
                <td><?php echo $trn['product_name'] ? $trn['product_name'] : $trn['product_code']; ?></td>
                <td><?php echo $trn['transaction_type']; ?></td>
                <td class="text-right"><?php echo $trn['nav_date']; ?></td>
                <td class="text-right"><?php echo $trn['nav_value']; ?></td>
                <td class="text-right"><?php echo $trn['transaction_unit']; ?></td>
                <td class="text-right"><?php echo $trn['transaction_amount']; ?></td>
                <td class="text-right"><?php echo $trn['transaction_fee']; ?></td>
                <td class="text-right"><?php echo $trn['total_amount']; ?></td>
                <td class="text-right"><?php echo $trn['saldo']; ?></td>
              </tr>

            <?php 
                endforeach;
              } else {*/
            ?>
              <tr>
                <td colspan="10" class="text-center"><?php echo $text_not_found; ?></td>
              </tr>
            <?php //} ?>
          </tbody>
        </table>
      </div>

      <div id="data-pagination"></div>
      <?php //echo $pagination_bootstrap; ?>

    </div><!--.content-->
  </div><!--.thumb-->

</div><!--.container-->

<script>
function navbarActive(navbar, selector){
     $('.nav.navbar-nav.custom-account li').each(function() {
        $(this).removeClass('active');
   });
   
     $('.nav.navbar-nav.custom-nav li').each(function(index, element) {
        $(this).removeClass('active');
   });
   
  $('#image-nav-home').attr('src', "<?php echo $this->templateResource('/img/ic_nav_home.png'); ?>");
  $('#image-nav-account').attr('src', "<?php echo $this->templateResource('/img/ic_nav_account-active.png'); ?>"); 
  $('#image-nav-cart').attr('src', "<?php echo $this->templateResource('/img/ic_nav_cart.png'); ?>");
 
  $('#navbar-acc-'+selector).addClass('active');
  $('#navbar-'+navbar).addClass('active');
 }

navbarActive('dashboard','transaction');


/* --- DATEPICKER --- */
$(function() {
   $( "#date_start" ).datepicker({
      altField:'#date_start_mobile',
      dateFormat: "dd-M-y",
        defaultDate: "+1w",
    	  changeMonth: true,
        numberOfMonths: 1,
        maxDate:0,
        minDate:'-'+<?php echo $trx_history_retention_in_days; ?>+'d',
	  onClose: function( selectedDate ) {
	     $( "#date_end" ).datepicker( "option", "minDate", selectedDate);
	  }
   });
   
   
   $( "#date_end" ).datepicker({
      altField:'#date_end_mobile',
      dateFormat: "dd-M-y",
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        maxDate:0,
        minDate:'-'+<?php echo $trx_history_retention_in_days; ?>+'d',
    	  onClose: function( selectedDate ) {
    	     $( "#date_start" ).datepicker( "option", "maxDate", selectedDate);
    	  }
   });
   
   $( "#date_start_mobile" ).datepicker({
      altField:'#date_start',
      dateFormat: "dd-M-y",
        defaultDate: "+1w",
    	  changeMonth: true,
        numberOfMonths: 1,
        maxDate:0,
        minDate:'-'+<?php echo $trx_history_retention_in_days; ?>+'d',
	  onClose: function( selectedDate ) {
	     $( "#date_end_mobile" ).datepicker( "option", "minDate", selectedDate);
	  }
   });
   
   
   $( "#date_end_mobile" ).datepicker({
      altField:'#date_end',
      dateFormat: "dd-M-y",
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        maxDate:0,
        minDate:'-'+<?php echo $trx_history_retention_in_days; ?>+'d',
	  onClose: function( selectedDate ) {
	     $( "#date_start_mobile" ).datepicker( "option", "maxDate", selectedDate);
	  }
   });
});

//** export pdf
$(function(){
  $('.export-pdf').on('click',function(e){
    e.preventDefault();
    var m_names = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
    var currentTime = new Date();
    var year = (currentTime.getFullYear()).toString().substr(2,2);
    var month = m_names[currentTime.getMonth()];
    var date = currentTime.getDate();

    var hours = currentTime.getHours() < 10 ? '0'+currentTime.getHours() : currentTime.getHours();
    var minutes = currentTime.getMinutes() < 10 ? '0'+currentTime.getMinutes() : currentTime.getMinutes();
    var seconds = currentTime.getSeconds() < 10 ? '0'+currentTime.getSeconds() : currentTime.getSeconds();

    var selected_date = date + "-" + month + "-" + year + " " + hours + ":" + minutes + ":" + seconds;

    location.href = $(this).data('href') + '&date_print=' + selected_date;
  });
});



/* -- Fitler & pagination -- */
$(function() {

  $('#filter_transaction .btn-reset, #filter_transaction_mobile .btn-reset').on('click',function(e){
    e.preventDefault();

    $('#filter_transaction')[0].reset();
    $('#filter_transaction_mobile')[0].reset();
    $('#filter_transaction').submit();
  });

  $('#filter_transaction, #filter_transaction_mobile').on('submit',function(e){
    e.preventDefault();

    $('#searchFilter').modal('hide');

    $('#data-table tbody').html('<tr><td class="text-center" colspan="10"><img src="/storefront/view/default_html5/img/loading_2.gif"></td></tr>');
    $('#data-pagination').empty();

    var filter_data = $(this).serialize();

    $.ajax({
      type: 'POST',
      url: 'index.php?rt=r/account/transactions/history' ,
      data: filter_data,
      dataType: 'json',
      success: function(response){
        if(response.html){
          $('#data-table tbody').html(response.html);
          $('#data-pagination').html(response.pagination);
          $('.export-pdf').data('href',response.export_url);
        } else {
          $('#data-table tbody').html('<tr><td colspan="10" class="text-center"><?php echo $text_not_found; ?></td></tr>');
          $('#data-pagination').empty();
        }
      },
      error: function(){

      }
    });
  });

  $('body').on('click','#data-pagination li a',function(e){
    e.preventDefault();

    var filter_data = $(this).attr('href');
    if(!filter_data){
      return;
    }

    $('#data-table tbody').html('<tr><td class="text-center" colspan="10"><img src="/storefront/view/default_html5/img/loading_2.gif"></td></tr>');

    $.ajax({
      type: 'POST',
      url: 'index.php?rt=r/account/transactions/history' ,
      data: filter_data,
      dataType: 'json',
      success: function(response){
        if(response.html){
          $('#data-table tbody').html(response.html);
          $('#data-pagination').html(response.pagination);
        } else {
          $('#data-table tbody').html('<tr><td colspan="10" class="text-center"><?php echo $text_not_found; ?></td></tr>');
          $('#data-pagination').empty();
        }
      },
      error: function(){

      }
    });
  });

});


/* --- NAVBAR MOBILE TITLE --- */
// $('#navbar-title-mobile').text('Transaction History');


/* --- MOBILE PAGINATION --- */
function mobilePagination(){
   
   var width = $('.navbar-top').width();
   
   if(width <= '768'){
      /* --- LEFT SECTION --- */
      $('#trans-history-pagination li:nth-child(3)').addClass('hidden');
      $('#trans-history-pagination li:nth-child(4) a').text('2');
	  
	  
      /* --- RIGHT SECTION --- */ 
      $('#trans-history-pagination li:nth-child(6)').addClass('hidden');
      $('#trans-history-pagination li:nth-child(7) a').text('4');
      $('#trans-history-pagination li:nth-child(8) a').text('5');
   }
   
}

mobilePagination();
</script>