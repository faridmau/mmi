<?php include($this->templateResource('/template/blocks/account.tpl')) ?>
<div class="heading hidden-xs">
  <div class="container">
    <h2 class="animated fadeInUp delayp1"><?php echo $text_consolidate; ?></h2>
    <p class="animated fadeInUp delayp1"></p>
  </div>
</div><!--.heading-->

<div class="container">

  <div class="alert alert-info alert-dismissible visible-xs visible-sm" role="alert">
    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <span class="glyphicon glyphicon-phone cl-blue-info"></span><?php echo $text_note_mobile; ?>
  </div>

  <div class="thumb">
    <div class="content p_15">

      <!--ACTION BAR-->
      <div class="clearfix m_b_30">
        <button data-href="<?php echo $export_url; ?>" class="btn btn-default btn-print pull-left export-pdf"><span class="glyphicon glyphicon-file"></span>&nbsp;<?php echo $text_pdf; ?></button>

        <form action="<?php echo $filter_url; ?>" method="post" id="filter_consolidate" class="pull-right form-inline form-period hidden-xs" role="form">
          <div class="form-group">
            <label class="m_r_10"><?php echo $text_transaction_date; ?></label>
            <?php echo $date_start; ?>
          </div>
          <div class="form-group">
            <label class="m_rl_5 typo-light"><?php echo $text_to ?></label>
            <?php echo $date_end; ?>
          </div>
          <button type="submit" class="btn btn-primary"><?php echo $button_go; ?></button>
          <button type="reset" class="btn btn-default btn-reset"><?php echo $button_reset; ?></button>
        </form>
      </div><!--.clearfix-->

      <!--PERIOD INFO ON MOBILE-->
      <div class="m_b_30 visible-xs" style="margin-top: -15px">
        <p class="m_b_10">
          
          <?php if($date_start_preview || $date_end_preview): ?>
            <span class="cl-aaa typo-light"><?php echo $text_transaction_date; ?>&nbsp;&nbsp;</span> <?php echo $date_start_preview; ?> &nbsp;<span class="typo-light cl-aaa"><?php echo $text_to; ?></span>&nbsp; <?php echo $date_end_preview; ?>
            <br>
          <?php endif; ?>
          <a href="" data-toggle="modal" data-target="#searchFilter"><?php echo $text_search; ?></a>
        </p>
      </div>

      <!--CHANGE DATE MODAL-->
      <div class="modal fade" id="searchFilter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h3 class="h4"><?php echo $text_search; ?></h3>
            </div>
            <div class="modal-body clearfix">
              <form action="<?php echo $filter_url; ?>" method="post" id="filter_consolidate_mobile" class="form-inline visible-xs" role="form">
                <div class="form-group">
                  <label class="m_r_10"><?php echo $text_transaction_date; ?></label>
                  <?php echo $date_start_mobile; ?>
                </div>
                <div class="form-group">
                  <label class="m_rl_5 typo-light"><?php echo $text_to; ?></label>
                  <?php echo $date_end_mobile; ?>
                </div>
                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-full"><?php echo $button_go; ?></button>
                </div>
                <button type="reset" class="btn btn-default btn-full btn-reset"><?php echo $button_reset; ?></button>
              </form>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->

      <!--BODY WITH RESULT FOUND-->
      <div class="">
        <h3 class="h4 m_b_15 m_t_5"><?php echo $text_product_summary; ?></h3>
        <div class="table-responsive m_b_30">
          <table class="table table-striped table-cons" id="data-table2"> 
            <thead>
              <tr> 
                <td rowspan="2" class="text-center"><?php echo $text_name; ?></td>
                <td rowspan="2" class="text-center"><?php echo $text_acc_no; ?></td>
                <td colspan="3" class="text-center"><?php echo $text_column_balance; ?></td>
                <td rowspan="2" class="text-center"><?php echo $text_nav; ?></td>
                <td rowspan="2" class="text-center"><?php echo $text_ending; ?></td>
                <td rowspan="2" class="text-center"><?php echo $text_realized; ?></td>
                <td rowspan="2" class="text-center"><?php echo $text_unrealized; ?></td>
              </tr>
               
              <tr> 
                <td class="text-center table-cons-row-2"><strong><?php echo $text_begining; ?></strong></td>
                <td class="text-center table-cons-row-2"><strong><?php echo $text_movement; ?></strong></td>
                <td class="text-center table-cons-row-2"><strong><?php echo $text_column_ending; ?></strong></td>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($investment_summary as $key => $data): ?>
                <tr> 
                  <td class="text-left"><?php echo $data['product_name']; ?></td>
                  <td class="text-left"><?php echo $data['inv_account_no']; ?></td>
                  <td class="text-right"><?php echo $data['beginning_balance']; ?></td>
                  <td class="text-right"><?php echo $data['movement'] < 0 ? '<span class="cl-red">('.number_format(abs($data['movement']),4,".",",").')</span>' :  '<span class="cl-green">'.number_format(abs($data['movement']),4,".",",").'</span>' ; ?></td>
                  <td class="text-right"><?php echo $data['ending_balance']; ?></td>
                  <td class="text-right"><?php echo $data['nav_value']; ?></td>
                  <td class="text-right"><?php echo $data['ending_balance_amount']; ?></td>
                  <td class="text-right"><?php echo $data['total_realized_gl'] < 0 ? '<span class="cl-red">('.number_format(abs($data['total_realized_gl']),2,".",",").')</span>' :  '<span class="cl-green">'.number_format($data['total_realized_gl'],2,".",",").'</span>' ; ?></td>
                  <td class="text-right">
                    <?php echo $data['unrealized_gl'] < 0 ? '<span class="cl-red">('.number_format(abs($data['unrealized_gl']),2,".",",").')</span>' :  '<span class="cl-green">'.number_format($data['unrealized_gl'],2,".",",").'</span>' ; ?></td>
                </tr>
              <?php endforeach; ?>

              <?php if(!$investment_summary){ ?>
              <tr>
                <td colspan="9" class="text-center"><?php echo $text_not_found; ?></td>
              </tr>
              <?php } ?>
            </tbody>
            <?php if($investment_summary){ ?>
              <tfoot>
                <tr> 
                  <td colspan="6" class="text-left"><?php echo $text_total; ?></td>
                  <td class="text-right"><?php echo $total_ending_balance; ?></td>
                  <td class="text-right"><?php echo $total_realized_gl < 0 ? '<span class="cl-red">('.number_format(abs($total_realized_gl),2,".",",").')</span>' :  '<span class="cl-green">'.number_format($total_realized_gl,2,".",",").'</span>' ; ?></td>
                  <td class="text-right"><?php echo $total_unrealized_gl < 0 ? '<span class="cl-red">('.number_format(abs($total_unrealized_gl),2,".",",").')</span>' :  '<span class="cl-green">'.number_format($total_unrealized_gl,2,".",",").'</span>' ; ?></td>
                </tr>
              </tfoot>
            <?php } ?>
          </table>
        </div><!--.table-responsive-->

        <h3 class="h4 m_b_15 m_t_5"><?php echo $text_transaction_detail; ?></h3>
        <div class="table-responsive">
          <table class="table table-striped table-cons" id="data-table"> 
            <thead>
              <tr> 
                <td class="text-center"><?php echo $text_trn_date; ?></td>
                <td class="text-center"><?php echo $text_trn_type; ?></td>
                <td class="text-center"><?php echo $text_transaction_amount; ?></td>
                <td class="text-center"><?php echo $text_fee_amount; ?></td>
                <td class="text-center"><?php echo $text_net_amount; ?></td>
                <td class="text-center"><?php echo $text_transaction_unit; ?></td>
                <td class="text-center"><?php echo $text_nav; ?></td>
                <td class="text-center"><?php echo $text_average_cost; ?></td>
                <td class="text-center"><?php echo $text_realized; ?></td>
              </tr>
            </thead>
            <tbody>
              <?php if(!$transaction_detail){ ?>
              <tr>
                <td colspan="9" class="text-center"><?php echo $text_not_found; ?></td>
              </tr>
              <?php } ?>
              
              <?php $i=0; $group = array(); ?>
              <?php foreach ($transaction_detail as $key => $data): ?>
                <?php if($group[($i-1)] != $data['group']){ ?>

                  <?php if($i && $data['group'] != $group[($i-1)]){ ?>
                    <tr> 
                      <td colspan="3" class="text-left"></td>
                      <td colspan="2" class="text-left"><strong>Ending Balance</strong></td>
                      <td class="text-right"><strong><?php echo $transaction_detail[($key-1)]['group_ending_balance']; ?></strong></td>
                      <td class="text-right"></td>
                      <td class="text-right"></td>
                      <td class="text-right"><?php echo $transaction_detail[($key-1)]['realized_gl'] < 0 ? '<span class="cl-red">('.number_format(abs($investment_summary[$i]['realized_gl']),2,".",",").')</span>' :  '<span class="cl-green">'.number_format($investment_summary[$i]['realized_gl'],2,".",",").'</span>' ; ?></td>
                    </tr>   
                  <?php } ?>

                  <tr>
                    <td colspan="9">
                      <strong><?php echo $data['product_name']; ?> - <?php echo $data['inv_account_no']; ?></strong>
                    </td>
                  </tr>
                  <tr> 
                    <td colspan="3" class="text-left"></td>
                    <td colspan="2" class="text-left"><strong>Beginning Balance</strong></td>
                    <td class="text-right"><strong><?php echo $data['group_begining_balance']; ?></strong></td>
                    <td class="text-right"></td>
                    <td class="text-right"></td>
                    <td class="text-right"></td>
                  </tr>

                  <?php $group[$i] = $data['group']; ?>
                  <?php $i++; ?>
                <?php } ?>
                <tr>
                  <td class="text-left"><?php echo $data['trx_date']; ?></td>
                  <td class="text-center"><?php echo $data['trx_type']; ?></td>
                  <td class="text-right"><?php echo $data['trx_amount'] < 0 ? '<span class="cl-red">('.number_format(abs($data['trx_amount']),2,".",",").')</span>' :  '<span class="cl-green">'.number_format($data['trx_amount'],2,".",",").'</span>' ; ?></td>
                  <td class="text-right"><?php echo $data['fee_amount']; ?></td>
                  <td class="text-right"><?php echo $data['net_trx_amount'] < 0 ? '<span class="cl-red">('.number_format(abs($data['net_trx_amount']),2,".",",").')</span>' :  '<span class="cl-green">'.number_format($data['net_trx_amount'],2,".",",").'</span>' ; ?></td>
                  <td class="text-right"><?php echo $data['trx_units'] < 0 ? '<span class="cl-red">('.number_format(abs($data['trx_units']),4,".",",").')</span>' :  '<span class="cl-green">'.number_format($data['trx_units'],4,".",",").'</span>' ; ?></td>
                  <td class="text-right"><?php echo $data['nav_per_unit']; ?></td>
                  <td class="text-right"><?php echo $data['average_cost']; ?></td>
                  <td class="text-right"><?php echo $data['realized_gl'] < 0 ? '<span class="cl-red">('.number_format(abs($data['realized_gl']),2,".",",").')</span>' :  '<span class="cl-green">'.number_format($data['realized_gl'],2,".",",").'</span>' ; ?></td>
                </tr>

                <?php if(!$transaction_detail[($key + 1)] && $is_last_page ){ ?>
                  <tr> 
                    <td colspan="3" class="text-left"></td>
                    <td colspan="2" class="text-left"><strong>Ending Balance</strong></td>
                    <td class="text-right"><strong><?php echo $data['group_ending_balance']; ?></strong></td>
                    <td class="text-right"></td>
                    <td class="text-right"></td>
                    <td class="text-right"><?php echo $data['group_realized_gl'] < 0 ? '<span class="cl-red">('.number_format(abs( $data['group_realized_gl']),2,".",",").')</span>' :  '<span class="cl-green">'.number_format( $data['group_realized_gl'],2,".",",").'</span>' ; ?></td>
                  </tr>
                <?php } ?>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div><!--.table-responsive-->

        <div id="data-pagination"></div>
        <?php //echo $pagination_bootstrap; ?>
      </div><!--hidden-->

    </div><!--.content-->
  </div><!--.thumb-->

</div><!--.container-->

<script>
	function navbarActive(navbar, selector){
       $('.nav.navbar-nav.custom-account li').each(function() {
          $(this).removeClass('active');
	   });
	   
       $('.nav.navbar-nav.custom-nav li').each(function(index, element) {
          $(this).removeClass('active');
	   });
	   
	   
    $('#image-nav-home').attr('src', "<?php echo $this->templateResource('/img/ic_nav_home.png'); ?>");
    $('#image-nav-account').attr('src', "<?php echo $this->templateResource('/img/ic_nav_account-active.png'); ?>"); 
    $('#image-nav-cart').attr('src', "<?php echo $this->templateResource('/img/ic_nav_cart.png'); ?>");
	   
   
       $('#navbar-acc-'+selector).addClass('active');
	   $('#navbar-'+navbar).addClass('active');
   }
	
	navbarActive('dashboard','consolidated');
	
	
	/* --- DATEPICKER --- */
	$(function() {
	   $( "#date_start" ).datepicker({
		  dateFormat: "dd-M-y",
          defaultDate: "+1w",
      	  changeMonth: true,
          numberOfMonths: 1,
          maxDate:0,
          minDate:'-'+<?php echo $trx_history_retention_in_days; ?>+'d',
		  onClose: function( selectedDate ) {
		     $( "#date_end" ).datepicker( "option", "minDate", selectedDate);
		  }
	   });
	   
	   
	   $( "#date_end" ).datepicker({
		  dateFormat: "dd-M-y",
          defaultDate: "+1w",
          changeMonth: true,
          numberOfMonths: 1,
          maxDate:0,
          minDate:'-'+<?php echo $trx_history_retention_in_days; ?>+'d',
		  onClose: function( selectedDate ) {
		     $( "#date_start" ).datepicker( "option", "maxDate", selectedDate);
		  }
	   });
	});
	
	
	/* --- MOBILE DATEPICKER --- */
	
	$(function() {
	   $( "#date_start_mobile" ).datepicker({
		  dateFormat: "dd-M-y",
          defaultDate: "+1w",
      	  changeMonth: true,
          numberOfMonths: 1,
          maxDate:0,
          minDate:'-'+<?php echo $trx_history_retention_in_days; ?>+'d',
		  onClose: function( selectedDate ) {
		     $( "#date_end_mobile" ).datepicker( "option", "minDate", selectedDate);
		  }
	   });
	   
	   
	   $( "#date_end_mobile" ).datepicker({
		  dateFormat: "dd-M-y",
          defaultDate: "+1w",
          changeMonth: true,
          numberOfMonths: 1,
          maxDate:0,
          minDate:'-'+<?php echo $trx_history_retention_in_days; ?>+'d',
		  onClose: function( selectedDate ) {
		     $( "#date_start_mobile" ).datepicker( "option", "maxDate", selectedDate);
		  }
	   });
	});

  $(function(){
    $('.export-pdf').on('click',function(e){
      e.preventDefault();
      var m_names = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
      var currentTime = new Date();
      var year = (currentTime.getFullYear()).toString().substr(2,2);
      var month = m_names[currentTime.getMonth()];
      var date = currentTime.getDate();

      var hours = currentTime.getHours() < 10 ? '0'+currentTime.getHours() : currentTime.getHours();
      var minutes = currentTime.getMinutes() < 10 ? '0'+currentTime.getMinutes() : currentTime.getMinutes();
      var seconds = currentTime.getSeconds() < 10 ? '0'+currentTime.getSeconds() : currentTime.getSeconds();

      var selected_date = date + "-" + month + "-" + year + " " + hours + ":" + minutes + ":" + seconds;

      location.href = $(this).data('href') + '&date_print=' + selected_date;
    });
  });

  /* -- Fitler & pagination -- */
$(function() {

  $('#filter_consolidate .btn-reset, #filter_consolidate_mobile .btn-reset').on('click',function(e){
    e.preventDefault();

    $('#filter_consolidate')[0].reset();
    $('#filter_consolidate_mobile')[0].reset();
    $('#filter_consolidate').submit();
  });

  $('#filter_consolidate, #filter_consolidate_mobile').on('submit',function(e){
    e.preventDefault();

    $('#data-table tbody, #data-table2 tbody').html('<tr><td class="text-center" colspan="10"><img src="/storefront/view/default_html5/img/loading_2.gif"></td></tr>');
    $('#data-pagination').empty();

    var filter_data = $(this).serialize();

    $.ajax({
      type: 'POST',
      url: 'index.php?rt=r/account/consolidate' ,
      data: filter_data,
      dataType: 'json',
      success: function(response){
        $('#searchFilter').modal('hide');
        $('.export-pdf').data('href',response.export_url);

        if(response.html_transaction){
          $('#data-table tbody').html(response.html_transaction);
          $('#data-pagination').html(response.pagination);
        } else {
          $('#data-table tbody').html('<tr><td colspan="9" class="text-center"><?php echo $text_not_found; ?></td></tr>');
          $('#data-pagination').empty();
        }

        if(response.html_summary){
          $('#data-table2 tbody').html(response.html_summary);
        } else {
          $('#data-table2 tbody').html('<tr><td colspan="9" class="text-center"><?php echo $text_not_found; ?></td></tr>');
        }
      },
      error: function(){

      }
    });
  });

  $('body').on('click','#data-pagination li a',function(e){
    e.preventDefault();

    var filter_data = $(this).attr('href');
    if(!filter_data){
      return;
    }

    $('#data-table tbody').html('<tr><td class="text-center" colspan="10"><img src="/storefront/view/default_html5/img/loading_2.gif"></td></tr>');

    $.ajax({
      type: 'POST',
      url: 'index.php?rt=r/account/consolidate' ,
      data: filter_data,
      dataType: 'json',
      success: function(response){
        if(response.html_transaction){
          $('#data-table tbody').html(response.html_transaction);
          $('#data-pagination').html(response.pagination);
        } else {
          $('#data-table tbody').html('<tr><td colspan="10" class="text-center"><?php echo $text_not_found; ?></td></tr>');
          $('#data-pagination').empty();
        }
      },
      error: function(){

      }
    });
  });
  
});
	
	
	/* --- NAVBAR MOBILE TITLE --- */
	// $('#navbar-title-mobile').text('Consolidated Statement');
	</script>