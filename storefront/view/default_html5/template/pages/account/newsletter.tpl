<?php include($this->templateResource('/template/blocks/account.tpl')) ?>
<div class="heading hidden-xs">
  <div class="container">
    <h2 class="animated fadeInUp delayp1"><?php echo $text_newsletter; ?></h2>
    <p class="animated fadeInUp delayp1"></p>
  </div>
</div><!--.heading-->

<div class="container">

  <div class="row">
    <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">

      <div class="thumb">
        <div class="content p_20">
          
          <br><br>
          <?php echo $form['form_open']; ?>
          <div class="form-group row clearfix m_b_0">
            <label class="col-sm-3 control-label"><?php echo $entry_newsletter; ?></label>
            <div class="col-sm-9">
              <div class="row">
                <div class="col-sm-4">
                  <div class="radio">
                    <label>
                      <input type="radio" name="newsletter" id="newsFrm_newsletter1" value="1" <?php if($newsletter_val=='1') echo 'checked'; ?> >
                      <?php echo $text_yes; ?>
                    </label>
                  </div>
                </div><!--.col-sm-4-->
                <div class="col-sm-4">
                  <div class="radio">
                    <label>
                      <input type="radio" name="newsletter" id="newsFrm_newsletter2" value="2" <?php if($newsletter_val=='2') echo 'checked'; ?> >
                      <?php echo $text_no; ?>
                    </label>
                  </div>
                </div><!--.col-sm-4-->
              </div><!--.row-->
            </div><!--.col-sm-9-->
          </div><!--.form-group-->
          <?php echo $this->getHookVar('newsletter_edit_sections'); ?>
          <button type="submit" class="btn btn-primary" style="width: 150px; display:block; margin:40px auto 0"><?php echo $button_save; ?></button>
          </form>
        </div><!--.content-->
      </div><!--.thumb-->

    </div><!--.col-->
  </div><!--.row-->

</div><!--.container-->