<?php include($this->templateResource('/template/blocks/account.tpl')) ?>
    
    <?php //include($prefix."static/analytics.php"); ?>
  </head>

    <div class="container hidden-xs">
      <div class="heading">
        <div class="content">
          <h2 class="animated fadeInUp delayp1"><?php echo $text_dasboard_main; ?></h2>
          <p class="animated fadeInUp delayp1"></p>
        </div>
      </div><!--.heading-->
    </div><!--.container-->

    <div class="container">

      <div class="row-5">
        <div class="col-md-12">

          <div id="reminder-message-wrapper" class="alert alert-success alert-dismissable hidden">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="right: 0">&times;</button>
            <span id="reminder-message"></span>
          </div>
          
        </div>  
        <div class="col-md-10">

          <div class="thumb reminder visible-xs visible-sm">
            <div class="content m_b_10" style="padding-bottom:0">
              <h2 class="h4 m_l_15" style="padding-bottom:10px"><!--<span class="glyphicon glyphicon-calendar"></span>--><?php echo $entry_reminders; ?></h2>
              <a class="add-reminder" data-toggle="modal" href="#addReminder" onClick="viewModalReminder('<?php echo $text_add_reminder ?>');hideDel();"><span class="glyphicon glyphicon-plus"></span></a>
              <div id="accordion">
                <div class="show-reminder" id="id-label-show-reminder"><?php echo $entry_show_reminders; ?></div>
                
                <div class="reminder-list">

                </div><!--.reminder-list-->
                <div class="reminder-list-loading" style="text-align:center"></div>
              </div><!--#accordion-->
            </div><!--.content-->
          </div><!--.thumb-->
          <script type="text/javascript">
            $(document).ready(function()
            {

            function loading_show()
            {
            $('#loading').html("<img src='images/loading.gif'/>").fadeIn('fast');
            }

            function loading_hide()
            {
            $('#loading').fadeOut();
            } 

            function loadData(page)
            {
            loading_show(); 
            $.ajax
            ({
            type: "POST",
            url: "<?php echo $urlPOST; ?>",
            data: "page="+page,
            success: function(msg)
            {
            $("#container").ajaxComplete(function(event, request, settings)
            {
            loading_hide();
            $("#container").html(msg);
            });
            }
            });
            }
            loadData(1); // For first time page load default results
            $('.next').on('click',function(){
            var page = $(this).attr('p');
            loadData(page);
            }); 
            });

            </script>

          <div class="thumb"> 
            <div class="content p_15 m_b_10">
              
              <a href="<?php echo $refresh_url; ?>" class="btn btn-sm btn-primary pull-right" style="margin-bottom: 10px;"><span class="glyphicon glyphicon-refresh"></span>&nbsp;<?php echo $entry_refresh; ?></a>
              
              <h3 class="h4 m_b_30"><?php echo $text_portofolio ?></h3>
              <div class="row">
                <div class="col-sm-6 col-md-5">
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="doughnut-container m_b_20">
                        <div id="canvas-holder">
                          <canvas id="chart-area" width="500" height="500"></canvas>
                        </div>
                      </div>
                    </div><!--.col-->
                    <div class="col-sm-12">
                      <ul class="doughnut-legend text-center">
                        <li><span style="background-color:#5B90BF"></span><?php echo $fund_type['EQ']; ?></li>
                        <li><span style="background-color:#5bbfa2"></span><?php echo $fund_type['FI']; ?></li>
                        <li><span style="background-color:#8fbf5b"></span><?php echo $fund_type['MM']; ?></li>
                      </ul>
                    </div><!--.col-->
                  </div><!--.row-->
                </div>
                <div class="col-sm-6 col-md-7">
                  <div class="analysis">
                    <h4 class="h5 m_b_15"><?php echo $entry_smart_analysis; ?></h4>
                    <div class="content">
                      <p><?php echo $entry_current_risk ?> 
                        <a href="<?php echo $ideal_url; ?>" target="_blank"><?php echo $risk_profile; ?></a>
                        <span class="typo-info">(<?php echo $text_last_update; ?>: <?php echo $risk_profile_last_update; ?>)</span>
                      </p>
                      <p><span class="glyphicon glyphicon-exclamation-sign" style="color: #5B90BF"></span> &nbsp;<?php echo $entry_your; ?> <strong style="color: #5B90BF"><?php echo $fund_type['EQ']; ?></strong> <?php echo $entry_is; ?> <?php echo $smart_analysis['EQ']; ?><?php if($smart_analysis['EQ'] != $entry_ideal){ ?> <?php echo $entry_supposed ?><?php } else { echo "."; } ?></p>
                      <p><span class="glyphicon glyphicon-exclamation-sign" style="color: #5bbfa2"></span> &nbsp;<?php echo $entry_your; ?> <strong style="color: #5bbfa2"><?php echo $fund_type['FI']; ?></strong> <?php echo $entry_is; ?> <?php echo $smart_analysis['FI']; ?><?php if($smart_analysis['FI'] != $entry_ideal){ ?> <?php echo $entry_supposed ?><?php } else { echo "."; } ?></p>
                      <p><span class="glyphicon glyphicon-exclamation-sign" style="color: #8fbf5b"></span> &nbsp;<?php echo $entry_your; ?> <strong style="color: #8fbf5b"><?php echo $fund_type['MM']; ?></strong> <?php echo $entry_is; ?> <?php echo $smart_analysis['MM']; ?><?php if($smart_analysis['MM'] != $entry_ideal){ ?> <?php echo $entry_supposed ?><?php } else { echo "."; } ?></p>
                      <p><?php echo $entry_inform; ?></p>
                    </div>
                  </div>
                </div>
              </div><!--.row-->
            </div><!--.content-->
          </div><!--.thumb-->

          <div class="thumb port-summary hidden-xs">
            <div class="content p_15 m_b_10">
              <h3 class="h4 m_b_30"><?php echo $entry_portfolio_summary; ?></h3>
              <table class="table">
                <thead>
                  <tr>
                    <td><?php echo $entry_column_category; ?></td>
                    <td><?php echo $entry_name; ?></td>
                    <td class="text-center"><?php echo $entry_holding; ?></td>
                    <td class="text-center"><?php echo $entry_unit; ?></td>
                    <td class="text-right"><?php echo $entry_market; ?></td>
                    <td class="text-center"><?php echo $entry_gain_loss; ?></td>
                    <td class="text-center"><?php echo $entry_init_subs; ?></td>
                    <td class="text-center" width="50"><?php echo $entry_composition; ?></td>
                    <td class="text-center"><?php echo $entry_action; ?></td>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($portfolio_balance_summary as $key => $data): ?>
                    <tr>
                      <td class="row-table <?php echo $data['row_class']; ?>"><p><strong><?php echo $data['category'] ? $data['category'] : $data['fund_type']; ?></strong></p></td>
                      <td><a href="<?php echo $data['trans_link']; ?>"><strong><?php echo $data['product_name']; ?></strong></a></td>
                      <td class="text-center"><?php echo $data['unit_holding']; ?></td>
                      <td class="text-right"><?php echo $data['navpu']; ?></td>
                      <td class="text-right"><?php echo $data['market_valuation']; ?></td>
                      <td class="text-right"><?php echo $data['gain_loss']; ?></td>
                      <td class="text-center"><?php echo $data['initial_subscription']; ?></td>
                      <td class="text-center"><?php echo $data['portfolio_percentage']; ?></td>
                      <td><a onclick="location.href='<?php echo $data['link']; ?>'" class="btn btn-sm btn-primary btn-subscribe pull-right" style="border-radius: 3px; margin-right: 0" <?php if(!$data['product_status']) echo 'disabled'; ?>>Subscribe &nbsp;<span class="glyphicon glyphicon-shopping-cart hidden-sm"></span></a></td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
              <p class="m_t_10 typo-light cl-aaa"><em><strong><?php echo $entry_note; ?></strong> <?php echo $text_update_pm; ?></em></p>
              
            </div>
          </div><!--.thumb-->


          <div class="thumb visible-xs">
            <div class="content p_15">
              <h3 class="h4 m_b_30"><?php echo $entry_portfolio_summary; ?></h3>
              <?php foreach ($portfolio_balance_summary as $key => $data): ?>
                <div class="table-port-xs clearfix">
                  <div class="row-table <?php echo $data['row_class']; ?> m_b_5"><p><strong><?php echo $data['category'] ? $data['category'] : $data['fund_type']; ?></strong></p></div>
                    <span class="typo-light"><?php echo $entry_name; ?> &nbsp;</span> <a href="<?php echo $prefix;?>account/trans-history.php"><strong><?php echo $data['product_name']; ?></strong></a><br>
                    <span class="typo-light"><?php echo $entry_holding; ?> &nbsp;</span> <?php echo $data['unit_holding']; ?><br>
                    <span class="typo-light"><?php echo $entry_unit; ?> &nbsp;</span> <?php echo $data['navpu']; ?><br>
                    <span class="typo-light"><?php echo $entry_market; ?> &nbsp;</span> <?php echo $data['market_valuation']; ?><br>
                    <span class="typo-light"><?php echo $entry_gain_loss; ?> &nbsp;</span> <?php echo $data['gain_loss']; ?> <br>
                    <span class="typo-light"><?php echo $entry_init_subs; ?> &nbsp;</span> <?php echo $data['initial_subscription']; ?> <br>
                    <span class="typo-light"><?php echo $entry_composition; ?> &nbsp;</span> <?php echo $data['portfolio_percentage']; ?><br>
                  <a onclick="location.href='<?php echo $data['link']; ?>'" class="btn btn-sm btn-primary btn-subscribe pull-right" <?php if(!$data['product_status']) echo 'disabled'; ?>><?php echo $entry_subscribe; ?> &nbsp;<span class="glyphicon glyphicon-shopping-cart hidden-sm"></span></a>
                </div>
              <?php endforeach; ?>

              <p class="m_t_10 typo-light cl-aaa"><em><strong><?php echo $entry_note; ?></strong> <?php echo $text_update_pm; ?></em></p>
              
            </div>
          </div><!--.thumb-->

          <div class="thumb">
            <div class="content p_15">
              <h3 class="h4 m_b_30"><?php echo $entry_history; ?></h3>
              <div class="canvas-line-container">
                <canvas id="canvas-line" height="300" width="600"></canvas>
              </div>
              <ul class="doughnut-legend-2 text-center">
                <?php foreach ($nav_products as $key => $data): ?>

                  <li>
                    <div class="radio">
                      <label>
                        <input type="radio" name="optionsRadios" class="nav_history_option" value="<?php echo $data['model']; ?>" <?php if(!$key) echo 'checked'; ?>>
                        <?php echo $data['name']; ?>
                      </label>
                    </div>
                  </li>
                <?php endforeach; ?>
              </ul>
            </div>
          </div><!--.thumb-->

        </div><!--.col-md-10-->

        <div class="col-md-2 hidden-xs hidden-sm">
          <div class="thumb reminder">
            <div class="content">
              <h2 class="h4 m_b_20 m_l_15"><!--<span class="glyphicon glyphicon-calendar"></span>--><?php echo $entry_reminders; ?></h2>
              <a class="add-reminder" data-toggle="modal" href="#addReminder" onClick="viewModalReminder('<?php echo $text_add_reminder ?>');hideDel();"><span class="glyphicon glyphicon-plus"></span></a>
              <div class="reminder-list-loading" style="text-align:center"></div>
              <div class="reminder-list">

                
            </div><!--.content-->
          </div><!--.thumb-->
        </div><!--.thumb.reminder-->

      </div><!--.row-->

    </div><!--.container-->

    
      

    <!--MODAL ADD REMINDER-->
    <div class="modal fade" id="addReminder">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 class="h4" id="reminder-title-form"><span></span></h3>
          </div>
          <div class="modal-body">
            <!-- <form class="form-horizontal" role="form"> -->
            <form id="ReminderFrm" class="form-horizontal" action="<?php echo $urlPOST; ?>" method="post" enctype="multipart/form-data">              
              <div class="form-group">
              <div id="field-group" class="form-group">
                <label class="col-lg-2 control-label"><?php echo $text_subject; ?></label>
                <div class="col-lg-10">
                  <?php echo $form[ 'descriptions' ]; ?>
                </div>
              </div>
              <div id="field-group2" class="form-group" style="margin-bottom:0">
                <label class="col-lg-2 control-label"><?php echo $text_date; ?></label>
                <div class="col-lg-10">
                  <input type="text" class="form-control" id="date-reminder" name="reminder_date">
                </div>
              </div>
              <input type="hidden" class="form-control" id="reminder_id" name="reminder_id">
            
            <div class="clearfix m_t_30">
              <button type="submit" class="btn btn-primary pull-right"><?php echo $button_save; ?></button>
              <button id="delete-reminder" class="btn btn-danger pull-right m_r_10" type="button" ><?php echo $entry_delete; ?></button>
            </div>
          </div>  
            </form>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal fade" id="">
        <div>asdashdl</div>
    </div>

    <!--MODAL VIEW / IDEAL PORTOFOLIO-->
    <div class="modal fade" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="heading">
            <div class="container" style="width:100%;">
              <h2 class="animated fadeInUp delayp1"><img src='<?php echo $this->templateResource('/img/loading_2.gif') ?>'></h2>
              <p class="animated fadeInUp delayp1"></p>
            </div>
          </div><!--.heading-->
          <div class="thumb">
            

            </div><!--.content-->
          </div><!--.thumb-->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div>

    <script>
    
    function loadReminder(){
      $('.reminder-list-loading').html("<img src='<?php echo $this->templateResource('/img/loading_2.gif') ?>'>").fadeIn('fast');
      $.ajax({
        url:"index.php?rt=r/account/account/getReminder",
        type:"POST",
        data:{page:1},
        dataType: "json",
        success: function(response){
          $('.reminder-list-loading').hide();
          $('.reminder-list').html(response);
     
        }
    
        });
    }
    $(document).ready(function(){
      loadReminder();
    });
    var idealPortoData = [
      {
        value: 80,
        color:"#5B90BF",
        highlight: "#6fa8da",
        label: "Equity"
      },
      {
        value: 10,
        color: "#5bbfa2",
        highlight: "#6ad9b8",
        label: "Fixed Income"
      },
      {
        value: 10,
        color: "#8fbf5b",
        highlight: "#a3db68",
        label: "Money Market"
      }

    ];

    //Fazrin ~ thousand separator function
    function addCommas(x, c, d, t) {
      var c = 2;
      var n = x, 
          c = isNaN(c = Math.abs(c)) ? 2 : c, 
          d = d == undefined ? "." : d, 
          t = t == undefined ? "," : t, 
          s = n < 0 ? "-" : "", 
          i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
          j = (j = i.length) > 3 ? j % 3 : 0;
         return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    }

    $('#portoTrigger').click(function(){ 
     /* --- DOUGHNUT --- */
     setTimeout(function () {  
       var porto = document.getElementById("chart-area-porto").getContext("2d");
       var ele = document.getElementById("chart-area-porto");
       ele.myDoughnut = new Chart(porto).Doughnut(
          idealPortoData, 
          {responsive : true, tooltipTemplate : "<%if (label){%><%=label%>: <%}%> <%= value %>%"});
      },500); //set timeout
    });

    //porto end

    var randomScalingFactor = function(){ return Math.round(Math.random()*5000)};

    var nav_history_label = <?php echo $nav_history_label; ?>;
    var nav_history_data = <?php echo $nav_history_data; ?>;
    
    // console.log(nav_history_data);
    // nav_history_data.forEach(function(value,idx){
      // nav_history_data[idx] = addCommas(value);
    // });
    // console.log(nav_history_data); return;

    var lineChartData = {
      labels : nav_history_label,
      datasets : [
        {
          label: "MIED",
          fillColor : "rgba(151,187,205,0.2)",
          strokeColor : "rgba(151,187,205,1)",
          pointColor : "rgba(151,187,205,1)",
          pointStrokeColor : "#fff",
          pointHighlightFill : "#fff",
          pointHighlightStroke : "rgba(151,187,205,1)",
          data : nav_history_data
        }
      ]

    }

    $('.nav_history_option').on("change",function(){
      $(".canvas-line-container").removeAttr('style');

      var product_code = $(this).val();
      $.ajax({
        type: 'POST',
        url: 'index.php?rt=r/product/product/get_nav_history',
        data: {product_code: product_code},
        success: function(result){

          if(result){
            $(".canvas-line-container").css("background","none");
            result = JSON.parse(result);

            var new_datasets = [{
                label: result.product_code,
                fillColor : "rgba(151,187,205,0.2)",
                strokeColor : "rgba(151,187,205,1)",
                pointColor : "rgba(151,187,205,1)",
                pointStrokeColor : "#fff",
                pointHighlightFill : "#fff",
                pointHighlightStroke : "rgba(151,187,205,1)",
                data : result.data
            }];

            window.myLine.destroy();

            var lineChartData = {
              labels : result.label,
              datasets : new_datasets
            }

            /* --- LINE CHART --- */
            var ctx = document.getElementById("canvas-line").getContext("2d");
            window.myLine = new Chart(ctx).Line(lineChartData, {
              responsive: true,
              tooltipTemplate : "<%if (label){%><%=label%>  ~ <%}%> <%= addCommas(value) %>",
              scaleLabel: "<%= addCommas(value) %>",
              onAnimationProgress: function(){
                $(".canvas-line-container").css("background","none");
              }
            });
          }
        }
      });
      

      
    });

  var doughnutData = <?php echo $portfolio_balance_chart; ?>;


    window.onload = function(){
		   
	  /* --- DOUGHNUT --- */
	  var ctd = document.getElementById("chart-area").getContext("2d");
	  window.myDoughnut = new Chart(ctd).Doughnut(doughnutData, {
        responsive : true, 
        tooltipTemplate : "<%if (label){%><%=label%>  ~ <%}%> <%= addCommas(value) %>%",
        onAnimationProgress: function(){
          $(".doughnut-container").css("background","none");
        }
    });
	  
	  /* --- LINE CHART --- */
	  var ctx = document.getElementById("canvas-line").getContext("2d");
	  window.myLine = new Chart(ctx).Line(lineChartData, {
	    responsive: true,
      tooltipTemplate : "<%if (label){%><%=label%>  ~ <%}%> <%= addCommas(value) %>",
      scaleLabel: "<%= addCommas(value) %>",
      onAnimationProgress: function(){
        $(".canvas-line-container").css("background","none");
      }
	  });
		 
	};
		
	function navbarActive(navbar, selector, imageNav, image){
       $('.nav.navbar-nav.custom-account li').each(function(index, element) {
          $(this).removeClass('active');
	   });
	   
       $('.nav.navbar-nav.custom-nav li').each(function(index, element) {
          $(this).removeClass('active');
	   });
	   
	   $('#image-nav-home').attr('src', "<?php echo $this->templateResource('/img/ic_nav_home.png'); ?>");
	   $('#image-nav-account').attr('src', "<?php echo $this->templateResource('/img/ic_nav_account-active.png'); ?>"); 
	   $('#image-nav-cart').attr('src', "<?php echo $this->templateResource('/img/ic_nav_cart.png'); ?>");
	   
   
       $('#navbar-acc-'+selector).addClass('active');
	   $('#navbar-'+navbar).addClass('active');
    }
	
	navbarActive('dashboard', 'dashboard');
	
	
	/* --- REMINDER DATEPICKER --- */
	$(function() {
	   $( "#date-reminder" ).datepicker({
	      altField:'#date-reminder',
		  altFormat: "d-M-y",
	   });
	});
	
	
	/* --- NAVBAR MOBILE TITLE --- */
	$('#navbar-title-mobile').text('Dashboard');
	
	$('#id-label-show-reminder').click(function(){
	   var text = $('#id-label-show-reminder').text();
	   
	   if(text == '<?php echo $entry_show_reminders ?>'){
	      $('#id-label-show-reminder').text('<?php echo $entry_hide_reminders; ?>');
	   }else{
	      $('#id-label-show-reminder').text('<?php echo $entry_show_reminders ?>');
	   }
	   $('.reminder-list').css('height','auto');
     $('.reminder-list').css('max-height','356px');
     
	});
	
	
	function aggresiveChart(x){
	   var myWindow = window.open(x, "aggressiveChart", "width=400, height=650");
	}
	
	$('#newWindow').click(function(){
	   aggresiveChart('<?php echo $prefix;?>account/ideal-portfolio.php')
	});
	
	
	function viewModalReminder(title){
	   
	   $('#ReminderFrm_descriptions').val('');
	   $('#date-reminder').val('');
     $('#reminder_id').val('');
     $('#reminder-title-form span').html(title);
	}
  function hideDel(){
     $('#delete-reminder').attr('style','display:none');
     $('.help-block').remove();
     $('#field-group').removeClass('has-error');
     $('#field-group2').removeClass('has-error');
     
  }
  function showDel(id){
     $('#delete-reminder').attr('style','display:block');
     $('.help-block').remove();
     $('#field-group').removeClass('has-error');
     $('#field-group2').removeClass('has-error');
     $('#delete-reminder').attr('id_del',id);
  }
  function editTitle(){
     $('#reminder-title-form span').html('<?php echo $text_detail_reminder ?>');
     
  }
	
	$('.detail-datepicker').each(function() {
	   
	   $(this).datepicker({
		  dateFormat: "d-M-y",
	   });
	   
    });
	
	
	/* --- ACCORDION --- */
	/*
	$(function() {
	   $( "#accordion" ).accordion({
	      collapsible: true,
	      active: false
	   });
	});
	*/
	
	function initHideReminder(){
	   
	   $('.accordion-custom').each(function(){
	      $(this).slideUp('fast');
	      //$(this).addClass('hidden');
	   });
	   
	   
	   if($('#id-label-show-reminder').hasClass('accordion-show')){
	      $('#id-label-show-reminder').removeClass('accordion-show');
	   }
	   
	   $('#id-label-show-reminder').addClass('accordion-hide');
	   
	}
	
	
	function initShowReminder(){
	   
	   $('.accordion-custom').each(function(){
	      $(this).slideUp('fast');
		  $(this).removeClass('hidden');
		  $(this).slideDown('fast');

	   });
	   
	   
	   if($('#id-label-show-reminder').hasClass('accordion-hide')){
	      $('#id-label-show-reminder').removeClass('accordion-hide');
	   }
	   
	   $('#id-label-show-reminder').addClass('accordion-show');
	   
	}
	
	
	$('#id-label-show-reminder').click(function(){
	   
	   if($(this).hasClass('accordion-hide')){
	      initShowReminder();
	   }else{
	      initHideReminder();  
	   }
    });
	$(document).ready(function(){
    $( "#ReminderFrm" ).validate({
      submitHandler: function() {
        $('#pleaseWaitDialog').modal('show');
        $.post('index.php?rt=r/account/account/addReminder', 
          $('form#ReminderFrm').serialize() , 
              function(data,msg){
                if (msg=='success') {
                  $('#pleaseWaitDialog').modal('hide');
                  $('#addReminder').modal('hide');
                  $('#reminder-message-wrapper').removeClass('hidden');
                  $('#reminder-message').html(data);
                  loadReminder();
                }else{
                  $('#addReminder').modal('hide');
                }
                  
              }, "json");
      },
      onkeyup: false,
      onfocusout: false,    
      errorElement: "p",
      errorClass: 'help-block',
      highlight: function(element) {
        $(element).parent().parent().addClass("has-error");
      },
      unhighlight: function(element) {
        $(element).parent().parent().removeClass("has-error");
      },
      rules: {
        descriptions: {
          required: true
        },
        reminder_date: {
          required: true
        }
      },
      messages: {
            descriptions: {
                required: '<?php echo $error_reminder_subject; ?>'
            },
            reminder_date: {
                required: '<?php echo $error_reminder_date; ?>'
            }
      }
    });
  });
	
	initHideReminder();
  function remPaging(page){
    $('.reminder-list-loading').html("<img src='<?php echo $this->templateResource('/img/loading_2.gif') ?>'>").fadeIn('fast');
    $.ajax({    
      type    : "POST",
      dataType: "json",
      url     : "index.php?rt=r/account/account/getReminder",
      data    : {
                  page:page
                },                
      success: function(data) {
          $('.reminder-list-loading').hide();
          $(".reminder-list").html(data);
          
      }
  });}

  function detailReminder(reminder_id){
    $('#pleaseWaitDialog').modal('show');
    $.ajax({    
      type    : "POST",
      dataType: "json",
      url     : "index.php?rt=r/account/account/getDetailReminder",
      data    : {
                  reminder_id:reminder_id
                },                
      success: function(data2) {
          $('#pleaseWaitDialog').modal('hide');
          $('#addReminder').modal('show');
          subject = data2['descriptions'];
          date = data2['reminder_date'];
          id = data2['reminder_id'];

          $('#ReminderFrm_descriptions').val(subject);
          $('#date-reminder').val(date);
          $('#reminder_id').val(id);
          
      }
    });
  }

  $('#delete-reminder').on('click',function(){
      $('#pleaseWaitDialog').modal('show');
      id_del = $(this).attr('id_del');
      $.ajax({    
      type    : "GET",
      dataType: "json",
      url     : "index.php?rt=r/account/account/deleteReminder&id="+id_del,               
      success: function(data) {
        $('#pleaseWaitDialog').modal('hide');
        $('#addReminder').modal('hide');
        $('#reminder-message-wrapper').removeClass('hidden');
        $('#reminder-message').html(data);
        loadReminder();
      }
    })
  });
</script>