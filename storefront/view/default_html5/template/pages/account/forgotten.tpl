<div class="heading">
  <div class="container">
    <h2 class="animated fadeInUp delayp1"><?php echo $heading_title; ?></h2>
    <p class="animated fadeInUp delayp1"></p>
  </div>
</div><!--.heading-->

<div class="container">

  <div class="row">
    <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">

      <div class="thumb">
        <div class="content p_30">
        	<?php if ($success) { ?>
			<div class="alert alert-success alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="right: 0">&times;</button>
				<?php echo $success; ?>
			</div>
			<?php } ?>

			<?php if ($error) { ?>
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="right: 0">&times;</button>
				<?php echo $error; ?>
			</div>
			<?php } ?>

          
          <!-- <form class="form-horizontal" role="form"> -->
          <?php echo  $form[ 'form_open' ]; ?>	
            <p class="m_b_20"><?php echo $help_text; ?></p>
            <div class="form-group">
              <label class="control-label col-sm-3"><?php echo $entry_email; ?></label>
              <div class="col-sm-9">
                
                <?php echo $form['fields']['email']; ?>
              </div>
            </div>
            <?php echo $this->getHookVar('password_forgotten_sections'); ?>
            <!--a href="<?php echo $prefix;?>account/login.php"><button type="button" class="btn btn-primary pull-right">Submit</button></a-->
	            <button href="<?php echo $prefix;?>account/login.php" class="btn btn-primary pull-right" type="submit"><?php echo $text_submit; ?></button>
	            <a href="<?php echo $back; ?>" class="btn btn-default pull-right m_r_10"><?php echo $form['back']->text ?></a>
            
          </form>
        </div><!--.content-->
      </div><!--.thumb-->

    </div><!--.col-->
  </div><!--.row-->

</div><!--.container-->

<script type="text/javascript">
	$(document).ready(function(){
		$('#forgottenFrm').addClass('form-horizontal');
	});
</script>
