<?php if ($success) { ?>

	<div class="heading">
	  <div class="container">
	    <h2 class="animated fadeInUp delayp1"><?php echo $text_subscribe_register; ?></h2>
	    <p class="animated fadeInUp delayp1"></p>
	  </div>
	</div><!--.heading-->
	<div class="container">

	  <div class="row">
	    <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
	    	<div class="alert alert-success alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="right: 0">&times;</button>
				<strong>Success!</strong> <?php echo $success; ?>
			</div>
			
			<div class="form-group">
              <div class="col-sm-12" style="text-align:center">
                <a class="btn btn-primary" href="#"><?php echo $button_continue; ?></a>
              </div>
            </div>
	    </div>
	  </div>
	</div>  

<?php }else{ ?>
	<div class="heading">
	  <div class="container">
	    <h2 class="animated fadeInUp delayp1"><?php echo $text_subscribe_register; ?></h2>
	    <p class="animated fadeInUp delayp1"></p>
	  </div>
	</div><!--.heading-->
<?php if ($error_warning) { ?>
<div class="container">

  <div class="row">
    <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
    	<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="right: 0">&times;</button>
			<?php echo $error_warning; ?>
		</div>
    </div>
  </div>
</div>  

<?php } ?>


<div class="container">

  <div class="row">
    <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">

    	<div class="thumb">
        	<div class="content p_30">
        		<?php echo $form['form_open']; ?>
        			
        			<?php
					if($form){
						foreach ($form as $field_name=>$field) { ?>
						<div class="form-group <?php echo (${'error_'.$field_name} ? 'has-error' : '')?>">
							<label class="col-sm-3 control-label"><?php echo ${'entry_'.$field_name}; ?></label>
							<div class="col-sm-9">
							    <?php echo $field; ?>
								<p class="help-block"><?php echo ${'error_'.$field_name}; ?></p>
							</div>
						</div>		
					<?php }
						}?>
					<?php echo $this->getHookVar('subscriber_hookvar'); ?>	
					<div class="form-group">
		              <div class="col-sm-12">
		                <button type="submit" class="btn btn-primary" style="width: 150px; display:block; margin: auto" title="<?php echo $form['continue']->name ?>"><?php echo $text_register; ?></button>
		              </div>
		            </div>
        		</form>	
        		<p class="text-center"><?php echo $text_account_already ?> </p>
        	</div>	
        </div>	
    </div>
  </div>  	
</div>

<?php } ?>

<script type="text/javascript">
	$(document).ready(function(){
		$('#SubscriberFrm input[type="text"]').addClass('form-control');
	    $('#SubscriberFrm input[type="password"]').addClass('form-control');
	    $('#SubscriberFrm select').addClass('form-control');
	    $('#SubscriberFrm').addClass('form-horizontal');
	    $('.required').remove();
	    $('#continue').addClass('btn-primary');
	});
</script>