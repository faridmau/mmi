<?php include($this->templateResource('/template/blocks/account.tpl')) ?>
<div class="heading hidden-xs">
  <div class="container">
    <h2 class="animated fadeInUp delayp1"><?php echo $heading_change_pass; ?></h2>
    <p class="animated fadeInUp delayp1"></p>
  </div>
</div><!--.heading-->
<div class="container">
  <div class="row">
    <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
    	<?php if ($success) { ?>
		<div class="alert alert-success alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="right: 0">&times;</button>
	        <?php echo $success; ?>
	      </div>
		<?php } ?>

		<?php if ($error_warning) { ?>
		<div class="alert alert-danger alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="right: 0">&times;</button>
	        <?php echo $error_warning; ?>
	      </div>
		<?php } ?>
	
      <div class="thumb">
        <div class="content p_20"><?php echo $text_help; ?><br><br>
          
          	<?php echo $form_open; ?>
          	
			<div class="form-group <?php if ($error_current_password) echo 'has-error'; ?>" >
				<label class="col-sm-4 control-label"><?php echo $entry_current_password; ?></label>
				<div class="col-sm-8">

					<?php echo $current_password; ?>
					<p class="help-block"><?php echo $error_current_password; ?></p>
				</div>
			</div>
			<div class="form-group <?php if ($error_password) echo 'has-error'; ?>" >
				<label class="col-sm-4 control-label"><?php echo $entry_password; ?></label>
				<div class="col-sm-8">

					<?php echo $password; ?>
					<p class="help-block"><?php echo $error_password; ?></p>
				</div>
			</div>
			<div class="form-group <?php if ($error_confirm) echo 'has-error'; ?>" >
				<label class="col-sm-4 control-label"><?php echo $entry_confirm_pass; ?></label>
				<div class="col-sm-8">

					<?php echo $confirm; ?>
					<p class="help-block"><?php echo $error_confirm; ?></p>
				</div>
			</div>
			<?php echo $this->getHookVar('password_edit_sections'); ?>	
            <div class="pull-right m_t_10">
              <input type="submit" class="btn btn-primary" value="<?php echo $button_save ?>"></input>
            </div>
          </form>
        </div><!--.content-->
      </div><!--.thumb-->

    </div><!--.col-->
  </div><!--.row-->

</div><!--.container-->

<script type="text/javascript">
$(document).ready(function(){
	$('#PasswordFrm').addClass('form-horizontal');
	$('#PasswordFrm input').addClass('form-control');
	$('.required').hide();
	
	
$("#PasswordFrm").submit(function(e){
  var publickey = "<?php echo publicKeyToHex(PRIVATE_KEY)?>";
  var rsakey = new RSAKey();
  rsakey.setPublic(publickey, "10001");
  var enc_curr_pass = rsakey.encrypt($('#PasswordFrm_current_password').val());
  var enc_new_pass = rsakey.encrypt($('#PasswordFrm_password').val());
  var enc_confirm = rsakey.encrypt($('#PasswordFrm_confirm').val());

  $('#PasswordFrm_current_password').val(enc_curr_pass);
  $('#PasswordFrm_password').val(enc_new_pass);
  $('#PasswordFrm_confirm').val(enc_confirm);

  console.log('current',enc_curr_pass);
  console.log('new', enc_new_pass);
  console.log('confrim',enc_confirm);
  // e.preventDefault();
  return ;
});

});
</script>