<?php include($this->templateResource('/template/blocks/account.tpl')) ?>
<div class="heading hidden-xs">
  <div class="container">
    <h2 class="animated fadeInUp delayp1">Transaction History</h2>
    <p class="animated fadeInUp delayp1"></p>
  </div>
</div><!--.heading-->

<div class="container">

  <div class="alert alert-info alert-dismissible visible-xs visible-sm" role="alert">
    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <span class="glyphicon glyphicon-phone cl-blue-info"></span> <strong>Mobile &amp; Tablet Table View Tips</strong> <br>Swipe the content of the table to the right to see the full information.
  </div>

  <div class="thumb">
    <div class="content p_15">

      <!--ACTION BAR-->
      <div class="clearfix m_b_30">
        <button class="btn btn-default btn-print pull-left"><span class="glyphicon glyphicon-file"></span>&nbsp;Export to PDF</button>

        <form class="pull-right form-inline form-period hidden-xs" role="form">
          <div class="form-group">
            <label class="m_r_10">Transaction Date</label>
            <input type="text" class="form-control" id="date-from">
          </div>
          <div class="form-group">
            <label class="m_rl_5 typo-light">to</label>
            <input type="text" class="form-control" id="date-to">
          </div>
          <div class="form-group">
            <select class="form-control">
                <option>Mandiri Investa Atraktif</option>
                <option>Mandiri Investasi Dana Utama</option>
                <option>Mandiri Investa Dana Syariah</option>
                <option>Mandiri Investa Pasar Uang</option>
                <option>Mandiri Dana Optima</option>
            </select>
          </div>
          <button type="submit" class="btn btn-primary">Go</button>
          <button type="submit" class="btn btn-default btn-reset">Reset</button>
        </form>
      </div><!--.clearfix-->

      <!--PERIOD INFO ON MOBILE-->
      <div class="m_b_30 visible-xs" style="margin-top: -15px">
        <p class="m_b_10">
          <span class="cl-aaa typo-light">Transaction Date&nbsp;&nbsp;</span> 11-Jun-14 &nbsp;<span class="typo-light cl-aaa">to</span>&nbsp; 16-Jun-14
          <br><a href="" data-toggle="modal" data-target="#searchFilter">Change search criteria</a>
        </p>
      </div>

      <!--CHANGE DATE MODAL-->
      <div class="modal fade" id="searchFilter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h3 class="h4">Change Search Criteria</h3>
            </div>
            <div class="modal-body clearfix">
              <form class="form-inline visible-xs" role="form">
                <div class="form-group">
                  <label class="m_r_10">Transaction Date</label>
                  <input type="text" class="form-control" value="11-Jun-14" id="date-from-mobile">
                </div>
                <div class="form-group">
                  <label class="m_rl_5 typo-light">to</label>
                  <input type="text" class="form-control" value="16-Jun-14" id="date-to-mobile">
                </div>
                <div class="form-group">
                  <select class="form-control">
                    <option>Mandiri Investa Atraktif</option>
                    <option>Mandiri Investasi Dana Utama</option>
                    <option>Mandiri Investa Dana Syariah</option>
                    <option>Mandiri Investa Pasar Uang</option>
                    <option>Mandiri Dana Optima</option>
                  </select>
                </div>
                <button type="submit" class="btn btn-primary btn-full m_b_10">Go</button>
                <button type="submit" class="btn btn-default btn-reset btn-full">Reset</button>
              </form>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->

      <!--BODY NO RESULT FOUND-->
      <p class="no-record hidden">No record found</p>

      <!--BODY WITH RESULT-->
      <div class="table-responsive">
        <table class="table table-striped"> 
          <thead>
            <tr> 
              <td>Date</td>
              <td>Product Name</td>
              <td>Transaction Type</td>
              <td class="text-right">NAV Date</td>
              <td class="text-right">NAVPU</td>
              <td class="text-right">Unit</td>
              <td class="text-right">Amount</td>
              <td class="text-right">Fee</td>
              <td class="text-right">Total Amount</td>
              <td class="text-right">Saldo (Unit)</td>
            </tr>
          </thead>
          <tbody>
            <?php 
            for($i=0;$i<10;$i++){
            ?>
            <tr> 
              <td>01-Mar-14</td>
              <td>RD Mandiri Investa</td>
              <td>Subscription</td>
              <td class="text-right">01-Mar-14</td>
              <td class="text-right">3,811.00</td>
              <td class="text-right">15,000.0000</td>
              <td class="text-right">57,165,000.00</td>
              <td class="text-right">571,650.00</td>
              <td class="text-right">57,736,650.00</td>
              <td class="text-right">15,000,000.0000</td>
            </tr>
            <?php 
            } 
            ?>
          </tbody>
        </table>
      </div>
      <div class="text-center">
        <ul class="pagination pagination-sm" id="trans-history-pagination">
          <li class="disabled"><a href="#">&laquo;</a></li>
          <li class="active"><a href="">1</a></li> 
          <li><a href="">2</a></li> 
          <li><a href="">3</a></li> 
          <li><a href="">..</a></li> 
          <li><a href="">7</a></li> 
          <li><a href="">8</a></li> 
          <li><a href="">9</a></li> 
          <li><a href="#">&raquo;</a></li>
        </ul>
      </div>

    </div><!--.content-->
  </div><!--.thumb-->

</div><!--.container-->

<script>
function navbarActive(navbar, selector){
     $('.nav.navbar-nav.custom-account li').each(function() {
        $(this).removeClass('active');
   });
   
     $('.nav.navbar-nav.custom-nav li').each(function(index, element) {
        $(this).removeClass('active');
   });
   
  $('#image-nav-home').attr('src', "<?php echo $this->templateResource('/img/ic_nav_home.png'); ?>");
  $('#image-nav-account').attr('src', "<?php echo $this->templateResource('/img/ic_nav_account-active.png'); ?>"); 
  $('#image-nav-cart').attr('src', "<?php echo $this->templateResource('/img/ic_nav_cart.png'); ?>");
 
  $('#navbar-acc-'+selector).addClass('active');
  $('#navbar-'+navbar).addClass('active');
 }

navbarActive('dashboard','transaction');


/* --- DATEPICKER --- */
$(function() {
   $( "#date-from" ).datepicker({
      altField:'#date-from-mobile',
	  dateFormat: "d-M-y",
        defaultDate: "+1w",
    	  changeMonth: true,
        numberOfMonths: 1,
	  onClose: function( selectedDate ) {
	     $( "#date-to" ).datepicker( "option", "minDate", selectedDate);
	  }
   });
   
   
   $( "#date-to" ).datepicker({
      altField:'#date-to-mobile',
	  dateFormat: "d-M-y",
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
	  onClose: function( selectedDate ) {
	     $( "#date-from" ).datepicker( "option", "maxDate", selectedDate);
	  }
   });
   
   $( "#date-from-mobile" ).datepicker({
      altField:'#date-from',
	  dateFormat: "d-M-y",
        defaultDate: "+1w",
    	  changeMonth: true,
        numberOfMonths: 1,
	  onClose: function( selectedDate ) {
	     $( "#date-to-mobile" ).datepicker( "option", "minDate", selectedDate);
	  }
   });
   
   
   $( "#date-to-mobile" ).datepicker({
      altField:'#date-to',
	  dateFormat: "d-M-y",
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
	  onClose: function( selectedDate ) {
	     $( "#date-from-mobile" ).datepicker( "option", "maxDate", selectedDate);
	  }
   });
});

/* --- NAVBAR MOBILE TITLE --- */
// $('#navbar-title-mobile').text('Transaction History');


/* --- MOBILE PAGINATION --- */
function mobilePagination(){
   
   var width = $('.navbar-top').width();
   
   if(width <= '768'){
      /* --- LEFT SECTION --- */
      $('#trans-history-pagination li:nth-child(3)').addClass('hidden');
      $('#trans-history-pagination li:nth-child(4) a').text('2');
	  
	  
      /* --- RIGHT SECTION --- */ 
      $('#trans-history-pagination li:nth-child(6)').addClass('hidden');
      $('#trans-history-pagination li:nth-child(7) a').text('4');
      $('#trans-history-pagination li:nth-child(8) a').text('5');
   }
   
}

mobilePagination();
</script>