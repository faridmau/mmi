<?php include($this->templateResource('/template/blocks/account.tpl')) ?>
<div class="heading hidden-xs">
  <div class="container">
    <h2 class="animated fadeInUp delayp1"><?php echo $text_history; ?></h2>
    <p class="animated fadeInUp delayp1"></p>
  </div>
</div><!--.heading-->

<div class="container">

  <div class="thumb">
    <div class="content p_15">

      <div class="orderhistory">
        <form>
          
          <table class="table table-striped" id="data-table">
            <thead>
              <tr class="hidden-xs">
                <th class="th-no hidden-xs" width="5%">#</th>
                <th class="th-date hidden-xs" width="20%"><?php echo $text_date; ?></th>
                <th class="th-orderno" width="20%"><?php echo $text_order; ?></th>
                <th class="th-product" width="20%"><?php echo $text_products; ?></th>
                <th class="th-amount hidden-xs" width="20%"><?php echo $text_amount; ?></th>
                <th class="th-status hidden-xs" width="20%"><?php echo $text_status; ?></th>
                <th class="th-action hidden-xs" width="15%"><?php echo $text_action; ?></th>
              </tr>
            </thead>
            <tbody>
              <?php  if($order_total){ ?>
                <?php foreach ($orders as $key => $order) { ?>
                  <tr>
                    <td class="td-no hidden-xs"><?php echo $order['no']; ?></td>
                    <td class="td-date hidden-xs"><?php echo $order['date_added']; ?></td>
                    <td class="td-orderno hidden-xs">#<?php echo $order['order_id']; ?></td>
                    <td class="td-products hidden-xs"><?php echo $order['products']; ?></td>
                    <td class="td-amount hidden-xs"><?php echo $order['total']; ?></td>
                    <td class="td-status green hidden-xs"><strong><?php echo $order['status']; ?></strong></td>
                    <td class="td-action hidden-xs"><a href="<?php echo $order['detail_link']; ?>" class="btn btn-primary">View Order</a></td>
                    <td class="m-orderhistory visible-xs">
                      <p><span class="m-caption"><?php echo $text_order; ?>&nbsp;&nbsp;</span><?php echo $order['order_id']; ?></p>
                      <p><span class="m-caption"><?php echo $text_date; ?>&nbsp;&nbsp;</span> <?php echo $order['date_added']; ?></p>
                      <p><span class="m-caption"><?php echo $text_amount; ?>&nbsp;&nbsp;</span> <?php echo $order['total']; ?></p>
                      <p class="td-status green"><span class="m-caption"><?php echo $text_status; ?>&nbsp;&nbsp;</span> <strong><?php echo $order['status']; ?></strong></p>
                      <a href="<?php echo $order['detail_link']; ?>" class="btn btn-primary m_t_10"><?php echo $text_view_order; ?></a>
                    </td>
                  </tr>
                <?php } ?>
              <?php } else { ?>
                <tr>
                  <td colspan="7"><?php echo $text_not_found; ?></td>
                </tr>
              <?php  } ?>
            </tbody>
          </table>
        </form>
        <div id="data-pagination">
          <?php echo $pagination_bootstrap; ?>
        </div>
      </div>

    </div><!--.content-->
  </div><!--.thumb-->
</div><!--.container-->

<script type="text/javascript">
/* -- Pagination -- */
$(function() {
  $('body').on('click','#data-pagination li a',function(e){
    e.preventDefault();

    var filter_data = $(this).attr('href');
    if(!filter_data){
      return;
    }

    $('#data-table tbody').html('<tr><td class="text-center" colspan="10"><img src="/storefront/view/default_html5/img/loading_2.gif"></td></tr>');

    $.ajax({
      type: 'POST',
      url: 'index.php?rt=r/account/history' ,
      data: filter_data,
      dataType: 'json',
      success: function(response){
        if(response.html){
          $('#data-table tbody').html(response.html);
          $('#data-pagination').html(response.pagination);
        } else {
          $('#data-table tbody').html('<tr><td colspan="10" class="text-center"><?php echo $text_not_found; ?></td></tr>');
          $('#data-pagination').empty();
        }
      },
      error: function(){

      }
    });
  });

});
</script>