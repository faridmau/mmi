<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15">
  <link href="storefront/view/default_html5/stylesheet/main.css" rel="stylesheet">
  
  <style type="text/css">
    body { background: none; }
    .heading1 { font-weight: normal; text-align: center; font-size: 25px; font-family:'Open Sans Light', 'Lucida Grande', 'Helvetica Neue', Helvetica, Arial, sans-serif;}
    .heading3 { font-size: 13px; margin-top: 20px; margin-bottom: 10px; font-family:'Open Sans Light', 'Lucida Grande', 'Helvetica Neue', Helvetica, Arial, sans-serif; }
    .watermark { position: absolute; right: 0; }
    .watermark img { height: 100px; }
    .top { text-align: center; padding-top: 100px; margin-bottom: 20px; }
    .heading1 { text-align: center; font-family:'Open Sans Light', 'Lucida Grande', 'Helvetica Neue', Helvetica, Arial, sans-serif; }
    .info { font-family:'Open Sans Regular', 'Lucida Grande', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 9px; margin-bottom: 10px; width: 100%; }
    .info .left { float: left; }
    .info .right { float: right; }
    .tftable {font-family:'Open Sans Regular', 'Lucida Grande', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size:10px;color:#333333;width:100%;border-width: 1px;border-color: #EEEEEE;border-collapse: collapse;}
    .tftable thead {font-size:10px;background:#fff;padding-top: 0;text-align:center;color: #333;}
    .tftable thead td {padding:5px; border-top: 1px solid #eee; border-bottom: 1px solid #eee; font-family: 'Open Sans Semibold', 'Lucida Grande', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-weight: normal;  }
    .tftable tbody tr:nth-child(even) {background:#ffffff;}
    .tftable tbody tr:nth-child(odd) {background:#F9F9F9;}
    .tftable tbody td {font-size:10px; padding:5px 5px; border-top: 1px solid #eee; border-bottom: 1px solid #eee; text-align: center;}
    .tftable tbody .tfoot-total {
      color: #333;
      font-size: 13px;
      padding: 8px 8px;
    }

    .order_info {
      margin-bottom: 20px;
    }
    .order_info table tr td {
      text-align: left;
    }

    .tftable tbody .order_product td {
      padding: 20px 15px;
    }

    @page { margin:30px 50px 40px; }
    .signature {
      font-size: 10px;
      color: #333;
      position: fixed;
      bottom: -30px;
      left: 0;
      z-index: 999;
      height: 20px;
      width: 100%;
    }

    .signature .left {
      display: inline-block;
      width: 49%;
    }

    .signature .right {
      display: inline-block;
      width: 50%;
      text-align: right;
    }



    .disclaimer {
      font-size: 10px;
      margin-top: 20px;
    }

    .disclaimer strong {
      font-weight: bold;
    }

    .typo-light {
      font-family: 'Open Sans Light', 'Lucida Grande', 'Helvetica Neue', Helvetica, Arial, sans-serif;
    }

    .cl-green {
      color: #188300;
    }

    .cl-red {
      color: #EC6857;
    }

    .cl-aaa {
      color: #aaa;
    }
  </style>
</head>
<body>
<div class="signature">
  <div class="left">
    &copy; <?php echo date("Y"); ?> <?php echo $text_reserved; ?>
  </div>
  <div class="right">
    <?php echo $text_print_date; ?>: <?php echo $date_print; ?>
  </div>
</div>

<div class="watermark">
  <img src="storefront/view/default_html5/image/mandiri-investasi.jpg">
</div>

<div class="top">
  <h1 class="heading1"><?php echo $heading_title_order; ?></h1>  
</div>

<div class="order_info">
  <table class="tftable">
    <tbody>
      <tr>
        <td><span class="cl-aaa typo-light"><?php echo $text_status; ?>&nbsp;</span> <strong class="cl-green"><?php echo $order['status']; ?></strong></td>
        <td><span class="cl-aaa typo-light"><?php echo $text_telephone; ?>&nbsp;</span> <?php echo $order['telephone']; ?></td>
      </tr>
      <tr>
        <td><span class="cl-aaa typo-light"><?php echo $text_email; ?>&nbsp;</span> <?php echo $order['email']; ?></td>
        <td><span class="cl-aaa typo-light"><?php echo $text_payment; ?>&nbsp;</span> <?php echo $order['payment_method']; ?></td>
      </tr>
      <tr>
        <td><span class="cl-aaa typo-light"><?php echo $text_date_added_only; ?>&nbsp;</span> <?php echo $order['date_added']; ?></td>
        <td><span class="cl-aaa typo-light"><?php echo $text_order_comment; ?>&nbsp;</span> <?php echo $order['comment']; ?></td>
      </tr>
    </tbody>
  </table>
</div>

<div>
  <table class="tftable">
    <thead>
      <tr>
        <td class="" width="25%"><?php echo $text_name; ?></td>
        <td class="text-right" width="20%"><?php echo $text_subscription; ?></td>
        <td class="text-right" width="20%"><?php echo $text_fee_ammount; ?></td>
        <td class="text-right" width="20%"><?php echo $text_total_only; ?></td>
      </tr>  
    </thead>
    <tbody>
       <?php foreach ($products as $key => $product): ?>
         <tr class="order_product">
          <td class="hidden-xs"><?php echo $product['name']; ?></td>
          <td class="hidden-xs text-right"><?php echo $product['quantity']; ?></td>
          <td class="hidden-xs text-right"><?php echo $product['fee_amount']; ?></td>
          <td class="hidden-xs text-right"><strong><?php echo $product['total']; ?></strong></td>
        </tr>
      <?php endforeach; ?>
      <!--TFOOT FOR DESKTOP-->
      <tr class="hidden-xs" >
        <td colspan="2" style="text-align: right"><?php echo $text_total_subs; ?></td>
        <td colspan="2" style="text-align: right"><strong><?php echo $order['total_subscription']; ?></strong></td>
      </tr>
      <tr class="hidden-xs" >
        <td colspan="2" style="text-align: right"><?php echo $text_total_fee; ?></td>
        <td colspan="2" style="text-align: right"><strong><?php echo $order['total_fee_amount']; ?></strong></td>
      </tr>
      <tr class="hidden-xs" >
        <td colspan="2" style="text-align: right"><?php echo $text_total_only; ?></td>
        <td colspan="2" class="font14 tfoot-total" style="text-align: right"><strong><?php echo $order['total']; ?></strong></td>
      </tr>
    </tbody> 
  </table>
</div>
</body>
</html>