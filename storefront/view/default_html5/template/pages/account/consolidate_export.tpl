<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15">
  <link href="storefront/view/default_html5/stylesheet/main.css" rel="stylesheet">
  
  <style type="text/css">
    body { background: none; }
    .heading1 { font-weight: normal; text-align: center; font-size: 25px; font-family:'Open Sans Light', 'Lucida Grande', 'Helvetica Neue', Helvetica, Arial, sans-serif;}
    .heading3 { font-weight: normal; font-size: 13px; margin-top: 10px; margin-bottom: 10px; font-family:'Open Sans Light', 'Lucida Grande', 'Helvetica Neue', Helvetica, Arial, sans-serif; }
    .watermark { position: absolute; right: 0; }
    .watermark img { height: 100px; }
    .top { text-align: center; padding-top: 100px; }
    .info { font-family:'Open Sans Regular', 'Lucida Grande', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 9px; margin-bottom: 0px; width: 100%; }
    .info .left { float: left; }
    .info .right { float: right; }
    .tftable {font-family:'Open Sans Regular', 'Lucida Grande', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size:10px;color:#333333;width:100%;border-width: 1px;border-color: #EEEEEE;border-collapse: collapse;}
    .tftable thead {font-size:10px;background:#fff;padding-top: 0;text-align:center;color: #333;}
    .tftable thead td {padding:5px; border-top: 1px solid #eee; border-bottom: 1px solid #eee; font-family: 'Open Sans Semibold', 'Lucida Grande', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-weight: normal;  }
    .tftable tbody tr:nth-child(even) {background:#ffffff;}
    .tftable tbody tr:nth-child(odd) {background:#F9F9F9;}
    .tftable tbody td {font-size:10px; padding:5px; border-top: 1px solid #eee; border-bottom: 1px solid #eee; text-align: center;}

    .text-left {
      text-align: left !important;
    }
    .text-right {
      text-align: right !important;
    }

    @page { margin:30px 50px 30px; }
    .signature {
      font-size: 10px;
      color: #333;
      position: fixed;
      bottom: -20px;
      left: 0;
      z-index: 999;
      height: 20px;
      width: 100%;
    }

    .signature .left {
      display: inline-block;
      width: 49%;
    }

    .signature .right {
      display: inline-block;
      width: 50%;
      text-align: right;
    }

    .disclaimer {
      font-size: 10px;
      margin-top: 20px;
    }

    .disclaimer strong {
      font-weight: bold;
    }

    .cl-green {
      color: #188300;
    }

    .cl-red {
      color: #EC6857;
    }
  </style>
</head>
<body>
<div class="signature">
  <div class="left">
    &copy; <?php echo date("Y"); ?> <?php echo $text_reserved; ?>
  </div>
  <div class="right">
    <?php echo $text_print_date; ?>: <?php echo $date_print; ?>
  </div>
</div>

<div class="watermark">
  <img src="storefront/view/default_html5/image/mandiri-investasi.jpg">
</div>

<div class="top">
  <h1 class="heading1"><?php echo $text_consolidate; ?></h1>
  <p> <?php echo $text_transaction_date; ?>: <?php echo $date_start; ?> <?php echo $text_to; ?> <?php echo $date_end; ?></p>
</div>

<div class="info">
  <?php echo $cust_name; ?><br>
  <?php echo $cust_address; ?><br>
  <?php echo $cust_city; ?> <?php echo $cust_postal_code; ?><br>
  <?php echo $cust_country; ?>
</div>

<h3 class="heading3"><?php echo $text_product_summary; ?></h3>
<div>
  <table class="tftable"> 
    <thead>
      <tr> 
        <td rowspan="2" class="text-center"><?php echo $text_name; ?></td>
        <td rowspan="2" class="text-center"><?php echo $text_acc_no; ?></td>
        <td colspan="3" class="text-center"><?php echo $text_column_balance; ?></td>
        <td rowspan="2" class="text-center"><?php echo $text_nav; ?></td>
        <td rowspan="2" class="text-center"><?php echo $text_ending; ?></td>
        <td rowspan="2" class="text-center"><?php echo $text_realized; ?></td>
        <td rowspan="2" class="text-center"><?php echo $text_unrealized; ?></td>
      </tr>
      <tr> 
        <td class="text-center table-cons-row-2"><?php echo $text_begining; ?></td>
        <td class="text-center table-cons-row-2"><?php echo $text_movement; ?></td>
        <td class="text-center table-cons-row-2"><?php echo $text_column_ending; ?></td>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($investment_summary as $key => $data): ?>
        <tr class="separator2"> 
          <td class="text-left"><?php echo $data['product_name']; ?></td>
          <td class="text-left"><?php echo $data['inv_account_no']; ?></td>
          <td class="text-right"><?php echo $data['beginning_balance']; ?></td>
          <td class="text-right"><?php echo $data['movement'] < 0 ? '<span class="cl-red">('.number_format(abs($data['movement']),4,".",",").')</span>' :  '<span class="cl-green">'.number_format(abs($data['movement']),4,".",",").'</span>' ; ?></td>
          <td class="text-right"><?php echo $data['ending_balance']; ?></td>
          <td class="text-right"><?php echo $data['nav_value']; ?></td>
          <td class="text-right"><?php echo $data['ending_balance_amount']; ?></td>
          <td class="text-right"><?php echo $data['total_realized_gl'] < 0 ? '<span class="cl-red">('.number_format(abs($data['total_realized_gl']),2,".",",").')</span>' :  '<span class="cl-green">'.number_format($data['total_realized_gl'],2,".",",").'</span>' ; ?></td>
          <td class="text-right">
            <?php echo $data['unrealized_gl'] < 0 ? '<span class="cl-red">('.number_format(abs($data['unrealized_gl']),2,".",",").')</span>' :  '<span class="cl-green">'.number_format($data['unrealized_gl'],2,".",",").'</span>' ; ?></td>
        </tr>
      <?php endforeach; ?>
      <tr class="separator2"> 
        <td colspan="6" class="text-left">Total</td>
        <td class="text-right"><?php echo $total_ending_balance; ?></td>
        <td class="text-right"><?php echo $total_realized_gl < 0 ? '<span class="cl-red">('.number_format(abs($total_realized_gl),2,".",",").')</span>' :  '<span class="cl-green">'.number_format($total_realized_gl,2,".",",").'</span>' ; ?></td>
        <td class="text-right"><?php echo $total_unrealized_gl < 0 ? '<span class="cl-red">('.number_format(abs($total_unrealized_gl),2,".",",").')</span>' :  '<span class="cl-green">'.number_format($total_unrealized_gl,2,".",",").'</span>' ; ?></td>
      </tr>
    </tbody>
  </table>
</div>

<h3 class="heading3"><?php echo $text_transaction_detail; ?></h3>
<div>
  <table class="tftable"> 
    <thead>
      <tr> 
        <td class="text-center"><?php echo $text_trn_date; ?></td>
        <td class="text-center"><?php echo $text_trn_type; ?></td>
        <td class="text-center"><?php echo $text_transaction_amount; ?></td>
        <td class="text-center"><?php echo $text_fee_amount; ?></td>
        <td class="text-center"><?php echo $text_net_amount; ?></td>
        <td class="text-center"><?php echo $text_transaction_unit; ?></td>
        <td class="text-center"><?php echo $text_nav; ?></td>
        <td class="text-center"><?php echo $text_average_cost; ?></td>
        <td class="text-center"><?php echo $text_realized; ?></td>
      </tr>
    </thead>
    <tbody>

      
      <?php $i=0; $group = array(); ?>
      <?php foreach ($transaction_detail as $key => $data): ?>
        <?php if($group[($i-1)] != $data['group']){ ?>
          <?php if($i && $data['group'] != $group[($i-1)]){ ?>
            <tr class="separator"> 
              <td colspan="3" class="text-left"></td>
              <td colspan="2" class="text-left"><strong>Ending Balance</strong></td>
              <td class="text-right"><strong><?php echo $transaction_detail[($key-1)]['group_ending_balance']; ?></strong></td>
              <td class="text-right"></td>
              <td class="text-right"></td>
              <td class="text-right"><?php echo $transaction_detail[($key-1)]['realized_gl'] < 0 ? '<span class="cl-red">('.number_format(abs($investment_summary[$i]['realized_gl']),2,".",",").')</span>' :  '<span class="cl-green">'.number_format($investment_summary[$i]['realized_gl'],2,".",",").'</span>' ; ?></td>
            </tr>   
          <?php } ?>
          

          <tr>
            <td colspan="9" class="text-left">
              <strong><?php echo $data['product_name']; ?> - <?php echo $data['inv_account_no']; ?></strong>
            </td>
          </tr>
          <tr> 
            <td colspan="3" class="text-left"></td>
            <td colspan="2" class="text-left"><strong><?php echo $text_beginning_balance; ?></strong></td>
            <td class="text-right"><strong><?php echo $data['group_begining_balance']; ?></strong></td>
            <td class="text-right"></td>
            <td class="text-right"></td>
            <td class="text-right"></td>
          </tr>

          <?php $group[$i] = $data['group']; ?>
          <?php $i++; ?>
        <?php } ?>
        <tr>
          <td class="text-left"><?php echo $data['trx_date']; ?></td>
          <td class="text-center"><?php echo $data['trx_type']; ?></td>
          <td class="text-right"><?php echo $data['trx_amount'] < 0 ? '<span class="cl-red">('.number_format(abs($data['trx_amount']),2,".",",").')</span>' :  '<span class="cl-green">'.number_format($data['trx_amount'],2,".",",").'</span>' ; ?></td>
          <td class="text-right"><?php echo $data['fee_amount']; ?></td>
          <td class="text-right"><?php echo $data['net_trx_amount'] < 0 ? '<span class="cl-red">('.number_format(abs($data['net_trx_amount']),2,".",",").')</span>' :  '<span class="cl-green">'.number_format($data['net_trx_amount'],2,".",",").'</span>' ; ?></td>
          <td class="text-right"><?php echo $data['trx_units'] < 0 ? '<span class="cl-red">('.number_format(abs($data['trx_units']),4,".",",").')</span>' :  '<span class="cl-green">'.number_format($data['trx_units'],4,".",",").'</span>' ; ?></td>
          <td class="text-right"><?php echo $data['nav_per_unit']; ?></td>
          <td class="text-right"><?php echo $data['average_cost']; ?></td>
          <td class="text-right"><?php echo $data['realized_gl'] < 0 ? '<span class="cl-red">('.number_format(abs($data['realized_gl']),2,".",",").')</span>' :  '<span class="cl-green">'.number_format($data['realized_gl'],2,".",",").'</span>' ; ?></td>
        </tr>

        <?php if(!$transaction_detail[($key + 1)] && $is_last_page ){ ?>
          <tr class="separator"> 
            <td colspan="3" class="text-left"></td>
            <td colspan="2" class="text-left"><strong><?php echo  $text_ending_balance; ?></strong></td>
            <td class="text-right"><strong><?php echo $data['group_ending_balance']; ?></strong></td>
            <td class="text-right"></td>
            <td class="text-right"></td>
            <td class="text-right"><?php echo $data['group_realized_gl'] < 0 ? '<span class="cl-red">('.number_format(abs( $data['group_realized_gl']),2,".",",").')</span>' :  '<span class="cl-green">'.number_format( $data['group_realized_gl'],2,".",",").'</span>' ; ?></td>
          </tr>
        <?php } ?>
      <?php endforeach; ?>
    </tbody>
  </table>
</div>

<div class="disclaimer">
  <strong><?php echo $text_disclaimer; ?>:</strong><br>
  <?php echo $entry_disclaimer; ?>
</div>
</body>
</html>
