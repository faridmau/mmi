<div class="heading">
  <div class="container">
    <h2 class="animated fadeInUp delayp1"><?php echo $heading_title; ?></h2>
    <p class="animated fadeInUp delayp1"></p>
  </div>
</div><!--.heading-->

<!-- main login begin -->
<div class="container">
  <div class="row">
    <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">

      <div class="thumb">
        <div class="content p_30">
        	<?php if ($success) { ?>
			<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="right: 0">×</button>
			<?php echo $success; ?>
			</div>
			<?php } ?>

			<?php if ($error) { ?>
			<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="right: 0">×</button>
			<?php echo $error; ?>
			</div>
			<?php } ?>
          <!-- Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. -->
         
          <?php echo $form2[ 'form_open' ]; ?>

            <div class="form-group">
              <label class="col-sm-3 control-label">
              	<?php 
				  	if ($noemaillogin) {
				  		echo $entry_loginname; 
				  	} else {
				  		echo $entry_email_address;
				  	}
				  ?></label>
              <div class="col-sm-9">
                <?php echo $form2[ 'loginname' ]?>
              </div>
            </div>
            <div class="form-group m_b_30">
              <label class="col-sm-3 control-label"><?php echo $entry_password; ?></label>
              <div class="col-sm-9">
                <?php echo $form2[ 'password' ]?>
                <a href="<?php echo $forgotten_pass; ?>" class="pull-right m_t_10"><?php echo $text_forgotten_password; ?></a>
                <?php if($noemaillogin) { ?>
				&nbsp;&nbsp;<a href="<?php echo $forgotten_login; ?>"><?php echo $text_forgotten_login; ?></a>
				<?php } ?>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">
                <button type="submit" class="btn btn-primary" style="width: 100px; display:block; margin: auto" title="<?php echo $form2['login_submit']->name ?>"><?php echo $form2['login_submit']->name ?></button>
              </div>
            </div>
          </form>

          <p class="text-center"><?php echo $text_have_access; ?> <a class="m_t_10" href="<?php echo $create_account;?>"><?php echo $entry_register_now; ?></a></p>

        </div><!--.content-->
      </div><!--.thumb-->

    </div><!--.col-->
  </div><!--.row-->

</div><!--.container-->


<script type="text/javascript">
	$(document).ready(function(){
		$(document).ready(function(){
			$('#loginFrm').addClass('form-horizontal');
		});
		$("#loginFrm").submit(function(){
			var publickey = "<?php echo publicKeyToHex(PRIVATE_KEY)?>";
			var rsakey = new RSAKey();
			rsakey.setPublic(publickey, "10001");
			var enc = rsakey.encrypt($('#loginFrm_password').val());
			$('#loginFrm_password').val(enc);
		});
		
	});
</script>