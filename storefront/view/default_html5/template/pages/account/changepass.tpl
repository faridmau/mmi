<style type="text/css">
span.error{color: #d23f34;}
</style>
<div class="heading">
  <div class="container">
    <h2 class="animated fadeInUp delayp1"><?php echo $change_pass_title ?></h2>
    <p class="animated fadeInUp delayp1"></p>
  </div>
</div><!--.heading-->

<div class="container">

  <div class="row">
    <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
    	<?php if ($success) { ?>
		<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<?php echo $success; ?>
		</div>
		<?php } ?>

		<?php if ($error_warning) { ?>
		<div class="alert alert-error">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<?php echo $error_warning; ?>
		</div>
		<?php } ?>
      <div class="thumb">
        <div class="content p_30">
          <br><br>
          <!-- <form class="form-horizontal" role="form" action="<?php echo $prefix;?>"> -->
          <?php if ($valid): ?>
          	
          	<?php echo $form['form_open']; ?>	
	          	<?php
								$field_list = array();
								array_push($field_list, 'old_password','password','confirm','key');
								foreach ($field_list as $field_name) {
								?>
									<div class="form-group <?php echo ${'error_'.$field_name} ? 'has-error' : ''; ?>">
										<label class="col-sm-4 control-label"><?php echo ${'entry_'.$field_name}; ?></label>
										<div class="col-sm-8">
										    <?php echo $form[$field_name]; ?>
											<span class="error"><?php echo ${'error_'.$field_name}; ?></span>
										</div>
									</div>		
								<?php
									}
								?>

	            <div class="pull-right m_t_10">
	              <input type="submit" class="btn btn-primary" value="<?php echo $text_save; ?>"></input>
	            </div>
	          </form>
	      <?php else: ?>	
	      <div class="alert alert-danger alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="right: 0">&times;</button>
	        <?php echo $text_invalid_key; ?>
	      </div>
          <?php endif ?>
          

        </div><!--.content-->
      </div><!--.thumb-->

    </div><!--.col-->
  </div><!--.row-->

</div><!--.container-->

<script type="text/javascript">
	//var lengths =  ;
	var er_pass = '<?php echo $password_length ?>';
	var not_match = '<?php echo $password_match ?>';
  $(document).ready(function(){
  		$('.required').hide();
  		$('#AccountFrm').addClass('form-horizontal');
  		
  		function rsa(){
  			var publickey = "<?php echo publicKeyToHex(PRIVATE_KEY)?>";
			var rsakey = new RSAKey();
			rsakey.setPublic(publickey, "10001");
			var enc_curr_pass = rsakey.encrypt($('#AccountFrm_old_password').val());
			var enc_pass = rsakey.encrypt($('#AccountFrm_password').val());
			var enc_confirm = rsakey.encrypt($('#AccountFrm_confirm').val());

			$('#AccountFrm_old_password').val(enc_curr_pass);
			$('#AccountFrm_password').val(enc_pass);
			$('#AccountFrm_confirm').val(enc_confirm);
  		}

  		$( "#AccountFrm" ).validate({
  			submitHandler: function(form) {
			    rsa();
			    form.submit();
			  },
		  //by default the error elements is a <label>
          errorElement: "span",
            highlight: function(element) {
	        $(element).parent().parent().addClass("has-error");
			    },
			    unhighlight: function(element) {
			        $(element).parent().parent().removeClass("has-error");
			    }
		});
  });

  
  </script>

<script type="text/javascript">

if (true) {};
console.log($('#AccountFrm_key').val());
</script>