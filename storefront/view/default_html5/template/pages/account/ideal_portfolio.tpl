<div class="heading">
  <div class="container">
    <h2 class="animated fadeInUp delayp1"><?php echo $text_head_portfolio; ?></h2>
    <p class="animated fadeInUp delayp1"></p>
  </div>
</div><!--.heading-->

<div class="container">

  <div class="row">
    <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">

      <!--<ol class="breadcrumb">
        <li><a href="<?php echo $prefix;?>account/dashboard.php">Dashboard</a></li>
        <li class="active">Ideal Portfolio Balance Chart</li>
      </ol>-->

      <div class="thumb">
        <div class="content p_30">
          <p class="m_b_30"><?php echo $entry_based_aggressive_first; ?><strong><?php echo $risk_profile; ?></strong><?php echo $entry_based_aggressive_last; ?></p>
            <div class="doughnut-container m_b_20">
              <div id="canvas-holder">
                <canvas id="chart-area" width="500" height="500"/></canvas>
              </div>
            </div>
            <ul class="doughnut-legend text-center">
              <li><span style="background-color:#5B90BF"></span><?php echo $fund_type['EQ']; ?></li>
              <li><span style="background-color:#5bbfa2"></span><?php echo $fund_type['FI']; ?></li>
              <li><span style="background-color:#8fbf5b"></span><?php echo $fund_type['MM']; ?></li>
            </ul>

        </div><!--.content-->
      </div><!--.thumb-->

    </div><!--.col-->
  </div><!--.row-->

</div><!--.container-->

<script>
function navbarActive(navbar, selector){
 $('.nav.navbar-nav.custom-account li').each(function() {
    $(this).removeClass('active');
 });
 
 $('.nav.navbar-nav.custom-nav li').each(function(index, element) {
    $(this).removeClass('active');
 });
 
 $('#image-nav-home').attr('src', '../assets/img/ic_nav_home.png');
 $('#image-nav-account').attr('src', '../assets/img/ic_nav_account-active.png');
 $('#image-nav-cart').attr('src', '../assets/img/ic_nav_cart.png');

 $('#navbar-acc-'+selector).addClass('active');
 $('#navbar-'+navbar).addClass('active');
}

navbarActive('dashboard','account');

    
var idealPortotData = <?php echo $ideal_portfolio; ?>;

//Fazrin ~ thousand separator function
function addCommas(x, c, d, t) {
  var c = 2;
  var n = x, 
      c = isNaN(c = Math.abs(c)) ? 2 : c, 
      d = d == undefined ? "." : d, 
      t = t == undefined ? "," : t, 
      s = n < 0 ? "-" : "", 
      i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
      j = (j = i.length) > 3 ? j % 3 : 0;
     return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

window.onload = function(){ 
 /* --- DOUGHNUT --- */
 var porto = document.getElementById("chart-area").getContext("2d");
 window.myDoughnut = new Chart(porto).Doughnut(idealPortotData, {responsive : true, tooltipTemplate : "<%if (label){%><%=label%>: <%}%> <%= addCommas(value) %>%", onAnimationProgress: function(){
          $(".doughnut-container").css("background","none");
   }});
};
</script>
