<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15">
  <link href="storefront/view/default_html5/stylesheet/main.css" rel="stylesheet">
  
  <style type="text/css">
    body { background: none; }
    .watermark { position: absolute; right: 0; }
    .watermark img { height: 100px; }
    .top { text-align: center; padding-top: 100px; }
    .heading1 { font-weight: normal; text-align: center; font-family:'Open Sans Light', 'Lucida Grande', 'Helvetica Neue', Helvetica, Arial, sans-serif; }
    .info { font-family:'Open Sans Regular', 'Lucida Grande', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 9px; margin-bottom: 10px; width: 100%; }
    .info .left { float: left; }
    .info .right { float: right; }
    .tftable {font-family:'Open Sans Regular', 'Lucida Grande', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size:10px;color:#333333;width:100%;border-width: 1px;border-color: #EEEEEE;border-collapse: collapse;}
    .tftable thead {font-size:10px;background:#fff;padding-top: 0;text-align:center;color: #333;}
    .tftable thead td {padding:5px; border-top: 1px solid #eee; border-bottom: 1px solid #eee; font-family: 'Open Sans Semibold', 'Lucida Grande', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-weight: normal;  }
    .tftable tbody tr:nth-child(even) {background:#ffffff;}
    .tftable tbody tr:nth-child(odd) {background:#F9F9F9;}
    .tftable tbody td {font-size:10px; padding:5px 3px; border-top: 1px solid #eee; border-bottom: 1px solid #eee; text-align: center;}

    @page { margin:30px 50px 50px; }
    .signature {
      font-size: 10px;
      color: #333;
      position: fixed;
      bottom: -40px;
      left: 0;
      z-index: 999;
      height: 20px;
      width: 100%;
    }

    .signature .left {
      display: inline-block;
      width: 49%;
    }

    .signature .right {
      display: inline-block;
      width: 50%;
      text-align: right;
    }

    .disclaimer {
      font-size: 10px;
      margin-top: 20px;
    }

    .disclaimer strong {
      font-weight: bold;
    }
  </style>
</head>
<body>

<div class="signature">
  <div class="left">
    &copy; <?php echo date("Y"); ?> <?php echo $text_reserved; ?>
  </div>
  <div class="right">
    <?php echo $text_print_date; ?>: <?php echo $date_print; ?>
  </div>
</div>

<div class="watermark">
  <img src="storefront/view/default_html5/image/mandiri-investasi.jpg">
</div>

<div class="top">
  <h1 class="heading1"><?php echo $text_transactions; ?></h1>
  <p> <?php echo $text_transaction_date; ?>: <?php echo $date_start; ?> <?php echo $text_to; ?> <?php echo $date_end; ?></p>
</div>

<div class="info">
  <?php echo $cust_name; ?><br>
  <?php echo $cust_address; ?><br>
  <?php echo $cust_city; ?> <?php echo $cust_postal_code; ?><br>
  <?php echo $cust_country; ?>
</div>

<div>
  <table class="tftable"> 
    <thead>
      <tr> 
        <td style="width:8%;"><?php echo $text_column_date; ?></td>
        <td style="width:15%;"><?php echo $text_column_name; ?></td>
        <td style="width:12%;"><?php echo $text_column_type; ?></td>
        <td style="width:8%;"><?php echo $text_column_nav_date; ?></td>
        <td><?php echo $text_column_navpu; ?></td>
        <td><?php echo $text_column_unit; ?></td>
        <td><?php echo $text_column_amount; ?></td>
        <td><?php echo $text_column_fee; ?></td>
        <td><?php echo $text_column_total_amount; ?></td>
        <td><?php echo $text_column_saldo; ?></td>
      </tr>
    </thead>
    <tbody>
      <?php
        if(count($transactions)){
          foreach($transactions as $trn): ?>

        <tr> 
          <td><?php echo $trn['transaction_date']; ?></td>
          <td><?php echo $trn['product_name'] ? $trn['product_name'] : $trn['product_code']; ?></td>
          <td><?php echo $trn['transaction_type']; ?></td>
          <td><?php echo $trn['nav_date']; ?></td>
          <td><?php echo $trn['nav_value']; ?></td>
          <td><?php echo $trn['transaction_unit']; ?></td>
          <td><?php echo $trn['transaction_amount']; ?></td>
          <td><?php echo $trn['transaction_fee']; ?></td>
          <td><?php echo $trn['total_amount']; ?></td>
          <td><?php echo $trn['saldo']; ?></td>
        </tr>

      <?php 
          endforeach;
        }
      ?>
    </tbody>
  </table>
</div>

<div class="disclaimer">
  <strong><?php echo $text_disclaimer; ?>:</strong><br>
  <?php echo $entry_disclaimer; ?>
</div>

</body>
</html>
