<?php include($this->templateResource('/template/blocks/account.tpl')) ?>

<div class="heading hidden-xs">
  <div class="container">
    <h2 class="animated fadeInUp delayp1"><?php echo $text_heading_edit; ?></h2>
    <p class="animated fadeInUp delayp1"></p>
  </div>
</div><!--.heading-->


<div class="container">
  
  <?php if ($ispending!='0') { ?>
    <div class="alert alert-danger alert-dismissable">
      <?php echo $errors_review; ?>
    </div>
  <?php } ?>

  <?php if ($success) { ?>
    <div class="alert alert-success alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="right: 0">&times;</button>
      <?php echo $success; ?>
    </div>
    <?php } ?>

    <?php if ($error_warning) { ?>
    <div class="alert alert-warning alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="right: 0">&times;</button>
      <?php echo $success; ?>
    </div>
    <?php } ?>
  
  <?php //print_r($data_account);?>
  <div class="thumb">
    <div class="content p_15">

      <form id="AccountFrm" role="form" method="post" class="form-horizontal" action="<?php echo $post_url; ?>"  enctype="multipart/form-data">
        <div class="row clearfix underlined">
          <div class="col-sm-6">
            <div class="form-group clearfix">
              <label class="col-xs-12 control-label"><?php echo $entry_firstname; ?><span class="hidden">(individual as in Identity Card / Driving License / Passport / KITAS)</span></label>
              <div class="col-xs-12">
                <input id="AccountFrm_firstname" name="firstname" type="text" class="form-control" value="<?php echo $data_account['first_name']; ?>" placeholder="<?php echo $holder_first; ?>">

              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group clearfix">
              <label class="col-xs-12 control-label"><?php echo $entry_lastname; ?></label>
              <div class="col-xs-12">
                <input id="AccountFrm_lastname" name="lastname" type="text" class="form-control" value="<?php echo $data_account['last_name']; ?>" placeholder="<?php echo $holder_last; ?>">
              </div>
            </div>
          </div>
        </div>
        <div class="row clearfix underlined">
          <div class="col-md-6">
            <div class="form-group clearfix">
              <label class="col-xs-12 control-label"><?php echo $entry_domicile_address; ?></label>
              <div class="col-xs-12">
                <textarea id="AccountFrm_domicile_address" name="domicile_address" class="form-control" style="height: 91px" placeholder="<?php echo $holder_domicile; ?>"><?php echo $data_account['domicile_address']; ?></textarea>
              </div>
            </div>
          </div><!--.col-sm-6-->
          <div class="col-md-6">
            <div class="form-group clearfix">
              <label class="col-md-4 control-label"><?php echo $entry_domicile_country; ?></label>
              <div class="col-md-8">
                <select  id="AccountFrm_domicile_country" name="domicile_country" class="form-control">
                  <?php foreach($countries as $key => $country) { ?>
                  <option value="<?php echo $key; ?>" <?php selected($data_account['domicile_country'],$key); ?>><?php echo $country; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group clearfix">
              <label class="col-md-4 control-label"><?php echo $entry_domicile_city; ?></label>
              <div class="col-md-8">
                <select id="AccountFrm_domicile_city" name="domicile_city" class="form-control">
                  <option value=""><?php echo $entry_select; ?></option>
                  <?php foreach($dom_cities as $key => $city) { ?>
                  <option value="<?php echo $key; ?>" <?php selected($data_account['domicile_city'],$key); ?>><?php echo $city; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group clearfix">
              <label class="col-md-4 control-label"><?php echo $entry_domicile_postal_code; ?></label>
              <div class="col-md-8">
                <input id="AccountFrm_domicile_postal_code" name="domicile_postal_code" type="text" class="form-control" value="<?php echo $data_account['domicile_postal_code']; ?>"placeholder="<?php echo $holder_postal; ?>">
              </div>
            </div>
          </div><!--.col-sm-6-->
        </div><!--.row-5-->
        <div class="row clearfix underlined">
          <div class="col-md-6">
            <div class="form-group clearfix">
              <label class="col-xs-12 control-label"><?php echo $entry_correspondance_address; ?></label>
              <div class="col-xs-12">
                <textarea id="AccountFrm_correspondence_address" name="correspondence_address" class="form-control" style="height: 91px"placeholder="<?php echo $holder_cores; ?>"><?php echo $data_account['correspondence_address']?></textarea>
              </div>
            </div>
          </div><!--.col-sm-6-->
          <div class="col-md-6">
            <div class="form-group clearfix">
              <label class="col-md-4 control-label"><?php echo $entry_domicile_country; ?></label>
              <div class="col-md-8">
                <select  id="AccountFrm_correspondence_country" name="correspondence_country" class="form-control">
                  <?php foreach($countries as $key => $city) { ?>
                  <option value="<?php echo $key; ?>" <?php selected($data_account['correspondence_country'],$key); ?>><?php echo $city; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group clearfix">
              <label class="col-md-4 control-label"><?php echo $entry_domicile_city; ?></label>
              <div class="col-md-8">
                <select id="AccountFrm_correspondence_city"  name="correspondence_city" class="form-control">
                  <option value=""><?php echo $entry_select; ?></option>
                  <?php foreach($cor_cities as $key => $city) { ?>
                  <option value="<?php echo $key; ?>" <?php selected($data_account['correspondence_city'],$key); ?>><?php echo $city; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group clearfix">
              <label class="col-md-4 control-label"><?php echo $entry_domicile_postal_code; ?></label>
              <div class="col-md-8">
                <input  id="AccountFrm_correspondence_postal_code" name="correspondence_postal_code" type="text" class="form-control" value="<?php echo $data_account['correspondence_postal_code']?>"placeholder="<?php echo $holder_postal; ?>">
              </div>
            </div>
          </div><!--.col-sm-6-->
          <div class="form-group clearfix">
            <div class="col-sm-12">
              <div class="checkbox">
                <label>
                  <input id="AccountFrm_copy_address" type="checkbox"> <?php echo $entry_same; ?><!-- Same as domicile address -->
                </label>
              </div>
            </div>
          </div>
        </div><!--.row-->

        <div class="row clearfix underlined">
          <div class="col-sm-6">
            <div class="form-group clearfix">
              <label class="col-md-4 control-label"><?php echo $entry_tax_id_no; ?></label>
              <div class="col-md-8">
                <input id="AccountFrm_tax_id_no" name="tax_id_no" type="text" class="form-control" value="<?php echo $data_account['tax_id_no']?>" placeholder="<?php echo $holder_tax_id; ?>">
              </div>
            </div>
            <div class="form-group clearfix">
              <label class="col-md-4 control-label"><?php echo $entry_tax_id_reg_date; ?></label>
              <div class="col-md-8">
                <input id="AccountFrm_tax_id_reg_date" name="tax_id_reg_date" type="text" class="form-control" value="<?php echo ($data_account['tax_id_reg_date']? date("d-M-y", strtotime($data_account['tax_id_reg_date'])) :"");?>" placeholder="<?php echo $holder_reg_tax; ?>">
              </div>
            </div>
            <div class="form-group clearfix">
              <label class="col-md-4 control-label"><?php echo $entry_phone_no; ?></label>
              <div class="col-md-8">
                <input id="AccountFrm_phone_no" name = "phone_no" type="text" class="form-control" value="<?php echo $data_account['phone_no']?>"placeholder="<?php echo $holder_phone; ?>">
              </div>
            </div>
          </div><!--.col-sm-6-->
          <div class="col-sm-6">
            <div class="form-group clearfix">
              <label class="col-md-4 control-label"><?php echo $entry_mobile_phone_no; ?></label>
              <div class="col-md-8">
                <input id="AccountFrm_mobile_phone_no" name="mobile_phone_no" type="text" class="form-control" value="<?php echo $data_account['mobile_phone_no']?>"placeholder="<?php echo $holder_mobile; ?>">
              </div>
            </div>
            <div id="email-wrapper" class="form-group clearfix">
              <label class="col-md-4 control-label"><?php echo $entry_email; ?></label>
              <div class="col-md-8">
                <input type="text" id="AccountFrm_email" name="email" class="form-control" value="<?php echo $data_account['email']?>"placeholder="<?php echo $holder_email; ?>">
                <span id="email-found"></span>
              </div>
            </div>
            <div class="form-group clearfix">
              <label class="col-md-4 control-label"><?php echo $entry_fax; ?></label>
              <div class="col-md-8">
                <input id="AccountFrm_fax_no" name="fax_no" type="text" class="form-control" value="<?php echo $data_account['fax_no']?>"placeholder="<?php echo $holder_fax; ?>">
              </div>
            </div>
          </div><!--.col-sm-6-->
        </div><!--.row-->

        <div class="row clearfix underlined">
          <div class="col-md-6">
            <p class="m_b_10"><strong><?php echo $entry_bank_account_information; ?></strong></p>
            <div class="form-group clearfix">
              <label class="col-md-4 control-label"><?php echo $entry_bank_account_name; ?></label>
              <div class="col-md-8">
                <input id="AccountFrm_bank_account_name" name="bank_account_name" type="text" class="form-control" value="<?php echo $data_account['bank_account_name']?>" placeholder="<?php echo $holder_acc_name; ?>">
              </div>
            </div>
            <div class="form-group clearfix">
              <label class="col-md-4 control-label"><?php echo $entry_bank_name ?></label>
              <div class="col-md-8">
                <select id="AccountFrm_bank_name" name="bank_name" class="form-control">
                  <option value=""><?php echo $entry_select; ?></option>
                  <?php foreach($banks as $bank) { ?>
                  <option value="<?php echo $bank['item_code']; ?>"<?php selected($data_account['bank_name'],$bank['item_code']); ?>><?php echo $bank['item_name'];?></option>  
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group clearfix">
              <label class="col-md-4 control-label"><?php echo $entry_bank_address; ?></label>
              <div class="col-md-8">
                <textarea id="AccountFrm_bank_address" name="bank_address" class="form-control"  placeholder="<?php echo $holder_bank_add; ?>"><?php echo $data_account['bank_address']?></textarea>
              </div>
            </div>
            <div class="form-group clearfix">
              <label class="col-md-4 control-label"><?php echo $entry_bank_account_no; ?></label>
              <div class="col-md-8">
                <input id="AccountFrm_bank_account_no" name="bank_account_no" type="text" class="form-control" value="<?php echo $data_account['bank_account_no']?>" placeholder="<?php echo $holder_acc_number; ?>">
              </div>
            </div>
          </div><!--.col-sm-6-->
          <div class="col-md-6 m_t_15 m_b_15">
            <strong><?php echo $entry_bank_account_note; ?></strong><br>
            <em><?php echo $entry_bank_account_note_text; ?></em>
          </div><!--.col-sm-6-->
        </div><!--.row-->

        
        <div class="row clearfix underlined">
          <div class="sec-head"><?php echo $entry_individual; ?></div>
          <div class="col-sm-6">
            <div class="form-group clearfix">
              <label class="col-xs-12 control-label"><?php echo $entry_place_birthday ?></label>
              <div class="col-xs-12">
                <input id="AccountFrm_place_of_birth" name="place_of_birth" type="text" class="form-control" value="<?php echo $data_account['place_of_birth']?>" placeholder="<?php echo $holder_place_birth; ?>">
              </div>
            </div>
          </div><!--.col-md-3-->
          <div class="col-sm-6">
            <div class="form-group clearfix">
              <label class="col-xs-12 control-label"><?php echo $entry_date_of_birth; ?></label>
              <div class="col-xs-12">
                <input id="AccountFrm_date_of_birth" name="date_of_birth" type="text" class="form-control" value="<?php echo ($data_account['date_of_birth']? date("d-M-y", strtotime($data_account['date_of_birth'])) :"");?>" placeholder="<?php echo $holder_birth; ?>">
              </div>
            </div>
          </div><!--.col-md-3-->
          <div class="col-sm-12" id="gender">
            <div class="form-group clearfix gender">
              <label class="col-sm-3 control-label"><?php echo $entry_gender; ?></label>
              <div class="col-sm-9">
                <div class="row">
                  <div class="col-sm-4">
                    <div class="radio">
                      <label>
                        <input type="radio" name="gender" id="AccountFrm_gender" value="M" <?php checked($data_account['gender'],"M"); ?>>
                        <?php echo $entry_male; ?>
                      </label>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="radio">
                      <label>
                        <input type="radio" name="gender" id="AccountFrm_gender2" value="F" <?php checked($data_account['gender'],"F"); ?>>
                        <?php echo $entry_female; ?>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div><!--.col-md-3-->
          <div class="col-md-12" id="marital">
            <div class="form-group clearfix marital">
              <label class="col-sm-3 control-label"><?php echo $entry_marital; ?></label>
              <div class="col-sm-9">
                <div class="row">
                  <div class="col-sm-4">
                    <div class="radio">
                      <label>
                        <input type="radio" name="marital_status" id="AccountFrm_marital_status" value="MRRD" <?php checked($data_account['marital_status'],'MRRD'); ?>>
                        <?php echo $radio_married; ?>
                      </label>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="radio">
                      <label>
                        <input type="radio" name="marital_status" id="AccountFrm_marital_status2" value="DVRC" <?php checked($data_account['marital_status'],'DVRC'); ?>>
                        <?php echo $radio_was_married; ?>
                      </label>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="radio">
                      <label>
                        <input type="radio" name="marital_status" id="AccountFrm_marital_status3" value="SNGL" <?php checked($data_account['marital_status'],'SNGL'); ?>>
                        <?php echo $radio_single; ?>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div><!--.col-md-3-->
          <div class="col-md-6">
            <div class="form-group clearfix">
              <label class="col-md-4 control-label"><?php echo $entry_religion; ?></label>
              <div class="col-md-8">
                <!-- Ambil dari abmmi_lookup dengan group_code = RELIGION -->
                <select id="AccountFrm_religion" name="religion" class="form-control">
                  <option value=""><?php echo $entry_select; ?></option>
                  <!-- <option value="1" <?php selected($data_account[religion],1); ?>>Moslem</option>
                  <option value="2" <?php selected($data_account[religion],2); ?>>Catholic</option>
                  <option value="3" <?php selected($data_account[religion],3); ?>>Christian</option> -->
                  <?php foreach($religion as $rg) {?>
                    <option value="<?php echo $rg['item_code']; ?>"  <?php selected($data_account['religion'],$rg['item_code']); ?>><?php echo $rg['item_name']; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div><!--.col-md-3-->  
        </div><!--.row-->

        <div class="row clearfix underlined">
          <div class="col-sm-6">
            <div class="form-group clearfix">
              <label class="col-sm-12 control-label"><?php echo $entry_id_types; ?></label>
              <div class="col-sm-4 m_b_10-xs">
                <select id="AccountFrm_id_type" name="id_type" class="form-control">
                  <option value=""><?php echo $entry_select; ?></option>
                  <?php foreach($ids as $item) { ?>
                  <option value="<?php echo $item['item_code']; ?>" <?php selected($data_account['id_type'],$item['item_code']); ?>><?php echo $item['item_name'];?></option>  
                  <?php } ?>
                </select>
              </div>
              <div class="col-sm-8">
                <input id="AccountFrm_id_number" name="id_number" type="text" class="form-control" value="<?php echo $data_account['id_number'] ?>"  placeholder="<?php echo $holder_id_num; ?>">
              </div>
            </div>
          </div><!--.col-sm-6-->
          <div class="col-sm-6">
            <div class="form-group clearfix">
              <label class="col-md-12 control-label"><?php echo $entry_id_expiry_date; ?></label>
              <div class="col-md-12">
                <input id="AccountFrm_id_expiry_date" name="id_expiry_date" type="text" class="form-control" value="<?php echo ($data_account['id_expiry_date']? date("d-M-y", strtotime($data_account['id_expiry_date'])) :"");?>"  placeholder="<?php echo $holder_exp; ?>">
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group clearfix">
              <label class="col-md-12 control-label"><?php echo $entry_nationality; ?></label>
              <div class="col-md-12">
                <select id="AccountFrm_nationality" name="nationality" class="form-control">
                  <option value=""><?php echo $entry_select; ?></option>
                  <?php foreach($nationalities as $item) { ?>
                  <option value="<?php echo $item['item_code']; ?>" <?php selected($data_account['nationality'],$item['item_code']); ?>><?php echo $item['item_name'];?></option>  
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group clearfix">
              <label class="col-md-12 control-label"><?php echo $entry_residence; ?></label>
              <div class="col-md-12">
                <select id="AccountFrm_residency_country" name="residency_country" class="form-control">
                  <?php foreach($countries as $key => $country) { ?>
                  <option value="<?php echo $key; ?>" <?php selected($data_account['residency_country'],$key); ?>><?php echo $country; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div><!--.col-sm-3-->
          <div class="col-xs-12">
            <div class="form-group clearfix">
              <label class="col-sm-3 control-label"><?php echo $entry_residency_status; ?></label>
              <div class="col-sm-9">
                <div class="row">
                  <div class="col-sm-4">
                    <div class="radio">
                      <label>
                        <input id="AccountFrm_residency_status" name="residency_status" type="radio" value="SELF_OWND"  <?php checked($data_account['residency_status'],'SELF_OWND'); ?>>
                        <?php echo $radio_self; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input id="AccountFrm_residency_status2" name="residency_status" type="radio" value="SPOUSE_OWND" <?php checked($data_account['residency_status'],'SPOUSE_OWND'); ?>>
                        <?php echo $radio_spouse_own; ?>
                      </label>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="radio">
                      <label>
                        <input id="AccountFrm_residency_status3" name="residency_status" type="radio" value="FMLY_OWND" <?php checked($data_account['residency_status'],'FMLY_OWND'); ?>>
                        <?php echo $radio_family_own; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input  id="AccountFrm_residency_status4" name="residency_status" type="radio" value="INST_OWND" <?php checked($data_account['residency_status'],"INST_OWND"); ?>>
                        <?php echo $radio_institution; ?>
                      </label>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="radio">
                      <label>
                        <input id="AccountFrm_residency_status3" name="residency_status" type="radio" value="RENT" <?php checked($data_account['residency_status'],'RENT'); ?>>
                        <?php echo $radio_rent; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input  id="AccountFrm_residency_status4" name="residency_status" type="radio" value="OTHR" <?php checked($data_account['residency_status'],"OTHR"); ?>>
                        <?php echo $entry_other; ?>
                      </label>
                    </div>
                  </div>
                </div><!--.row-->
              </div>
            </div>
          </div><!--.col-md-3-->
        </div><!--.row-->

        <div class="row">
          <div class="col-xs-12">
            <div class="form-group clearfix">
              <label class="col-sm-3 control-label"><?php echo $entry_occupation; ?></label>
              <div class="col-sm-9">
                <div class="row">
                  <div class="col-sm-4">
                    <div class="radio">
                      <label>
                        <input type="radio" id="AccountFrm_occupation" name="occupation" value="ENTRPRNR_BIZMN" <?php checked($data_account['occupation'],"ENTRPRNR_BIZMN"); ?>>
                        <?php echo $radio_entrepreneur; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" id="AccountFrm_occupation1" name="occupation" value="STDNT" <?php checked($data_account['occupation'],"STDNT"); ?>>
                        <?php echo $radio_student; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" id="AccountFrm_occupation2" name="occupation" value="HS_WF" <?php checked($data_account['occupation'],"HS_WF"); ?>>
                        <?php echo $radio_housewife; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" id="AccountFrm_occupation2" name="occupation" value="CORP_EMPLY" <?php checked($data_account['occupation'],"CORP_EMPLY"); ?>>
                        <?php echo $radio_private_employee; ?>
                      </label>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="radio">
                      <label>
                        <input type="radio" id="AccountFrm_occupation3" name="occupation" value="ROE" <?php checked($data_account['occupation'],"ROE"); ?>>
                        <?php echo $radio_enterprises; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" id="AccountFrm_occupation4" name="occupation" value="MTRY" <?php checked($data_account['occupation'],"MTRY"); ?>>
                        <?php echo $radio_militery; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" id="AccountFrm_occupation5" name="occupation" value="RTRMNT" <?php checked($data_account['occupation'],"RTRMNT"); ?>>
                        <?php echo $radio_pension; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" id="AccountFrm_occupation5" name="occupation" value="LCTR_TCHRS" <?php checked($data_account['occupation'],"LCTR_TCHRS"); ?>>
                        <?php echo $radio_lecturer; ?>
                      </label>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="radio">
                      <label>
                        <input type="radio" name="occupation" id="AccountFrm_occupation5" value="GVRNMNT_BUMN" <?php checked($data_account['occupation'],"GVRNMNT_BUMN"); ?>>
                        <?php echo $radio_government_employee; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="occupation" id="AccountFrm_occupation5" value="PRFSSNL" <?php checked($data_account['occupation'],"PRFSSNL"); ?>>
                        <?php echo $radio_profesional; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="occupation" id="AccountFrm_occupation5" value="OTHR" <?php checked($data_account['occupation'],"OTHR"); ?>>
                        <?php echo $entry_other; ?>
                        <!--input type="text" class="form-control pull-left" placeholder="Others" style="width: 80%; height: 20px; padding: 2px 4px"-->
                      </label>
                    </div>
                  </div>
                </div><!--.row-->
              </div>
            </div>
          </div><!--.col-xs-12-->
        </div>

        <div class="row clearfix underlined">
          <div class="col-sm-6">
            <div class="form-group clearfix">
              <label class="col-md-12 control-label"><?php echo $entry_company_name; ?></label>
              <div class="col-md-12">
                <input id="AccountFrm_company_name" name="company_name" type="text" class="form-control" value="<?php echo $data_account['company_name'];?>"  placeholder="<?php echo $holder_company; ?>">
              </div>
            </div>
          </div><!--.col-sm-6-->
          <div class="col-sm-6">
            <div class="form-group clearfix">
              <label class="col-md-12 control-label"><?php echo $entry_company_type; ?></label>
              <div class="col-md-12">
                <select id="AccountFrm_company_type" name="company_type" class="form-control">
                  <option value=""><?php echo $entry_select; ?></option>
                  <?php foreach($company_type as $item) { ?>
                  <option value="<?php echo $item['item_code']; ?>" <?php selected($data_account['company_type'],$item['item_code']); ?>><?php echo $item['item_name'];?></option>  
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group clearfix">
              <label class="col-md-12 control-label"><?php echo $entry_education; ?></label>
              <div class="col-md-12">
                <select id="AccountFrm_education" name="education" class="form-control">
                  <option value=""><?php echo $entry_select; ?></option>
                  <?php foreach($education as $item) { ?>
                  <option value="<?php echo $item['item_code']; ?>" <?php selected($data_account['education'],$item['item_code']); ?>><?php echo $item['item_name'];?></option>  
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col-sm-6" id="sp-name">
            <div class="form-group clearfix">
              <label class="col-md-12 control-label"><?php echo $entry_spouse_name; ?></label>
              <div class="col-md-12">
                <input id="AccountFrm_spouse_name" name="spouse_name" type="text" class="form-control" value="<?php echo $data_account['spouse_name'];?>"  placeholder="<?php echo $holder_spouse; ?>">
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group clearfix">
              <label class="col-md-12 control-label"><?php echo $entry_spouse_occupation; ?></label>
              <div class="col-md-12">
                <input id="AccountFrm_spouse_occupation" name="spouse_occupation" type="text" class="form-control" value="<?php echo $data_account['spouse_occupation'];?>"  placeholder="<?php echo $holder_occuption; ?>">
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group clearfix">
              <label class="col-md-12 control-label"><?php echo $entry_mother_name; ?></label>
              <div class="col-md-12">
                <input id="AccountFrm_mother_name" name="mother_name" type="text" class="form-control" value="<?php echo $data_account['mother_name'];?>" placeholder="<?php echo $holder_mother; ?>">
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group clearfix">
              <label class="col-md-12 control-label"><?php echo $entry_father_name; ?></label>
              <div class="col-md-12">
                <input id="AccountFrm_father_name" name="father_name" type="text" type="text" class="form-control" value="<?php echo $data_account['father_name'];?>" placeholder="<?php echo $holder_father; ?>">
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group clearfix">
                  <label class="col-md-12 control-label"><?php echo $entry_inheritor; ?></label>
                  <div class="col-md-12">
                    <input id="AccountFrm_inheritor" name="inheritor" type="text" class="form-control" value="<?php echo $data_account['inheritor'];?>" placeholder="<?php echo $holder_in_name; ?>">
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group clearfix">
                  <label class="col-md-12 control-label"><?php echo $entry_inheritor_relationship; ?></label>
                  <div class="col-md-12">
                    <select id="AccountFrm_inheritor_relationship" name="inheritor_relationship" class="form-control">
                      <option value=""><?php echo $entry_select; ?></option>
                      <?php foreach($inheritor_relationship as $item) { ?>
                      <option value="<?php echo $item['item_code']; ?>" <?php selected($data_account['inheritor_relationship'],$item['item_code']); ?>><?php echo $item['item_name'];?></option>  
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div><!--.row-->

        <div class="row clearfix">
          <div class="col-xs-12">
            <p class="m_b_10"><strong><?php echo $entry_financial; ?></strong></p>
            <div class="form-group clearfix gross">
              <label class="col-sm-3 control-label"><?php echo $entry_gross; ?></label>
              <div class="col-sm-9 gross_income">
                <div class="row">
                  <div class="col-sm-4">
                    <div class="radio">
                      <label>
                        <input type="radio" name="gross_income" id="AccountFrm_gross_income" value="RNG01" <?php checked($data_account['gross_income'],"RNG01");?> >
                        < 10
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="gross_income" id="AccountFrm_gross_income" value="RNG02" <?php checked($data_account['gross_income'],"RNG02");?> >
                        10 - 50
                      </label>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="radio">
                      <label>
                        <input type="radio" name="gross_income" id="AccountFrm_gross_income" value="RNG03" <?php checked($data_account['gross_income'],"RNG03");?> >
                        > 50 - 100
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="gross_income" id="AccountFrm_gross_income" value="RNG04" <?php checked($data_account['gross_income'],"RNG04");?> >
                        > 100 - 500
                      </label>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="radio">
                      <label>
                        <input type="radio" name="gross_income" id="AccountFrm_gross_income" value="RNG05" <?php checked($data_account['gross_income'],"RNG05");?> >
                        > 500 - 1000
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="gross_income" id="AccountFrm_gross_income" value="RNG06" <?php checked($data_account['gross_income'],"RNG06");?> >
                        > 1000
                      </label>
                    </div>
                  </div>
                </div><!--.row-->
              </div>
            </div>
          </div><!--.col-xs-12-->
        </div><!--.row-->

        <div class="row clearfix underlined">
          <div class="col-xs-12">
            <div class="form-group clearfix">
              <label class="col-sm-12 control-label"><?php echo $entry_main_income; ?></label>
              <div class="col-sm-12">
                <div class="row main-income">
                  <div class="col-sm-3">
                    <div class="radio">
                      <label>
                        <input type="radio" name="source_of_income" id="optionMainSource" value="ALLWNC" <?php checked($data_account['source_of_income'],"ALLWNC");?> > <?php echo $radio_allowance; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="source_of_income" id="optionMainSource" value="BIZ_BNFTS" <?php checked($data_account['source_of_income'],"BIZ_BNFTS");?> > <?php echo $radio_business_provit; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="source_of_income" id="optionMainSource" value="BQST" <?php checked($data_account['source_of_income'],"BQST");?> > <?php echo $entry_bequest; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="source_of_income" id="optionMainSource" value="DPST" <?php checked($data_account['source_of_income'],"DPST");?> > <?php echo $entry_deposit; ?>
                      </label>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="radio">
                      <label>
                        <input type="radio" name="source_of_income" id="optionMainSource" value="INCM" <?php checked($data_account['source_of_income'],"INCM");?> > <?php echo $entry_income; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="source_of_income" id="optionMainSource" value="INHRTNC" <?php checked($data_account['source_of_income'],"INHRTNC");?> > <?php echo $entry_inheritance; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="source_of_income" id="optionMainSource" value="INTRST_SVNG" <?php checked($data_account['source_of_income'],"INTRST_SVNG");?> > <?php echo $entry_interest_saving; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="source_of_income" id="optionMainSource" value="INVST_RSLTS" <?php checked($data_account['source_of_income'],"INVST_RSLTS");?> > <?php echo $radio_investment_return; ?>
                      </label>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="radio">
                      <label>
                        <input type="radio" name="source_of_income" id="optionMainSource" value="LOAN" <?php checked($data_account['source_of_income'],"LOAN");?> > <?php echo $radio_loan; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="source_of_income" id="optionMainSource" value="LTTRY" <?php checked($data_account['source_of_income'],"LTTRY");?> > <?php echo $entry_lottery ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="source_of_income" id="optionMainSource" value="PNSN_BNFT" <?php checked($data_account['source_of_income'],"PNSN_BNFT");?> > <?php echo $radio_pension_fund; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="source_of_income" id="optionMainSource" value="PRNT_CHLD" <?php checked($data_account['source_of_income'],"PRNT_CHLD");?> > <?php echo $radio_parents; ?>
                      </label>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="radio">
                      <label>
                        <input type="radio" name="source_of_income" id="optionMainSource" value="RENT" <?php checked($data_account['source_of_income'],"RENT");?> > <?php echo $radio_rent_fee; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="source_of_income" id="optionMainSource" value="SLRY" <?php checked($data_account['source_of_income'],"SLRY");?> > <?php echo $radio_salary ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="source_of_income" id="optionMainSource" value="SPOUSE" <?php checked($data_account['source_of_income'],"SPOUSE");?> > <?php echo $radio_spouse; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="source_of_income" id="optionMainSource" value="OTHR" <?php checked($data_account['source_of_income'],"OTHR");?> > <?php echo $entry_other ?>
                        <!--input type="text" class="form-control pull-left" placeholder="Others" style="width: 80%; height: 20px; padding: 2px 4px"-->
                      </label>
                    </div>
                  </div>
                </div><!--.row-->
              </div>
            </div>
          </div><!--.col-xs-12-->
        </div><!--.row-->

        <div class="row clearfix underlined">
          <div class="col-xs-12">
            <div class="form-group clearfix">
              <label class="col-sm-3 control-label"><?php echo $entry_additional_income; ?></label>
              <div class="col-sm-9">
                <div class="row">
                  <div class="col-sm-4">
                    <div class="radio">
                      <label>
                        <input type="radio" name="additional_income" id="optionAddIncome" value="RNG01" <?php checked($data_account['additional_income'],"RNG01");?> >
                        < 10
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="additional_income" id="optionAddIncome" value="RNG02" <?php checked($data_account['additional_income'],"RNG02");?> >
                        10 - 50
                      </label>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="radio">
                      <label>
                        <input type="radio" name="additional_income" id="optionAddIncome" value="RNG03" <?php checked($data_account['additional_income'],"RNG03");?> >
                        > 50 - 100
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="additional_income" id="optionAddIncome" value="RNG04" <?php checked($data_account['additional_income'],"RNG04");?> >
                        > 100 - 500
                      </label>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="radio">
                      <label>
                        <input type="radio" name="additional_income" id="optionAddIncome" value="RNG05" <?php checked($data_account['additional_income'],"RNG05");?> >
                        > 500 - 1000
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="additional_income" id="optionAddIncome" value="RNG06" <?php checked($data_account['additional_income'],"RNG06");?> >
                        > 1000
                      </label>
                    </div>
                  </div>
                </div><!--.row-->
              </div>
            </div>
            <div class="row clearfix underlined">
              <div class="col-xs-12">
                <div class="form-group clearfix">
                  <label class="col-sm-12 control-label"><?php echo $entry_add_source_of_income; ?></label>
                  <div class="col-sm-12">
                    <div class="row">
                      <div class="col-sm-3">
                        <div class="radio">
                          <label>
                            <input type="radio" name="add_source_of_income" id="optionAdditionalSource" value="ALLWNC" <?php checked($data_account['add_source_of_income'],"ALLWNC");?> > <?php echo $radio_allowance; ?>
                          </label>
                        </div>
                        <div class="radio">
                          <label>
                            <input type="radio" name="add_source_of_income" id="optionAdditionalSource" value="BIZ_BNFTS" <?php checked($data_account['add_source_of_income'],"BIZ_BNFTS");?> > <?php echo $entry_bussiness_benefits; ?>
                          </label>
                        </div>
                        <div class="radio">
                          <label>
                            <input type="radio" name="add_source_of_income" id="optionAdditionalSource" value="BQST" <?php checked($data_account['add_source_of_income'],"BQST");?> > <?php echo $entry_bequest ?>
                          </label>
                        </div>
                        <div class="radio">
                          <label>
                            <input type="radio" name="add_source_of_income" id="optionAdditionalSource" value="DPST" <?php checked($data_account['add_source_of_income'],"DPST");?> > <?php echo $entry_deposit ?>
                          </label>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="radio">
                          <label>
                            <input type="radio" name="add_source_of_income" id="optionAdditionalSource" value="INCM" <?php checked($data_account['add_source_of_income'],"INCM");?> > <?php echo $entry_income ?>
                          </label>
                        </div>
                        <div class="radio">
                          <label>
                            <input type="radio" name="add_source_of_income" id="optionAdditionalSource" value="INHRTNC" <?php checked($data_account['add_source_of_income'],"INHRTNC");?> > <?php echo $entry_inheritance; ?>
                          </label>
                        </div>
                        <div class="radio">
                          <label>
                            <input type="radio" name="add_source_of_income" id="optionAdditionalSource" value="INTRST_SVNG" <?php checked($data_account['add_source_of_income'],"INTRST_SVNG");?> > <?php echo $entry_interest_saving; ?>
                          </label>
                        </div>
                        <div class="radio">
                          <label>
                            <input type="radio" name="add_source_of_income" id="optionAdditionalSource" value="INVST_RSLTS" <?php checked($data_account['add_source_of_income'],"INVST_RSLTS");?> > <?php echo $radio_investment_return; ?>
                          </label>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="radio">
                          <label>
                            <input type="radio" name="add_source_of_income" id="optionAdditionalSource" value="LOAN" <?php checked($data_account['add_source_of_income'],"LOAN");?> > <?php echo $radio_loan; ?>
                          </label>
                        </div>
                        <div class="radio">
                          <label>
                            <input type="radio" name="add_source_of_income" id="optionAdditionalSource" value="LTTRY" <?php checked($data_account['add_source_of_income'],"LTTRY");?> > <?php echo $entry_lottery; ?>
                          </label>
                        </div>
                        <div class="radio">
                          <label>
                            <input type="radio" name="add_source_of_income" id="optionAdditionalSource" value="PNSN_BNFT" <?php checked($data_account['add_source_of_income'],"PNSN_BNFT");?> > <?php echo $radio_pension_fund; ?>
                          </label>
                        </div>
                        <div class="radio">
                          <label>
                            <input type="radio" name="add_source_of_income" id="optionAdditionalSource" value="PRNT_CHLD" <?php checked($data_account['add_source_of_income'],"PRNT_CHLD");?> > <?php echo $radio_parents; ?>
                          </label>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="radio">
                          <label>
                            <input type="radio" name="add_source_of_income" id="optionAdditionalSource" value="RENT" <?php checked($data_account['add_source_of_income'],"RENT");?> > <?php echo$radio_rent_fee; ?>
                          </label>
                        </div>
                        <div class="radio">
                          <label>
                            <input type="radio" name="add_source_of_income" id="optionAdditionalSource" value="SLRY" <?php checked($data_account['add_source_of_income'],"SLRY");?> > <?php echo $radio_salary; ?>
                          </label>
                        </div>
                        <div class="radio">
                          <label>
                            <input type="radio" name="add_source_of_income" id="optionAdditionalSource" value="SPOUSE" <?php checked($data_account['add_source_of_income'],"SPOUSE");?> > <?php echo $radio_spouse; ?>
                          </label>
                        </div>
                        <div class="radio">
                          <label>
                            <input type="radio" name="add_source_of_income" id="optionAdditionalSource" value="OTHR" <?php checked($data_account['add_source_of_income'],"OTHR");?> > <?php echo $entry_other; ?>
                            <!--input type="text" class="form-control pull-left" placeholder="Others" style="width: 80%; height: 20px; padding: 2px 4px"-->
                          </label>
                        </div>
                      </div>
                    </div><!--.row-->
                  </div>
                </div>
              </div><!--.col-xs-12-->
            </div><!--.row-->
                
                <!--div class="form-group clearfix">
                  <label class="col-sm-4 col-md-3 control-label">Additional Source of Income</label>
                  <div class="col-sm-8 col-md-9">
                    <input type="text" class="form-control">
                  </div>
                </div-->
              </div><!--.col-xs-12-->
            </div><!--.row-->

        <div class="row clearfix underlined">
          <div class="sec-head"><?php echo $entry_unit_holder;?></div>
          <div class="col-xs-12">
            <div class="form-group clearfix">
              <label class="col-sm-12 control-label"><?php echo $entry_source_fund; ?></label>
              <div class="col-sm-12">
                <div class="row source">
                  <div class="col-sm-3">
                    <div class="radio">
                      <label>
                        <input type="radio" name="source_of_fund" id="source_of_fund" value="ALLWNC" <?php checked($data_account['source_of_fund'],"ALLWNC");?> > <?php echo $radio_allowance; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="source_of_fund" id="source_of_fund" value="BIZ_BNFTS" <?php checked($data_account['source_of_fund'],"BIZ_BNFTS");?> > <?php echo $entry_bussiness_benefits; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="source_of_fund" id="source_of_fund" value="BQST" <?php checked($data_account['source_of_fund'],"BQST");?> ><?php echo $entry_bequest; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="source_of_fund" id="source_of_fund" value="DPST" <?php checked($data_account['source_of_fund'],"DPST");?> > <?php echo $entry_deposit; ?>
                      </label>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="radio">
                      <label>
                        <input type="radio" name="source_of_fund" id="source_of_fund" value="INCM" <?php checked($data_account['source_of_fund'],"INCM");?> > <?php echo $entry_income; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="source_of_fund" id="source_of_fund" value="INHRTNC" <?php checked($data_account['source_of_fund'],"INHRTNC");?> > <?php echo $entry_inheritance; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="source_of_fund" id="source_of_fund" value="INTRST_SVNG" <?php checked($data_account['source_of_fund'],"INTRST_SVNG");?> > <?php echo $entry_interest_saving; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="source_of_fund" id="source_of_fund" value="INVST_RSLTS" <?php checked($data_account['source_of_fund'],"INVST_RSLTS");?> > <?php echo $radio_invesment; ?>
                      </label>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="radio">
                      <label>
                        <input type="radio" name="source_of_fund" id="source_of_fund" value="LOAN" <?php checked($data_account['source_of_fund'],"LOAN");?> > <?php echo $radio_loan; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="source_of_fund" id="source_of_fund" value="LTTRY" <?php checked($data_account['source_of_fund'],"LTTRY");?> > <?php echo $entry_lottery; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="source_of_fund" id="source_of_fund" value="PNSN_BNFT" <?php checked($data_account['source_of_fund'],"PNSN_BNFT");?> > <?php echo $radio_pension_fund; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="source_of_fund" id="source_of_fund" value="PRNT_CHLD" <?php checked($data_account['source_of_fund'],"PRNT_CHLD");?> > <?php echo $radio_parents ?>
                      </label>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="radio">
                      <label>
                        <input type="radio" name="source_of_fund" id="source_of_fund" value="RENT" <?php checked($data_account['source_of_fund'],"RENT");?> > <?php echo $radio_rented_house; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="source_of_fund" id="source_of_fund" value="SLRY" <?php checked($data_account['source_of_fund'],"SLRY");?> > <?php echo $radio_salary; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="source_of_fund" id="source_of_fund" value="SPOUSE" <?php checked($data_account['source_of_fund'],"SPOUSE");?> > <?php echo $radio_spouse; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="source_of_fund" id="source_of_fund" value="OTHR" <?php checked($data_account['source_of_fund'],"OTHR");?> > <?php echo $entry_other; ?>
                        <!--input type="text" class="form-control pull-left" placeholder="Others" style="width: 80%; height: 20px; padding: 2px 4px"-->
                      </label>
                    </div>
                  </div>
                </div><!--.row-->
              </div>
            </div>
          </div><!--.col-xs-12-->
        </div><!--.row-->

        <div class="row clearfix underlined">
          <div class="col-xs-12">
            <div class="form-group clearfix">
              <label class="col-sm-12 control-label"><?php echo $entry_objective; ?></label>
              <div class="col-sm-12">
                <div class="row inv-obj">
                  <div class="col-sm-3">
                    <div class="radio">
                      <label>
                        <input type="radio" name="inv_objective" id="inv_objective" value="CRDT_PRCTTN" <?php checked($data_account['inv_objective'],"CRDT_PRCTTN");?> > <?php echo $entry_credit_protection; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="inv_objective" id="inv_objective" value="INCM_PRCTTN" <?php checked($data_account['inv_objective'],"INCM_PRCTTN");?> > <?php echo $entry_income_protection; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="inv_objective" id="inv_objective" value="INVST" <?php checked($data_account['inv_objective'],"INVST");?> > <?php echo $entry_investment; ?>
                      </label>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="radio">
                      <label>
                        <input type="radio" name="inv_objective" id="inv_objective" value="PRC_GAIN" <?php checked($data_account['inv_objective'],"PRC_GAIN");?> > <?php echo $entry_gain; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="inv_objective" id="inv_objective" value="PRSNL" <?php checked($data_account['inv_objective'],"PRSNL");?> > <?php echo $entry_personal; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="inv_objective" id="inv_objective" value="OTHR" <?php checked($data_account['inv_objective'],"OTHR");?> > <?php echo $entry_other; ?>
                      </label>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="radio">
                      <label>
                        <input type="radio" name="inv_objective" id="inv_objective" value="REVENUE" <?php checked($data_account['inv_objective'],"REVENUE");?> > <?php echo $entry_revenue; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="inv_objective" id="inv_objective" value="SPCLTN" <?php checked($data_account['inv_objective'],"SPCLTN");?> > <?php echo $entry_speculation; ?>
                      </label>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="radio">
                      <label>
                        <input type="radio" name="inv_objective" id="inv_objective" value="SVNG" <?php checked($data_account['inv_objective'],"SVNG");?> > <?php echo $entry_saving; ?>
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="inv_objective" id="inv_objective" value="TRN" <?php checked($data_account['inv_objective'],"TRN");?> > <?php echo $entry_transaction; ?>
                      </label>
                    </div>                        
                    <!--div class="radio">
                      <label>
                        <input type="radio" name="inv_objective" id="inv_objective">
                        <input type="text" class="form-control pull-left" placeholder="Others" style="width: 80%; height: 20px; padding: 2px 4px">
                      </label>
                    </div-->
                  </div>
                </div><!--.row-->
              </div>
            </div>
          </div><!--.col-xs-12-->
        </div><!--.row-->

        <div class="btn-placeholder">
          <?php if ($ispending!='0'): ?>
            <button id="button-submit" type="submit" class="btn btn-primary pull-right disabled" title="<?php echo $errors_review ?>"><?php echo $button_save; ?></button>
          <?php else: ?>
            <button id="button-submit" type="submit" class="btn btn-primary pull-right"><?php echo $button_save; ?></button>
          <?php endif ?>
          
          <a class="btn btn-default pull-right m_r_10" href="<?php echo $back; ?>"><?php echo $button_back; ?></a>
        </div>

      </form>

    </div><!--.content-->
  </div><!--.thumb-->

<?php 
  function checked($getValue,$value){
    if($getValue==$value){
      echo "checked";
      if(is_null($getValue) && $value==1){
        echo "checked";
      }
    }
  }

  function selected($getValue,$value){
    if($getValue==$value){
      echo "selected";
      if(is_null($getValue) && $value==1){
        echo "selected";
      }
    }
  }
?>
<script type="text/javascript">
  
  $(document).ready(function(){

      $('#error-wrapper').hide();
      $('#button-submit').on('click',function(){
        $('div#errors').html('');
      });
      function rsa(){
        var publickey = "<?php echo publicKeyToHex(PRIVATE_KEY)?>";
        var rsakey = new RSAKey();
        rsakey.setPublic(publickey, "10001");
        var enc_name = rsakey.encrypt($('#AccountFrm_bank_account_name').val());
        var enc_no = rsakey.encrypt($('#AccountFrm_bank_account_no').val());

        // console.log(enc_name, enc_no);return;
        
        $('#AccountFrm_bank_account_name').val(enc_name);
        $('#AccountFrm_bank_account_no').val(enc_no);

      }
        /*$('.datepicker').datepicker({
          format: 'yyyy-mm-dd'
        });*/
      $("#AccountFrm_copy_address").click(function(){
        var address = $('#AccountFrm_domicile_address').val();
        var city = $('#AccountFrm_domicile_city').val();
        var code = $('#AccountFrm_domicile_postal_code').val();
        var country = $('#AccountFrm_domicile_country').val();

          if ($(this).is(':checked')) {
            $('#AccountFrm_correspondence_address').val(address);

            //$('select[name=\'correspondence_city\']').load('index.php?rt=common/zone&country_id='+ $('#AccountFrm_domicile_country').val() +'&zone_id=<?php echo $zone_id; ?>');
            $('#AccountFrm_correspondence_city option').remove();
            $('#AccountFrm_domicile_city option').clone().appendTo('#AccountFrm_correspondence_city');
            $('#AccountFrm_correspondence_city option[value='+city+']').attr('selected', 'selected');
            //$('#AccountFrm_correspondence_city').val(city);
            $('#AccountFrm_correspondence_postal_code').val(code);
            $('#AccountFrm_correspondence_country').val(country);
          }else{
            if (city!='') {
              $('#AccountFrm_correspondence_city option').remove();  
            };
            $('#AccountFrm_correspondence_address').val('');
            $('#AccountFrm_correspondence_city').val('');
            $('#AccountFrm_correspondence_postal_code').val('');
            $('#AccountFrm_correspondence_country').val('');
          }
        
      });

      //added by fazrin - datepicker fields

      $("#AccountFrm_date_of_birth").datepicker({ dateFormat: 'dd-M-y' });
      $("#AccountFrm_id_expiry_date").datepicker({ dateFormat: 'dd-M-y' });
      $("#AccountFrm_tax_id_reg_date").datepicker({ dateFormat: 'dd-M-y' });
      
      $("#AccountFrm_residency_txt").click(function(){
        $("#AccountFrm_residency_status5").prop("checked", true);
      });

      $("#AccountFrm_occupation_txt").click(function(){
        $("#AccountFrm_occupation6").prop("checked", true);
      });

      $("#AccountFrm_source_of_income_txt").click(function(){
        $("#AccountFrm_source_of_income9").prop("checked", true);
      });

      $("#AccountFrm_source_of_fund_txt").click(function(){
        $("#AccountFrm_source_of_fund6").prop("checked", true);
      });

      $("#AccountFrm_inv_objective_txt").click(function(){
        $("#AccountFrm_inv_objective2").prop("checked", true);
      });

    
    var validate = true;
    if(validate){
     $( "#AccountFrm" ).validate({
        submitHandler: function(form) {
          $.post('index.php?rt=r/account/account/emailCheck', 
            $('form#AccountFrm').serialize(),
              function(data){
                  if (data!=0) {
                    // alert('email found');
                    $('#email-wrapper').addClass('has-error');
                    $('#AccountFrm_email').after('<p for="AccountFrm_spouse_name" generated="true" class="help-block" style="display: block;"><?php echo $errors_duplictae_email; ?></p>');
                    $(window).scrollTop($('#email-wrapper').offset().top);
                    return false;
                  }else{
                    
                    if(document.getElementById('AccountFrm_marital_status').checked) {
            
                      sp = $('#AccountFrm_spouse_name').val();
                      if (sp=='') {
                        
                        $('#sp-name').addClass('has-error');
                        $('#AccountFrm_spouse_name').after('<p for="AccountFrm_spouse_name" generated="true" class="help-block" style="display: block;"><?php echo $spouse_nm; ?></p>');
                        $(window).scrollTop($('#sp-name').offset().top);
                        return false;
                      }else{
                        rsa();
                        form.submit();
                      }
                    }else{
                      rsa();
                      form.submit();
                    }
                  };
              }, "json"
          );
          
        },
        
          //by default validation will run on input keyup and focusout
          //set this to false to validate on submit only
          onkeyup: false,
          onfocusout: false,
          //by default the error elements is a <label>
          errorElement: "p",
          // //place all errors in a <div id="errors"> element
          // errorPlacement: function(error, element) {
              
          //     error.appendTo("div#errors");
          // },
                highlight: function(element) {
              $(element).parent().parent().addClass("has-error");
          },
          unhighlight: function(element) {
              $(element).parent().parent().removeClass("has-error");
          },
          errorPlacement: function(error, element) {
             if (element.attr("name") == "gender") {
                  error.insertAfter('.gender');
    
              }
              else if (element.attr("name") == "marital_status") {
                  error.insertAfter('.marital');
                 
              }
              
              else if (element.attr("name") == "gross_income") {
                  error.insertBefore('.gross');
                 
              }
              else if (element.attr("name") == "source_of_income") {
                  error.insertBefore('.main-income');
                 
              }
              else if (element.attr("name") == "source_of_fund") {
                  error.insertBefore('.source');
                 
              }
              else if (element.attr("name") == "inv_objective") {
                  error.insertBefore('.inv-obj');
                 
              }
               else {
                  error.insertAfter(element);
              }
          },
          errorClass: 'help-block',
        rules: {
          firstname: {
            required: true
          },
          lastname: {
            required: true
          },
          domicile_address:{
            required: true
          },
          domicile_city:{
            required: true
          },
          domicile_postal_code:{
            required: true
          },
          domicile_country:{
            required: true
          },
          tax_id_no:{
            required: true  
          },
          tax_id_reg_date:{
            required: true  
          },
          phone_no:{
            required: true    
          },
          mobile_phone_no:{
            required: true      
          },
          email:{
            required: true,
            email:true        
          },
          bank_account_name:{
            required: true  
          },
          bank_name:{
            required: true    
          },
          bank_account_no:{
            required:true
          },
          place_of_birth:{
            required:true
          },
          date_of_birth:{
            required:true
          },
          gender:{
            required:true
          },
          marital_status:{
            required:true
          },
          religion:{
            required:true
          },
          company_name:{
            required:true
          },
          education:{
            required:true
          },
          mother_name:{
            required:true
          },
          inheritor:{
            required:true
          },
          inheritor_relationship:{
            required:true
          },
          id_number:{
            required:true
          },
          id_expiry_date:{
            required:true
          },
          nationality:{
            required:true
          },
          source_of_income:{
            required:true
          },
          source_of_fund:{
            required:true
          },
          gross_income:{
            required:true
          },
          inv_objective:{
            required:true
          },
          correspondence_address:{
            required:true
          },
          correspondence_city:{
            required:true
          },
          correspondence_postal_code:{
            required:true
          },
          correspondence_country:{
            required:true
          },
          id_type:{
            required:true
          },
          residency_country:{
            required:true
          }
        },
          messages: {
              firstname: {
            required: "<?php echo $errors_firstname; ?>"
          },
          lastname: {
            required: "<?php echo $errors_lastname; ?>"
          },
          domicile_address:{
            required: "<?php echo $errors_domicile_address; ?>"
          },
          domicile_city:{
            required: "<?php echo $errors_domicile_city; ?>"
          },
          domicile_postal_code:{
            required: "<?php echo $errors_domicile_postal_code; ?>"
          },
          domicile_country:{
            required: "<?php echo $errors_domicile_country; ?>"
          },
          tax_id_no:{
            required: "<?php echo $errors_tax_id_no; ?>"
          },
          tax_id_reg_date:{
            required: "<?php echo $errors_tax_id_reg_date; ?>"
          },
          phone_no:{
            required: "<?php echo $errors_phone_no; ?>"
          },
          mobile_phone_no:{
            required: "<?php echo $errors_mobile_phone_no; ?>"   
          },
          email:{
            required: "<?php echo $errors_email_required; ?>",
            email:"<?php echo $errors_email_valid; ?>"
          },
          bank_account_name:{
            required: "<?php echo $errors_bank_account_name; ?>"  
          },
          bank_name:{
            required: "<?php echo $errors_bank_name; ?>"
          },
          bank_account_no:{
            required: "<?php echo $errors_bank_account_no; ?>"
          },
          place_of_birth:{
            required: "<?php echo $errors_place_of_birth; ?>"
          },
          date_of_birth:{
            required: "<?php echo $errors_date_of_birth; ?>"
          },
          gender:{
            required: "<?php echo $errors_gender; ?>"
          },
          marital_status:{
            required: "<?php echo $errors_marital_status; ?>"
          },
          religion:{
            required: "<?php echo $errors_religion; ?>"
          },
          company_name:{
            required: "<?php echo $errors_company_name; ?>"
          },
          education:{
            required: "<?php echo $errors_education; ?>"
          },
          mother_name:{
            required: "<?php echo $errors_mother_name; ?>"
          },
          inheritor:{
            required: "<?php echo $errors_inheritor; ?>"
          },
          inheritor_relationship:{
            required: "<?php echo $errors_inheritor_relationship; ?>"
          },
          id_number:{
            required: "<?php echo $errors_id_number; ?>"
          },
          id_expiry_date:{
            required: "<?php echo $errors_id_expiry_date; ?>"
          },
          nationality:{
            required: "<?php echo $errors_nationality; ?>"
          },
          source_of_income:{
            required: "<?php echo $errors_source_of_income; ?>"
          },
          source_of_fund:{
            required: "<?php echo $errors_source_of_fund; ?>"
          },
          gross_income:{
            required: "<?php echo $errors_gross_income; ?>"
          },
          inv_objective:{
            required: "<?php echo $errors_inv_objective; ?>"
          },
          correspondence_address:{
            required: "<?php echo $errors_correspondence_address; ?>"
          },
          correspondence_city:{
            required: "<?php echo $errors_correspondence_city; ?>"
          },
          correspondence_postal_code:{
            required: "<?php echo $errors_correspondence_postal_code; ?>"
          },
          correspondence_country:{
            required: "<?php echo $errors_correspondence_country; ?>"
          },
          id_type:{
            required:"<?php echo $errors_id_type; ?>"
          },
          residency_country:{
            required:"<?php echo $errors_residency_country; ?>"
          }
          }
      });
    }
  });
  </script>
<script type="text/javascript">
  ispend = '<?php echo $ispending ?>';
  if (ispend!='0') {
    $('#AccountFrm input').attr('disabled','disabled');
    $('#AccountFrm select').attr('disabled','disabled');
    $('#AccountFrm textarea').attr('disabled','disabled');
  };
  $('#AccountFrm_domicile_country').change( function(){
      $('select[name=\'domicile_city\']').load('index.php?rt=common/zone&country_id=' + $(this).val() + '&zone_id=<?php echo $zone_id; ?>');
  });
  //$('select[name=\'domicile_city\']').load('index.php?rt=common/zone&country_id='+ $('#AccountFrm_domicile_country').val() +'&zone_id=<?php echo $zone_id; ?>');

  $('#AccountFrm_correspondence_country').change( function(){
      $('select[name=\'correspondence_city\']').load('index.php?rt=common/zone&country_id=' + $(this).val() + '&zone_id=<?php echo $zone_id; ?>');
  });
  //$('select[name=\'correspondence_city\']').load('index.php?rt=common/zone&country_id='+ $('#AccountFrm_correspondence_country').val() +'&zone_id=<?php echo $zone_id; ?>');
</script>
<style type="text/css">
.help-block{
  color: #d23f34;  
}
.marital,.gender{
  margin-bottom: 0;
}
#marital,#gender{
  margin-bottom: 15px;
}
</style>