<div class="heading">
  <div class="container">
    <h2 class="animated fadeInUp delayp1"><?php echo $heading_title; ?></h2>
    <p class="animated fadeInUp delayp1"></p>
  </div>
</div><!--.heading-->

<div class="container">

  <div class="row">
    <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <?php if ($success) { ?>
      <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="right: 0">&times;</button>
        <strong>Success!</strong> <?php echo $success; ?>
      </div>
      <?php } ?>
      <?php if ($error_warning) { ?>
      <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="right: 0">&times;</button>
        <?php echo $error_warning; ?>
      </div>
      <?php } ?>

      

      <div class="thumb">
        <div class="content p_30">
          <?php echo $form['form_open']; ?>
            <h3 class="h4 m_b_30 m_t_5"><?php echo $text_acc_detail; ?></h3>
              <?php
                $field_list = array();
                array_push($field_list,'email','password', 'confirm');
                foreach ($field_list as $field_name) {
              ?>
                <div class="form-group <?php echo ${'error_'.$field_name} ? 'has-error' : ''; ?>">
                  <label class="col-sm-3 control-label"><?php echo ${'entry_'.$field_name}; ?></label>
                  <div class="col-sm-9">
                    <?php echo $form[$field_name]; ?>
                    <p class="help-block"><?php echo ${'error_'.$field_name}; ?></p>
                  </div>
                </div>  
              <?php
                }
              ?>
            <h3 class="h4 m_b_30" style="margin-top: 50px"><?php echo $text_your_details; ?></h3>
              <?php
                $field_list = array();
                array_push($field_list,'cif','firstname', 'lastname','telephone','id_type','id_number');
                foreach ($field_list as $field_name) {
              ?>
                <div class="form-group <?php echo ${'error_'.$field_name} ? 'has-error' : ''; ?>">
                  <label class="col-sm-3 control-label"><?php echo ${'entry_'.$field_name}; ?></label>
                  <div class="col-sm-9">
                    <?php echo $form[$field_name]; ?>
                    <p class="help-block"><?php echo ${'error_'.$field_name}; ?></p>
                  </div>
                </div>  
              <?php
                }
              ?>
            <h3 class="h4 m_b_30" style="margin-top: 50px"><?php echo $text_bank; ?></h3>
              <?php
                $field_list = array();
                array_push($field_list,'bank_name','bank_account_name', 'bank_account_no');
                foreach ($field_list as $field_name) {
              ?>
                <div class="form-group <?php echo ${'error_'.$field_name} ? 'has-error' : ''; ?>">
                  <label class="col-sm-3 control-label"><?php echo ${'entry_'.$field_name}; ?></label>
                  <div class="col-sm-9">
                    <?php echo $form[$field_name]; ?>
                    <p class="help-block"><?php echo ${'error_'.$field_name}; ?></p>
                  </div>
                </div>  
              <?php
                }
              ?>
              <input type="hidden" name="bank_nm" id="bank_nm">
              <?php echo $this->getHookVar('customer_attributes'); ?>
            <div class="form-group m_b_30">
              <div class="col-sm-9 col-sm-offset-3">
                <div class="checkbox" style="padding-left: 10px">
                  <?php if ($text_agree) { ?>
                       <label>
                        <?php echo $form['agree']; ?> <?php echo $text_agree; ?> <a data-toggle="modal" href="#privacypolicy"><?php echo $text_agree_href_text; ?></a>
                      </label>

                  <?php } ?>
                </div>
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-12">
                <button type="submit" class="btn btn-primary" style="width: 150px; display:block; margin: auto" title="<?php echo $form['continue']->name ?>"><?php echo $text_register; ?></button>
              </div>
            </div>

          </form>

          <p class="text-center"><?php echo $text_account_already ?> </p>
        </div><!--.content-->
      </div><!--.thumb-->

    </div><!--.col-->
  </div><!--.row-->

</div><!--.container-->

</div>



<script type="text/javascript">
  $(document).ready(function(){
    function openModalRemote(id, url){
      $(id).modal({remote: url});
    }
    var input2 = '<input value="" placeholder="" class="form-control">';
    $('#AccountFrm input[type="text"]').addClass('form-control');
    $('#AccountFrm input[type="password"]').addClass('form-control');
    $('#AccountFrm select').addClass('form-control');
    $('#AccountFrm').addClass('form-horizontal');
    
    $('.required').hide();
    $("#AccountFrm").submit(function(){
      var publickey = "<?php echo publicKeyToHex(PRIVATE_KEY)?>";
      var rsakey = new RSAKey();
      rsakey.setPublic(publickey, "10001");
      var enc_pass = rsakey.encrypt($('#AccountFrm_password').val());
      var enc_confirm = rsakey.encrypt($('#AccountFrm_confirm').val());

      var bank_name = rsakey.encrypt($('#AccountFrm_bank_name').val());
      var acc_name = rsakey.encrypt($('#AccountFrm_bank_account_name').val());
      var acc_no = rsakey.encrypt($('#AccountFrm_bank_account_no').val());

      $('#AccountFrm_password').val(enc_pass);
      $('#AccountFrm_confirm').val(enc_confirm);

      $('#bank_nm').val(bank_name);
      $('#AccountFrm_bank_account_name').hide();
      $('#AccountFrm_bank_account_name').after(input2);
      $('#AccountFrm_bank_account_name').val(acc_name);
      $('#AccountFrm_bank_account_no').hide();
      $('#AccountFrm_bank_account_no').after(input2);
      $('#AccountFrm_bank_account_no').val(acc_no);

      return;
    });
    
  });
  </script>
