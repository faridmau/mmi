<?php include($this->templateResource('/template/blocks/account.tpl')) ?>
<div class="heading hidden-xs">
  <div class="container">
    <h2 class="animated fadeInUp delayp1"><?php echo $heading_title_order; ?></h2>
    <p class="animated fadeInUp delayp1"></p>
  </div>
</div><!--.heading-->

<div class="container">
  <div class="clearfix">
    
    <button class="btn btn-default btn-print pull-right export-pdf" style="margin-bottom: 10px;" data-href="<?php echo $export_url;?>"><span class="glyphicon glyphicon-file"></span>&nbsp;<?php echo $text_export_pdf; ?></button>
  </div>

    <div class="thumb orderinfo">
      <div class="content p_15 m_b_10">
        <div class="row">
          <div class="col-sm-6">
            <p class=""><span class="cl-aaa typo-light"><?php echo $text_status; ?>&nbsp;</span> <strong class="cl-green"><?php echo $order['status']; ?></strong></p>
            <p><span class="cl-aaa typo-light"><?php echo $text_email; ?>&nbsp;</span> <?php echo $order['email']; ?></p>
            <p class="orderinfo-last"><span class="cl-aaa typo-light"><?php echo $text_date_added_only; ?>&nbsp;</span> <?php echo $order['date_added']; ?></p>
          </div>
          <div class="col-sm-6">
            <p class=""><span class="cl-aaa typo-light"><?php echo $text_telephone; ?>&nbsp;</span> <?php echo $order['telephone']; ?></p>
            <p><span class="cl-aaa typo-light"><?php echo $text_payment; ?>&nbsp;</span> <?php echo $order['payment_method']; ?></p>
            <p class=""><span class="cl-aaa typo-light"><?php echo $text_order_comment; ?>&nbsp;</span> <?php echo $order['comment']; ?></p>
          </div>
        </div>
      </div>
    </div>

    <div class="thumb">
      <div class="content p_15 m_b_10">
        <div class="">
          <form>
            
            <table class="table">
              <thead>
                <tr class="hidden-xs">
                  <th class="" width="15%"><?php echo $text_image; ?></th>
                  <th class="" width="25%"><?php echo $text_name; ?></th>
                  <th class="text-right" width="20%"><?php echo $text_subscription; ?></th>
                  <th class="text-right" width="20%"><?php echo $text_fee_ammount; ?></th>
                  <th class="text-right" width="20%"><?php echo $text_total_only; ?></th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($products as $key => $product): ?>
                   <tr>
                    <td class="hidden-xs"><a href="<?php echo $prefix;?>product/index.php"><img class="img-responsive" src="<?php echo $product['thumb']['thumb_url']; ?>" width="150"></a></td>
                    <td class="hidden-xs"><a href="<?php echo $prefix;?>product/index.php"><?php echo $product['name']; ?></a></td>
                    <td class="hidden-xs text-right"><?php echo $product['quantity']; ?></td>
                    <td class="hidden-xs text-right"><?php echo $product['fee_amount']; ?></td>
                    <td class="hidden-xs text-right"><strong><?php echo $product['total']; ?></strong></td>

                    <!--TBODY FOR MOBILE-->
                    <td class="m-orderhistory visible-xs">
                      <img class="pull-left img-responsive m_b_10" src="<?php echo $prefix;?>assets/img/img_product-2.jpg" width="100">
                      <p class="" style="padding-left: 115px"><span class="m-caption"><?php echo $text_name; ?>&nbsp;&nbsp;</span><a href=""><?php echo $product['name']; ?></a></p>
                      <p class="" style="padding-left: 115px"><span class="m-caption"><?php echo $text_subscription; ?>&nbsp;&nbsp;</span> <?php echo $product['quantity']; ?></p>
                      <p class="" style="padding-left: 115px"><span class="m-caption"><?php echo $text_fee_ammount; ?>&nbsp;&nbsp;</span> <?php echo $product['fee_amount']; ?></p>
                      <p class="" style="padding-left: 115px"><span class="m-caption"><?php echo $text_total_only; ?>&nbsp;&nbsp;</span> <strong><?php echo $product['total']; ?></strong></p>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
              
              <tfoot>

                <!--TFOOT FOR DESKTOP-->
                <tr class="hidden-xs" >
                  <td colspan="3" style="text-align: right"><?php echo $text_total_subs; ?></td>
                  <td colspan="2" style="text-align: right"><strong><?php echo $order['total_subscription']; ?></strong></td>
                </tr>
                <tr class="hidden-xs" >
                  <td colspan="3" style="text-align: right"><?php echo $text_total_fee; ?></td>
                  <td colspan="2" style="text-align: right"><strong><?php echo $order['total_fee_amount']; ?></strong></td>
                </tr>
                <tr class="hidden-xs" >
                  <td colspan="3" style="text-align: right"><?php echo $text_total_only; ?></td>
                  <td colspan="2" class="font14 tfoot-total" style="text-align: right"><strong><?php echo $order['total']; ?></strong></td>
                </tr>
                <!--<td class="hidden-xs text-right"><strong>Rp200.000</strong></td>
                <td class="hidden-xs text-right"><strong>Rp4.000</strong></td>
                <td class="hidden-xs text-right tfoot-total"><strong>Rp204.000</strong></td>-->

                <!--TFOOTER FOR MOBILE-->
                <tr>
                  <td class="visible-xs">
                    <p class="visible-xs clearfix">
                      <span class="m-caption typo-light cl-aaa"><?php echo $text_total_subs; ?>&nbsp;&nbsp;</span> 
                      <span class="font14 pull-right typo-light"><?php echo $order['total_subscription']; ?></span>
                    </p>
                    <p class="visible-xs clearfix">
                      <span class="m-caption typo-light cl-aaa"><?php echo $text_total_fee ?>&nbsp;&nbsp;</span> 
                      <span class="font14 pull-right typo-light"><?php echo $order['total_fee_amount']; ?></span>
                    </p>
                    <p class="visible-xs clearfix">
                      <span class="m-caption typo-light cl-aaa"><?php echo $text_total_only; ?>&nbsp;&nbsp;</span> 
                      <strong class="font14 pull-right tfoot-total"><?php echo $order['total']; ?></strong>
                    </p>
                  </td>
                </tr>
              </tfoot>

            </table>
          </form>
        </div>

      </div><!--.content-->
    </div><!--.thumb-->

    <div class="btn-placeholder">          
      <a class="btn btn-default pull-right" href="<?php echo $back_url; ?>"><?php echo $button_back; ?></a>
    </div>
    </div><!--.container-->

    <script type="text/javascript">
      $(function(){
        $('.export-pdf').on('click',function(e){
          e.preventDefault();
          var m_names = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
          var currentTime = new Date();
          var year = (currentTime.getFullYear()).toString().substr(2,2);
          var month = m_names[currentTime.getMonth()];
          var date = currentTime.getDate();

          var hours = currentTime.getHours() < 10 ? '0'+currentTime.getHours() : currentTime.getHours();
          var minutes = currentTime.getMinutes() < 10 ? '0'+currentTime.getMinutes() : currentTime.getMinutes();
          var seconds = currentTime.getSeconds() < 10 ? '0'+currentTime.getSeconds() : currentTime.getSeconds();

          var selected_date = date + "-" + month + "-" + year + " " + hours + ":" + minutes + ":" + seconds;

          location.href = $(this).data('href') + '&date_print=' + selected_date;
        });
      });
    </script>