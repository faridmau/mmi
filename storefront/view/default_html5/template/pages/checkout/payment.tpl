<div class="heading">
  <div class="container">
    <div class="row order-steps">
      <div class="col-xs-3">
        <div class="step-item clearfix">
          <div class="ic-step"><span class="glyphicon glyphicon-ok"></span></div>
          <p class="hidden-xs"><?php echo $entry_step_chart ?></p>
        </div>
      </div>
      <div class="col-xs-3">
        <div class="step-item active clearfix">
          <div class="ic-step">2</div>
          <p class="hidden-xs"><?php echo $entry_step_pay; ?></p>
        </div>
      </div>
      <div class="col-xs-3">
        <div class="step-item clearfix">
          <div class="ic-step">3</div>
          <p class="hidden-xs"><?php echo $entry_confirm; ?></p>
        </div>
      </div>
      <div class="col-xs-3">
        <div class="step-item clearfix">
          <div class="ic-step">4</div>
          <p class="hidden-xs"><?php echo $entry_step_proccess; ?></p>
        </div>
      </div>
      <div class="col-xs-3">
        <div class="step-item clearfix">
          <div class="ic-step">5</div>
          <p class="hidden-xs"><?php echo $entry_finish; ?></p>
        </div>
      </div>
    </div>

    <h2 class="animated fadeInUp delayp1"><?php echo $heading_title; ?></h2>
    <p class="animated fadeInUp delayp1"><?php echo $text_note; ?></p>
  </div>
</div><!--.heading-->

<div class="container">

  <div class="row">

    <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">

      <div class="thumb">

        <div class="content p_20">

			<?php if ($success) { ?>
			
			<div class="alert alert-success alert-dismissable">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="right: 0">×</button>
		        <?php echo $success; ?>
		      </div>
			<?php } ?>

			<?php if ($error_warning) { ?>
			<div class="alert alert-danger alert-dismissable">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="right: 0">×</button>
		        <?php echo $error_warning; ?>
		      </div>
			<?php } ?>


			<?php 
			if ($coupon_status) {
				echo $coupon_form;
			}
		    if ($balance) { ?>
				<h4 class="heading4"><?php echo $text_balance; ?></h4>
		    	<div class="registerbox">
					<table class="table table-striped table-bordered">
						<tr><td><?php echo $balance;?>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $apply_balance_button; ?></td></tr>
					</table>
				</div>

		    <?php } ?>

			<?php echo $this->getHookVar('payment_extensions_pre_hook'); ?>

			<?php echo $form['form_open'];?>	

	<?php if( $payment_methods ) { ?>			
		<div class="registerbox">		
	        <table class="table table-striped table-bordered">
				<?php echo $this->getHookVar('payment_extensions_pre_payments_hook'); ?>

				<?php foreach ($payment_methods as $payment_method) { ?>
				<!-- payment item begin -->
				<div class="form-group row clearfix m_b_20">
		            <div class="col-xs-6">
		              <div class="radio">
		                <label for="payment_payment_method<?php echo $payment_method['id']; ?>">
		                  
	                  <?php echo $payment_method['radio']; ?>
	                  <?php $icon = $payment_method['icon'];
										if ( count ($icon) ) {  ?>
										<?php if ( is_file(DIR_RESOURCE . $icon['image']) ) { ?>
											<img style="width:100%" src="resources/<?php echo $icon['image']; ?>" title="<?php echo $icon['title']; ?>" />
											<?php } else if (!empty( $icon['resource_code'] )) { ?>
											<?php echo $icon['resource_code']; ?>
										<?php } } ?>
		                </label>
		              </div>
		            </div><!--.col-sm-4-->
		            <div class="col-xs-6">
		            	<label>
		            		<a data-toggle="modal" href="#howtoPay<?php echo $payment_method['id']; ?>"><?php echo $text_learn; ?> <?php echo $payment_method['title']; ?>?</a>
		            	</label>
		            </div><!--.col-sm-4--> 

		          </div><!--.form-group-->

		          <!-- payment item end -->

		          <!-- modal -->
				<div class="modal fade" id="howtoPay<?php echo $payment_method['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="howtoPay" aria-hidden="true">
				      <div class="modal-dialog">
				        <div class="modal-content">
				          <div class="modal-header">
				            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				            <h3 class="h4"><?php echo $text_pay; ?> </h3>
				          </div>
				          <div class="modal-body clearfix">
				            <h5 class="m_b_15"><?php echo ${'description_'.$payment_method['id']}; ?></h5>
				            <p class="m_b_20"><?php echo ${'content_'.$payment_method['id']}; ?></p>
							
				          </div>
				        </div><!-- /.modal-content -->
				      </div><!-- /.modal-dialog -->
				    </div><!-- /.modal -->

				<?php } ?>
				<?php echo $this->getHookVar('payment_extensions_post_payments_hook'); ?>


			</table>
		</div>
		<?php } ?>
		
		<?php echo $this->getHookVar('payment_extensions_hook'); ?>
		
		<?php echo $this->getHookVar('order_attributes'); ?>
		
		
		<div class="form-group m_b_10">
            <div class="checkbox">
              <label>
                <input type="checkbox" name="agree" id="payment_agree" value="1"><?php echo $text_terms; ?></a>
              </label>
            </div>
          </div>

          <div class="m_t_20">
          	<?php echo $this->getHookVar('buttons_pre'); ?>
			<?php// echo $buttons; ?>
			<button type="submit" class="btn btn-primary pull-right" id="payment-link">&nbsp;<?php echo $button_continue; ?></button>
            <a href="<?php echo $back; ?>" class="btn btn-default pull-right m_r_10">&nbsp;<?php echo $button_back; ?></a>
			<?php echo $this->getHookVar('buttons_post'); ?>
           
          </div>
		</form>

          </div><!--.form-group-->

        </div><!--.content-->
      </div><!--.thumb-->

    </div><!--.col-->
  </div><!--.row-->
</div><!--.container-->