<link href="<?php echo $this->templateResource('/stylesheet/ecash.css'); ?>" rel="stylesheet">

<div class="container">
      <div class="main-content">
        
        <div class="row-5">
          <div class="col-sm-3 hidden-xs">
            <div class="box clearfix">
              <div class="invoice">
                <div class="content">
                  <h3 style="margin-top: 0"><?php echo $pay_to; ?></h3>
                  <h2><?php echo $product['name']?></h2>

                  <h3><?php echo $description; ?></h3>
                  <p><?php echo $product['description']; ?></p>

                  <h3><?php echo $total_payment; ?></h3>
                  <p>Rp <?php echo $product['total_payment']; ?></p>
                </div><!--.content-->
              </div><!--.invoice-->
            </div><!--.box-->
          </div><!--.col-->
          <div class="box col-sm-9">
            <div class="login">
              <div class="head">
                <div class="logo"><img src="<?php echo $this->templateResource('/img/clickpaylogo.png'); ?>"></div>
              </div>
              <div class="midbar text-center p_t_5 m_b_20">STEP 2</div>
              <div class="main">
                  <p style="display: block"><?php echo $success_transaction; ?></p>

                <div class="visible-xs payment-info">
                  <span class="typo-light cl-aaa"><?php echo $pay_to; ?></span><br>
                  <?php echo $product['name']?><br>
                  <br>
                  <span class="typo-light cl-aaa"><?php echo $description; ?>  </span><br>
                  <?php echo $product['description']; ?><br>
                  <br>
                  <span class="typo-light cl-aaa"><?php echo $total_payment; ?></span><br>
                  Rp <?php echo $product['total_payment']; ?><br>
                </div>

                <div class="form-group clearfix">
                  <div class="col-xs-12 ">
                    <a href="<?php echo $finish_button;?>" class="btn btn-primary pull-right">OK</a>
                  </div>
                </div>

              </div><!--.main-->
            </div><!--.login-->

          </div><!--.col-->


        </div>

      </div>
    </div>