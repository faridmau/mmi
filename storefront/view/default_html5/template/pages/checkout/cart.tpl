	<div class="heading">
  <div class="container">
    <div class="row order-steps">
      <div class="col-xs-3">
        <div class="step-item active clearfix">
          <div class="ic-step">1</div>
          <p class="hidden-xs"><?php echo $text_shopping_cart; ?></p>
        </div>
      </div>
      <div class="col-xs-3">
        <div class="step-item clearfix">
          <div class="ic-step">2</div>
          <p class="hidden-xs"><?php echo $text_payment_method; ?></p>
        </div>
      </div>
      <div class="col-xs-3">
        <div class="step-item clearfix">
          <div class="ic-step">3</div>
          <p class="hidden-xs pay-confirm"><?php echo $text_confirmation; ?></p>
        </div>
      </div>
      <div class="col-xs-3">
        <div class="step-item clearfix">
          <div class="ic-step">4</div>
          <p class="hidden-xs pay-process">
            <?php echo $text_payment_processing; ?>
          </p>
        </div>
      </div>
      <div class="col-xs-3">
        <div class="step-item clearfix">
          <div class="ic-step">5</div>
          <p class="hidden-xs pay-confirm"><?php echo $text_finish; ?></p>
        </div>
      </div>
    </div>
    
    <h2 class="animated fadeInUp delayp1"><?php echo $text_shopping_cart; ?></h2>
    <p class="animated fadeInUp delayp1">Lorem ipsum dolor siamet</p>
  </div>
</div><!--.heading-->

<div class="container">

  <div class="thumb">
    <div class="content p_15 m_b_10">

		<?php if ($weight) { ?>
			<span class="subtext">(<?php echo $weight; ?>)</span>
		<?php } ?>

		<?php if ($success) { ?>
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?php echo $success; ?>
			</div>
		<?php } ?>

		<?php if (count($error_warning) > 0) {
			foreach ($error_warning as $error) {
				?>
				<div class="alert alert-error">
					<strong><?php echo $error; ?></strong>
				</div>
			<?php
			}
		} ?>
      <div class="shopcart">
        <?php echo $form['form_open']; ?>
        <div class="col-md-12 cart-head hidden-sm hidden-xs">
        	<!-- header -->
        	<div class="col-md-2"><?php echo $column_image; ?></div>
        	<div class="col-md-4"><?php echo $column_name; ?></div>
        	<div class="col-md-2 text-right"><?php echo $column_subscription; ?></div>
        	<div class="col-md-2 text-right"><?php echo $column_fee; ?></div>
          <div class="col-md-2 text-right"><?php echo $column_total; ?></div>
        </div>

          <!-- content form to be repeated -->
          <?php $class = 'odd'; ?>
					<?php foreach ($products as $product) { ?>
					<?php $pk = $product['key']; ?>
					<div class="col-md-12 cart-product">
						<div class="col-md-2"><a href="<?php echo $product['href']; ?>"><?php echo $product['thumb']['thumb_html']; ?></a></div>
	        	<div class="col-md-4 padding-mobile">
	        		<span class="text-mobile"><?php echo $column_name; ?></span>&nbsp;<a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
	        		<!-- reminder section begin -->
							<div class="form-group">
                <div class="checkbox typo-light">
                  <label>
                  	<?php if(!$cart[$pk]['reminder']) { ?>
                    	<input type="checkbox" id="cart-check-<?php echo $pk;?>" onClick="unDisabled('<?php echo $pk;?>')"><?php echo $text_reminder_topup; ?>
                  	<?php }else{ ?>
                  		<input type="checkbox" id="cart-check-<?php echo $pk;?>" onClick="reDisabled('<?php echo $pk;?>')" checked><?php echo $text_reminder_topup; ?>
                  	<?php } ?>
                  </label>
                </div>
                <div class="row-5">
                  <div class="col-sm-5">
                  	<?php if(!$cart[$pk]['reminder']) $disabled = "disabled"; ?>
                    <select class="form-control" onChange="remDate(<?php echo $pk;?>)" <?php echo $disabled;?> name="rem_date_<?php echo $pk;?>" id="cart-date-<?php echo $product['key'];?>">
                      <option selected disabled ><?php echo $text_every_date; ?></option>
			              
	                      <?php if ($this->config->get('storefront_language_id')==1): ?>
	                      	<?php for($j=1;$j<=28;$j++){ ?>
	                        	<option value="<?php echo $j; ?>" <?php echo selected($j,$cart[$pk]['reminder']['date']); ?>><?php echo $text_only_every; ?> <?php echo $j;?><?php 
				                      $days = substr("'".$j."'", strlen(intval($j)),1);
				                      if ($j>=4 && $j<=20) {
				                      echo 'th';
				                      }else if($j<4 || $j>20){
				                       // var_dump($days);
				                       switch ($days) {
					                      case '1': echo 'st'; break;
					                      case '2': echo 'nd'; break;
					                      case '3': echo 'rd'; break;
					                      default : echo 'th'; break;
					                      }
				                      }
				                      
				                      ?>
	                        		<?php echo $text_only_every_date; ?></option>
	                      	<?php } ?>	
	                      <?php else: ?>
	                      	<?php for($j=1;$j<=28;$j++){ ?>
	                        	<option value="<?php echo $j; ?>" <?php echo selected($j,$cart[$pk]['reminder']['date']); ?>><?php echo $text_only_every_date; ?> <?php echo $j;?></option>
	                      	<?php } ?>
	                      <?php endif ?>
                    </select>
                  </div>
                  <div class="col-sm-5">
                    <select class="form-control" onChange="remRepeat(<?php echo $product['key'];?>)" <?php echo $disabled;?> name="rem_repeat_<?php echo $product['key'];?>" id="rem-repeat-<?php echo $product['key'];?>">
                      <option selected disabled ><?php echo $text_how_many_times; ?></option>
			                  <?php for($j=1;$j<=99;$j++){ ?>
	                      <option value="<?php echo $j; ?>" <?php echo selected($j,$cart[$pk]['reminder']['repeat']); ?> ><?php echo $j;?> <?php echo ($j=='1')? $text_time:$text_times ?></option>
	                      <?php } ?>
                    </select>
                  </div>
                  <div class="col-xs-12 m_t_10 text-mobile">
                  	<?php echo $column_subscription; ?>
                  </div>
            			<div class="col-xs-12 m_t_10 hidden-sm hidden-xs">
            				<a href="<?php echo $this->html->getSecureURL('checkout/cart').'&remove_id='.$product['key']; ?>" style="color: #cc0000;"><span class="glyphicon glyphicon-remove"></span><?php echo $remove_item; ?></a>
            			</div>	
                </div>
              </div>
                <!-- reminder section END -->
              <?php if (!$product['stock']) { ?>
							<span style="color: #FF0000; font-weight: bold;">***</span>
							<?php } ?>
							<div>
								<?php foreach ($product['option'] as $option) { ?>
									-
									<small><?php echo $option['name']; ?> <?php echo $option['value']; ?></small><br/>
								<?php } ?>
							</div>
	        	</div><!-- name -->
	        	<div class="col-md-2 text-right padding-mobile"><input type="text" 
								id = "<?php echo $product['quantity']['id'];?>"
								name = "<?php echo $product['quantity']['name'];?>"
								value = "<?php echo $product['quantity']['value'];?>"
								class = "<?php echo $product['quantity']['style'];?>"
								<?php echo $product['quantity']['attr'];?>
							><span id="error_minimum-<?php echo $product['key']; ?>" class="minimum_error"></span></div>
	        	<div class="col-md-2 text-right padding-mobile"><span class="m-caption text-mobile"><?php echo $column_fee; ?></span><a href="<?php echo $product['href'] ;?>" target="_blank" class="fee-amount" id="fee-amount-<?php echo $product['key']; ?>">&nbsp;Rp. <?php echo $product['fee_amount']; ?></a></div>
	          <div class="col-md-2 text-right padding-mobile"><span class="m-caption text-mobile"><?php echo $column_total; ?></span><span class="control-label"><strong id="total-<?php echo $product['key']?>">&nbsp;<?php echo $product['total']; ?></strong></span></div>
	        	<div class="col-md-2 text-right padding-mobile">
      				<span class="m-caption text-mobile"><a href="<?php echo $this->html->getSecureURL('checkout/cart').'&remove_id='.$product['key']; ?>" style="color: #cc0000;"><span class="glyphicon glyphicon-remove"></span><?php echo $remove_item; ?></a></span>
      			</div>
	        </div>
					<?php } ?>

        <!-- footer -->
        <div id="cart-footer">
					<div class="col-md-12 cart-head">
	        	<div class="col-md-offset-6 col-md-2 text-right cart-footer-label"><?php echo $text_sub_total; ?><br/></div>
	        	<div class="col-md-offset-2 col-md-2 text-right cavrt-footer-value"><strong><label id="sub-total"></label></strong></div>
	        </div>
	        <div class="col-md-12 cart-head">
	        	<div class="col-md-offset-6 col-md-2 text-right cart-footer-label"><?php echo $total_fee; ?><br/></div>
	        	<div class="col-md-offset-2 col-md-2 text-right cart-footer-value fee-total"><strong><label id="fee-total"></label></strong></div>
	        </div>
	        <div class="col-md-12 cart-head">
	        	<div class="col-md-offset-6 col-md-2 text-right cart-footer-label"><?php echo $column_total; ?><br/></div>
	        	<div class="col-md-offset-2 col-md-2 text-right tfoot-total"><strong><label id="total"></label></strong></div>
	        </div>
	      </div>

      </div>

      <div class="m_t_20">
		<?php echo $this->getHookVar('pre_top_cart_buttons'); ?>
		<?php if ($form['checkout']) { ?>
			<a href="<?php echo $checkout; ?>" id="cart_checkout1" class="btn btn-primary pull-right"
			   title="<?php echo $button_checkout; ?>">
				<i class="icon-shopping-cart icon-white"></i>
				<?php echo $button_checkout; ?>
			</a>
			<a href="#" class="btn btn-default pull-right m_r_10"><?php echo $shop_again; ?></a>
		<?php } ?>
		<!-- <button title="<?php //echo $button_update; ?>" class="btn pull-right mr10" id="cart_update"
				value="<?php //echo $form['update']->form ?>" type="submit">
			<i class="icon-refresh"></i>
			<?php //echo $button_update; ?>
		</button> -->
		<?php echo $this->getHookVar('post_top_cart_buttons'); ?>
	</div>
	</form>
    </div><!--.content-->
  </div><!--.thumb-->

  

  <!--<div class="thumb">
    <div class="content p_15 m_b_10">
      <div class="row">
        <div class="col-sm-6">
          <p class=""><span class="cl-aaa typo-light">Date Added&nbsp;</span> 07/07/2014 06:10:10 AM</p>
        </div>
        <div class="col-sm-6">
          <p class=""><span class="cl-aaa typo-light">Customer's Order Comment&nbsp;</span> Follow instruction to make your mandiri e-cash</p>
        </div>
      </div>
    </div>
  </div>-->

</div><!--.container-->

<?php if ($estimates_enabled || $coupon_status) {
	$pull_side = 'pull-right';
	if ($estimates_enabled) {
		$pull_side = 'pull-left';
	}
	?>

	<div class="cart-info coupon-estimate container-fluid row-fluid">
		<?php if ($coupon_status) { ?>
			<div class="<?php echo $pull_side; ?> coupon">
				<table class="table table-striped ">
					<tr>
						<?php if ($coupon_status) { ?>
							<th class="align_center"><?php echo $text_coupon_codes ?></th>
						<?php } ?>
					</tr>
					<tr>
						<td>
							<?php
							if ($coupon_status) {
								echo $coupon_form;
							}
							?>
						</td>
					</tr>
				</table>
			</div>
		<?php
		}
		if ($estimates_enabled) { ?>
			<div class="pull-right estimate">
				<table class="table table-striped">
					<tr>
						<th class="align_center"><?php echo $text_estimate_shipping_tax ?></th>
					</tr>
					<tr>
						<td>
							<div class="registerbox">
								<?php echo $form_estimate['form_open']; ?>
								<div class="control-group">
									<label class="control-label"><?php echo $text_estimate_country; ?></label>

									<div class="controls">
										<?php echo $form_estimate['country_zones']; ?>
									</div>
								</div>
								<div class="form-inline">
									<label class="checkbox"><?php echo $text_estimate_postcode; ?></label>
									<?php echo $form_estimate['postcode']; ?>
									<button title="<?php echo $form_estimate['submit']->name; ?>" class="btn mr10"
											value="<?php echo $form_estimate['submit']->form ?>" type="submit">
										<i class="icon-check"></i>
										<?php echo $form_estimate['submit']->name; ?>
									</button>
								</div>
								<div class="form-inline shippings-offered mt20">
									<label class="control-label"><?php echo $text_estimate_shipments; ?></label>
									<label class="shipments"><?php echo $form_estimate['shippings']; ?></label>
								</div>
								</form>
							</div>
						</td>
					</tr>
				</table>
			</div>
		<?php } ?>

	</div>
<?php } ?>

<?php 
	function selected($a,$b){
		if($a==$b){
			echo " selected";
		}
	}
?>

<script type="text/javascript"><!--
	//Fazrin ~ function untuk thousand separator 
	//~~~~~~~~~~~~~~~ start ~~~~~~~~~~~~~~~~~~~~
	var subscribes = $(".subscribe-qty");
	var total_subscribes = subscribes.length;
	
	function addCommas(x) {
	  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}

	function removeCommas(x) {
	  return x.toString().replace(/,/g, "");
	}
	
	for(var c=0;c<total_subscribes;c++){
		var value_to_convert = $(subscribes[c]).val();
		$(subscribes[c]).val(addCommas(value_to_convert));
	}

	$(".subscribe-qty").focusout(function(){
		var value = $(this).val();
		$(this).val(addCommas(value));
	});

	$(".subscribe-qty").focusin(function(){
		var value = $(this).val();
		$(this).val(removeCommas(value));
	});

	//~~~~~~~~~~~~~~~ end ~~~~~~~~~~~~~~~~~~~~

	//Fazrin ~ function untuk reminder date
	//~~~~~~~~~~~~~~~~~~~~~~~~~ start ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	function remDate(id,val){
		var date_reminder = $('#cart-date-'+id).val();
		var data = {
			'product_id' : id,
			'date' : date_reminder,
			// 'fee_amount' : fee_amount
		};

		$.ajax({
				type: 'POST',
				url: 'index.php?rt=r/checkout/cart/addReminderSession',
				data: data,
				success: function (data) {
					console.log('reminder added to session');					
				},
				error: function(data){
					console.log('error '+data);
				}
			});
	}

	function remRepeat(id,val){
		var repeat = $('#rem-repeat-'+id).val();
		var data = {
			'product_id' : id,
			'repeat' : repeat,
			// 'fee_amount' : fee_amount
		};

		$.ajax({
				type: 'POST',
				url: 'index.php?rt=r/checkout/cart/addReminderSession',
				data: data,
				success: function (data) {
					console.log('reminder added to session');					
				},
				error: function(data){
					console.log('error '+data);
				}
			});
	}

	function removeRemiderSession(i){
		var data = {
			'product_id' : i,
		};

		$.ajax({
				type: 'POST',
				url: 'index.php?rt=r/checkout/cart/removeReminderSession',
				data: data,
				success: function (data) {
					console.log('reminder removed from session');					
				},
				error: function(data){
					console.log('error men'+data);
				}
			});
	}
	//~~~~~~~~~~~~~~~~~~~~~~~~~ end ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		
	function unDisabled(i){
		
	   //var asd = $('#cart-check-'+i).prop('checked');
	   
	   $('#cart-date-'+i).removeAttr('disabled');
	   $('#rem-repeat-'+i).removeAttr('disabled');
	   $('#cart-check-'+i).removeAttr('onclick');
	   $('#cart-check-'+i).attr('onclick', 'reDisabled('+i+')');
	   //$('#cart-update-'+i).removeClass('hidden');
	   
	   unDisabledM(i);
	   
	}
	
	
	function reDisabled(i){
		 removeRemiderSession(i);
	   //var asd = $('#cart-check-'+i).prop('checked');
	   
	   $('#cart-date-'+i).attr('disabled', 'disabled');
	   $('#rem-repeat-'+i).attr('disabled', 'disabled');
	   $('#cart-check-'+i).removeAttr('onclick');
	   $('#cart-check-'+i).attr('onclick', 'unDisabled('+i+')');
	   //$('#cart-update-'+i).addClass('hidden');
	   
	   reDisabledM(i);
	   
	}
	
	
	/* --- MOBILE --- */
	function unDisabledM(i){
		
	   //var asd = $('#cart-check-'+i).prop('checked');
	   
	   $('#cart-date-mobile-'+i).removeAttr('disabled');
	   $('#rem-repeat-mobile-'+i).removeAttr('disabled');
	   $('#cart-check-mobile-'+i).removeAttr('onclick');
	   $('#cart-check-mobile-'+i).attr('onclick', 'reDisabled('+i+')');
	   //$('#cart-update-mobile-'+i).removeClass('hidden');
	   
	}
	
	
	function reDisabledM(i){
		
	   //var asd = $('#cart-check-'+i).prop('checked');
	   
	   $('#cart-date-mobile-'+i).attr('disabled', 'disabled');
	   $('#rem-repeat-mobile-'+i).attr('disabled', 'disabled');
	   $('#cart-check-mobile-'+i).removeAttr('onclick');
	   $('#cart-check-mobile-'+i).attr('onclick', 'unDisabled('+i+')');
	   //$('#cart-update-mobile-'+i).addClass('hidden');
	   
	}

	function showUpdate(i){
	   var value   = $('#cart-subscription-'+i).val();
	   var curr    = $('#cart-subscription-hidden-'+i).val();
	   
	   //alert('Value: '+value+' & Curr: '+curr);
	   
	   if(value == curr){
	      $('#cart-update-'+i).addClass('hidden');
	      $('#cart-update-mobile-'+i).addClass('hidden');
		  //alert('Atas');
	   }else{
	      $('#cart-update-'+i).removeClass('hidden');
	      $('#cart-update-mobile-'+i).removeClass('hidden');
		  //alert('Bawah');
	   }
	   
	}
	
	
	function showUpdateMobile(i){
	   var value   = $('#cart-subscription-mobile-'+i).val();
	   var curr    = $('#cart-subscription-mobile-hidden-'+i).val();
	   
	   //alert('Value: '+value+' & Curr: '+curr);
	   
	   if(value == curr){
	      $('#cart-update-'+i).addClass('hidden');
	      $('#cart-update-mobile-'+i).addClass('hidden');
		  //alert('Atas');
	   }else{
	      $('#cart-update-'+i).removeClass('hidden');
	      $('#cart-update-mobile-'+i).removeClass('hidden');
		  //alert('Bawah');
	   }
	   
	}
	
	
	$('.custom-date').each(function(index) {
	   
	   $(function() {
	      $( "#cart-date-"+index ).datepicker({
	         altField:'#cart-date-'+index,
		     altFormat: "d-M-y",
	      });
	   });
	   
    });
	
	$('.custom-subs-large').each(function(index) {
	   $('#cart-subscription-'+index).focusout(function(e) {
	      $('#cart-subscription-'+index).val($('#cart-subscription-hidden-'+index).val());
	   });
	   
	   
	   $('#cart-subscription-mobile-'+index).focusout(function(e) {
	      $('#cart-subscription-mobile-'+index).val($('#cart-subscription-mobile-hidden-'+index).val());
	   });
    });
	
		jQuery(function ($) {
			
			display_shippings();

			$('#estimate_country_zones').change(function () {
				//zone is changed, need to reset poscode
				$("#estimate input[name=\'postcode\']").val('')
				display_shippings();
			})

			$('#shippings').on("change", function () {
				display_totals();
			})

			$('#estimate').submit(function () {
				display_shippings();
				return false;
			});
		});

		/* fazrin */
		var typingTimer;                //timer identifier
		var doneTypingInterval = 1000;  //time in ms, 5 second for example

		function updateTotalSubscript(i){
			var str = $('#quantity-'+i).val();
			var fee_amount = $('#fee-amount-'+i);
			var total_product = $('#total-'+i);
			var dataForm = {
				'product_id' : i,
				'value' : str,
				// 'fee_amount' : fee_amount
			};

			$('#cart_checkout1').attr("disabled", true);

			clearTimeout(typingTimer);
    	typingTimer = setTimeout(
    	function(){
				$.ajax({
					type: 'POST',
					url: 'index.php?rt=r/checkout/cart/reCountTotal',
					data: dataForm,
					success: function (data) {
						var obj = jQuery.parseJSON(data);
						// $("#sub-total").html("Rp. "+obj.subtotal+".00");
						total_product.html("Rp. "+obj.total_product);
						fee_amount.html("Rp. "+obj.tax);
						// console.log(dataForm.value,obj.minimum);
						if( dataForm.value==0 || parseInt(dataForm.value,10)<parseInt(obj.minimum,10)){
							$('#cart_checkout1').attr("disabled", true);
							$("#error_minimum-"+dataForm.product_id).html("<?php echo $minimum_error; ?>"+obj.minimum+".");
						}else{
							$("#error_minimum-"+dataForm.product_id).html("");
						}
						// $('#fee-total').html("Rp. "+obj.total_fee);
						// $('#total').html("Rp. "+obj.grand_total);
						if($(".minimum_error").text()==""){
							$('#cart_checkout1').removeAttr("disabled");
						}else{
							$('#cart_checkout1').attr("disabled", true);
						}

						display_totals();
					},
					error: function(data){
						console.log('error men'+data);
					}
				})
			}, doneTypingInterval);
		}

		/*function reCount_totals(i){
			console.log('called');return;
			var str = $('#quantity-'+i).val();
			var data = {
				'product_id' : i,
				'value' : str
			};

			$.ajax({
				type: 'POST',
				url: 'index.php?rt=r/checkout/cart/reCountTotal',
				data: data,
				success: function (data) {
					console.log(data);
					$("#sub-total").html(data['subtotal']);
					$("#total").html(data['total']);
				},
				error: function(data){
					console.log('error men'+data);
				}
			});
		}*/
		
		function focusSubscription(i){
		   // var str = $('#quantity-'+i).val();
		   // // console.log(str);
		   // $('#quantity-'+i).val(str.replace(',', ''));
		   // $('#cart-subscription-mobile-'+i).val(str.replace(',', ''));
		}

		var  display_shippings = function() {
			var postcode = encodeURIComponent($("#estimate input[name=\'postcode\']").val());
			var country_id = encodeURIComponent($('#estimate_country').val());
			var zone_id = $('#estimate_country_zones').val();

			var replace_obj = $('.shippings-offered label.shipments');
			replace_obj;
			$.ajax({
				type: 'POST',
				url: 'index.php?rt=r/checkout/cart/change_zone_get_shipping_methods',
				dataType: 'json',
				data: 'country_id=' + country_id + '&zone_id=' + zone_id + '&postcode=' + postcode,
				beforeSend: function () {
					$(replace_obj).html('<div class="progress progress-striped active" style="width: 170px;"><div class="bar" style="width: 100%;"></div></div>');
				},
				complete: function () {
				},
				success: function (data) {
					$(replace_obj).html('');
					$('.shippings-offered label.control-label').hide();
					if (data && data.selectbox) {
						if (data.selectbox != '') {
							$(replace_obj).show();
							$('.shippings-offered label.control-label').show();
							$(replace_obj).css('visibility', 'visible');
							$(replace_obj).html(data.selectbox);
						}
					}
					display_totals();
				}
			});

		}

		//load total with AJAX call
		var display_totals = function () {
			var shipping_method = '';
			var coupon = encodeURIComponent($("#coupon input[name=\'coupon\']").val());
			shipping_method = encodeURIComponent($('#shippings :selected').val());
			if (shipping_method == 'undefined') {
				shipping_method = '';
			}
			$.ajax({
				type: 'POST',
				url: 'index.php?rt=r/checkout/cart/recalc_totals',
				dataType: 'json',
				data: 'shipping_method=' + shipping_method + '&coupon=' + coupon,
				beforeSend: function () {
					var html = '';
					html += '<tr>';
					html += '<td><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></td>';
					html += '</tr>';
					$('.cart-info.totals table#totals_table').html(html);
				},
				complete: function () {
				},
				success: function (data) {
					if (data && data.totals.length) {
						var html = '';
						for (var i = 0; i < data.totals.length; i++) {
							var grand_total = '';
							if (data.totals[i].id == 'total') {
								grand_total = 'totalamout';
							}
							html += '<tr>';
							html += '<td><span class="extra bold ' + grand_total + '">' + data.totals[i].title + '</span></td>';
							html += '<td><span class="bold ' + grand_total + '">' + data.totals[i].text + '</span></td>';
							html += '</tr>';

						}
						$('#sub-total').html(data.totals[0]['text']);
						$('#fee-total').html(data.totals[1]['text']);
						$('#total').html(data.totals[2]['text']);

						$('#mobile-sub-total').html(data.totals[0]['text']);
						$('#mobile-fee-total').html('-');
						$('#mobile-total').html(data.totals[1]['text']);
						//$('.cart-info.totals table#totals_table').html(html);
					}
				}
			});
		}

		var  show_error = function(parent_element, message) {
			var html = '<div class="alert alert-error">' + message + '</div>';
			$(parent_element).before(html);
		}

//--></script>