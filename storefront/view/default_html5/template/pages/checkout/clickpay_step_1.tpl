<link href="<?php echo $this->templateResource('/stylesheet/ecash.css'); ?>" rel="stylesheet">
<div class="container m_b_30">
      <div class="main-content">
        
        <div class="row-5">
          <div class="col-sm-3 hidden-xs">
            <div class="box clearfix">
              <div class="invoice">
                <div class="content">
                  <h3 style="margin-top: 0"><?php echo $pay_to; ?></h3>
                  <h2><?php echo $product['name']?></h2>

                  <h3><?php echo $description; ?></h3>
                  <p><?php echo $product['description']; ?></p>

                  <h3><?php echo $total_payment; ?></h3>
                  <p>Rp <?php echo $product['total_payment']; ?></p>
                </div><!--.content-->
              </div><!--.invoice-->
            </div><!--.box-->
          </div><!--.col-->
          <div class="box col-sm-9">
            <div class="login">
              <div class="head">
                <div class="logo"><img src="<?php echo $this->templateResource('/img/clickpaylogo.png'); ?>"></div>
              </div>
              <div class="main">

                <div class="visible-xs payment-info">
                  <span class="typo-light cl-aaa"><?php echo $pay_to; ?></span><br>
                  <?php echo $product['name']?><br>
                  <br>
                  <span class="typo-light cl-aaa"><?php echo $description; ?>  </span><br>
                  <?php echo $product['description']; ?><br>
                  <br>
                  <span class="typo-light cl-aaa"><?php echo $total_payment; ?></span><br>
                  Rp <?php echo $product['total_payment']; ?><br>
                </div>

                <form action="<?php echo $callback; ?>" method="POST" class="form-horizontal" role="form">
                  <div class="form-group">
                    <label class="col-sm-5 control-label" style="padding-top: 0; margin-top: -2px"><?php echo $insert_debit_number; ?></label>
                    <div class="col-sm-7">
                      <input type="text" name="debit_no" class="form-control">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label"><?php echo $token_response; ?></label>
                    <div class="col-sm-7">
                      <input type="text" name="response_token" class="form-control">
                    </div>
                  </div>

                  <p style="margin: 30px 0 20px"><?php echo $get_token_steps; ?></p>
                  <div class="form-group">
                    <label class="col-sm-5 control-label" style="padding-top: 0; margin-top: -2px"><?php echo $token_step_1; ?></label>
                    <div class="col-sm-7">
                      <input type="text" class="form-control" disabled value="3">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label" style="padding-top: 0; margin-top: -2px"><?php echo $token_step_2; ?></label>
                    <div class="col-sm-7">
                      <input type="text" class="form-control" disabled value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label" style="padding-top: 0; margin-top: -2px"><?php echo $token_step_3; ?></label>
                    <div class="col-sm-7">
                      <input type="text" class="form-control" disabled value="4291823">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-5 control-label" style="padding-top: 0; margin-top: -2px"><?php echo $token_step_4; ?></label>
                    <div class="col-sm-7">
                      <input type="text" class="form-control" disabled value="8617">
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                      <a href="#">
                      <button type="submit" class="btn btn-primary gradient pull-right"><?php echo $button_send; ?></button>
                      </a>
                      <a href="<?php echo $link_confirm; ?>" class="btn btn-default gradient pull-right m_r_10"><?php echo $button_cancel; ?></a>
                    </div>
                  </div>
                </form>

              </div><!--.main-->
            </div><!--.login-->

          </div><!--.col-->


        </div>

      </div>
    </div>