<link href="<?php echo $this->templateResource('/stylesheet/ecash.css'); ?>" rel="stylesheet">

<div class="container">
  <div class="main-content">
    
    <div class="row-5">
      <div class="col-sm-3 hidden-xs">
        <div class="box clearfix">
          <div class="invoice">
            <div class="content">
              <h3 style="margin-top: 0">Pembayaran ke</h3>
              <h2>Reksa Dana Mandiri Investa Dana Syariah</h2>

              <h3>Keterangan</h3>
              <p>Transaksi top up Reksa Dana Mandiri Investa Dana Syariah</p>

              <h3>Jumlah Pembayaran</h3>
              <p>Rp 510.000,00</p>
            </div><!--.content-->
          </div><!--.invoice-->
        </div><!--.box-->
      </div><!--.col-->
      <div class="box col-sm-9">
        <div class="login">
          <div class="head">
            <div class="logo"><img src="<?php echo $this->templateResource('/img/e-cashlogo.png'); ?>"></div>
          </div>
          <div class="midbar text-center p_t_5 m_b_20 visible-xs">STEP 3</div>
          <div class="midbar hidden-xs"></div>
          <div class="steps-container clearfix hidden-xs">
            <div class="col-md-4">
              <div class="steps active"><span class="glyphicon glyphicon-th"></span></div>
            </div>
            <div class="col-md-4">
              <div class="steps"> <span class="glyphicon glyphicon-phone"></span></div>
            </div>
            <div class="col-md-4">
              <div class="steps"><span class="glyphicon glyphicon-ok"></span></div>
            </div>
          </div>
          <div class="main">
              <p style="display: block">Transaksi berhasil! Terimakasih telah menggunakan mandiri e-cash. Klik tombol "OK" untuk melanjutkan proses pembelian.
            </p>

            <div class="visible-xs payment-info">
              <span class="typo-light cl-aaa">Pembayaran ke</span><br>
              Reksa Dana Mandiri Investa Dana Syariah<br>
              <br>
              <span class="typo-light cl-aaa">Keterangan</span><br>
              Transaksi top up Reksa Dana Mandiri Investa Dana Syariah<br>
              <br>
              <span class="typo-light cl-aaa">Jumlah Pembayaran</span><br>
              Rp510.000,00<br>
            </div>

            <div class="form-group clearfix">
              <div class="col-xs-12 ">
                <a href="<?php echo $confirm_page;?>" class="btn btn-primary pull-right">OK</a>
              </div>
            </div>

          </div><!--.main-->
        </div><!--.login-->

      </div><!--.col-->


    </div>

  </div>
</div>