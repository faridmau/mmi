<link href="<?php echo $this->templateResource('/stylesheet/ecash.css'); ?>" rel="stylesheet">

<div class="container">
  <div class="main-content">
    
    <div class="row-5">
      <div class="col-sm-3 hidden-xs">
        <div class="box clearfix">
          <div class="invoice">
            <div class="content">
              <h3 style="margin-top: 0">Pembayaran ke</h3>
              <h2>Reksa Dana Mandiri Investa Dana Syariah</h2>

              <h3>Keterangan</h3>
              <p>Transaksi top up Reksa Dana Mandiri Investa Dana Syariah</p>

              <h3>Jumlah Pembayaran</h3>
              <p>Rp 510.000,00</p>
            </div><!--.content-->
          </div><!--.invoice-->
        </div><!--.box-->
      </div><!--.col-->
      <div class="box col-sm-9">
        <div class="login">
          <div class="head">
            <div class="logo"><img src="<?php echo $this->templateResource('/img/e-cashlogo.png'); ?>" ></div>
          </div>
          <div class="midbar text-center p_t_5 m_b_20 visible-xs">STEP 1</div>
          <div class="midbar hidden-xs"></div>
          <div class="steps-container clearfix hidden-xs">
            <div class="col-md-4">
              <div class="steps active"><span class="glyphicon glyphicon-th"></span></div>
            </div>
            <div class="col-md-4">
              <div class="steps"> <span class="glyphicon glyphicon-phone"></span></div>
            </div>
            <div class="col-md-4">
              <div class="steps"><span class="glyphicon glyphicon-ok"></span></div>
            </div>
          </div>
          <div class="main">
            <p>Masukkan nomor ponsel e-cash &amp; PIN e-cash Anda 
            dan tekan tombol <b>kirim</b> untuk melanjutkan atau tekan tombol <b>batal</b> untuk kembali ke menu sebelumnya. 
            <span class="info">(Pastikan detail dan jumlah pembayaran benar.)</span></p>

            <div class="visible-xs payment-info">
              <span class="typo-light cl-aaa">Pembayaran ke</span><br>
              Reksa Dana Mandiri Investa Dana Syariah<br>
              <br>
              <span class="typo-light cl-aaa">Keterangan</span><br>
              Transaksi top up Reksa Dana Mandiri Investa Dana Syariah<br>
              <br>
              <span class="typo-light cl-aaa">Jumlah Pembayaran</span><br>
              Rp510.000,00<br>
            </div>

            <form action="<?php echo $prefix;?>payment/e-cash/otp.php" method="POST" class="form-horizontal" role="form">
              <div class="form-group">
                <label class="col-sm-3 control-label">No. Ponsel</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">PIN</label>
                <div class="col-sm-9">
                  <input type="password" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                  <a href="<?php echo $button_step2; ?>"><button type="button" class="btn btn-primary gradient pull-right">Kirim</button></a>
                  <button class="btn btn-default gradient pull-right">Batal</button>
                </div>
              </div>
            </form>

          </div><!--.main-->
        </div><!--.login-->

      </div><!--.col-->


    </div>

  </div>
</div>