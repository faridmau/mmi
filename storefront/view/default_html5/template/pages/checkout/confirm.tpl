<script type="text/javascript">
	$(document).ready(function(){
		$('.m-orderhistory img').addClass('pull-left img-responsive m_b_10');
		$('.pay-now-button button').addClass('btn btn-primary pull-right');
		$('.pay-now-button #back').addClass('btn btn-default pull-right m_r_10');

		$('.pay-now-button a#checkout').addClass('btn btn-primary pull-right');
		$('.pay-now-button a.btn.pull-left').addClass('btn btn-default pull-right m_r_10');	
	});
</script>
<div class="heading">
  <div class="container">
    <div class="row order-steps">
      <div class="col-xs-3">
        <div class="step-item clearfix">
          <div class="ic-step"><span class="glyphicon glyphicon-ok"></span></div>
          <p class="hidden-xs"><?php echo $text_shopping_cart; ?></p>
        </div>
      </div>
      <div class="col-xs-3">
        <div class="step-item clearfix">
          <div class="ic-step"><span class="glyphicon glyphicon-ok"></span></div>
          <p class="hidden-xs"><?php echo $text_payment_method; ?></p>
        </div>
      </div>
      <?php
      	$confirm = $payment_process = '';
      	if($transaction_status==0) {
      		$confirm = 'active';
      	}else if($transaction_status==2 || $transaction_status==1){
      		$payment_process = 'active';
      	}
      ?>
      <div class="col-xs-3">
        <div class="step-item <?php echo $confirm;?> clearfix">
          <div class="ic-step">
          	<?php if($transaction_status==2 || $transaction_status==1){ ?>
          			<span class="glyphicon glyphicon-ok"></span>
          		<?php }else{ ?>
          			3
          		<?php }?>
          	</div>
          <p class="hidden-xs"><?php echo $text_confirmation; ?></p>
        </div>
      </div>
      <div class="col-xs-3">
        <div class="step-item <?php echo $payment_process;?> clearfix">
          <div class="ic-step">4</div>
          <p class="hidden-xs"><?php echo $text_payment_process; ?></p>
        </div>
      </div>
      <div class="col-xs-3">
        <div class="step-item clearfix">
          <div class="ic-step">5</div>
          <p class="hidden-xs"><?php echo $text_finish; ?></p>
        </div>
      </div>
    </div>

    <?php if($transaction_status==2 || $transaction_status==1){ ?>
   			<h2 class="animated fadeInUp delayp1"><?php echo $text_payment_process; ?></h2>
    <?php }else{ ?>
        <h2 class="animated fadeInUp delayp1"><?php echo $heading_title; ?></h2>
    <?php } ?>
    <p class="animated fadeInUp delayp1"></p>
  </div>
</div><!--.heading-->

<div class="container">

  <div class="thumb orderinfo">
    <div class="content p_15 m_b_10">
    	<?php if ($success) { ?>
			
		<div class="alert alert-success alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="right: 0">×</button>
	        <?php echo $success; ?>
	      </div>
		<?php } ?>

		<?php if ($error_warning) { ?>
		<div class="alert alert-danger alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="right: 0">×</button>
	        <?php echo $error_warning; ?>
	      </div>
		<?php } ?>

		<?php
		if ($payment_method || $balance || $this->getHookVar('payment_method')) { ?>

			<div class="row">
		        <div class="col-xs-6">
		          <h3 class="h4 m_b_20 m_t_5"><?php echo $text_order; ?></h3>
		        </div>
		      </div>


				<?php if ($payment_method) { ?>

					<div class="row">
				        <div class="col-sm-6">
				          <p class="orderinfo-last"><span class="cl-aaa typo-light"><?php echo $text_date; ?></span> <?php echo date('d-M-y h:i:s A',strtotime($date_added)); ?> </p>
				          <p style="margin-bottom: 10px"><span class="cl-aaa typo-light"><?php echo $text_email; ?> </span> <?php echo $email; ?></p>
				        </div>
				        <div class="col-sm-6">
				          <p class=""><span class="cl-aaa typo-light"><?php echo $text_phone; ?></span> <?php echo $telephone; ?></p>
				          <p style="margin-bottom: 10px"><span class="cl-aaa typo-light"><?php echo $text_payment; ?></span> <?php echo $payment_method; ?></p>
				        </div>
				      </div>
				    </div>

					
					<!-- <a class="btn btn-mini" href="<?php echo $checkout_payment_edit; ?>">
						<i class="icon-edit"></i>
						<?php //echo $text_edit_payment; ?>
					</a> -->
						
				<?php }
				if($balance){?>
					<tr>
						<td class="align_left"><?php echo $balance;?></td>
						<td class="align_left">&nbsp;</td>
						<td class="align_left">&nbsp;</td>
						<td class="align_right">
							<?php if($disapply_balance){ ?>
							<a class="btn btn-mini" href="<?php echo $disapply_balance['href']; ?>">
								<i class="icon-edit"></i>
								<?php echo $disapply_balance['text']; ?>
							</a>
							<?php }?>
						</td>
					</tr>

				<?php }
				if($this->getHookVar('payment_method')){?>
					<tr>
						<td class="align_left"><?php echo $this->getHookVar('payment_method_title');?></td>
						<td class="align_left">&nbsp;</td>
						<td class="align_left">&nbsp;</td>
						<td class="align_right"><?php echo $this->getHookVar('payment_method'); ?></td>
					</tr>
				<?php }	?>

		<?php } ?>

      
  </div>

  <!--ORDER 1-->
  <div class="thumb">
    <div class="content p_15 m_b_10">
      <div class="row">
        <div class="col-xs-6">
          <h3 class="h4 m_b_20 m_t_5"><?php echo $text_order; ?></h3>
        </div>
        <div class="col-xs-6">
        	<?php if($transaction_status==0) { ?>
          <a href="<?php echo $cart; ?>" class="btn btn-default pull-right"><?php echo $text_edit_basket; ?></a>
        	<?php }?>
        </div>
      </div>

			<?php echo $form['form_open']; ?>
			<!-- <p><?php echo $text_accept_agree ?><a onclick="openModalRemote('#returnPolicyModal', '<?php echo $text_accept_agree_href; ?>'); return false;"
						href="<?php echo $text_accept_agree_href; ?>"><b><?php echo $text_accept_agree_href_link; ?></b></a></p>

		 -->
			
				<?php foreach ($products as $product) { ?>

				<div class="row">
			        <div class="col-sm-10">
			          <table class="table">
			            <thead>
			              <tr class="hidden-xs">
			                <th class="" width="15%"><?php echo $column_image; ?></th>
			                <th class="" width="25%"><?php echo $column_name; ?></th>
			                <th class="text-right" width="20%"><?php echo $column_subscription; ?></th>
			                <th class="text-right" width="20%"><?php echo $column_fee; ?></th>
			                <th class="text-right" width="20%"><?php echo $column_total; ?></th>
			              </tr>
			            </thead>
			            <tbody>
			              <tr>
			                
			                <td class="hidden-xs payment-confirm"><a href="<?php echo $product['href']; ?>"><?php echo $product['thumb']['thumb_html']; ?></a></td>
			                <td class="hidden-xs"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></td>
			                <td class="hidden-xs text-right"><?php echo $product['quantity'];?></td>
			                <td class="hidden-xs text-right"><?php echo $product['tax'];?></td>
			                <td class="hidden-xs text-right"><strong><?php echo $product['total']; ?></strong></td>

			                <!--TBODY FOR MOBILE-->
			                <td class="m-orderhistory visible-xs">
			                  <a href="<?php echo $product['href']; ?>"><?php echo $product['thumb']['thumb_html']; ?></a>
			                  <p class="" style="padding-left: 115px"><span class="m-caption"><?php echo $column_name; ?>&nbsp;&nbsp;</span><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></p>
			                  <p class="" style="padding-left: 115px"><span class="m-caption"><?php echo $column_subscription;?><?php echo $product['quantity'];?></span></p>
			                  <p class="" style="padding-left: 115px"><span class="m-caption"><?php echo $column_fee; ?><?php echo $product['tax'];?></span></p>
			                  <p class="" style="padding-left: 115px"><span class="m-caption"><?php echo $column_total; ?>&nbsp;&nbsp;</span> <strong><?php echo $product['total']; ?></strong></p>
			                </td>

			              </tr>
			            </tbody>
			          </table>
			        </div><!--.col-sm-10-->
				      <?php if($product['trx_status'] && $product['trx_status']=='in_process'){ ?>
				      <div class="col-sm-2">
				        <!-- if processing -->
	              <div class="pay-load">
	                <img src="<?php echo $this->templateResource('/img/loading-2.gif')?>">
	                <p><?php echo $text_processing; ?></p>
	              </div>
	            </div>
              <?php }else if($product['trx_status'] && $product['trx_status']=='SUCCESS') { ?>
              <!-- if paid -->
              <div class="col-sm-2">
	              <a href="#" class="btn btn-primary btn-pay btn-paid"><span class="glyphicon glyphicon-ok"></span>
	               &nbsp;<?php echo $text_success_paid; ?></a>
	            </div>
	            <?php }else if($product['trx_status'] && $product['trx_status']=='FAILED') { ?>
              <!-- if paid -->
              <div class="col-sm-2">
	              <a href="#" class="btn btn-primary btn-pay btn-paid"><span class="glyphicon glyphicon-ok"></span>
	               &nbsp;<?php echo $text_failed; ?></a>
	            </div>
	            <?php } ?>

              <!--<a href="<?php echo $prefix;?>payment/e-cash/index.php" class="btn btn-primary btn-pay">&nbsp;Pay Order 2</a>-->
			      </div><!--.row-->

				<?php } ?>
				<?php echo $this->getHookVar('list_more_product_last'); ?>
				

			<?php if ($comment) { ?>
				<h4 class="heading4"><?php echo $text_comment; ?></h4>
				<div class="container"><?php echo $comment; ?></div>
			<?php } ?>

			<?php echo $this->getHookVar('order_attributes'); ?>

			
    </div><!--.content-->
  </div><!--.thumb-->

  <?php if(!$transaction_status==2){ ?>
  
  <div class="thumb">
    <div class="content p_15">
      <table class="table">
        <tfoot>
        	
          <!--TFOOT FOR DESKTOP-->
          <?php foreach ($totals as $total) { ?>
			    	<tr class="hidden-xs" >
	            <td colspan="3" style="text-align: right"><?php echo $total['title']; ?></td>
	            <td colspan="2" style="text-align: right;" <?php if ( $total['title'] == 'Total:') echo 'class="font14 tfoot-total"'; ?> ><?php echo $total['text']; ?></td>
	          </tr>
		    	<?php } ?>

          <!--TFOOTER FOR MOBILE-->
          	<tr>
          		<td class="visible-xs">
		           <?php foreach ($totals as $total) { ?>
		              <p class="visible-xs clearfix">
		                <span class="m-caption typo-light cl-aaa"><?php echo $total['title']; ?></span>
				            <span class="font14 pull-right typo-light"><b><?php echo $total['text']; ?></span>
			  	        </p>
					    <?php } ?>
				    	</td>
            </tr>
            
        </tfoot>
      </table>
    </div>
  </div><!--.thumb-->
  
  <div class="m_t_20 pay-now-button">
    <!-- <button href="<?php echo $prefix;?>payment/e-cash/index.php" class="btn btn-primary pull-right" id="payment-link">&nbsp;Pay Now</button>
    <a href="<?php echo $prefix;?>order/shopping-cart.php" class="btn btn-default pull-right m_r_10">&nbsp;Back</a>
 -->
  <?php echo $this->getHookVar('payment_pre'); ?>
  <div class="pull-right">
		<?php echo $payment; ?>
	</div>
	<?php echo $this->getHookVar('payment_post'); ?>
  </div>

  <?php } ?>
  <?php if($transaction_status==1) { ?>
  		<a href="<?php echo $redirect_success; ?>" class="btn btn-primary pull-right m_r_10">DONE</a>
  <?php } ?>

</div><!--.container-->
<?php
// echo '==>'.$transaction_status; 
//code untuk redirect juga transaksi berhasil / masih ada
if($transaction_status==2){
	//redirect ke halaman payment
	// echo "==>".$redirect_payment_method;
	header( "Refresh:3; url=".$redirect_payment_method, true, 303);
}else if($transaction_status==1){
	$this->cart->clear();
	// echo "==>".$redirect_success;
	header( "Refresh:3; url=".$redirect_success, true, 303);
}
?>