
<div class="heading">
  <div class="container">
    <h2 class="animated fadeInUp delayp1"><?php echo $text_error; ?></h2>
    <p class="animated fadeInUp delayp1"></p>
  </div>
</div><!--.heading-->

<div class="container-fluid">

	<div class="control-group">
	    <div class="controls">
	    	<div class="span4 mt20 mb20"style="text-align:center">
	    		<?php 
	    			if (empty($continue)){
	    				$continue = $button_continue->href;
	    			} 
	    		?>	    	
	    		<a href="<?php echo $continue; ?>" class="btn btn-primary" title="<?php echo $button_continue->text ?>">
	    		    <i class="icon-arrow-right"></i>
	    		    <?php echo $button_continue->text ?>
	    		</a>
	    	</div>	
	    </div>
	</div>

</div>