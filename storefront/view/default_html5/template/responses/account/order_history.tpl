<?php foreach ($orders as $key => $order) { ?>
  <tr>
    <td class="td-no hidden-xs"><?php echo $order['no']; ?></td>
    <td class="td-date hidden-xs"><?php echo $order['date_added']; ?></td>
    <td class="td-orderno hidden-xs">#<?php echo $order['order_id']; ?></td>
    <td class="td-products hidden-xs"><?php echo $order['products']; ?></td>
    <td class="td-amount hidden-xs"><?php echo $order['total']; ?></td>
    <td class="td-status green hidden-xs"><strong><?php echo $order['status']; ?></strong></td>
    <td class="td-action hidden-xs"><a href="<?php echo $order['detail_link']; ?>" class="btn btn-primary">View Order</a></td>
    <td class="m-orderhistory visible-xs">
      <p><span class="m-caption"><?php echo $text_order; ?>&nbsp;&nbsp;</span><?php echo $order['order_id']; ?></p>
      <p><span class="m-caption"><?php echo $text_date; ?>&nbsp;&nbsp;</span> <?php echo $order['date_added']; ?></p>
      <p><span class="m-caption"><?php echo $text_amount; ?>&nbsp;&nbsp;</span> <?php echo $order['total']; ?></p>
      <p class="td-status green"><span class="m-caption"><?php echo $text_status; ?>&nbsp;&nbsp;</span> <strong><?php echo $order['status']; ?></strong></p>
      <a href="<?php echo $order['detail_link']; ?>" class="btn btn-primary m_t_10"><?php echo $text_view_order; ?></a>
    </td>
  </tr>
<?php } ?>