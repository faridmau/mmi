<?php foreach ($investment_summary as $key => $data): ?>
  <tr> 
    <td class="text-left"><?php echo $data['product_name']; ?></td>
    <td class="text-left"><?php echo $data['inv_account_no']; ?></td>
    <td class="text-right"><?php echo $data['beginning_balance']; ?></td>
    <td class="text-right"><?php echo $data['movement'] < 0 ? '<span class="cl-red">('.number_format(abs($data['movement']),4,".",",").')</span>' :  '<span class="cl-green">'.number_format(abs($data['movement']),4,".",",").'</span>' ; ?></td>
    <td class="text-right"><?php echo $data['ending_balance']; ?></td>
    <td class="text-right"><?php echo $data['nav_value']; ?></td>
    <td class="text-right"><?php echo $data['ending_balance_amount']; ?></td>
    <td class="text-right"><?php echo $data['total_realized_gl'] < 0 ? '<span class="cl-red">('.number_format(abs($data['total_realized_gl']),2,".",",").')</span>' :  '<span class="cl-green">'.number_format($data['total_realized_gl'],2,".",",").'</span>' ; ?></td>
    <td class="text-right">
      <?php echo $data['unrealized_gl'] < 0 ? '<span class="cl-red">('.number_format(abs($data['unrealized_gl']),2,".",",").')</span>' :  '<span class="cl-green">'.number_format($data['unrealized_gl'],2,".",",").'</span>' ; ?></td>
  </tr>
<?php endforeach; ?>
<?php if($investment_summary){ ?>
  <tr> 
    <td colspan="6" class="text-left"><?php echo $text_total; ?></td>
    <td class="text-right"><?php echo $total_ending_balance; ?></td>
    <td class="text-right"><?php echo $total_realized_gl < 0 ? '<span class="cl-red">('.number_format(abs($total_realized_gl),2,".",",").')</span>' :  '<span class="cl-green">'.number_format($total_realized_gl,2,".",",").'</span>' ; ?></td>
    <td class="text-right"><?php echo $total_unrealized_gl < 0 ? '<span class="cl-red">('.number_format(abs($total_unrealized_gl),2,".",",").')</span>' :  '<span class="cl-green">'.number_format($total_unrealized_gl,2,".",",").'</span>' ; ?></td>
  </tr>
<?php } ?>