<?php $i=0; $group = array(); ?>
<?php foreach ($transaction_detail as $key => $data): ?>
  <?php if($group[($i-1)] != $data['group']){ ?>

    <?php if($i && $data['group'] != $group[($i-1)]){ ?>
      <tr> 
        <td colspan="3" class="text-left"></td>
        <td colspan="2" class="text-left"><strong>Ending Balance</strong></td>
        <td class="text-right"><strong><?php echo $transaction_detail[($key-1)]['group_ending_balance']; ?></strong></td>
        <td class="text-right"></td>
        <td class="text-right"></td>
        <td class="text-right"><?php echo $transaction_detail[($key-1)]['realized_gl'] < 0 ? '<span class="cl-red">('.number_format(abs($investment_summary[$i]['realized_gl']),2,".",",").')</span>' :  '<span class="cl-green">'.number_format($investment_summary[$i]['realized_gl'],2,".",",").'</span>' ; ?></td>
      </tr>   
    <?php } ?>

    <tr>
      <td colspan="9">
        <strong><?php echo $data['product_name']; ?> - <?php echo $data['inv_account_no']; ?></strong>
      </td>
    </tr>
    <tr> 
      <td colspan="3" class="text-left"></td>
      <td colspan="2" class="text-left"><strong>Beginning Balance</strong></td>
      <td class="text-right"><strong><?php echo $data['group_begining_balance']; ?></strong></td>
      <td class="text-right"></td>
      <td class="text-right"></td>
      <td class="text-right"></td>
    </tr>

    <?php $group[$i] = $data['group']; ?>
    <?php $i++; ?>
  <?php } ?>
  <tr>
    <td class="text-left"><?php echo $data['trx_date']; ?></td>
    <td class="text-center"><?php echo $data['trx_type']; ?></td>
    <td class="text-right"><?php echo $data['trx_amount'] < 0 ? '<span class="cl-red">('.number_format(abs($data['trx_amount']),2,".",",").')</span>' :  '<span class="cl-green">'.number_format($data['trx_amount'],2,".",",").'</span>' ; ?></td>
    <td class="text-right"><?php echo $data['fee_amount']; ?></td>
    <td class="text-right"><?php echo $data['net_trx_amount'] < 0 ? '<span class="cl-red">('.number_format(abs($data['net_trx_amount']),2,".",",").')</span>' :  '<span class="cl-green">'.number_format($data['net_trx_amount'],2,".",",").'</span>' ; ?></td>
    <td class="text-right"><?php echo $data['trx_units'] < 0 ? '<span class="cl-red">('.number_format(abs($data['trx_units']),4,".",",").')</span>' :  '<span class="cl-green">'.number_format($data['trx_units'],4,".",",").'</span>' ; ?></td>
    <td class="text-right"><?php echo $data['nav_per_unit']; ?></td>
    <td class="text-right"><?php echo $data['average_cost']; ?></td>
    <td class="text-right"><?php echo $data['realized_gl'] < 0 ? '<span class="cl-red">('.number_format(abs($data['realized_gl']),2,".",",").')</span>' :  '<span class="cl-green">'.number_format($data['realized_gl'],2,".",",").'</span>' ; ?></td>
  </tr>

  <?php if(!$transaction_detail[($key + 1)] && $is_last_page ){ ?>
    <tr> 
      <td colspan="3" class="text-left"></td>
      <td colspan="2" class="text-left"><strong>Ending Balance</strong></td>
      <td class="text-right"><strong><?php echo $data['group_ending_balance']; ?></strong></td>
      <td class="text-right"></td>
      <td class="text-right"></td>
      <td class="text-right"><?php echo $data['group_realized_gl'] < 0 ? '<span class="cl-red">('.number_format(abs( $data['group_realized_gl']),2,".",",").')</span>' :  '<span class="cl-green">'.number_format( $data['group_realized_gl'],2,".",",").'</span>' ; ?></td>
    </tr>
  <?php } ?>
<?php endforeach; ?>
