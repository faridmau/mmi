<?php foreach($transactions as $trn): ?>

  <tr class="<?php if(!empty($trn['frontend_trx_id'])) echo 'highlight'; ?>"> 
    <td><?php echo $trn['transaction_date']; ?></td>
    <td><?php echo $trn['product_name'] ? $trn['product_name'] : $trn['product_code']; ?></td>
    <td><?php echo $trn['transaction_type']; ?></td>
    <td class="text-right"><?php echo $trn['nav_date']; ?></td>
    <td class="text-right"><?php echo $trn['nav_value']; ?></td>
    <td class="text-right"><?php echo $trn['transaction_unit']; ?></td>
    <td class="text-right"><?php echo $trn['transaction_amount']; ?></td>
    <td class="text-right"><?php echo $trn['transaction_fee']; ?></td>
    <td class="text-right"><?php echo $trn['total_amount']; ?></td>
    <td class="text-right"><?php echo $trn['saldo']; ?></td>
  </tr>

<?php endforeach; ?>