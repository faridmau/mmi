<?php
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' )) {
	header ( 'Location: static_pages/' );
}
class ControllerPagesIndexHome extends AController {
	
	public function main() {
    //init controller data
    $this->extensions->hk_InitData($this,__FUNCTION__);
		$this->document->setTitle( $this->config->get('config_title') );
		$this->document->setDescription( $this->config->get('config_meta_description') );
		$this->document->setKeywords( $this->config->get('config_meta_keywords') );
		
		$this->view->assign('heading_title', sprintf($this->language->get('heading_title'), $this->config->get('store_name')) );
		$this->loadModel('setting/store');
		
		if (!$this->config->get('config_store_id')) {
			$this->view->assign('welcome', html_entity_decode($this->config->get('config_description_' . $this->config->get('storefront_language_id')), ENT_QUOTES, 'UTF-8') );
		} else {
			$store_info = $this->model_setting_store->getStore($this->config->get('config_store_id'));
			
			if ($store_info) {
				$this->view->assign('welcome', html_entity_decode($store_info['description'], ENT_QUOTES, 'UTF-8') );
			} else {
				$this->view->assign('welcome', '');
			}
		}
		
		$this->view->assign('special', $this->html->getURL('product/special') );

		$this->processTemplate();

    //init controller data
    $this->extensions->hk_UpdateData($this,__FUNCTION__);
	}

	public function checkUserApprove(){
		//init controller data
    $this->extensions->hk_InitData($this,__FUNCTION__);
    $this->loadModel('account/customer');
		
		$date = date('Y-m-d h:i:s');
		echo " | ".$date.' = ';
		
		//get all users who has approved status = 4
		$customers_waiting = $this->model_account_customer->getWaitingCustomers();

		if(empty($customers_waiting))	exit;
		foreach($customers_waiting as $key => $value){
			echo " - ".$value['email'] . " | ";
			$this->_sendMail($value['customer_id']);
		}

		// $this->showDebug($customers_waiting);
		$this->extensions->hk_UpdateData($this,__FUNCTION__);
	}

	/**
	 * @param int $id  - customer_id
	 */
	private function _sendMail($id){
			$this->loadModel('account/customer');
			// send email to customer
			$customer_info = $this->model_account_customer->getCustomer($id);
			if ($customer_info) {

				$this->loadLanguage('mail/customer');
				$this->loadModel('setting/store');

				/* key for first login reset password */
				$password = substr(md5(rand()), 0, 7);
				$key = substr(md5(rand()), 0, 20);
				// $reset_link = $this->html->getURL('account/reset&key='.$key);

				// echo "key = $key reset link : ".$reset_link." | ".HTTP_SERVER." | ".REAL_HOST;exit;
				
				$store_info = $this->model_setting_store->getSetting('details',$customer_info['store_id']);
				$reset_link = $store_info['config_url']."index.php?rt=account/changepass&key=".$key;	

				if ($store_info) {
					$store_name = $store_info['store_name'];
					$store_url = $store_info['config_url'] . 'index.php?rt=account/password';
				} else {
					$store_name = $this->config->get('store_name');
					$store_url = $this->config->get('config_url') . 'index.php?rt=account/login';
				}
				$message = "Hi ".$customer_info['firstname'].",\n\n";
				$message .= sprintf($this->language->get('text_welcome'), $store_name) . "\n\n";
				$message .= sprintf($this->language->get('text_login'), $store_name) ." ". $store_name.".\n";
				$message .= $this->language->get('text_username_password') . "\n";
				// $message .= $this->language->get('text_services') . "\n\n";
				$message .= "URL= ".$reset_link."\n";
				$message .= "Email = ".$customer_info['email']."\n\n";
				$message .= $this->language->get('text_thanks') . "\n";
				$message .= $store_name;

				//insert key into database
				$this->model_account_customer->keyReset($customer_info['email'], $key);

				$mail = new AMail( $this->config );
				$mail->setTo($customer_info['email']);
				$mail->setFrom($this->config->get('store_main_email'));
				$mail->setSender($store_name);
				$mail->setSubject(sprintf($this->language->get('text_subject'), $store_name));
				$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
				if($mail->send()){
					$curr_date = Date('Y-m;d h:i:s');
					$this->model_account_customer->editCustomerField('sync_datetime',$curr_date,'customer_id',$id);
					$this->model_account_customer->editCustomerField('approved',5,'customer_id',$id);
				}
				$date = date('Y-m-d h:i:s');
				echo $date;
			}
	}

	public function checkUserEditApprove(){
		//init controller data
    $this->extensions->hk_InitData($this,__FUNCTION__);
    $this->loadModel('account/customer_profile');
		
		$date = date('Y-m-d h:i:s');
		echo " | ".$date.' = ';
		
		//get all users who has approved status = 4
		$customers_waiting = $this->model_account_customer_profile->getWaitingCustomers();
		// $this->showDebug($customers_waiting);

		if(empty($customers_waiting))	exit;
		foreach($customers_waiting as $key => $value){
			echo " - ".$value['email'] . " | ";
			$this->_sendEditApproveMail($value);
		}

		// $this->showDebug($customers_waiting);
		$this->extensions->hk_UpdateData($this,__FUNCTION__);
	}

	/**
	 * @param int $id  - customer_id
	 */
	private function _sendEditApproveMail($data){
			//get customer by holder_id
			// $this->showDebug($data);
			$customer_id = $this->model_account_customer_profile->getCustomerId($data['holder_id']);
			// send email to customer
			$customer_info = $this->model_account_customer_profile->getCustomerInfo($customer_id);
			// $this->showDebug($customer_info);
			if ($customer_info) {

				$this->loadLanguage('mail/customer');
				$this->loadModel('setting/store');

				$store_info = $this->model_setting_store->getSetting('details',$customer_info['store_id']);
				//$this->model_setting_store->getStore($customer_info['store_id']);

				if ($store_info) {
					$store_name = $store_info['store_name'];
					$store_url = $store_info['config_url'] . 'index.php?rt=account/password';
				} else {
					$store_name = $this->config->get('store_name');
					$store_url = $this->config->get('config_url') . 'index.php?rt=account/login';
				}

				$message  = 'Hi '.$customer_info['firstname'].','."\n\n";
				$message .= sprintf($this->language->get('text_edit_content'), $store_name) ."\n\n";
				$message .= $this->language->get('text_thanks') . "\n";
				$message .= $store_name;

				$mail = new AMail( $this->config );
				$mail->setTo($customer_info['email']);
				$mail->setFrom($this->config->get('store_main_email'));
				$mail->setSender($store_name);
				$mail->setSubject($store_name.' - '.$this->language->get('text_edit_profile_approved'));
				$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
				if($mail->send()){
					//jika email berhasil dikirimkan
					//update table customer dengan data yg ada di table customer_profile
					//delete record from table customer_profile_temp
					$this->model_account_customer_profile->updateFieldCustomer('approved','1',$data['holder_id']);
				}else{
					//jika email tidak berhasil dikirim
					echo "gagal kirim email";
				}
			}
	}

	public function sendReminderMail(){
		$this->loadModel('account/reminder');
	    $this->loadModel('tool/lookup');
	    $this->loadLanguage('account/reminder');

	    $this->loadModel('setting/store');
    
	    $reminders = $this->model_account_reminder->reminderEmail();
	    $day_rm = $this->model_setting_store->getSettingValue('general','send_reminder_email_days_prior_due_date');
	    
	    $date_now = date("Y-m-d");

	    // $date_before = date('Y-m-d', strtotime($date_now. ' + '.$day_rm.' days'));

	    foreach ($reminders as $reminder) {
	      
			//Calculate the difference between two dates
			$date_reminder=$reminder['reminder_date'];
			$date_before_reminder = date('Y-m-d', strtotime($date_reminder. ' - '.$day_rm.' days'));

			// mengecek jarak hari
			if ($date_now>=$date_before_reminder&&$date_now<=$date_reminder) {
				// mengirim email ke reminder yang jaraknya sama dengan setting di lookup
				// send email to customer
				
					$this->loadLanguage('mail/customer');
					$this->loadModel('setting/store');

					$store_info = $this->model_setting_store->getSetting('details',$customer_info['store_id']);
					//$this->model_setting_store->getStore($customer_info['store_id']);

					if ($store_info) {
						$store_name = $store_info['store_name'];
						$store_url = $store_info['config_url'] . 'index.php?rt=account/password';
					} else {
						$store_name = $this->config->get('store_name');
						$store_url = $this->config->get('config_url') . 'index.php?rt=account/login';
					}

					$message = 'Hi '.$reminder['firstname'].',<br>';
					$message .= $this->language->get('entry_subject').$reminder['descriptions'];
					$message .= $this->language->get('entry_due_on').date("d-M-Y", strtotime($reminder['reminder_date'])). "<br><br><br>";
		            $message .= $this->language->get('text_edit_thanks') . "<br>";
		            $message .= $store_name;

					$mail = new AMail( $this->config );
					$mail->setTo($reminder['email']);
					$mail->setFrom($this->config->get('store_main_email'));
					$mail->setSender($store_name);
					$mail->setSubject($store_name.' - '.$this->language->get('heading_title_notification'));
					$mail->setHtml($message);
					$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));

					if($mail->send()){
						//jika email berhasil dikirimkan
						
					}else{
						//jika email tidak berhasil dikirim
						echo "gagal kirim email";
					}
					
			}
	    }
		
	}

	public function sendAfterNavMail(){
		$this->loadModel('account/order');
		$this->loadModel('setting/store');

		$orders = $this->model_account_order->getAfterNavOrders();

		$language = new ALanguage($this->registry);
		$language->load('mail/after_nav');

		foreach ($orders as $key => $data) {

			// HTML Mail
			$template = new ATemplate();
			$template->data['title'] = 'Testing email after nav';
			$template->data['trx_id'] = $data['trx_id'];
			$template->data['trx_amount'] = $this->currency->format($data['trx_amount'], $data['currency'], $data['value']) ;
			$template->data['nav_value'] = $this->currency->format($data['nav_value'], $data['currency'], $data['value']) ;
			$template->data['trx_units'] = number_format($data['trx_units'],4,".",",");
			$template->data['trx_date'] = date("d-M-y h:i:s A", strtotime($data['trx_date']));
			$template->data['name'] = $data['name'];
			$template->data['email'] = $data['email'];
			$template->data['payment_method'] = $data['payment_method'];
			$template->data['product_name'] = ucwords(strtolower($data['product_name']));

			$template->data['logo_src'] = $this->config->get('config_logo');
			$template->data['text_trx'] = $language->get('text_trx');
			$template->data['text_name'] = $language->get('text_name');
			$template->data['text_date'] = $language->get('text_date');
			$template->data['text_payment'] = $language->get('text_payment');
			$template->data['text_email'] = $language->get('text_email');
			$template->data['text_subs_amount'] = $language->get('text_subs_amount');

			$template->data['text_thanks'] = $language->get('text_thanks');
			$template->data['text_note'] = $language->get('text_note');
			$template->data['text_note_notification'] = $language->get('text_note_notification');
			$template->data['text_reserved'] = $language->get('text_reserved');
			$template->data['text_confirmation_letter'] = $language->get('text_confirmation_letter');

			$html = $template->fetch('mail/after_nav_notification.tpl');

			$store_info = $this->model_setting_store->getSetting('details',$customer_info['store_id']);
			//$this->model_setting_store->getStore($data['store_id']);
			if ($store_info) {
				$store_name = $store_info['store_name'];
				$store_url = $store_info['config_url'] . 'index.php?rt=account/password';
			} else {
				$store_name = $this->config->get('store_name');
				$store_url = $this->config->get('config_url') . 'index.php?rt=account/login';
			}

			$subject = sprintf($language->get('text_subject'), $store_name, $data['trx_id']);
			$text = $language->get('text_footer');

			$mail = new AMail( $this->config );
			$mail->setTo($data['email']);
			$mail->setFrom($this->config->get('store_main_email'));
			$mail->setSender($store_name);
			$mail->setSubject($subject);
			$mail->setHtml($html);
			$mail->setText(html_entity_decode($text, ENT_QUOTES, 'UTF-8'));
			if($mail->send()){
				$this->model_account_order->updateAfterNavData($data['trx_id']); // Update order status to '5'
				//jika email berhasil dikirimkan
			}else{
				//jika email tidak berhasil dikirim
				echo "gagal kirim email";
			}
		}
	}

}
?>