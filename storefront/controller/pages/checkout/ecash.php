<?php
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' )) {
	header ( 'Location: static_pages/' );
}
class ControllerPagesCheckoutEcash extends AController {
	
	public function main() {
    //init controller data
    $this->extensions->hk_InitData($this,__FUNCTION__);
		
		// $this->processTemplate('pages/checkout/ecash_step_1.tpl' );
    $this->redirect($this->html->getSecureURL('checkout/ecash/step1'));
    //init controller data
    $this->extensions->hk_UpdateData($this,__FUNCTION__);
	}

	public function step1() {
    //init controller data
    $this->extensions->hk_InitData($this,__FUNCTION__);
		
		$this->processTemplate('pages/checkout/ecash_step_1.tpl' );
    $button_step2 = $this->html->getSecureURL('checkout/ecash/step2');
    $this->view->assign('button_step2',$button_step2);
    //init controller data
    $this->extensions->hk_UpdateData($this,__FUNCTION__);
	}

	public function step2() {
    //init controller data
    $this->extensions->hk_InitData($this,__FUNCTION__);

		$button_step3 = $this->html->getSecureURL('checkout/ecash/step3');
    $this->view->assign('button_step3',$button_step3);
		$this->processTemplate('pages/checkout/ecash_step_2.tpl' );

    //init controller data
    $this->extensions->hk_UpdateData($this,__FUNCTION__);
	}

	public function step3() {
    //init controller data
    $this->extensions->hk_InitData($this,__FUNCTION__);

    $confirm_link = $this->html->getSecureURL('checkout/confirm');
    $this->view->assign('confirm_page',$confirm_link);

		$this->processTemplate('pages/checkout/ecash_step_3.tpl' );

    //init controller data
    $this->extensions->hk_UpdateData($this,__FUNCTION__);
	}

}