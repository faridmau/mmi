<?php 
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' )) {
	header ( 'Location: static_pages/' );
}
class ControllerPagesAccountReset extends AController {
	public $errors = array();
	public $data;

	public function main() {
		$this->loadCryptLib();
		$this->loadModel('tool/lookup');
		$this->loadModel('account/customer');

		//cek dari tabel customer where reset key = key
		$key = $this->request->get['key'];
		$custSyncdate = $this->model_account_customer->getFieldCustom('sync_datetime','reset_key',$key);

		$lang = $this->language->getContentLanguageID();
		//ambil dari table lookup
		$this->loadModel('setting/store');
		
		$expired = $this->model_setting_store->getSettingValue('general','expire_link');

		$datetime1 = new DateTime(date("Y/m/d"));
		$datetime2 = new DateTime($custSyncdate);
		$interval = $datetime1->diff($datetime2);
		/*$this->showDebug($datetime1,false);
		$this->showDebug($datetime2,false);
		$this->showDebug($expired['item_name'],false);
		$this->showDebug($interval,false);*/

		if(is_null($expired)) $expired['item_name'] = 1;
		if($interval->invert==1 && $interval->days > $expired['item_name']){
			//delete the reset key
			$this->model_account_customer->editCustomerField('reset_key','','reset_key',$key);
		}

	  //init controller data
	  $this->extensions->hk_InitData($this,__FUNCTION__);

		if ($this->customer->isLogged()) {
	  		$this->redirect($this->html->getSecureURL('account/account'));
    	}

		$this->document->setTitle( $this->language->get('heading_title') );
		
		$this->loadModel('account/customer');
		$request_data = $this->request->post;
		
		$rsa = new Crypt_RSA();
    $request_data["password"] = $this->decrypt($this->privatekey,$request_data["password"]);
    $request_data["confirm"] = $this->decrypt($this->privatekey,$request_data["confirm"]);
    
		if ( $this->request->server['REQUEST_METHOD'] == 'POST') {
			$this->errors = array_merge($this->errors,$this->model_account_customer->validateResetPass($request_data));
    		if ( !$this->errors ) {
    			
    			$key = $request_data['key'];
    			$custID = $this->model_account_customer->getFieldCustom('customer_id','reset_key',$key);
    			
    			$this->model_account_customer->resetPassByKey($request_data['password'],$request_data['key']);
	  			
	  			$this->model_account_customer->editCustomerField('approved', 1,'customer_id',$custID);

					$this->extensions->hk_UpdateData($this,__FUNCTION__);
		  		$this->redirect($this->html->getSecureURL('account/success/reset'));
	  		}else{
	  			$this->redirect($this->html->getSecureURL('account/reset&key='.$request_data['key']));	
	  		}
    } 

  	$isKeyFound = $this->model_account_customer->isKeyValid($this->request->get['key']);

  	if ($isKeyFound) {
  		$form = new AForm();
      $form->setForm(array( 'form_name' => 'AccountFrm' ));
      $this->data['form'][ 'form_open' ] = $form->getFieldHtml( array( 'type' => 'form',
                                                                       'name' => 'AccountFrm',
                                                                       'action' => $this->html->getSecureURL('account/reset')));

			$this->data['form'][ 'password' ] = $form->getFieldHtml( array(
	                                                                    'type' => 'password',
			                                                               'name' => 'password',
			                                                               'style' => 'form-control',
			                                                               'placeholder' => $this->language->get('holder_password'),
			                                                               'value' => $this->request->post['password'],
			                                                               'required' => true ));
			$this->data['form'][ 'confirm' ] = $form->getFieldHtml( array(
	                                                                   'type' => 'password',
			                                                               'name' => 'confirm',
			                                                               'style' => 'form-control',
			                                                               'placeholder' => $this->language->get('holder_confirm'),
			                                                               'value' => $this->request->post['confirm'],
			                                                               'required' => true ));
			$this->data['form'][ 'key' ] = $form->getFieldHtml( array(
	                                                                   'type' => 'hidden',
			                                                               'name' => 'key',
			                                                               'value' => $this->request->get['key'],
			                                                               'required' => true ));

			$this->data['form'][ 'continue' ] = $form->getFieldHtml( array(
	                                                                   'type' => 'submit',
			                                                               'name' => $this->language->get('button_continue') ));
			$valid = true;
    }
      	

		$this->data['error_password'] = $this->errors['password'];
		$this->data['error_confirm'] = $this->errors['confirm'];
    $this->data['action'] = $this->html->getSecureURL('account/reset') ;
		
		$this->view->assign('valid',$valid);
		$this->view->batchAssign($this->data);
    $this->processTemplate('pages/account/reset.tpl');

    //init controller data
    $this->extensions->hk_UpdateData($this,__FUNCTION__);
  	}

}