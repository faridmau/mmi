<?php 
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' )) {
  header ( 'Location: static_pages/' );
}
class ControllerPagesAccountConsolidate extends AController {
  private $error = array();
  public $data;
    public function main() {

        //init controller data
        $this->extensions->hk_InitData($this,__FUNCTION__);

        if (!$this->customer->isLogged()) {
          $this->redirect($this->html->getSecureURL('account/login'));
        }
        $this->loadLanguage('account/consolidate');
    
        $this->document->setTitle($this->language->get('text_consolidate') );
        $this->loadLanguage('account/account');

        if($this->request->post['date_start'] || $this->request->post['date_end']){
          $this->redirect($this->html->getSecureURL('account/consolidate','&date_start='.$this->request->post['date_start'].'&date_end='.$this->request->post['date_end']));
        }

        if (isset($this->request->get['page'])) {
          $page = $this->request->get['page'];
        } else {
          $page = 1;
        }

        if (isset($this->request->get['limit'])) {
          $limit = (int)$this->request->get['limit'];
          $limit = $limit>50 ? 50 : $limit;
        } else {
          $limit = $this->config->get('config_catalog_limit');
        }

        $date_start = date("Y-m-d",strtotime($this->request->get['date_start']));
        $date_end = date("Y-m-d",strtotime($this->request->get['date_end']));

        $this->loadModel('account/customer');
        $investment_summary = $this->model_account_customer->getInvestmentSummary($date_start, $date_end);
        
        $total_ending_balance = 0.00;
        $total_realized_gl = 0.00;
        $total_unrealized_gl = 0.00;

        foreach ($investment_summary as $key => $data) {
          $total_ending_balance += $data['ending_balance_amount'];
          $total_realized_gl += $data['total_realized_gl'];
          $total_unrealized_gl += $data['unrealized_gl'];

          $investment_summary[$key]['product_name'] = ucwords(strtolower($data['product_name']));
          $investment_summary[$key]['ending_balance_amount'] = number_format($data['ending_balance_amount'],2,".",",");
          $investment_summary[$key]['nav_value'] = number_format($data['nav_value'],2,".",",");
          $investment_summary[$key]['ending_balance'] = number_format($data['ending_balance'],4,".",",");
          $investment_summary[$key]['beginning_balance'] = number_format($data['beginning_balance'],4,".",",");
        }

        $this->data['total_ending_balance'] = number_format($total_ending_balance,2,".",",");
        $this->data['total_realized_gl'] = $total_realized_gl;
        $this->data['total_unrealized_gl'] = $total_unrealized_gl;

        $this->data['investment_summary'] = $investment_summary;

        $transaction_detail = $this->model_account_customer->getTransactionDetails(($page - 1) * $limit, $limit, $date_start, $date_end );
        
        foreach ($transaction_detail as $key => $data) {
          $transaction_detail[$key]['product_name'] = ucwords(strtolower($data['product_name']));
          $transaction_detail[$key]['trx_date'] = dateISO2Display($data['trx_date'], 'j-M-y');
          $transaction_detail[$key]['realized_gl'] = $data['realized_gl'] ? $data['realized_gl'] : '0.00';
          $transaction_detail[$key]['fee_amount'] = number_format($data['fee_amount'],2,".",",");
          $transaction_detail[$key]['nav_per_unit'] = number_format($data['nav_per_unit'],2,".",",");
          $transaction_detail[$key]['average_cost'] = number_format($data['average_cost'],2,".",",");
          $transaction_detail[$key]['group'] = str_replace(' ', '-', strtolower($data['product_name']).'-'.$data['inv_account_no']);
          
          $transaction_detail_balance = $this->getTransactionBalance($investment_summary, $transaction_detail[$key]['product_name'], $data['inv_account_no']);

          $transaction_detail[$key]['group_balance'] = $transaction_detail_balance;
          $transaction_detail[$key]['group_begining_balance'] = $transaction_detail_balance['begining_balance'];
          $transaction_detail[$key]['group_ending_balance'] = $transaction_detail_balance['ending_balance']; 
          $transaction_detail[$key]['group_realized_gl'] = $transaction_detail_balance['realized_gl'] ; 
        }
        $this->data['transaction_detail'] = $transaction_detail;

        $trans_total = $this->model_account_customer->getTransactionDetailTotal($date_start, $date_end);
        $this->data['is_last_page'] = $page >= ceil($trans_total / $limit) ? true : false;

        $this->data['pagination_bootstrap'] = HtmlElementFactory::create( array (
                    'type' => 'Pagination',
                    'name' => 'pagination',
                    'text'=> $this->language->get('text_pagination'),
                    'text_limit' => $this->language->get('text_per_page'),
                    'total' => $trans_total,
                    'page'  => $page,
                    'limit' => $limit,
                    'url' => $this->html->getURL('account/consolidate', '&limit=' . $limit . '&page={page}&date_start='.$this->request->get['date_start'].'&date_end='.$this->request->get['date_end']),
                    'style' => 'pagination'));

        $this->data['date_start'] = HtmlElementFactory::create( array ( 
             'type' => 'input',
             'name' => 'date_start',
             'value'=> $this->request->get['date_start'],
             'style'=> 'form-control',
             'attr' => 'readonly',
        ));

        $this->data['date_end'] = HtmlElementFactory::create( array ( 
             'type' => 'input',
             'name' => 'date_end',
             'value'=> $this->request->get['date_end'],
             'style'=> 'form-control',
             'attr' => 'readonly',
        ));

        $this->data['date_start_mobile'] = HtmlElementFactory::create( array ( 
         'type' => 'input',
         'name' => 'date_start',
         'value'=> $this->request->get['date_start'],
         'style'=> 'form-control',
         'id'=>'date_start_mobile',
         'attr' => 'readonly',
        ));

        $this->data['date_end_mobile'] = HtmlElementFactory::create( array ( 
             'type' => 'input',
             'name' => 'date_end',
             'value'=> $this->request->get['date_end'],
             'style'=> 'form-control',
             'id'=>'date_end_mobile',
             'attr' => 'readonly',
        ));

        $this->data['date_start_preview'] = $this->request->get['date_start'];
        $this->data['date_end_preview'] = $this->request->get['date_end'];

        $this->data['export_url'] = $this->html->getSecureURL('account/consolidate/export','&date_start='.$this->request->get['date_start'].'&date_end='.$this->request->get['date_end']);
        $this->data['filter_url'] = $this->html->getSecureURL('account/consolidate');
        //trx_history_retention_in_days
        $this->loadModel('setting/store');
        $this->data['trx_history_retention_in_days'] = $this->model_setting_store->getSettingValue('general','trx_history_retention_in_days');

        $this->view->batchAssign($this->data);
    
        $this->processTemplate('pages/account/consolidate.tpl');

        //init controller data
        $this->extensions->hk_UpdateData($this,__FUNCTION__);
    }

    public function export(){
      //init controller data
        $this->extensions->hk_InitData($this,__FUNCTION__);

        if (!$this->customer->isLogged()) {
          $this->redirect($this->html->getSecureURL('account/login'));
        }

        $this->loadLanguage('account/consolidate');
        $this->loadLanguage('account/account');

        $template = new ATemplate();

        $date_start = date("Y-m-d",strtotime($this->request->get['date_start']));
        $date_end = date("Y-m-d",strtotime($this->request->get['date_end']));

        $this->loadModel('account/customer');
        $investment_summary = $this->model_account_customer->getInvestmentSummary($date_start, $date_end);
        
        $total_ending_balance = 0.00;
        $total_realized_gl = 0.00;
        $total_unrealized_gl = 0.00;

        foreach ($investment_summary as $key => $data) {
          $total_ending_balance += $data['ending_balance_amount'];
          $total_realized_gl += $data['total_realized_gl'];
          $total_unrealized_gl += $data['unrealized_gl'];

          $investment_summary[$key]['product_name'] = ucwords(strtolower($data['product_name']));
          $investment_summary[$key]['ending_balance_amount'] = number_format($data['ending_balance_amount'],2,".",",");
          $investment_summary[$key]['nav_value'] = number_format($data['nav_value'],2,".",",");
          $investment_summary[$key]['ending_balance'] = number_format($data['ending_balance'],4,".",",");
          $investment_summary[$key]['beginning_balance'] = number_format($data['beginning_balance'],4,".",",");
        }

        $template->data['total_ending_balance'] = number_format($total_ending_balance,2,".",",");
        $template->data['total_realized_gl'] = $total_realized_gl;
        $template->data['total_unrealized_gl'] = $total_unrealized_gl;
        $template->data['investment_summary'] = $investment_summary;

        $transaction_detail = $this->model_account_customer->getTransactionDetails(false, false, $date_start, $date_end );
        
        foreach ($transaction_detail as $key => $data) {
          $transaction_detail[$key]['product_name'] = ucwords(strtolower($data['product_name']));
          $transaction_detail[$key]['trx_date'] = dateISO2Display($data['trx_date'], 'j-M-y');
          $transaction_detail[$key]['realized_gl'] = $data['realized_gl'] ? $data['realized_gl'] : '0.00';
          $transaction_detail[$key]['fee_amount'] = number_format($data['fee_amount'],2,".",",");
          $transaction_detail[$key]['nav_per_unit'] = number_format($data['nav_per_unit'],2,".",",");
          $transaction_detail[$key]['average_cost'] = number_format($data['average_cost'],2,".",",");
          $transaction_detail[$key]['group'] = str_replace(' ', '-', strtolower($data['product_name']).'-'.$data['inv_account_no']);
          
          $transaction_detail_balance = $this->getTransactionBalance($investment_summary, $transaction_detail[$key]['product_name'], $data['inv_account_no']);

          $transaction_detail[$key]['group_balance'] = $transaction_detail_balance;
          $transaction_detail[$key]['group_begining_balance'] = $transaction_detail_balance['begining_balance'];
          $transaction_detail[$key]['group_ending_balance'] = $transaction_detail_balance['ending_balance']; 
          $transaction_detail[$key]['group_realized_gl'] = $transaction_detail_balance['realized_gl'] ; 
        }

        $template->data['transaction_detail'] = $transaction_detail;

        $trans_total = $this->model_account_customer->getTransactionDetailTotal($date_start, $date_end);
        $template->data['is_last_page'] = true;

        // HTML Mail
        $template->data['date_start'] = $this->request->get['date_start'];
        $template->data['date_end'] = $this->request->get['date_end'];

        $this->loadModel('account/customer_profile');

        $customer = $this->model_account_customer->getCustomer($this->customer->getId());
        $customer_info = $this->model_account_customer_profile->getCustomer($customer['holder_id']);
        $customer_country = $this->model_account_customer->getCountryByCode($customer_info['correspondence_country']);

        $template->data['cust_name'] = $customer['firstname'].' '.$customer['lastname'];
        $template->data['cust_address'] = $customer_info['correspondence_address'];
        
        $template->data['cust_postal_code'] = $customer_info['correspondence_postal_code'];
        $template->data['cust_country'] = $customer_country;

        $this->loadModel('localisation/zone');
        $cor_zone = $this->model_localisation_zone->getZonesByCountryCode($customer_info['correspondence_country']);
        foreach ($cor_zone as $key => $data) {
          if($data['code'] == $customer_info['correspondence_city']){
            $customer_city = $data['name'];
          }
        }

        $template->data['cust_city'] = $customer_city;
        $template->data['date_print'] = $this->request->get['date_print'] ? $this->request->get['date_print'] : date("d-M-y H:i:s");

        //** Multi language
        $template->data['text_consolidate'] = $this->language->get('text_consolidate');
        $template->data['text_transaction_date'] = $this->language->get('text_transaction_date');

        $template->data['text_product_summary'] = $this->language->get('text_product_summary');
        $template->data['text_name'] = $this->language->get('text_name');
        $template->data['text_acc_no'] = $this->language->get('text_acc_no');
        $template->data['text_column_balance'] = $this->language->get('text_column_balance');
        $template->data['text_nav'] = $this->language->get('text_nav');
        $template->data['text_ending'] = $this->language->get('text_ending');
        $template->data['text_realized'] = $this->language->get('text_realized');
        $template->data['text_unrealized'] = $this->language->get('text_unrealized');
        $template->data['text_begining'] = $this->language->get('text_begining');
        $template->data['text_movement'] = $this->language->get('text_movement');
        $template->data['text_column_ending'] = $this->language->get('text_column_ending');

        $template->data['text_transaction_detail'] = $this->language->get('text_transaction_detail');
        $template->data['text_trn_date'] = $this->language->get('text_trn_date');
        $template->data['text_trn_type'] = $this->language->get('text_trn_type');
        $template->data['text_transaction_amount'] = $this->language->get('text_transaction_amount');
        $template->data['text_fee_amount'] = $this->language->get('text_fee_amount');
        $template->data['text_net_amount'] = $this->language->get('text_net_amount');
        $template->data['text_transaction_unit'] = $this->language->get('text_transaction_unit');
        $template->data['text_average_cost'] = $this->language->get('text_average_cost');

        $template->data['text_beginning_balance'] = $this->language->get('text_beginning_balance');
        $template->data['text_ending_balance'] = $this->language->get('text_ending_balance');

        $template->data['text_to'] = $this->language->get('text_to');
        $template->data['text_reserved'] = $this->language->get('text_reserved');
        $template->data['text_print_date'] = $this->language->get('text_print_date');
        $template->data['text_disclaimer'] = $this->language->get('text_disclaimer');
        $template->data['entry_disclaimer'] = $this->language->get('entry_disclaimer');

        $html = $template->fetch('pages/account/consolidate_export.tpl');

        // For testing
        // echo $html;
        // return;

        require_once(DIR_ROOT . '/dompdf/dompdf_config.inc.php');
        $dompdf = new DOMPDF();
        $dompdf->set_base_path(DIR_ROOT.'/');
        $dompdf->load_html($html);
        $dompdf->set_paper('a4', 'landscape');
        $dompdf->render();

        $canvas = $dompdf->get_canvas();
        $font = Font_Metrics::get_font('Open Sans Light', 'normal');
        $canvas->page_text(400, 572, "{PAGE_NUM} ".$this->language->get('text_of')." {PAGE_COUNT}", $font, 7, array(0,0,0));

        $pdf = $dompdf->output();

        header('Content-Disposition: attachment; filename="consolidate_statement_'.date("YmdHis").'.pdf"');
        header('Content-Type: application/pdf'); 
        header('Content-Length: ' . strlen($pdf));
        header('Connection: close');
        echo $pdf;

        //$dompdf->stream("consolidate_statement_".date("YmdHis").".pdf");

        //init controller data
        $this->extensions->hk_UpdateData($this,__FUNCTION__);
    }

    private function getTransactionBalance($investment_summary, $product_name, $inv_account_no){
      $beginning_balance = 'undefined';
      $ending_balance = 'undefined';
      $realized_gl = 0;

      foreach ($investment_summary as $key => $data) {
        if($data['product_name'] == $product_name && $data['inv_account_no'] == $inv_account_no){
          $beginning_balance = $data['beginning_balance'];
          $ending_balance = $data['ending_balance'];
          $realized_gl = $data['total_realized_gl'];
        }
      }

      $result = array(
        'begining_balance'=>$beginning_balance,
        'ending_balance'=>$ending_balance,
        'realized_gl'=>$realized_gl
      );

      return $result;
    }
}