<?php 
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' )) {
	header ( 'Location: static_pages/' );
}
class ControllerPagesAccountReminder extends AController {
	
	public $errors = array();
	public $data;

	public function main() {
		
	  //init controller data
	  $this->extensions->hk_InitData($this,__FUNCTION__);

		if (!$this->customer->isLogged()) {
	  		$this->redirect($this->html->getSecureURL('account/account'));
    	}

    	$this->loadModel('account/customer');
		$this->loadLanguage('account/account');

		if (!empty($this->request->get['delete_id'])) {
			$this->model_account_reminder->deleteReminder($this->request->get['delete_id']);
			$this->session->data['reminder_deleted'] = $this->language->get('reminder_deleted');
		}
		// ajax paging
		

		$request_data = $this->request->post;
		
		$customer = $this->model_account_customer->getCustomer($this->customer->getId());
		if ( $this->request->server['REQUEST_METHOD'] == 'POST'&&!empty($request_data['descriptions'])&&!empty($request_data['reminder_date'])) {
			$request_data['descriptions'] = trim($request_data['descriptions']);
			$request_data['customer_id'] = $this->customer->getId();
			$request_data['reminder_date'] = date("Y-m-d", strtotime($request_data['reminder_date']));
			$request_data['created_by'] = $customer['firstname'].' '.$customer['lastname'];
			
			
			if (empty($request_data['reminder_id'])) {
				$this->model_account_reminder->addReminder($request_data);
				$this->session->data['reminder_saved'] = $this->language->get('reminder_saved');
			}
			else{
				$this->model_account_reminder->editReminder($request_data);
				$this->session->data['reminder_saved'] = $this->language->get('reminder_edited');
			}	
		
	  	}
	  	$this->redirect($this->html->getSecureURL('account/account'));
		$this->extensions->hk_UpdateData($this,__FUNCTION__);
    }
}
  	