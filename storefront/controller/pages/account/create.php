<?php 
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' )) {
  header ( 'Location: static_pages/' );
}
class ControllerPagesAccountCreate extends AController {
  public $errors = array();
  public $data;
  public function main() {
    $this->loadCryptLib();
    
    //init controller data
    $this->extensions->hk_InitData($this,__FUNCTION__);

    if ($this->customer->isLogged()) {
        $this->redirect($this->html->getSecureURL('account/account'));
      }

    $this->document->setTitle( $this->language->get('heading_title') );
    
    $this->loadModel('account/customer');
    $this->loadModel('account/messages');
    $this->loadModel('tool/lookup');
    // $this->loadModel('account/customer_profile');
    
    $request_data = $this->request->post;
    // var_dump($request_data);exit();

    $rsa = new Crypt_RSA();
    
    $request_data["password"] = $this->decrypt($this->privatekey,$request_data["password"]);
    $request_data["confirm"] = $this->decrypt($this->privatekey,$request_data["confirm"]);

    $request_data["bank_name"] = $this->decrypt($this->privatekey,$request_data["bank_nm"]);
    $request_data["bank_account_name"] = $this->decrypt($this->privatekey,$request_data["bank_account_name"]);
    $request_data["bank_account_no"] = $this->decrypt($this->privatekey,$request_data["bank_account_no"]);
      
    if ( $this->request->server['REQUEST_METHOD'] == 'POST') {

      // $this->showDebug($request_data,true);
      
      $this->model_account_messages->addMessageApproval($request_data);
      $this->errors = array_merge($this->errors,$this->model_account_customer->validateRegistrationData($request_data));
        if ( !$this->errors ) {
        //if allow login as email, need to set loginname = email
        if (!$this->config->get('prevent_email_as_login')) {
          $request_data['loginname'] = $request_data['email'];
        }
        

        $customer_id = $this->model_account_customer->addCustomer($request_data);

        // $request_data['cust_profile_id'] = $customer_id; 
        //var_dump($request_data);exit();
        // $customer_profile = $this->model_account_customer_profile->addCustomerProfile($request_data);

        unset($this->session->data['guest']);

        //login customer after create account is approvement and email activation are disabled in settings
        if (!$this->config->get('config_customer_approval')  && !$this->config->get('config_customer_email_activation') ){
          $this->customer->login($request_data['loginname'], $request_data['password']);
        }

        $this->loadLanguage('mail/account_create');
        $subject = sprintf($this->language->get('text_subject'), $this->config->get('store_name'));
        //$message = sprintf($this->language->get('text_welcome'), $this->config->get('store_name')) . "\n\n";
        if (!$this->config->get('config_customer_approval')) {
          if($this->config->get('config_customer_email_activation')){
            $activation = true; // sign of activation email
            $code = md5(mt_rand(1,3000));
            $email = $this->request->post['email'];
            $this->session->data['activation'] = array(
                                  'customer_id' => $customer_id,
                                  'code' => $code,
                                  'email' => $email);

            $message .= sprintf($this->language->get('text_activate'), "\n".$this->html->getSecureURL('account/login', '&activation='.$code.'&email='.$email) ) . "\n";
          }else{
            $message .= $this->language->get('text_login') . "\n";
          }
        } else {
          //$message .= $this->language->get('text_approval') . "\n";
        }
        if( !$activation ){
          //$message .= $this->html->getSecureURL('account/login') . "\n\n";
          //$message .= $this->language->get('text_services') . "\n\n";
        }
        
        $message .= $this->language->get('text_mail_approval') . "<br><br>";
        // $message .= $this->language->get('text_notify') . "\n\n\n\n";
        $message .= $this->language->get('text_thanks') . "<br>";
        $message .= $this->config->get('store_name');

        $mail = new AMail( $this->config );
        $mail->setTo($this->request->post['email']);
        $mail->setFrom($this->config->get('store_main_email'));
        $mail->setSender($this->config->get('store_name'));
        $mail->setSubject($subject);
        $mail->setHtml($message);
        $mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
        $mail->send();

        $this->extensions->hk_UpdateData($this,__FUNCTION__);
          $this->redirect($this->html->getSecureURL('account/success'));
        }
      } 

        $this->document->initBreadcrumb( array (
          'href'      => $this->html->getURL('index/home'),
          'text'      => $this->language->get('text_home'),
          'separator' => FALSE
         )); 

        $this->document->addBreadcrumb( array ( 
          'href'      => $this->html->getURL('account/account'),
          'text'      => $this->language->get('text_account'),
          'separator' => $this->language->get('text_separator')
         ));
    
        $this->document->addBreadcrumb( array ( 
          'href'      => $this->html->getURL('account/create'),
          'text'      => $this->language->get('text_create'),
          'separator' => $this->language->get('text_separator')
         ));

    if($this->config->get('prevent_email_as_login')){
      $this->data['noemaillogin'] = true;
    }

    $form = new AForm();
    $form->setForm(array( 'form_name' => 'AccountFrm' ));
    $this->data['form'][ 'form_open' ] = $form->getFieldHtml( array( 'type' => 'form',
                                                                         'name' => 'AccountFrm',
                                                                         'action' => $this->html->getSecureURL('account/create')));

    if($this->config->get('prevent_email_as_login')){ // require login name
      $this->data['form'][ 'loginname' ] = $form->getFieldHtml( array(
                                       'type' => 'input',
                                       'name' => 'loginname',
                                       'value' => $this->request->post['loginname'],
                                       'required' => true ));
    }
    
    $this->data['form'][ 'firstname' ] = $form->getFieldHtml( array(
                                                                   'type' => 'input',
                                                                   'name' => 'firstname',
                                                                   'placeholder' => $this->language->get('holder_first'),
                                                                   'value' => $this->request->post['firstname'],
                                                                   'required' => true ));
    $this->data['form'][ 'lastname' ] = $form->getFieldHtml( array(
                                                                   'type' => 'input',
                                                                   'name' => 'lastname',
                                                                   'placeholder' => $this->language->get('holder_last'),
                                                                   'value' => $this->request->post['lastname'],
                                                                   'required' => true ));
    $this->data['form'][ 'email' ] = $form->getFieldHtml( array(
                                                                   'type' => 'input',
                                                                   'name' => 'email',
                                                                   'placeholder' => $this->language->get('holder_email'),
                                                                   'value' => $this->request->get_or_post('email'),
                                                                   'required' => true ));
    $this->data['form'][ 'telephone' ] = $form->getFieldHtml( array(
                                                                   'type' => 'input',
                                                                   'name' => 'telephone',
                                                                   'placeholder' => $this->language->get('holder_phone'),
                                                                   'value' => $this->request->post['telephone'],
                                                                   'required' => true ));
    // $this->data['form'][ 'fax' ] = $form->getFieldHtml( array(
  //                                                                      'type' => 'input',
    //                                                                'name' => 'fax',
    //                                                                'value' => $this->request->post['fax'],
    //                                                                'required' => false ));
    $this->data['form'][ 'company' ] = $form->getFieldHtml( array(
                                                                   'type' => 'input',
                                                                   'name' => 'company',
                                                                   'value' => $this->request->post['company'],
                                                                   'required' => false ));
    $this->data['form'][ 'address_1' ] = $form->getFieldHtml( array(
                                                                   'type' => 'input',
                                                                   'name' => 'address_1',
                                                                   'value' => $this->request->post['address_1'],
                                                                   'required' => true ));
    $this->data['form'][ 'address_2' ] = $form->getFieldHtml( array(
                                                                       'type' => 'input',
                                                                   'name' => 'address_2',
                                                                   'value' => $this->request->post['address_2'],
                                                                   'required' => false ));
    $this->data['form'][ 'city' ] = $form->getFieldHtml( array(
                                                                       'type' => 'input',
                                                                   'name' => 'city',
                                                                   'value' => $this->request->post['city'],
                                                                   'required' => true ));
    $this->data['form'][ 'postcode' ] = $form->getFieldHtml( array(
                                                                       'type' => 'input',
                                                                   'name' => 'postcode',
                                                                   'value' => $this->request->post['postcode'],
                                                                   'required' => true ));

    // add new field
    $this->data['form'][ 'id_number' ] = $form->getFieldHtml( array(
                                                                   'type' => 'input',
                                                                   'name' => 'id_number',
                                                                   'placeholder' => $this->language->get('holder_id_number'),
                                                                   'value' => $this->request->post['id_number'],
                                                                   'required' => true ));
    $type_id = $this->model_tool_lookup->getLookupData('ID_TYPE');
    $options_id['0']=$this->language->get('holder_select');
    foreach ($type_id as $item) {
      $options_id[ $item['item_code'] ] = $item['item_name'];
    }

    $this->data['form'][ 'id_type' ] = $form->getFieldHtml( array(
                                                                   'type' => 'selectbox',
                                                                   'name' => 'id_type',
                                                                   'options'=>$options_id,
                                                                   'value' => $this->request->post['id_type'],
                                                                   'required' => true ));
    $this->data['form'][ 'cif' ] = $form->getFieldHtml( array(
                                                                   'type' => 'input',
                                                                   'name' => 'cif',
                                                                   'placeholder' => $this->language->get('holder_cif'),
                                                                   'value' => $this->request->post['cif'],
                                                                   'required' => true ));
    $this->data['form'][ 'bank_account_name' ] = $form->getFieldHtml( array(
                                                                       'type' => 'input',
                                                                   'name' => 'bank_account_name',
                                                                   'placeholder' => $this->language->get('holder_account_name'),
                                                                   'value' => $request_data['bank_account_name'],
                                                                   'required' => true ));

    $bank_name = $this->model_tool_lookup->getLookupData('BANK_NAME');
    $options_bank['']=$this->language->get('holder_select');
    foreach ($bank_name as $bank) {
      $options_bank[ $bank['item_code'] ] = $bank['item_name'];
    }

    $this->data['form'][ 'bank_name' ] = $form->getFieldHtml( array(
                                                                   'type' => 'selectbox',
                                                                   'name' => 'bank_name',
                                                                   'value' => $this->request->post['bank_name'],
                                                                   'options' => $options_bank,
                                                                   'required' => true ));
    $this->data['form'][ 'bank_account_no' ] = $form->getFieldHtml( array(
                                                                   'type' => 'input',
                                                                   'name' => 'bank_account_no',
                                                                   'placeholder' => $this->language->get('holder_account_no'),
                                                                   'value' => $request_data['bank_account_no'],
                                                                   'required' => true ));

    $this->loadModel('localisation/country');
        $countries = $this->model_localisation_country->getCountries();
        $options = array("FALSE" => $this->language->get('text_select') );
        foreach($countries as $item){
            $options[ $item['country_id'] ] = $item['name'];
        }

    $this->data['form'][ 'country_id' ] = $form->getFieldHtml( array(
                                                              'type' => 'selectbox',
                                                               'name' => 'country_id',
                                                                'options'=>$options,
                                                               'value' => ( isset($this->request->post['country_id']) ? $this->request->post['country_id'] : $this->config->get('config_country_id')),
                                                               'required' => true ));
    $this->view->assign('zone_id', $this->request->post['zone_id'], 'FALSE' );
    $this->data['form'][ 'zone_id' ] = $form->getFieldHtml( array(
                                                                       'type' => 'selectbox',
                                                                   'name' => 'zone_id',
                                                                   'required' => true ));

    $this->data['form'][ 'password' ] = $form->getFieldHtml( array(
                                                                       'type' => 'password',
                                                                   'name' => 'password',
                                                                   'placeholder' => $this->language->get('holder_password'),
                                                                   'value' => $this->request->post['password'],
                                                                   'required' => true ));
    $this->data['form'][ 'confirm' ] = $form->getFieldHtml( array(
                                                                       'type' => 'password',
                                                                   'name' => 'confirm',
                                                                   'placeholder' => $this->language->get('holder_verify'),
                                                                   'value' => $this->request->post['confirm'],
                                                                   'required' => true ));

    $newsletter = '';
    $this->data['form'][ 'newsletter' ] = $form->getFieldHtml( array(
                                                                       'type' => 'radio',
                                                                   'name' => 'newsletter',
                                                                   'value' => (!is_null($this->request->get_or_post('newsletter')) ? $this->request->get_or_post('newsletter') : -1),
                                                                   'options' => array(
                                                                           '1' => $this->language->get('text_yes'),
                                                                           '0' => $this->language->get('text_no'),
                                                                       ) ));

    $agree = isset($this->request->post['agree']) ? $this->request->post['agree'] : FALSE;
    $this->data['form'][ 'agree' ] = $form->getFieldHtml( array(
                                                                    'type' => 'checkbox',
                                                                'name' => 'agree',
                                                                'value' => 1,
                                                                'checked' => $agree ));

    $this->data['form'][ 'continue' ] = $form->getFieldHtml( array(
                                                                       'type' => 'submit',
                                                                   'name' => $this->language->get('button_continue') ));

    
    $this->data['error_warning'] = $this->errors['warning'];
    $this->data['error_loginname'] = $this->errors['loginname'];
    $this->data['error_firstname'] = $this->errors['firstname'];
    $this->data['error_lastname'] = $this->errors['lastname'];
    $this->data['error_email'] = $this->errors['email'];
    $this->data['error_telephone'] = $this->errors['telephone'];
    $this->data['error_password'] = $this->errors['password'];
    $this->data['error_confirm'] = $this->errors['confirm'];
    $this->data['error_address_1'] = $this->errors['address_1'];
    $this->data['error_city'] = $this->errors['city'];
    $this->data['error_postcode'] = $this->errors['postcode'];
    $this->data['error_country'] = $this->errors['country'];
    $this->data['error_zone'] = $this->errors['zone'];

    //validatian custom field begin
    $this->data['error_id_number'] = $this->errors['id_number'];
    $this->data['error_id_type'] = $this->errors['id_type'];
    $this->data['error_cif'] = $this->errors['cif'];

    $this->data['error_bank_account_name'] = $this->errors['bank_account_name'];
    $this->data['error_bank_name'] = $this->errors['bank_name'];
    $this->data['error_bank_account_no'] = $this->errors['bank_account_no'];

    $this->data['action'] = $this->html->getSecureURL('account/create') ;
    $this->data['login_user'] = $this->html->getSecureURL('account/login') ;
    $this->data['newsletter'] = $this->request->post['newsletter'];

    if ($this->config->get('config_account_id')) {

      $this->loadModel('catalog/content');
      $content_info = $this->model_catalog_content->getContent($this->config->get('config_account_id'));
      
      if ($content_info) {
        $text_agree = $this->language->get('text_agree');
        $this->data['text_agree_href'] = $this->html->getURL('r/content/content/loadInfo', '&content_id=' . $this->config->get('config_account_id'));
        $this->data['text_agree_href_text'] = $content_info['title'];
        $this->data['text_privacy_desc'] = $content_info['description'];
        $this->data['text_privacy_content'] = html_entity_decode($content_info['content']);
        $this->data['text_privacy_update'] = date("F j, Y",strtotime($content_info['updated']));
      } else {
        $text_agree = '';
      }
    } else {
      $text_agree = '';
    }
        $this->data['text_agree'] = $text_agree;

        $text_account_already = sprintf($this->language->get('text_account_already'), $this->html->getSecureURL('account/login') );
        $this->data['text_account_already'] = $text_account_already;

        $this->view->batchAssign($this->data);
        $this->processTemplate('pages/account/create.tpl');

        //init controller data
        $this->extensions->hk_UpdateData($this,__FUNCTION__);
    }

}