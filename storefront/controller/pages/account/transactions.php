<?php 
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' )) {
	header ( 'Location: static_pages/' );
}
class ControllerPagesAccountTransactions extends AController {
	public $data = array();
	
	/**
	 * Main Controller function to show transaction hitory. 
	 * Note: Regular orders are considered in the transactions. 
	 */
	public function main() {

        //init controller data
        $this->extensions->hk_InitData($this,__FUNCTION__);
        $this->loadLanguage('account/account');
    	if (!$this->customer->isLogged()) {
      		$this->session->data['redirect'] = $this->html->getSecureURL('account/transactions');

	  		$this->redirect($this->html->getSecureURL('account/login'));
    	}

    	$this->redirect($this->html->getSecureURL('account/transactions/history'));
 
    	$this->document->setTitle( $this->language->get('text_transactions') );
    	$this->view->assign("heading_title", $this->language->get('text_transactions'));

      	$this->document->resetBreadcrumbs();

      	$this->document->addBreadcrumb( array ( 
        	'href'      => $this->html->getURL('index/home'),
        	'text'      => $this->language->get('text_home'),
        	'separator' => FALSE
      	 )); 

      	$this->document->addBreadcrumb( array ( 
        	'href'      => $this->html->getURL('account/account'),
        	'text'      => $this->language->get('text_account'),
        	'separator' => $this->language->get('text_separator')
      	 ));
		
      	$this->document->addBreadcrumb( array ( 
        	'href'      => $this->html->getURL('account/transactions'),
        	'text'      => $this->language->get('text_transactions'),
        	'separator' => $this->language->get('text_separator')
      	 ));
				
		$this->loadModel('account/customer');

		$trans_total = $this->model_account_customer->getTotalTransactionHistory();
		
		$balance = $this->customer->getBalance();
		$this->data['balance_amount'] = $this->currency->format($balance);
				
		if ($trans_total) {
			$this->data['action'] = $this->html->getURL('account/transactions');
			
			if (isset($this->request->get['page'])) {
				$page = $this->request->get['page'];
			} else {
				$page = 1;
			}

			if (isset($this->request->get['limit'])) {
				$limit = (int)$this->request->get['limit'];
				$limit = $limit>50 ? 50 : $limit;
			} else {
				$limit = $this->config->get('config_catalog_limit');
			}
			
      		$trans = array();
			
			$results = $this->model_account_customer->getTransactions(($page - 1) * $limit, $limit);

			foreach ($results as $result) {
        		$trans[] = array(
								'customer_transaction_id'   => $result['customer_transaction_id'],
								'order_id'       => $result['order_id'],
								'section'     => $result['section'],
								'credit'      => $this->currency->format($result['credit']),
								'debit'      => $this->currency->format($result['debit']),
								'transaction_type' => $result['transaction_type'],
								'description' => $result['description'],
								'create_date' => dateISO2Display($result['create_date'],$this->language->get('date_format_short'))
        		);
      		}

      		$this->data['transactions'] = $trans;


			$this->data['pagination_bootstrap'] = HtmlElementFactory::create( array (
										'type' => 'Pagination',
										'name' => 'pagination',
										'text'=> $this->language->get('text_pagination'),
										'text_limit' => $this->language->get('text_per_page'),
										'total'	=> $trans_total,
										'page'	=> $page,
										'limit'	=> $limit,
										'url' => $this->html->getURL('account/transactions', '&limit=' . $limit . '&page={page}'),
										'style' => 'pagination'));


			$this->data['continue'] =  $this->html->getSecureURL('account/account');

			$this->view->setTemplate('pages/account/transactions.tpl');
    	} else {
			$this->data['continue'] = $this->html->getSecureURL('account/account');

			$this->view->setTemplate('pages/account/transactions.tpl');
		}

		$this->data['button_continue'] = HtmlElementFactory::create( array ('type' => 'button',
																		   'name' => 'continue_button',
																		   'text'=> $this->language->get('button_continue'),
																		   'style' => 'button'));

		$this->view->batchAssign($this->data);
		$this->processTemplate();
    //init controller data
    $this->extensions->hk_UpdateData($this,__FUNCTION__);
	}

	/**
	 * Main Controller function to show transaction hitory. 
	 * Note: Regular orders are considered in the transactions. 
	 */
	public function history() {
        //init controller data
        $this->extensions->hk_InitData($this,__FUNCTION__);
        $this->loadLanguage('account/account');
    	if (!$this->customer->isLogged()) {
      		$this->session->data['redirect'] = $this->html->getSecureURL('account/transactions');

	  		$this->redirect($this->html->getSecureURL('account/login'));
    	}

    	if($this->request->post['date_start'] || $this->request->post['date_end'] || $this->request->post['product_type']){
          $this->redirect($this->html->getSecureURL('account/transactions/history','&date_start='.$this->request->post['date_start'].'&date_end='.$this->request->post['date_end'].'&product_type='.$this->request->post['product_type']));
        }
 
    	$this->document->setTitle( $this->language->get('text_transactions') );
    	$this->view->assign("heading_title", $this->language->get('text_transactions'));

      	$this->document->resetBreadcrumbs();

      	$this->document->addBreadcrumb( array ( 
        	'href'      => $this->html->getURL('index/home'),
        	'text'      => $this->language->get('text_home'),
        	'separator' => FALSE
      	 )); 

      	$this->document->addBreadcrumb( array ( 
        	'href'      => $this->html->getURL('account/account'),
        	'text'      => $this->language->get('text_account'),
        	'separator' => $this->language->get('text_separator')
      	 ));
		
      	$this->document->addBreadcrumb( array ( 
        	'href'      => $this->html->getURL('account/transactions'),
        	'text'      => $this->language->get('text_transactions'),
        	'separator' => $this->language->get('text_separator')
      	 ));
				
		$this->loadModel('account/customer');


		$cond = array();	
  		if(!empty($this->request->get['product_type'])){
  			$cond['product_type'] = $this->request->get['product_type'];
  		}
  		if(!empty($this->request->get['date_start'])){
  			$cond['date_start'] = date("Y-m-d",strtotime($this->request->get['date_start']));
  		}
  		if(!empty($this->request->get['date_end'])){
  			$cond['date_end'] = date("Y-m-d",strtotime($this->request->get['date_end']));
  		}

		$trans_total = $this->model_account_customer->getTotalTransactionHistory($cond);
		
		$balance = $this->customer->getBalance();
		$this->data['balance_amount'] = $this->currency->format($balance);
				
		if ($trans_total) {
			$this->data['action'] = $this->html->getURL('account/transactions');
			
			if (isset($this->request->get['page'])) {
				$page = $this->request->get['page'];
			} else {
				$page = 1;
			}

			if (isset($this->request->get['limit'])) {
				$limit = (int)$this->request->get['limit'];
				$limit = $limit>50 ? 50 : $limit;
			} else {
				$limit = $this->config->get('config_catalog_limit');
			}
			
     		$trans = array();
			$results = $this->model_account_customer->getTransactionHistory(($page - 1) * $limit, $limit, $cond);
			
			foreach ($results as $result) {
        		$trans[] = array(
        			'frontend_trx_id' => $result['frontend_trx_id'],
					'transaction_date'   => dateISO2Display($result['trx_date'], 'j-M-y'),
					'product_code'       => $result['product_code'],
					'product_name'		=> ucwords(strtolower($result['name'])),
					'transaction_type'     => $result['trxtype_desc'],
					'nav_date'      => dateISO2Display($result['nav_date'], 'j-M-y'),
					'nav_value'      => number_format($result['nav_value'],2,".",","),
					'transaction_unit' => number_format($result['units'],4,".",","),
					'transaction_amount' => number_format($result['amount'],2,".",","),
					'transaction_fee'=> number_format($result['fee_amount'],2,".",","),
					'total_amount'=> number_format( ($result['amount'] + $result['fee_amount']) ,2,".",",") ,
					'saldo'=> number_format( $result['average_cost'] ,4,".",",")  
        		);
      		}

      		$this->data['transactions'] = $trans;

			$this->data['pagination_bootstrap'] = HtmlElementFactory::create( array (
										'type' => 'Pagination',
										'name' => 'pagination',
										'text'=> $this->language->get('text_pagination'),
										'text_limit' => $this->language->get('text_per_page'),
										'total'	=> $trans_total,
										'page'	=> $page,
										'limit'	=> $limit,
										'url' => $this->html->getURL('account/transactions/history', '&limit=' . $limit . '&page={page}'.'&date_start='.$this->request->get['date_start'].'&date_end='.$this->request->get['date_end'].'&product_type='.$this->request->get['product_type']),
										'style' => 'pagination'
										));


			$this->data['continue'] =  $this->html->getSecureURL('account/account');

			$this->view->setTemplate('pages/account/transaction_history.tpl');
    	} else {
			$this->data['continue'] = $this->html->getSecureURL('account/account');

			$this->view->setTemplate('pages/account/transaction_history.tpl');
		}

		$product_types[] = $this->language->get('text_select_product');
		$product_types += $this->model_account_customer->getProductOptions();
		$this->data['product_type'] = HtmlElementFactory::create( array ( 'type' => 'selectbox',
			   'name' => 'product_type',
			   'options'=> $product_types,
			   'value'=> $this->request->get['product_type'],
			   'style'=> 'form-control'
		));

		$this->data['date_start'] = HtmlElementFactory::create( array ( 
			   'type' => 'input',
			   'name' => 'date_start',
			   'value'=> $this->request->get['date_start'],
			   'style'=> 'form-control',
			   'attr'=> 'readonly'
		));

		$this->data['date_end'] = HtmlElementFactory::create( array ( 
			   'type' => 'input',
			   'name' => 'date_end',
			   'value'=> $this->request->get['date_end'],
			   'style'=> 'form-control',
			   'attr'=> 'readonly'
		));

		$this->data['date_start_mobile'] = HtmlElementFactory::create( array ( 
			   'type' => 'input',
			   'name' => 'date_start',
			   'value'=> $this->request->get['date_start'],
			   'style'=> 'form-control',
			   'id'=>'date_start_mobile',
			   'attr'=> 'readonly'
		));

		$this->data['date_end_mobile'] = HtmlElementFactory::create( array ( 
			   'type' => 'input',
			   'name' => 'date_end',
			   'value'=> $this->request->get['date_end'],
			   'style'=> 'form-control',
			   'id'=>'date_end_mobile',
			   'attr'=> 'readonly'
		));

		$this->data['date_start_value'] = $this->request->get['date_start'];
		$this->data['date_end_value'] = $this->request->get['date_end'];

		$this->data['export_url'] = $this->html->getSecureURL('account/transactions/exportHistory','&date_start='.$this->request->get['date_start'].'&date_end='.$this->request->get['date_end'].'&product_type='.$this->request->get['product_type']);
		$this->data['filter_url'] = $this->html->getSecureURL('account/transactions/history');

		$this->data['button_continue'] = HtmlElementFactory::create( array ('type' => 'button',
																		   'name' => 'continue_button',
																		   'text'=> $this->language->get('button_continue'),
																		   'style' => 'button'));

		//trx_history_retention_in_days
		$this->loadModel('setting/store');
		$this->data['trx_history_retention_in_days'] = $this->model_setting_store->getSettingValue('general','trx_history_retention_in_days');

		$this->view->batchAssign($this->data);
		$this->processTemplate();
        //init controller data
        $this->extensions->hk_UpdateData($this,__FUNCTION__);
	}

	public function exportHistory(){
		//init controller data
        $this->extensions->hk_InitData($this,__FUNCTION__);
        $this->loadLanguage('account/account');

    	if (!$this->customer->isLogged()) {
      		$this->session->data['redirect'] = $this->html->getSecureURL('account/transactions');

	  		$this->redirect($this->html->getSecureURL('account/login'));
    	}

		require_once(DIR_ROOT . '/dompdf/dompdf_config.inc.php');

		$cond = array();	
  		if(!empty($this->request->get['product_type'])){
  			$cond['product_type'] = $this->request->get['product_type'];
  		}
  		if(!empty($this->request->get['date_start'])){
  			$cond['date_start'] = date("Y-m-d",strtotime($this->request->get['date_start']));
  		}
  		if(!empty($this->request->get['date_end'])){
  			$cond['date_end'] = date("Y-m-d",strtotime($this->request->get['date_end']));
  		}

		$this->loadModel('account/customer');
		$results = $this->model_account_customer->getTransactionHistory(false, false, $cond);
			
  		$trans = array();
		foreach ($results as $result) {
    		$trans[] = array(
				'transaction_date'   => dateISO2Display($result['trx_date'], 'j-M-y'),
				'product_code'       => $result['product_code'],
				'product_name'		=> ucwords(strtolower($result['name'])),
				'transaction_type'     => $result['trxtype_desc'],
				'nav_date'      => dateISO2Display($result['nav_date'], 'j-M-y'),
				'nav_value'      => number_format($result['nav_value'],2,".",","),
				'transaction_unit' => number_format($result['units'],4,".",","),
				'transaction_amount' => number_format($result['amount'],2,".",","),
				'transaction_fee'=> number_format($result['fee_amount'],2,".",","),
				'total_amount'=> number_format( ($result['amount'] + $result['fee_amount']) ,2,".",",") ,
				'saldo'=> number_format( $result['average_cost'] ,4,".",",")  
    		);

    		if(!empty($this->request->get['product_type'])){
    			$product_name = ucwords(strtolower($result['name']));
    		}
  		}

  		// HTML Mail
		$template = new ATemplate();
		$template->data['transactions'] = $trans;
		$template->data['heading_title'] = $this->language->get('text_transactions');
		$template->data['date_start'] = $this->request->get['date_start'];
    $template->data['date_end'] = $this->request->get['date_end'];
    $template->data['generated_date'] = date("d-M-y");
    $template->data['product_name'] = $product_name ? $product_name : 'All';

		$this->loadModel('account/customer_profile');

		$customer = $this->model_account_customer->getCustomer($this->customer->getId());
		$customer_info = $this->model_account_customer_profile->getCustomer($customer['holder_id']);
		$customer_country = $this->model_account_customer->getCountryByCode($customer_info['correspondence_country']);

		$template->data['cust_name'] = $customer['firstname'].' '.$customer['lastname'];
		$template->data['cust_address'] = $customer_info['correspondence_address'];
		
		$template->data['cust_postal_code'] = $customer_info['correspondence_postal_code'];
		$template->data['cust_country'] = $customer_country;

		$this->loadModel('localisation/zone');
		$cor_zone = $this->model_localisation_zone->getZonesByCountryCode($customer_info['correspondence_country']);
		foreach ($cor_zone as $key => $data) {
			if($data['code'] == $customer_info['correspondence_city']){
				$customer_city = $data['name'];
			}
		}

		$template->data['cust_city'] = $customer_city;
		$template->data['date_print'] = $this->request->get['date_print'] ? $this->request->get['date_print'] : date("d-M-y H:i:s");

		//** Multi language
		$template->data['text_transactions'] = $this->language->get('text_transactions');
		$template->data['text_column_date'] = $this->language->get('text_column_date');
		$template->data['text_column_name'] = $this->language->get('text_column_name');
		$template->data['text_column_type'] = $this->language->get('text_column_type');
		$template->data['text_column_nav_date'] = $this->language->get('text_column_nav_date');
		$template->data['text_column_navpu'] = $this->language->get('text_column_navpu');
		$template->data['text_column_unit'] = $this->language->get('text_column_unit');
		$template->data['text_column_amount'] = $this->language->get('text_column_amount');
		$template->data['text_column_fee'] = $this->language->get('text_column_fee');
		$template->data['text_column_total_amount'] = $this->language->get('text_column_total_amount');
		$template->data['text_column_saldo'] = $this->language->get('text_column_saldo');
		$template->data['text_transaction_date'] = $this->language->get('text_transaction_date');
		$template->data['text_to'] = $this->language->get('text_to');
		$template->data['text_reserved'] = $this->language->get('text_reserved');
		$template->data['text_print_date'] = $this->language->get('text_print_date');
		$template->data['text_disclaimer'] = $this->language->get('text_disclaimer');
		$template->data['entry_disclaimer'] = $this->language->get('entry_disclaimer');

		$html = $template->fetch('pages/account/transaction_history_export.tpl');

		// For Testing
		// echo $html;
		// return;

		$dompdf = new DOMPDF();
		$dompdf->set_base_path(DIR_ROOT.'/');
		$dompdf->load_html($html);
		$dompdf->set_paper('a4', 'landscape');
		$dompdf->render();

		$canvas = $dompdf->get_canvas();
		$font = Font_Metrics::get_font('Open Sans Light', 'normal');
        $canvas->page_text(400, 572, "{PAGE_NUM} ".$this->language->get('text_of')." {PAGE_COUNT}", $font, 7, array(0,0,0));

		$pdf = $dompdf->output();

		header('Content-Disposition: attachment; filename="transaction_history_'.date("YmdHis").'.pdf"');
		header('Content-Type: application/pdf'); 
		header('Content-Length: ' . strlen($pdf));
		header('Connection: close');
		echo $pdf;

		//$dompdf->stream("transaction_history_".date("YmdHis").".pdf");

		//init controller data
        $this->extensions->hk_UpdateData($this,__FUNCTION__);
	}

}

