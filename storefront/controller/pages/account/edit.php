<?php
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' )) {
	header ( 'Location: static_pages/' );
}
class ControllerPagesAccountEdit extends AController {
	private $error = array();
  public $data;
    
	public function main() {
    //init controller data
    $this->extensions->hk_InitData($this,__FUNCTION__);
    $this->loadCryptLib();

		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->html->getSecureURL('account/edit');

			$this->redirect($this->html->getSecureURL('account/login'));
		}

		$this->loadLanguage('account/account');
		$this->document->setTitle( $this->language->get('heading_title') );
		
		$this->loadModel('account/customer');
		$this->loadModel('account/customer_profile');
		$this->loadModel('account/country');
		$this->loadModel('localisation/country');
		$this->loadModel('localisation/zone');
    	$this->loadModel('tool/lookup');
		
		if ($this->request->server['REQUEST_METHOD'] != 'POST') {
			$customer_info = $this->model_account_customer->getCustomer($this->customer->getId());
			
		}

		//get customer holder_id
		$holder_id = $this->model_account_customer->getField($this->customer->getID(),"holder_id");
		$cif = $this->model_account_customer->getField($this->customer->getID(),"cif");
		// $this->showDebug($this->request->post,true);

		$customer_profile_info = $this->model_account_customer_profile->getCustomer($holder_id);
		// if (!$customer_profile_info) {
		$customer_profile_temp = $this->model_account_customer_profile->getCustomer($holder_id,"customer_profiles_temp");
		// }
		
		
		// check is profile pending
		$isPending = $this->model_account_customer_profile->isPendingProfile($holder_id);
		
		if ( $this->request->server['REQUEST_METHOD'] == 'POST'&&$isPending=='0') {
			
			$request_data = $this->request->post;
			
			$request_data['cif'] = $cif;
			$this->request->post['firstname'] = $this->model_account_customer->getField($this->customer->getID(),"firstname");
			$this->request->post['lastname'] = $this->model_account_customer->getField($this->customer->getID(),"lastname");
			$request_data['full_name'] = $request_data['firstname'].' '.$request_data['lastname'];
			// encrypt account name & account name 
			$rsa = new Crypt_RSA();
      $request_data["bank_account_name"] = $this->decrypt($this->privatekey,$request_data["bank_account_name"]);
      $request_data["bank_account_no"] = $this->decrypt($this->privatekey,$request_data["bank_account_no"]);
			
			// $this->showDebug($request_data,false);
	      	
			$request_data['holder_id'] = $holder_id;
			$table = "customer_profiles_temp";	

			$request_data['id_expiry_date'] = date("Y-m-d", strtotime($request_data['id_expiry_date']));
			$request_data['date_of_birth'] = date("Y-m-d", strtotime($request_data['date_of_birth']));
			$request_data['tax_id_reg_date'] = date("Y-m-d", strtotime($request_data['tax_id_reg_date']));
			
			//var_dump($request_data);exit();
			$customer = $this->model_account_customer->getCustomer($this->customer->getId());
  			
  			//$subject = sprintf($this->language->get('text_subject'),$this->config->get('store_name'));
			$subject = $this->config->get('store_name').' - '.$this->language->get('text_subject_email');
			
  			$message .= 'Hi '.$customer["firstname"].",\n\n";
  			$message .= $this->language->get('text_edit_notification')."\n\n\n\n";
  			$message .= $this->language->get('text_thanks')."\n";
  			$message .= $this->config->get('store_name');

				$mail = new AMail( $this->config );
				$mail->setTo($customer['email']);
				$mail->setFrom($this->config->get('store_main_email'));
				$mail->setSender($this->config->get('store_name'));
				$mail->setSubject($subject);
				$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
				$mail->send();
				
				$request_data['approved'] = 0;

			if ($customer_profile_temp) {
				
				// $customer_profile = $this->model_account_customer_profile->updateCustomerProfile($request_data,'customer_profiles_temp');
				// mengecek profile ada atau belum di 'customer_profiles' tabel

				// $isProfileFound = $this->model_account_customer_profile->getCustomer($holder_id);

				// if ($isProfileFound) {
				$this->model_account_customer_profile->updateFieldCustomer('approved',0,$holder_id);
				$this->model_account_customer_profile->updateCustomerProfile($request_data,'customer_profiles_temp');
				// }else{
				// 	$this->model_account_customer_profile->addCustomerProfile($request_data);
				// }

			} else {
				// $this->showDebug($holder_id);
				// $this->model_account_customer_profile->addCustomerProfile($request_data);
					$this->model_account_customer_profile->updateFieldCustomer('approved',0,$holder_id);
					$this->model_account_customer_profile->addCustomerProfile($request_data,'customer_profiles_temp');
			}

			
				$this->error = $this->model_account_customer->validateEditData($request_data);
				$this->session->data['success'] = $this->language->get('text_success');
				$this->session->data['edit_profile'] = 'profile';
				$this->redirect($this->html->getSecureURL('account/success'));
		}

		//check if existing customer has loginname = email. Redirect if not allowed
		$reset_loginname = false;
		if ($this->config->get('prevent_email_as_login') && $this->customer->isLoginnameAsEmail() ) {
		    $this->error['warning'] = $this->language->get('loginname_update_required');
		    $reset_loginname = true;
		}
		
  	$this->document->resetBreadcrumbs();
  	$this->document->addBreadcrumb( array ( 
    	'href'      => $this->html->getURL('index/home'),
    	'text'      => $this->language->get('text_home'),
    	'separator' => FALSE
  	 )); 
  	$this->document->addBreadcrumb( array ( 
    	'href'      => $this->html->getURL('account/account'),
    	'text'      => $this->language->get('text_account'),
    	'separator' => $this->language->get('text_separator')
  	 ));

  	$this->document->addBreadcrumb( array ( 
    	'href'      => $this->html->getURL('account/edit'),
    	'text'      => $this->language->get('text_edit'),
    	'separator' => $this->language->get('text_separator')
  	 ));

  		
		/* fazrin - data untuk form pada saat di load */
		$this->view->assign('post_url',$this->html->getURL('account/edit'));

		$this->view->assign('data_account',$customer_profile_info);
		$this->view->assign('error_warning', $this->error['warning'] );
		$this->view->assign('error_loginname', $this->error['loginname'] );
		$this->view->assign('error_firstname', $this->error['firstname'] );
		$this->view->assign('error_lastname', $this->error['lastname'] );
		$this->view->assign('error_email', $this->error['email'] );
		$this->view->assign('error_telephone', $this->error['telephone'] );

		if (isset($request_data['loginname'])) {
			$loginname = $request_data['loginname'];
		} elseif (isset($customer_info)) {
      $loginname = $customer_info['loginname'];
		}

		if (isset($request_data['firstname'])) {
			$firstname = $request_data['firstname'];
		} elseif (isset($customer_info)) {
      $firstname = $customer_info['firstname'];
		}

		if (isset($request_data['lastname'])) {
			$lastname = $request_data['lastname'];
		} elseif (isset($customer_info)) {
      $lastname = $customer_info['lastname'];
		}

		if (isset($request_data['email'])) {
			$email = $request_data['email'];
		} elseif (isset($customer_info)) {
      $email = $customer_info['email'] ;
		}

		if (isset($request_data['phone_no'])) {
			$telephone = $request_data['phone_no'];
		} elseif (isset($customer_info)) {
            $telephone = $customer_info['phone_no'];
		}

		if (isset($request_data['fax'])) {
			$fax = $request_data['fax'];
		} elseif (isset($customer_info)) {
            $fax = $customer_profile_info['fax'];
		}

		// set value on textfield
		if (isset($request_data['full_name'])) {
			$full_name = $request_data['full_name'];
		} elseif (isset($customer_profile_info)) {
			if (empty($customer_profile_info['full_name'])) {
				$full_name = $customer_info['firstname'].' '.$customer_info['lastname'];
			} else {
				$full_name = $customer_profile_info['full_name'];
			}
		}

		if (isset($request_data['domicile_address'])) {
			$domicile_address = $request_data['domicile_address'];
		} elseif (isset($customer_profile_info)) {
            $domicile_address = $customer_profile_info['domicile_address'];
		}

		if (isset($request_data['bank_address'])) {
			$bank_address = $request_data['bank_address'];
		} elseif (isset($customer_profile_info)) {
            $bank_address = $customer_profile_info['bank_address'];
		}
		if (isset($request_data['bank_account_name'])) {
			$bank_address = $request_data['bank_account_name'];
		} elseif (isset($customer_profile_info)) {
            $bank_address = $customer_profile_info['bank_account_name'];
		}
		if (isset($request_data['bank_account_no'])) {
			$bank_address = $request_data['bank_account_no'];
		} elseif (isset($customer_profile_info)) {
            $bank_address = $customer_profile_info['bank_account_no'];
		}

		if (isset($request_data['religion'])) {
			$religion = $request_data['religion'];
		} elseif (isset($customer_profile_info)) {
            $religion = $customer_profile_info['religion'];
		}

		if (isset($request_data['gender'])) {
			$gender = $request_data['gender'];
		} elseif (isset($customer_profile_info)) {
            $gender = $customer_profile_info['gender'];
		}

		if (isset($request_data['marital_status'])) {
			$marital_status = $request_data['marital_status'];
		} elseif (isset($customer_profile_info)) {
      $marital_status = $customer_profile_info['marital_status'];
		}

		if (isset($request_data['residency_status'])) {
			$residency_status = $request_data['residency_status'];
		} elseif (isset($customer_profile_info)) {
      $residency_status = $customer_profile_info['residency_status'];
		}

		if (isset($request_data['residency_status_txt'])) {
			$residency_status_txt = $request_data['residency_status_txt'];
		} elseif (isset($customer_profile_info)) {
       $residency_status_txt = $customer_profile_info['residency_status_txt'];
		}

		if (isset($request_data['occupation'])) {
			$occupation = $request_data['occupation'];
		} elseif (isset($customer_profile_info)) {
      $occupation = $customer_profile_info['occupation'];
		}

		if (isset($request_data['occupation_txt'])) {
			$occupation_txt = $request_data['occupation_txt'];
		} elseif (isset($customer_profile_info)) {
            $occupation_txt = $customer_profile_info['occupation_txt'];
		}

		if (isset($request_data['gross_income'])) {
			$gross_income = $request_data['gross_income'];
		} elseif (isset($customer_profile_info)) {
            $gross_income = $customer_profile_info['gross_income'];
		}

		if (isset($request_data['additional_income'])) {
			$additional_income = $request_data['additional_income'];
		} elseif (isset($customer_profile_info)) {
            $additional_income = $customer_profile_info['additional_income'];
		}

		if (isset($request_data['add_source_of_income'])) {
			$add_source_of_income = $request_data['add_source_of_income'];
		} elseif (isset($customer_profile_info)) {
            $add_source_of_income = $customer_profile_info['add_source_of_income'];
		}

		if (isset($request_data['source_of_income'])) {
			$source_of_income = $request_data['source_of_income_'];
		} elseif (isset($customer_profile_info)) {
            $source_of_income = $customer_profile_info['source_of_income'];
		}

		if (isset($request_data['source_of_income_txt'])) {
			$source_of_income_txt = $request_data['source_of_income_txt'];
		} elseif (isset($customer_profile_info)) {
            $source_of_income_txt = $customer_profile_info['source_of_income_txt'];
		}

		if (isset($request_data['source_of_fund'])) {
			$source_of_fund = $request_data['source_of_fund'];
		} elseif (isset($customer_profile_info)) {
            $source_of_fund = $customer_profile_info['source_of_fund'];
		}
		if (isset($request_data['source_of_fund_txt'])) {
			$source_of_fund_txt = $request_data['source_of_fund_txt'];
		} elseif (isset($customer_profile_info)) {
            $source_of_fund_txt = $customer_profile_info['source_of_fund_txt'];
		}
		
		if (isset($request_data['inv_objective'])) {
			$inv_objective = $request_data['inv_objective'];
		} elseif (isset($customer_profile_info)) {
            $inv_objective = $customer_profile_info['inv_objective'];
		}
		if (isset($request_data['inv_objective_txt'])) {
			$inv_objective_txt = $request_data['inv_objective_txt'];
		} elseif (isset($customer_profile_info)) {
       $inv_objective_txt = $customer_profile_info['inv_objective_txt'];
		}
		
		//get data for select
		$this->data['religion'] = $this->model_tool_lookup->getLookupData('RELIGION');
    $countries = $this->model_localisation_country->getCountries();

		$this->data['countries'] = array("" => $this->language->get('text_select') );
    foreach($countries as $item){
        $this->data['countries'][ $item['iso_code_2'] ] = $item['name'];
    }

    $dom_cities = $this->model_localisation_zone->getZonesByCountryCode($customer_profile_info['domicile_country']);
    if (!is_null($dom_cities)) {
    	foreach ($dom_cities as $item) {
	      $options_city[ $item['code'] ] = $item['name'];
	    }
		// var_dump($options_city);
	    $this->data['dom_cities'] = $options_city;
    }

    $cor_cities = $this->model_localisation_zone->getZonesByCountryCode($customer_profile_info['correspondence_country']);
    if (!is_null($cor_cities)) {
    	foreach ($cor_cities as $item) {
	      $options_cor[ $item['code'] ] = $item['name'];
	    }
		
	    $this->data['cor_cities'] = $options_cor;
    }
    
    

	// bank name selectbox end
	$this->data['banks'] = $this->model_tool_lookup->getLookupData('BANK_NAME');
    $this->data['ids'] = $this->model_tool_lookup->getLookupData('ID_TYPE');
    $this->data['nationalities'] = $this->model_tool_lookup->getLookupData('NATIONALITY');
    $this->data['company_type'] = $this->model_tool_lookup->getLookupData('COMPANY_TYPE');
    $this->data['education'] = $this->model_tool_lookup->getLookupData('EDUCATION');
    $this->data['inheritor_relationship'] = $this->model_tool_lookup->getLookupData('INHERITOR_RELATIONSHIP');
    
    // $this->showDebug($this->data['banks']);

	// pending assign
	$this->data['ispending'] = $isPending;

	$this->data['back'] = $this->html->getSecureURL('account/account');
	$this->view->batchAssign($this->data);
    $this->processTemplate('pages/account/edit.tpl');

    //init controller data
    $this->extensions->hk_UpdateData($this,__FUNCTION__);
	}

	public function getCity(){
		$city = "bandung";
		return $city;
	}
}
?>