<?php 
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' )) {
	header ( 'Location: static_pages/' );
}
class ControllerPagesAccountChangepass extends AController {
	
	public $errors = array();
	public $data;

	public function main() {
		$this->loadCryptLib();
		$this->loadModel('tool/lookup');
		$this->loadModel('account/customer');
		$this->loadLanguage('account/reset');

		//cek dari tabel customer where reset key = key
		$key = $this->request->get['key'];
		$custSyncdate = $this->model_account_customer->getFieldCustom('sync_datetime','reset_key',$key);

		$lang = $this->language->getContentLanguageID();
		//ambil dari table lookup
		// $expired = $this->model_tool_lookup->getLookupDescription("EXPIRE_LINK");
		$expired = $this->config->get('expire_link');

		$datetime1 = new DateTime(date("Y/m/d"));
		$datetime2 = new DateTime($custSyncdate);
		$interval = $datetime1->diff($datetime2);

		if(is_null($expired)) $expired['item_name'] = 1;
		if($interval->invert==1 && $interval->days > $expired['item_name']){
			//delete the reset key
			$this->model_account_customer->editCustomerField('reset_key','','reset_key',$key);
		}

	  //init controller data
	  $this->extensions->hk_InitData($this,__FUNCTION__);

		if ($this->customer->isLogged()) {
	  		$this->redirect($this->html->getSecureURL('account/account'));
    	}

		$this->document->setTitle( $this->language->get('change_pass_title') );
		
		$this->loadModel('account/customer');
		$request_data = $this->request->post;
		
		$rsa = new Crypt_RSA();

		if ( $this->request->server['REQUEST_METHOD'] == 'POST') {
			
	    $request_data["password"] = $this->decrypt($this->privatekey,$request_data["password"]);
	    $request_data["confirm"] = $this->decrypt($this->privatekey,$request_data["confirm"]);
			$request_data["old_password"] = $this->decrypt($this->privatekey,$request_data["old_password"]);

			$this->errors = array_merge($this->errors,$this->model_account_customer->validateChangePass($request_data));
    		if ( !$this->errors ) {
    			
    			$key = $request_data['key'];
    			$custID = $this->model_account_customer->getFieldCustom('customer_id','reset_key',$key);
    			$loginname = $this->model_account_customer->getFieldCustom('email','customer_id',$custID);
    			$password = $request_data["password"];
    			// var_dump($loginname);exit();
    			$this->model_account_customer->resetPassByKey($request_data['password'],$request_data['key']);
	  			
	  			$this->model_account_customer->editCustomerField('approved', 1,'customer_id',$custID);
	  			$this->model_account_customer->updateAuditStatus($custID, 1);

				$this->extensions->hk_UpdateData($this,__FUNCTION__);

			      if (isset($loginname) && isset($password) && $this->_validate($loginname, $password)) {
			        unset($this->session->data['guest']);
			        unset($this->session->data['account']);

			        $address_id = $this->customer->getAddressId();
			        $this->loadModel('account/address');
			        $address =  $this->model_account_address->getAddress($address_id);
			        $this->tax->setZone($address['country_id'], $address['zone_id']);

			        if ( $this->session->data['redirect'] ) {
			          $redirect_url = $this->session->data['redirect'];
			          unset($this->session->data['redirect']);
			          $this->redirect( $redirect_url );
			        } else {
			          $this->redirect($this->html->getSecureURL('account/account'));
			        } 
			      }

		  		// $this->redirect($this->html->getSecureURL('account/success/reset'));
	  		}else{
	  			$this->redirect($this->html->getSecureURL('account/changepass&key='.$request_data['key']));	

	  		}
    } 

  	$isKeyFound = $this->model_account_customer->isKeyValid($this->request->get['key']);

  	if ($isKeyFound) {
  		$form = new AForm();
      $form->setForm(array( 'form_name' => 'AccountFrm' ));
      $this->data['form'][ 'form_open' ] = $form->getFieldHtml( array( 'type' => 'form',
                                                                       'name' => 'AccountFrm',
                                                                       'action' => $this->html->getSecureURL('account/changepass')));

      $this->data['form'][ 'old_password' ] = $form->getFieldHtml( array(
	                                                                    'type' => 'password',
			                                                               'name' => 'old_password',
			                                                               'style' => 'form-control',
			                                                               'placeholder' => $this->language->get('holder_old_password'),
			                                                               'value' => $this->request->post['old_password'],
			                                                               'required' => true ));
			$this->data['form'][ 'password' ] = $form->getFieldHtml( array(
	                                                                    'type' => 'password',
			                                                               'name' => 'password',
			                                                               'style' => 'form-control',
			                                                               'placeholder' => $this->language->get('holder_password'),
			                                                               'value' => $this->request->post['password'],
			                                                               'required' => true ));
			$this->data['form'][ 'confirm' ] = $form->getFieldHtml( array(
	                                                                   'type' => 'password',
			                                                               'name' => 'confirm',
			                                                               'style' => 'form-control',
			                                                               'placeholder' => $this->language->get('holder_confirm'),
			                                                               'value' => $this->request->post['confirm'],
			                                                               'required' => true ));
			$this->data['form'][ 'key' ] = $form->getFieldHtml( array(
	                                                                   'type' => 'hidden',
			                                                               'name' => 'key',
			                                                               'value' => $this->request->get['key'],
			                                                               'required' => true ));

			$this->data['form'][ 'continue' ] = $form->getFieldHtml( array(
	                                                                   'type' => 'submit',
			                                                               'name' => $this->language->get('button_continue') ));
			$valid = true;
    }
      	
    	
		$this->data['error_old_password'] = $this->session->data["error_old_password"];
		$this->data['error_password'] = $this->session->data['password'];
		$this->data['error_confirm'] = $this->session->data['confirm'];
    	//var_dump($this->session);
   		$this->data['action'] = $this->html->getSecureURL('account/changepass') ;
		
		$this->view->assign('valid',$valid);
		$this->view->batchAssign($this->data);
		//var_dump($this->session->data);			
    	$this->processTemplate('pages/account/changepass.tpl');

    //init controller data
    
    $this->extensions->hk_UpdateData($this,__FUNCTION__);
    unset($this->session->data['error_old_password']);
    unset($this->session->data['password']);
    unset($this->session->data['confirm']);

  	}
  	private function _validate($loginname, $password) {
      if (!$this->customer->login( $loginname, $password )) {
          $this->error['message'] = $this->language->get('error_login');
      }else{
        $this->loadModel('account/address');
      $address = $this->model_account_address->getAddress($this->customer->getAddressId());

        $this->session->data['country_id'] = $address['country_id'];
        $this->session->data['zone_id'] = $address['zone_id'];
  
      //check if existing customer has loginname = email. Redirect if not allowed
      if ($this->config->get('prevent_email_as_login') && $this->customer->isLoginnameAsEmail() ) {
        $this->redirect($this->html->getSecureURL('account/edit'));
      }
      }
  
      if (!$this->error) {
          return TRUE;
      } else {
          return FALSE;
      }   
    }
}