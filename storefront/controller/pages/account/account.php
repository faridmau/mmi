<?php 
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>
  
 UPGRADE NOTE: 
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.  
------------------------------------------------------------------------------*/

class ControllerPagesAccountAccount extends AController {
	public $data = array();
	public function main() {

    //init controller data

    $this->extensions->hk_InitData($this,__FUNCTION__);

    if (!$this->customer->isLogged()) {
      $this->session->data['redirect'] = $this->html->getSecureURL('account/account');
      $this->redirect($this->html->getSecureURL('account/login'));
    }

    if (!$this->customer->customerAccess()) {
       $this->redirect($this->html->getSecureURL('account/denied'));
    }

    $this->document->resetBreadcrumbs();

    $this->document->addBreadcrumb( array ( 
    	'href'      => $this->html->getURL('index/home'),
    	'text'      => $this->language->get('text_home'),
    	'separator' => FALSE
     )); 

    $this->document->addBreadcrumb( array ( 
    	'href'      => $this->html->getURL('account/account'),
    	'text'      => $this->language->get('text_account'),
    	'separator' => $this->language->get('text_separator')
     ));

    $this->document->setTitle( $this->language->get('heading_title') );

    $this->data['success'] = '';
    if (isset($this->session->data['success'])) {
    	$this->data['success'] = $this->session->data['success'];
    	unset($this->session->data['success']);
    }

    $this->data['information'] = $this->html->getSecureURL('account/edit');
    $this->data['password'] = $this->html->getSecureURL('account/password');
    $this->data['address'] = $this->html->getSecureURL('account/address');
    $this->data['history'] = $this->html->getSecureURL('account/history');
    $this->data['download'] = $this->html->getSecureURL('account/download');
    $this->data['newsletter'] = $this->html->getSecureURL('account/newsletter');
    $this->data['transactions'] = $this->html->getSecureURL('account/transactions');

    $this->loadLanguage('common/header');
    $this->data['logout'] = $this->html->getSecureURL('account/logout');

    $this->data['customer_name'] = $this->customer->getFirstName();

    $balance = $this->customer->getBalance();

    $this->data['balance_amount'] = $this->currency->format($balance);
    if($balance!=0 || ($balance==0 && $this->config->get('config_zero_customer_balance'))){
      $this->data['balance'] = $this->language->get('text_balance_checkout').' '.$this->data['balance_amount'];
    }


    $this->loadModel('account/address');
    $this->data['total_adresses'] = $this->model_account_address->getTotalAddresses();

    $this->data['total_downloads'] = $this->download->getTotalDownloads();

    $this->loadModel('account/order');
    $this->data['total_orders'] = $this->model_account_order->getTotalOrders();


    // **NAV Historical chart //
    $this->loadModel('account/customer');

    $this->loadModel('catalog/product');
    $nav_products = $this->model_catalog_product->getHistoricalProducts();
    foreach ($nav_products as $key => $data) {
      $nav_products[$key]['name'] = ucwords(strtolower($data['name']));
    }

    $this->data['nav_products'] = $nav_products;

    $nav_history_label = array();
    $nav_history_data = array();

    $nav_history = array_reverse($this->model_account_customer->getNavHistory( $nav_products[0]['model'] ));

    foreach ($nav_history as $key => $data) {
      $nav_history_label[] = dateISO2Display($data['nav_date'], 'j-M-y');
      $nav_history_data[] = $data['nav_per_unit'] ? $data['nav_per_unit'] : 0;
    }

    $nav_history_label = array_unique($nav_history_label);

    $this->data['nav_history_label'] = json_encode($nav_history_label);
    $this->data['nav_history_data'] = json_encode($nav_history_data);
    // **END NAV Historical chart //


    // **PORTFOLIO BALANCE //
    $portfolio_balance = $this->getPortfolioBalance();
    $this->data['portfolio_balance_chart'] = json_encode($portfolio_balance['portfolio_balance_chart']);
    $this->data['portfolio_balance_summary'] = $portfolio_balance['portfolio_balance_summary'];

    $risk_profile = $this->getRiskprofile();
    $this->data['risk_profile'] = $risk_profile['risk_profile'];
    $this->data['smart_analysis'] = $risk_profile['analysis'];
    $this->data['risk_profile_last_update'] = $risk_profile['risk_profile_last_update'];

    $this->loadModel('tool/lookup');    
    $this->data['fund_type'] = $this->model_tool_lookup->getFundType();
    // **END PORTFOLIO BALANE //

    $urlPOST = $this->html->getSecureURL('account/reminder');
    $this->view->assign('urlPOST',$urlPOST);

    // make reminder form
    $form = new AForm();
    $form->setForm(array( 'form_name' => 'ReminderFrm' ));
    $this->data['form'][ 'form_open' ] = $form->getFieldHtml( array( 'type' => 'form',
                                                                   'name' => 'ReminderFrm',
                                                                   'action' => $this->html->getSecureURL('account/reminder')));

    $this->data['form'][ 'descriptions' ] = $form->getFieldHtml( array(
                                                                  'type' => 'input',
                                                                 'name' => 'descriptions',
                                                                 'style' => 'form-control'
                                                                 ));
    $this->data['form'][ 'reminder_date' ] = $form->getFieldHtml( array(
                                                                  'type' => 'input',
                                                                 'name' => 'date',
                                                                 'style' => 'form-control'
                                                                 ));
    // reminder section
    $this->loadModel('account/reminder');
    $reminders = $this->model_account_reminder->getReminders($this->customer->getId());

    $this->data['reminders'] = $reminders;
    $this->data['delete_url'] = $this->html->getSecureURL('account/reminder');

    // reminder messages
    $this->data['reminder_saved'] = $this->session->data['reminder_saved'];
    $this->data['reminder_deleted'] = $this->session->data['reminder_deleted'];
    
    
    $this->data['ideal_url'] = $this->html->getSecureURL('account/account/idealportfolio');
    $this->data['refresh_url'] = $this->html->getSecureURL('account/account');

    $this->view->batchAssign($this->data);

    // $this->processTemplate('pages/account/account.tpl');
    $cart_link = $this->html->getSecureURL('checkout/cart');
    $trans_link = $this->html->getSecureURL('account/transactions');
    $this->view->assign('cart_link',$cart_link);
    $this->view->assign('trans_link',$trans_link);
    
    $this->processTemplate('pages/account/dashboard.tpl');

    //init controller data
    $this->extensions->hk_UpdateData($this,__FUNCTION__);
    unset($this->session->data['reminder_saved']);
    unset($this->session->data['reminder_deleted']);
  }

  public function idealportfolio(){
    //init controller data
    $this->extensions->hk_InitData($this,__FUNCTION__);

    if (!$this->customer->isLogged()) {
      $this->session->data['redirect'] = $this->html->getSecureURL('account/account');
      $this->redirect($this->html->getSecureURL('account/login'));
    }

    if (!$this->customer->customerAccess()) {
       $this->redirect($this->html->getSecureURL('account/denied'));
    }

    $this->document->resetBreadcrumbs();

    $this->document->addBreadcrumb( array ( 
      'href'      => $this->html->getURL('index/home'),
      'text'      => $this->language->get('text_home'),
      'separator' => FALSE
     )); 

    $this->document->addBreadcrumb( array ( 
      'href'      => $this->html->getURL('account/account/idealportfolio'),
      'text'      => $this->language->get('text_account'),
      'separator' => $this->language->get('text_separator')
     ));

    $this->document->setTitle( $this->language->get('heading_title') );

    $portfolio_balance_chart = $this->getRiskprofile();

    $this->data['risk_profile'] = $portfolio_balance_chart['risk_profile'];
    $this->data['ideal_portfolio'] = json_encode($portfolio_balance_chart['data']);

    $this->loadModel('tool/lookup');    
    $this->data['fund_type'] = $this->model_tool_lookup->getFundType();

    $this->view->batchAssign($this->data);
    $this->processTemplate('pages/account/ideal_portfolio.tpl');

    //init controller data
    $this->extensions->hk_UpdateData($this,__FUNCTION__);
  }

  private function getRiskprofile($data){
    $this->loadModel('account/customer');
    $risk_profile = $this->model_account_customer->getRiskprofile();
    $risk_profile_alloc = $this->model_account_customer->getRiskprofileAlloc($risk_profile['risk_profile']);
    $risk_profile_threshold = $this->config->get('risk_profile_threshold');

    $portfolio_balance = $this->model_account_customer->getPortfolioBalance();
    foreach ($portfolio_balance as $key => $data) {
      $portfolio_balances[$data['fund_type']]['portfolio_percentage'] += $data['portfolio_percentage'];
    }

    foreach ($risk_profile_alloc as $key => $data) {
      $lowerbound = $data['allocation_pct'] - $risk_profile_threshold;
      $upperbound = $data['allocation_pct'] + $risk_profile_threshold;

      if(!$portfolio_balances[$data['asset_class']]['portfolio_percentage']){
        $portfolio_balances[$data['asset_class']]['portfolio_percentage'] = 0;
      }

      if( $portfolio_balances[$data['asset_class']]['portfolio_percentage'] >= $lowerbound && $portfolio_balances[$data['asset_class']]['portfolio_percentage'] <= $upperbound ) {
        $risk_profile_analysis[$data['asset_class']] = $this->language->get('entry_ideal');
      } else if( $portfolio_balances[$data['asset_class']]['portfolio_percentage'] < $lowerbound ){
        $risk_profile_analysis[$data['asset_class']] = $this->language->get('entry_lower');
      } else if( $portfolio_balances[$data['asset_class']]['portfolio_percentage'] > $upperbound ){
        $risk_profile_analysis[$data['asset_class']] = $this->language->get('entry_higher');;
      } else {
        $risk_profile_analysis[$data['asset_class']] = $this->language->get('entry_undefined');;
      }

      $risk_profile_ideal[$data['asset_class']] = (int)$data['allocation_pct'];
    }

    $results = array(
      'risk_profile'=>$risk_profile['riskprofiledesc'],
      'risk_profile_last_update'=> dateISO2Display($risk_profile['risk_profile_last_update'], 'j-M-y'),
      'analysis'=> $risk_profile_analysis,
      'data'=>array(
        array(
          'value'=>$risk_profile_ideal['EQ'],
          'label'=>$this->language->get('entry_equity'),
          'color'=>'#5B90BF',
          'highlight'=>'#6fa8da',
        ),
        array(
          'value'=>$risk_profile_ideal['FI'],
          'label'=>$this->language->get('entry_fix_income'),
          'color'=>'#5bbfa2',
          'highlight'=>'#6ad9b8',
        ),
        array(
          'value'=>$risk_profile_ideal['MM'],
          'label'=>$this->language->get('entry_money_market'),
          'color'=>'#8fbf5b',
          'highlight'=>'#a3db68',
        )
      )
    );

    return $results;
  }

  private function getPortfolioBalance(){
    $this->loadModel('account/customer');
    $this->loadModel('catalog/product');

    $portfolio_balance = $this->model_account_customer->getPortfolioBalance();

    $portfolio_balance_summary = array();
    $portfolio_balance_chart = array();

    $default_from = date("Y-m-d",strtotime(date("Y-m-d")." -90 day"));
    $default_to = date("Y-m-d");

    foreach ($portfolio_balance as $key => $data) {
      $data['product_name'] = ucwords(strtolower($data['product_name']));
      $data['initial_subscription'] = dateISO2Display($data['initial_subscription'], 'j-M-y');
      $data['tmp_percentage'] = (int)$data['portfolio_percentage'];
      $data['portfolio_percentage'] = number_format($data['portfolio_percentage'],2,".",",").'%';
      $data['trans_link'] = $this->html->getSecureURL('account/transactions/history','&date_start='.$default_from.'&date_end='.$default_to.'&product_type='.$data['product_code']);
      $data['unit_holding'] = number_format($data['unit_holding'],4,".",",");
      $data['navpu'] = number_format($data['navpu'],2,".",",");

      $gain_loss = number_format(abs($data['gain_loss']),2,".",",");
      $data['gain_loss'] = $data['gain_loss'] > 0 ? '<span class="cl-green">'.$gain_loss.'</span>' : '<span class="cl-red">('.$gain_loss.')</span>' ;
      
      $data['tmp_market_valuation'] = $data['market_valuation'];
      $data['market_valuation'] = number_format($data['market_valuation'],2,".",",");

      $data['link'] = $this->html->getSecureURL('checkout/cart','&product_id='.$data['product_id']); 
      $data['is_featured'] = $this->model_catalog_product->isFeaturedProduct($data['product_id']);

      switch ($data['fund_type']) {
        case 'EQ':
          $data['row_class'] = 'cl-equity';
          break;

        case 'FI':
          $data['row_class'] = 'cl-fincome';
          break;

        case 'MM':
          $data['row_class'] = 'cl-mmarket';
          break;
        
        default:
          $data['row_class'] = 'cl-equity';
          break;
      }
      
      $portfolio_balance_summary[$data['product_code']] = $data;      
    }

    foreach ($portfolio_balance_summary as $key => $data) {
      $market_valuation[$data['fund_type']] += $data['tmp_market_valuation'];

      switch ($data['fund_type']) {
        case 'EQ':
          $portfolio_balance_charts[$data['fund_type']]['color'] = '#5B90BF';
          $portfolio_balance_charts[$data['fund_type']]['highlight'] = '#6fa8da';
          $portfolio_balance_charts[$data['fund_type']]['label'] = $data['category'].': '. number_format($market_valuation[$data['fund_type']],2,".",",");
          break;

        case 'FI':
          $portfolio_balance_charts[$data['fund_type']]['color'] = '#5bbfa2';
          $portfolio_balance_charts[$data['fund_type']]['highlight'] = '#6ad9b8';
          $portfolio_balance_charts[$data['fund_type']]['label'] = $data['category'].': '. number_format($market_valuation[$data['fund_type']],2,".",",");
          break;

        case 'MM':
          $portfolio_balance_charts[$data['fund_type']]['color'] = '#8fbf5b';
          $portfolio_balance_charts[$data['fund_type']]['highlight'] = '#a3db68';
          $portfolio_balance_charts[$data['fund_type']]['label'] = $data['category'].': '. number_format($market_valuation[$data['fund_type']],2,".",",");
          break;
        
        default:
          $portfolio_balance_charts[$data['fund_type']]['color'] = '#5B90BF';
          $portfolio_balance_charts[$data['fund_type']]['highlight'] = '#6fa8da';
          $portfolio_balance_charts[$data['fund_type']]['label'] = $data['category'].': '. number_format($market_valuation[$data['fund_type']],2,".",",");
          break;
      }

      $portfolio_balance_charts[$data['fund_type']]['value'] += (int)$data['tmp_percentage']; 
    }

    foreach ($portfolio_balance_charts as $key => $data) {
      $portfolio_balance_chart[] = $data;
    }

    $results = array(
      'portfolio_balance_summary' => $portfolio_balance_summary,
      'portfolio_balance_chart' => $portfolio_balance_chart
    );

    return $results;

  }
}
