<?php 
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' )) {
	header ( 'Location: static_pages/' );
}
class ControllerResponsesAccountHistory extends AController {
	public $data = array();
	
	/**
	 * Main controller function to show order history
	 */
	public function main() {
		
	    //init controller data
	    $this->extensions->hk_InitData($this,__FUNCTION__);
	    $this->loadLanguage('account/account');

		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->html->getSecureURL('account/history');

			$this->redirect($this->html->getSecureURL('account/login'));
		}

		$template = new ATemplate();
		$request = $this->request->post;
				
		$this->loadModel('account/order');
		$order_total = $this->model_account_order->getTotalOrders();
		
		if (isset($request['page'])) {
			$page = $request['page'];
		} else {
			$page = 1;
		}

		if (isset($request['limit'])) {
			$limit = (int)$request['limit'];
			$limit = $limit>50 ? 50 : $limit;
		} else {
			$limit = $this->config->get('config_catalog_limit');
		}
		
		$limit = 2;
		$orders = array();
		
		$results = $this->model_account_order->getOrders(($page - 1) * $limit, $limit);

		$i = ($page * $limit)+1-$limit;
		foreach ($results as $result) {
			$product_total = $this->model_account_order->getTotalOrderProductsByOrderId($result['order_id']);
			$button = HtmlElementFactory::create( array (  'type' => 'button',
												   'name' => 'button_edit',
												   'text'=> $this->language->get('button_view'),
												   'style' => 'button',
	                                               'attr'  => ' onclick = "viewOrder('.$result['order_id'].');" ' ));
			$orders[] = array(
						'order_id'   => $result['order_id'],
						'name'       => $result['firstname'] . ' ' . $result['lastname'],
						'status'     => $result['status'],
						'date_added' => dateISO2Display($result['date_added'], $this->language->get('date_format_short')),
						'products'   => $product_total,
						'total'      => $this->currency->format($result['total'], $result['currency'], $result['value']),
						'href'       => $this->html->getSecureURL('account/invoice', '&order_id=' . $result['order_id']),
						'button'     => $button->getHtml(),
						'no'=>$i,
						'detail_link' => $this->html->getSecureURL('account/history/detail','&order_id='.$result['order_id'])
			);

			$i++;
		}

		$template->data['orders'] =$orders;

		$template->data['pagination_bootstrap'] = HtmlElementFactory::create( array (
									'type' => 'Pagination',
									'name' => 'pagination',
									'text'=> $this->language->get('text_pagination'),
									'text_limit' => $this->language->get('text_per_page'),
									'total'	=> $order_total,
									'page'	=> $page,
									'limit'	=> $limit,
									'url' => 'limit=' . $limit . '&page={page}',
									'style' => 'pagination'));

		$this->data['html'] = $template->fetch('responses/account/order_history.tpl');
		$this->data['pagination'] = $template->fetch('responses/account/pagination.tpl');

	    //init controller data
	    $this->extensions->hk_UpdateData($this,__FUNCTION__);

	    $this->load->library('json');
		$this->response->setOutput(AJson::encode($this->data));
	}

	public function detail(){
		$this->extensions->hk_InitData($this,__FUNCTION__);
		$this->loadLanguage('account/account');

		$this->document->setTitle( $this->language->get('heading_title_order') );

		$order_id = $this->request->get['order_id'];

		$this->loadModel('account/order');
		$result = $this->model_account_order->getOrder($order_id);
		$result['date_added'] = dateISO2Display($result['date_added'], $this->language->get('date_format_short')).' '.dateISO2Display($result['date_added'], $this->language->get('time_format'));
		$result['status'] = $this->model_account_order->getOrderStatus($order_id);
		// $result['total'] = $this->currency->format($result['total'], $result['currency'], $result['value']);

		$total_subscription = 0;
		$total_fee_amount = 0;

		$products = $this->model_account_order->getOrderProducts($order_id);
		$resource = new AResource('image');

		foreach ($products as $key => $product) {
			$total_subscription += $product['quantity'];
			$total_fee_amount += $product['fee_amount'];

			$thumbnail = $resource->getMainThumb('products',
			                                     $product['product_id'],
			                                     (int)$this->config->get('config_image_product_width'),
			                                     (int)$this->config->get('config_image_product_height'),true);

			$products[$key]['quantity'] = $this->currency->format($product['quantity'], $result['currency'], $result['value']);
			$products[$key]['fee_amount'] = $this->currency->format($product['fee_amount'], $result['currency'], $result['value']);
			$products[$key]['total'] = $this->currency->format($product['fee_amount']+$product['quantity']);//$product['total'], $result['currency'], $result['value']
			$products[$key]['thumb'] = $thumbnail;
		}

		$result['total_subscription'] = $this->currency->format($total_subscription, $result['currency'], $result['value']);;
		$result['total_fee_amount'] = $this->currency->format($total_fee_amount, $result['currency'], $result['value']);;
		$result['total'] = $this->currency->format($product['fee_amount']+$product['quantity']);

		$this->view->setTemplate('pages/account/order_history_detail.tpl');
		$this->view->assign('order',$result);
		$this->view->assign('products',$products);
		$this->view->assign('back_url',$this->html->getSecureURL('account/history'));
		$this->view->assign('export_url',$this->html->getSecureURL('account/history/exportDetail','&order_id='.$this->request->get['order_id']));
		$this->processTemplate();
		//init controller data
    	$this->extensions->hk_UpdateData($this,__FUNCTION__);
	}

	public function exportDetail(){
		$this->extensions->hk_InitData($this,__FUNCTION__);
		$this->loadLanguage('account/account');

		$this->document->setTitle( $this->language->get('heading_title_order') );

		$order_id = $this->request->get['order_id'];

		$this->loadModel('account/order');
		$result = $this->model_account_order->getOrder($order_id);
		$result['date_added'] = dateISO2Display($result['date_added'], $this->language->get('date_format_short')).' '.dateISO2Display($result['date_added'], $this->language->get('time_format'));
		$result['status'] = $this->model_account_order->getOrderStatus($order_id);
		// $result['total'] = $this->currency->format($result['total'], $result['currency'], $result['value']);

		$total_subscription = 0;
		$total_fee_amount = 0;

		$products = $this->model_account_order->getOrderProducts($order_id);
		$resource = new AResource('image');

		foreach ($products as $key => $product) {
			$total_subscription += $product['quantity'];
			$total_fee_amount += $product['fee_amount'];

			$thumbnail = $resource->getMainThumb('products',
			                                     $product['product_id'],
			                                     (int)$this->config->get('config_image_product_width'),
			                                     (int)$this->config->get('config_image_product_height'),true);

			$products[$key]['quantity'] = $this->currency->format($product['quantity'], $result['currency'], $result['value']);
			$products[$key]['fee_amount'] = $this->currency->format($product['fee_amount'], $result['currency'], $result['value']);
			$products[$key]['total'] = $this->currency->format($product['fee_amount']+$product['quantity']);//$product['total'], $result['currency'], $result['value']
			$products[$key]['thumb'] = $thumbnail;
		}

		$result['total_subscription'] = $this->currency->format($total_subscription, $result['currency'], $result['value']);;
		$result['total_fee_amount'] = $this->currency->format($total_fee_amount, $result['currency'], $result['value']);;
		$result['total'] = $this->currency->format($product['fee_amount']+$product['quantity']);

		$template = new ATemplate();
		$template->data['order'] = $result;
		$template->data['products'] = $products;
		$template->data['date_print'] = $this->request->get['date_print'] ? $this->request->get['date_print'] : date("d-M-y H:i:s");

		//** Multi Language
		$template->data['heading_title_order'] = $this->language->get('heading_title_order');
		$template->data['text_status'] = $this->language->get('text_status');
		$template->data['text_email'] = $this->language->get('text_email');
		$template->data['text_date_added_only'] = $this->language->get('text_date_added_only');
		$template->data['text_telephone'] = $this->language->get('text_telephone');
		$template->data['text_payment'] = $this->language->get('text_payment');
		$template->data['text_order_comment'] = $this->language->get('text_order_comment');

		$template->data['text_image'] = $this->language->get('text_image');
		$template->data['text_name'] = $this->language->get('text_name');
		$template->data['text_subscription'] = $this->language->get('text_subscription');
		$template->data['text_fee_ammount'] = $this->language->get('text_fee_ammount');
		$template->data['text_total_only'] = $this->language->get('text_total_only');

		$template->data['text_total_subs'] = $this->language->get('text_total_subs');
		$template->data['text_total_fee'] = $this->language->get('text_total_fee');

		$template->data['text_reserved'] = $this->language->get('text_reserved');
		$template->data['text_print_date'] = $this->language->get('text_print_date');

		$html = $template->fetch('pages/account/order_history_detail_export.tpl');

		// For testing
		// echo $html;
		// return;

		require_once(DIR_ROOT . '/dompdf/dompdf_config.inc.php');
		$dompdf = new DOMPDF();
		$dompdf->set_base_path(DIR_ROOT.'/');
		$dompdf->load_html($html);
		$dompdf->set_paper('a4', 'landscape');
		$dompdf->render();
		$pdf = $dompdf->output();

        header('Content-Disposition: attachment; filename="transaction_history_'.date("YmdHis").'.pdf"');
        header('Content-Type: application/pdf'); 
        header('Content-Length: ' . strlen($pdf));
        header('Connection: close');
        echo $pdf;

		//$dompdf->stream("transaction_history_".date("YmdHis").".pdf");
		//init controller data
    	$this->extensions->hk_UpdateData($this,__FUNCTION__);
	}

}
