<?php 
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' )) {
	header ( 'Location: static_pages/' );
}
class ControllerResponsesAccountTransactions extends AController {
	public $data = array();
	
	/**
	 * Main Controller function to show transaction hitory. 
	 * Note: Regular orders are considered in the transactions. 
	 */
	public function main() { 
	}

	/**
	 * Main Controller function to show transaction hitory. 
	 * Note: Regular orders are considered in the transactions. 
	 */
	public function history() {

        //init controller data
        $this->extensions->hk_InitData($this,__FUNCTION__);
        $this->loadLanguage('account/account');
    	if (!$this->customer->isLogged()) {
      		$this->session->data['redirect'] = $this->html->getSecureURL('account/transactions');
	  		$this->redirect($this->html->getSecureURL('account/login'));
    	}

    	$template = new ATemplate();

    	$request = $this->request->post;
		$this->loadModel('account/customer');

		$cond = array();	
  		if(!empty($request['product_type'])){
  			$cond['product_type'] = $request['product_type'];
  		}
  		if(!empty($request['date_start'])){
  			$cond['date_start'] = date("Y-m-d",strtotime($request['date_start']));
  		}
  		if(!empty($request['date_end'])){
  			$cond['date_end'] = date("Y-m-d",strtotime($request['date_end']));
  		}

		$trans_total = $this->model_account_customer->getTotalTransactionHistory($cond);
		
		$balance = $this->customer->getBalance();
		$template->data['balance_amount'] = $this->currency->format($balance);
			
		if (isset($request['page'])) {
			$page = $request['page'];
		} else {
			$page = 1;
		}

		if (isset($request['limit'])) {
			$limit = (int)$request['limit'];
			$limit = $limit>50 ? 50 : $limit;
		} else {
			$limit = $this->config->get('config_catalog_limit');
		}
			
 		$trans = array();

 		if($trans_total){
 			$results = $this->model_account_customer->getTransactionHistory(($page - 1) * $limit, $limit, $cond);
		
			foreach ($results as $result) {
	    		$trans[] = array(
	    			'frontend_trx_id' => $result['frontend_trx_id'],
					'transaction_date'   => dateISO2Display($result['trx_date'], 'j-M-y'),
					'product_code'       => $result['product_code'],
					'product_name'		=> ucwords(strtolower($result['name'])),
					'transaction_type'     => $result['trxtype_desc'],
					'nav_date'      => dateISO2Display($result['nav_date'], 'j-M-y'),
					'nav_value'      => number_format($result['nav_value'],2,".",","),
					'transaction_unit' => number_format($result['units'],4,".",","),
					'transaction_amount' => number_format($result['amount'],2,".",","),
					'transaction_fee'=> number_format($result['fee_amount'],2,".",","),
					'total_amount'=> number_format( ($result['amount'] + $result['fee_amount']) ,2,".",",") ,
					'saldo'=> number_format( $result['average_cost'] ,4,".",",")  
	    		);
	  		}	
 		}
		

  		$template->data['transactions'] = $trans;

		$template->data['pagination_bootstrap'] = HtmlElementFactory::create( array (
									'type' => 'Pagination',
									'name' => 'pagination',
									'text'=> $this->language->get('text_pagination'),
									'text_limit' => $this->language->get('text_per_page'),
									'total'	=> $trans_total,
									'page'	=> $page,
									'limit'	=> $limit,
									'url' => 'limit=' . $limit . '&page={page}'.'&date_start='.$request['date_start'].'&date_end='.$request['date_end'].'&product_type='.$request['product_type'],
									'style' => 'pagination'
									));
		$this->data['export_url'] = $this->html->getSecureURL('account/transactions/exportHistory','&date_start='.$request['date_start'].'&date_end='.$request['date_end'].'&product_type='.$request['product_type']);

		$this->data['html'] = $template->fetch('responses/account/transaction_history.tpl');
		$this->data['pagination'] = $template->fetch('responses/account/pagination.tpl');

        //init controller data
        $this->extensions->hk_UpdateData($this,__FUNCTION__);

        $this->load->library('json');
		$this->response->setOutput(AJson::encode($this->data));
	}


}

