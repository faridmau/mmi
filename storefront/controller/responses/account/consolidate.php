<?php 
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' )) {
  header ( 'Location: static_pages/' );
}
class ControllerResponsesAccountConsolidate extends AController {
  private $error = array();
  public $data;
    public function main() {

        //init controller data
        $this->extensions->hk_InitData($this,__FUNCTION__);

        if (!$this->customer->isLogged()) {
          $this->redirect($this->html->getSecureURL('account/login'));
        }

        $template = new ATemplate();
        $request = $this->request->post;

        $this->loadLanguage('account/consolidate');
        $this->loadLanguage('account/account');

        if (isset($request['page'])) {
          $page = $request['page'];
        } else {
          $page = 1;
        }

        if (isset($request['limit'])) {
          $limit = (int)$request['limit'];
          $limit = $limit>50 ? 50 : $limit;
        } else {
          $limit = $this->config->get('config_catalog_limit');
        }

        $date_start = date("Y-m-d",strtotime($request['date_start']));
        $date_end = date("Y-m-d",strtotime($request['date_end']));

        $this->loadModel('account/customer');
        $investment_summary = $this->model_account_customer->getInvestmentSummary($date_start, $date_end);
        
        $total_ending_balance = 0.00;
        $total_realized_gl = 0.00;
        $total_unrealized_gl = 0.00;

        foreach ($investment_summary as $key => $data) {
          $total_ending_balance += $data['ending_balance_amount'];
          $total_realized_gl += $data['total_realized_gl'];
          $total_unrealized_gl += $data['unrealized_gl'];

          $investment_summary[$key]['product_name'] = ucwords(strtolower($data['product_name']));
          $investment_summary[$key]['ending_balance_amount'] = number_format($data['ending_balance_amount'],2,".",",");
          $investment_summary[$key]['nav_value'] = number_format($data['nav_value'],2,".",",");
          $investment_summary[$key]['ending_balance'] = number_format($data['ending_balance'],4,".",",");
          $investment_summary[$key]['beginning_balance'] = number_format($data['beginning_balance'],4,".",",");
        }

        $template->data['total_ending_balance'] = number_format($total_ending_balance,2,".",",");
        $template->data['total_realized_gl'] = $total_realized_gl;
        $template->data['total_unrealized_gl'] = $total_unrealized_gl;

        $template->data['investment_summary'] = $investment_summary;

        $transaction_detail = $this->model_account_customer->getTransactionDetails(($page - 1) * $limit, $limit, $date_start, $date_end );
        
        foreach ($transaction_detail as $key => $data) {
          $transaction_detail[$key]['product_name'] = ucwords(strtolower($data['product_name']));
          $transaction_detail[$key]['trx_date'] = dateISO2Display($data['trx_date'], 'j-M-y');
          $transaction_detail[$key]['realized_gl'] = $data['realized_gl'] ? $data['realized_gl'] : '0.00';
          $transaction_detail[$key]['fee_amount'] = number_format($data['fee_amount'],2,".",",");
          $transaction_detail[$key]['nav_per_unit'] = number_format($data['nav_per_unit'],2,".",",");
          $transaction_detail[$key]['average_cost'] = number_format($data['average_cost'],2,".",",");
          $transaction_detail[$key]['group'] = str_replace(' ', '-', strtolower($data['product_name']).'-'.$data['inv_account_no']);
          
          $transaction_detail_balance = $this->getTransactionBalance($investment_summary, $transaction_detail[$key]['product_name'], $data['inv_account_no']);

          $transaction_detail[$key]['group_balance'] = $transaction_detail_balance;
          $transaction_detail[$key]['group_begining_balance'] = $transaction_detail_balance['begining_balance'];
          $transaction_detail[$key]['group_ending_balance'] = $transaction_detail_balance['ending_balance']; 
          $transaction_detail[$key]['group_realized_gl'] = $transaction_detail_balance['realized_gl'] ; 
        }
        $template->data['transaction_detail'] = $transaction_detail;

        $trans_total = $this->model_account_customer->getTransactionDetailTotal($date_start, $date_end);
        $template->data['is_last_page'] = $page >= ceil($trans_total / $limit) ? true : false;

        $template->data['pagination_bootstrap'] = HtmlElementFactory::create( array (
                    'type' => 'Pagination',
                    'name' => 'pagination',
                    'text'=> $this->language->get('text_pagination'),
                    'text_limit' => $this->language->get('text_per_page'),
                    'total' => $trans_total,
                    'page'  => $page,
                    'limit' => $limit,
                    'url' => $this->html->getURL('limit=' . $limit . '&page={page}&date_start='.$request['date_start'].'&date_end='.$request['date_end']),
                    'style' => 'pagination'));

        $this->data['export_url'] = $this->html->getSecureURL('account/consolidate/export','&date_start='.$request['date_start'].'&date_end='.$request['date_end']);
    
        $this->data['html_transaction'] = $template->fetch('responses/account/consolidate.tpl');
        $this->data['html_summary'] = $template->fetch('responses/account/consolidate_summary.tpl');
        $this->data['pagination'] = $template->fetch('responses/account/pagination.tpl');

        //init controller data
        $this->extensions->hk_UpdateData($this,__FUNCTION__);

        $this->load->library('json');
        $this->response->setOutput(AJson::encode($this->data));
    }

    private function getTransactionBalance($investment_summary, $product_name, $inv_account_no){
      $beginning_balance = 'undefined';
      $ending_balance = 'undefined';
      $realized_gl = 0;

      foreach ($investment_summary as $key => $data) {
        if($data['product_name'] == $product_name && $data['inv_account_no'] == $inv_account_no){
          $beginning_balance = $data['beginning_balance'];
          $ending_balance = $data['ending_balance'];
          $realized_gl = $data['total_realized_gl'];
        }
      }

      $result = array(
        'begining_balance'=>$beginning_balance,
        'ending_balance'=>$ending_balance,
        'realized_gl'=>$realized_gl
      );

      return $result;
    }
}