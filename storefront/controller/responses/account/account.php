<?php
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright 2011 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if (!defined('DIR_CORE')) {
	header('Location: static_pages/');
}
/** @noinspection PhpUndefinedClassInspection */
class ControllerResponsesAccountAccount extends AController {

	public $data = array();

	public function main() {}

	public function addReminder(){

		// var_dump($this->request->post);exit();
		$request = $this->request->post;

		$this->extensions->hk_InitData($this, __FUNCTION__);
		$this->loadModel('account/reminder');
		$this->loadLanguage('account/account');
		
		$request['customer_id'] = $this->customer->getId();
		$request['reminder_date'] = date("Y-m-d", strtotime($request['reminder_date']));
		
		if ($request['reminder_id']=='') {
			$this->model_account_reminder->addReminder($request);
			$message = $this->language->get('reminder_saved');
		}else{
			$this->model_account_reminder->editReminder($request);
			$message = $this->language->get('reminder_edited');
		}
		
		//init controller data
		$this->extensions->hk_UpdateData($this, __FUNCTION__);
		
		$this->load->library('json');
		$this->response->setOutput(AJson::encode($message));

	}
	public function deleteReminder(){
		
		// var_dump($this->request->post);exit();
		$id = $this->request->get['id'];

		$this->extensions->hk_InitData($this, __FUNCTION__);
		$this->loadModel('account/reminder');
		$this->loadLanguage('account/account');
		
		if ($id!='') {
			$this->model_account_reminder->deleteReminder($id);
			$message = $this->language->get('reminder_deleted');
		}

		//init controller data
		$this->extensions->hk_UpdateData($this, __FUNCTION__);
		
		$this->load->library('json');
		$this->response->setOutput(AJson::encode($message));

	}
	public function getDetailReminder(){
		
		$this->extensions->hk_InitData($this, __FUNCTION__);
		$this->loadModel('account/reminder');
		$this->loadLanguage('account/account');
		
		$reminder_id = $this->request->post['reminder_id'];

		$reminder = $this->model_account_reminder->getDetailReminder($reminder_id);
		$reminder['reminder_date'] = date("d-M-y", strtotime($reminder['reminder_date']));
		//init controller data
		$this->extensions->hk_UpdateData($this, __FUNCTION__);
		
		$this->load->library('json');
		$this->response->setOutput(AJson::encode($reminder));

	}
	public function getReminder(){
		//init controller data
		
		$this->extensions->hk_InitData($this, __FUNCTION__);
		$this->loadModel('account/reminder');
		$this->loadLanguage('account/account');
		$page = $this->request->post['page'];

		  if($page==1){
		   $start = 0;  
		  }else{
		  	$start = ($page-1)*10;
		  }

		$reminders = $this->model_account_reminder->getReminders($this->customer->getId(),$start);
		$totalreminders = $this->model_account_reminder->totalReminders($this->customer->getId());
		$delete_url = $this->html->getSecureURL('account/reminder');
		
		$text_detail_reminder = 'a a a';
		$dateNow = strtotime(date('Y-m-d'));
		foreach ($reminders as $key => $reminder) {
			//$html .= "<a data-toggle='modal' href='#addReminder' onClick=detailReminder('".$reminders[$key]['reminder_id']."');viewModalReminder('".$reminders[$key]['descriptions']."','".date("d-M-Y", strtotime($reminders[$key]['reminder_date']))."','".$reminders[$key]['reminder_id']."','');showDel();urlDelReminder('".$delete_url."&delete_id=".$reminders[$key]['reminder_id']."');>";
			$html .= "<a data-toggle='modal' href='#' onClick=detailReminder('".$reminders[$key]['reminder_id']."');editTitle();showDel('".$reminders[$key]['reminder_id']."');>";
			
			if (strtotime($reminders[$key]['reminder_date'])<$dateNow) {
				$html .= '<p class="timestamp" ><span class="cl-red">'.date("d-M-y", strtotime($reminders[$key]['reminder_date'])).'</span></p>';
			}else{
				$html .= '<p class="timestamp" >'.date("d-M-y", strtotime($reminders[$key]['reminder_date'])).'</p>';
			}
			
			$html .= '<p class="remind-item" >'.$reminders[$key]['descriptions'].' </p>';
			$html .= '</a>';
		}
		if ($page == 0) $page = 1;					//if no page var is given, default to 1.
		$prev = $page - 1;							//previous page is page - 1
		$next = $page + 1;							//next page is page + 1
		$prev_='';
		$lastpage = ceil($totalreminders/10);	
		$next_='';

		if ($page > 1) 
			$prev_.= '<a  class="prev" p="'.$prev.'" onClick=remPaging("'.$prev.'");>&#8592;</a>';
		else{
			 //$prev_.= '<a class="prev" p="'.$prev.'" onClick=remPaging("'.$prev.'");>&#8592;</a>';	
			}

		if ($page < $lastpage) 
			$next_.= '<a class="next" p="'.$next.'" onClick=remPaging("'.$next.'");>&#8594;</a>';
		else{
			//$next_.= '<a class="next" p="'.$next.'" onClick=remPaging("'.$next.'");>&#8594;</a>';
			}
				
		$html .= '<div class="clearfix reminder-page">';
		$retVal = ($reminders) ? $page : '0' ;
		$html .= '<p class="pull-left">'.$this->language->get('entry_page').' '.$retVal.$this->language->get('entry_of').$lastpage.'</p>';
		$html .= '<p class="pull-right" id="arrows">'.$prev_.$next_.'</p></div>';
			
		$rows = $this->model_account_reminder->totalReminders($this->customer->getId());
		

		//init controller data
		$this->extensions->hk_UpdateData($this, __FUNCTION__);
		
		$this->load->library('json');
		$this->response->setOutput(AJson::encode($html));
	}
	public function emailCheck(){
		$this->extensions->hk_InitData($this, __FUNCTION__);
		$this->loadModel('account/customer_profile');
		$this->loadLanguage('account/edit');
		
		$email = $this->request->post['email'];
		// var_dump($email);exit();
		$result = $this->model_account_customer_profile->emailCheck($email);
		
		//init controller data
		$this->extensions->hk_UpdateData($this, __FUNCTION__);
		
		$this->load->library('json');
		$this->response->setOutput(AJson::encode($result));

	}
}