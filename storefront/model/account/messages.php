<?php
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' )) {
	header ( 'Location: static_pages/' );
}
class ModelAccountMessages extends Model {
	public function addMessageApproval($data) {
		$sql = "INSERT INTO " . $this->db->table("messages") . "
			  SET	title = 'Approval',
					message = 'Approval user : ".$this->db->escape($data['firstname'])." ".$this->db->escape($data['lastname'])."',
					status = 'A',
					create_date = NOW()
					";
		$this->db->query($sql);
		
	}
		
}
