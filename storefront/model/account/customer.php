<?php
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>
  
 UPGRADE NOTE: 
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.  
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' )) {
	header ( 'Location: static_pages/' );
}
/** @noinspection PhpUndefinedClassInspection */
/**
 * Class ModelAccountCustomer
 * @property ModelCatalogContent $model_catalog_content
 */
class ModelAccountCustomer extends Model {
	/**
	 * @param array $data
	 * @return int
	 */
	public function addCustomer($data) {
		$key_sql = '';
		if ( $this->dcrypt->active ) {
			$data = $this->dcrypt->encrypt_data($data, 'customers');
			$key_sql = ", key_id = '" . (int)$data['key_id'] . "'";
		}
		if(!(int)$data['customer_group_id']){
			$data['customer_group_id'] = (int)$this->config->get('config_customer_group_id');
		}
		if(!isset($data['status'])){
			if($this->config->get('config_customer_email_activation')){ // if need to activate via email  - disable status
				$data['status'] = 0;
			}else{
				$data['status'] = 1;
			}
		}
		if(isset($data['approved'])){
			$data['approved'] = (int)$data['approved'];
		}else{
			if(!$this->config->get('config_customer_approval')){
				$data['approved'] = 1;
			}
		}
		// delete subscription accounts for given email
		$subscriber = $this->db->query("SELECT customer_id
										FROM " . $this->db->table("customers") . "
										WHERE LOWER(`email`) = LOWER('" . $this->db->escape($data['email']) . "')
											AND customer_group_id IN (SELECT customer_group_id
																		  FROM ".$this->db->table('customer_groups')."
																		  WHERE `name` = 'Newsletter Subscribers')"
    );
		foreach($subscriber->rows as $row){
			$this->db->query("DELETE FROM " . $this->db->table("customers") . " WHERE customer_id = '" . (int)$row['customer_id'] . "'");
			$this->db->query("DELETE FROM " . $this->db->table("addresses") . " WHERE customer_id = '" . (int)$row['customer_id'] . "'");
		}

    $params['store_id'] = "'" . (int)$this->config->get('config_store_id') . "'";
    $params['loginname'] = "'".$this->db->escape($data['loginname'])."'";
    $params['firstname'] = "'".$this->db->escape($data['firstname'])."'";
    $params['lastname'] = "'".$this->db->escape($data['lastname'])."'";
    $params['email'] = "'".$this->db->escape($data['email'])."'";
    $params['telephone'] = "'".$this->db->escape($data['telephone'])."'";
    $params['fax'] = "'".$this->db->escape($data['fax'])."'";
    $params['password'] = "'".$this->db->escape(AEncryption::getHash($data['password']))."'";
    $params['newsletter'] = (int)$data['newsletter'];
    $params['customer_group_id'] = "'".(int)$data['customer_group_id']."'";
    $params['approved'] = "'".(int)$data['approved']."'";
    $params['status'] = "'".(int)$data['status']."'".$key_sql."";
    $params['id_number'] ="'".$this->db->escape($data['id_number'])."'";
    $params['id_type'] = "'".$this->db->escape($data['id_type']) ."'";
    $params['cif'] = "'".$this->db->escape($data['cif']) . "'";
    // $params['holder_id'] = "'".$this->db->escape($data['cif']) . "'";
    $params['bank_account_name'] = "'" . $this->db->escape($data['bank_account_name'])."'";
    $params['bank_name'] = "'".$this->db->escape($data['bank_name'])."'";
    $params['bank_account_no'] = "'".$this->db->escape($data['bank_account_no'])."'";
    $params['ip'] = "'".$this->db->escape($data['ip'])."'";
    $params['date_added'] = "NOW()";
    
     // user_login | IP | Login date | Logout date | Activity | Data Before | Data After
    	
    $audit_log=array(
      "IP" => $this->request->server['REMOTE_ADDR'],
      "user_login" => $this->session->data['user_id'],
      "Activity" => "Create Account",
      "login" => $this->session->data['user_last_login'],
      "logout" => $this->session->data['logout_time']
    );
    // $this->db->insert_with_trail($this->db->table("customers"),$params,$audit_log);
    $sql = "INSERT INTO ".$this->db->table('customers')." SET ";
    foreach($params as $key => $val){
    	$sql .= $key." = ".$val.", ";
    }
    $sql = substr($sql,0,-2);

		$this->db->query($sql);
		$customer_id = $this->db->getLastId();
		
		$key_sql = '';
		if ( $this->dcrypt->active ) {
			$data = $this->dcrypt->encrypt_data($data, 'addresses');
			$key_sql = ", key_id = '" . (int)$data['key_id'] . "'";
		}
      	$this->db->query("INSERT INTO " . $this->db->table("addresses") . " 
				  SET customer_id = '" . (int)$customer_id . "', 
				  		firstname = '" . $this->db->escape($data['firstname']) . "', 
				  		lastname = '" . $this->db->escape($data['lastname']) . "', 
				  		company = '" . $this->db->escape($data['company']) . "', 
				  		address_1 = '" . $this->db->escape($data['address_1']) . "', 
				  		address_2 = '" . $this->db->escape($data['address_2']) . "', 
				  		city = '" . $this->db->escape($data['city']) . "', 
				  		postcode = '" . $this->db->escape($data['postcode']) . "', 
				  		country_id = '" . (int)$data['country_id'] . "'" . 
				  		$key_sql . ",
				  		zone_id = '" . (int)$data['zone_id'] . "'");
		
		$address_id = $this->db->getLastId();
    $this->db->query("UPDATE " . $this->db->table("customers") . " SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");
		return $customer_id;
	}

	/**
	 * @param array $data
	 */
	public function editCustomer($data) {
		$key_sql = '';
		if ( $this->dcrypt->active ) {
			$data = $this->dcrypt->encrypt_data($data, 'customers');
			$key_sql = ", key_id = '" . (int)$data['key_id'] . "'";
		}

    	//update login only if needed
    	$loginname = '';
    	if ( !empty($data['loginname'] ) ) {
    		$loginname = " loginname = '" . $this->db->escape($data['loginname'])  . "', ";
    	}

		$this->db->query("UPDATE " . $this->db->table("customers") . " 
						  SET 	firstname = '" . $this->db->escape($data['firstname']) . "', 
						  		lastname = '" . $this->db->escape($data['lastname']) . "', " . $loginname . "
						  		email = '" . $this->db->escape($data['email']) . "', 
						  		telephone = '" . $this->db->escape($data['telephone']) . "', 
						  		fax = '" . $this->db->escape($data['fax']) . "'"
						  		. $key_sql .
						  		" WHERE customer_id = '" . (int)$this->customer->getId() . "'");
	}

	public function getWaitingCustomers(){
		$sql = "select customer_id, email from ".$this->db->table("customers")." where approved = 4";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	/**
	 * @param string $loginname
	 * @param string $password
	 */
	public function editPassword($loginname, $password) {
		$password = AEncryption::getHash($password);
      	$this->db->query("UPDATE " . $this->db->table("customers") . " SET password = '" . $this->db->escape($password) . "' WHERE loginname = '" . $this->db->escape($loginname) . "'");
	}

	/**
	 * @param int $newsletter
	 * @param int $customer_id - optional parameter for unsubscribe page!
	 */
	public function editNewsletter($newsletter,$customer_id=0) {
		$customer_id = (int)$customer_id ? (int)$customer_id : (int)$this->customer->getId();
		$this->db->query("UPDATE " . $this->db->table("customers") . " SET newsletter = '" . (int)$newsletter . "' WHERE customer_id = '" . $customer_id . "'");
	}

	/**
	 * @param $customer_id
	 * @param $status
	 * @return bool
	 */
	public function editStatus($customer_id, $status) {
		$customer_id = (int)$customer_id;
		$status = (int)$status;
		if(!$customer_id){ return false; }
		$this->db->query( "UPDATE " . $this->db->table("customers") . "
						   SET status = '" . (int)$status . "'
						   WHERE customer_id = '" . $customer_id . "'" );
		return true;
	}

	/**
	 * @param int $customer_id
	 * @return array
	 */
	public function getCustomer($customer_id, $table = "customers") {
		$sql = "SELECT * FROM " . $this->db->table("customers") . " WHERE customer_id = '" . (int)$customer_id . "'";
		$query = $this->db->query($sql);
		$result_row = $this->dcrypt->decrypt_data($query->row, 'customers');
		return $result_row;
	}

	/**
	 * @param string $email
	 * @param bool $no_subscribers - sign that needed list without subscribers
	 * @return int
	 */
	public function getTotalCustomersByEmail($email, $no_subscribers=true) {
		$sql = "SELECT COUNT(*) AS total
				FROM " . $this->db->table("customers") . "
				WHERE LOWER(`email`) = LOWER('" . $this->db->escape($email) . "')";
		if($no_subscribers){
			$sql .= " AND customer_group_id NOT IN (SELECT customer_group_id FROM ".$this->db->table('customer_groups')." WHERE `name` = 'Newsletter Subscribers')";
		}
		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}

	/**
	 * @param string $email
	 * @return array
	 */
	public function getCustomerByEmail($email,$approved=false) {
		$sql = "SELECT * FROM " . $this->db->table("customers") . " WHERE LOWER(`email`) = LOWER('" . $this->db->escape($email) . "')";
		if ( $approved) {
			$sql .= ' AND approved = 1';
		}
		
		//assuming that data is not encrypted. Can not call these otherwise
		$query = $this->db->query($sql);
		return $query->row;
	}

	/**
	 * @param string $cif
	 * @return bool
	 */
	public function checkCIF($cif) {
		//assuming that data is not encrypted. Can not call these otherwise
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . $this->db->table("customers") . " WHERE cif = '".$cif."'");
		$data=$query->row;
		return $data['total'];
	}

	/**
	 * added by fazrin - get Field from table customer
	 * @param string $field
	 * @return int
	 */
	public function getField($customer_id, $field) {
		//assuming that data is not encrypted. Can not call these otherwise
		$sql = "SELECT $field FROM " . $this->db->table("customers") . " WHERE customer_id = '".$customer_id."'";
		$query = $this->db->query($sql);
		$data=$query->row;
		return $data[$field];
	}

	/**
	 * added by fazrin - get Field from table customer
	 * @param string $field
	 * @return int
	 */
	public function getFieldCustom($field, $key, $key_value) {
		//assuming that data is not encrypted. Can not call these otherwise
		$sql = "SELECT $field FROM " . $this->db->table("customers") . " WHERE $key = '".$key_value."'";
		$query = $this->db->query($sql);
		$data=$query->row;
		return $data[$field];
	}

	/**
	 * @param int $customer_id
	 * @param string $field
	 * @param mixed $value
	 */
	public function editCustomerField($field, $field_value, $key, $value) {
		$sql = "UPDATE ". $this->db->table("customers") . " SET $field = '".$field_value."' WHERE $key = '".$value."'";
		$query = $this->db->query($sql);
	}

	/**
	 * @param int $customer_id
	 */
	public function updateAuditStatus($customer_id,$status) {
		$sql = "UPDATE ". $this->db->table("activation_audit") . " SET status = $status WHERE customer_id = '".$customer_id."'";
		$query = $this->db->query($sql);
	}

	/**
	 * @param string $loginname
	 * @param string $email
	 * @return array
	 */
	public function getCustomerByLoginnameAndEmail($loginname, $email) {
		$query = $this->db->query("SELECT * FROM " . $this->db->table("customers") . " WHERE LOWER(`loginname`) = LOWER('" . $this->db->escape($loginname) . "')");
		//validate it is correct row by matchign decrypted email;
		$result_row = $this->dcrypt->decrypt_data($query->row, 'customers');		
		if ( strtolower($result_row['email']) == strtolower($email) ) {
			return $result_row;
		} else {
			return array();
		}				
	}

	/**
	 * @param string $lastname
	 * @param string $email
	 * @return array
	 */
	public function getCustomerByLastnameAndEmail($lastname, $email) {
		$query = $this->db->query("SELECT * FROM " . $this->db->table("customers") . " WHERE LOWER(`lastname`) = LOWER('" . $this->db->escape($lastname) . "')");
		//validate if we have row with matchign decrypted email;		
		$result_row = array();
		foreach ($query->rows as $result) {
			if ( strtolower($email) == strtolower($this->dcrypt->decrypt_field($result['email'], $result['key_id'])) ) {
				$result_row = $result;
				break;
			}
		}	
				
		if ( count($result_row) ) {
			$result_row = $this->dcrypt->decrypt_data($result_row, 'customers');
			return $result_row;
		} else {
			return array();
		}				
	}

	/**
	 * @param string $loginname
	 * @return bool
	 */
	public function is_unique_loginname( $loginname ) {
		if( empty($loginname) ) {
			return false;
		}
      	$query = $this->db->query("SELECT COUNT(*) AS total
      	                           FROM " . $this->db->table("customers") . "
      	                           WHERE LOWER(`loginname`) = LOWER('" . $loginname . "')");
      	if ($query->row['total'] > 0) {
      		return false;
      	} else {
      		return true;
      	}                           
	}

	public function emailCheck($email) {
    
    $table_customer = $this->db->query("SELECT count(*) as total
                   FROM " . $this->db->table('customers') . "
                   WHERE email = '" . $email . "'")->row['total'];

    $table_customer_profile = $this->db->query("SELECT count(*) as total
                   FROM " . $this->db->table('customer_profiles') . "
                   WHERE email = '" . $email . "'")->row['total'];

    $table_customer_profile_temp = $this->db->query("SELECT count(*) as total
                   FROM " . $this->db->table('customer_profiles_temp') . "
                   WHERE email = '" . $email . "'")->row['total'];
    if ($table_customer!=false||$table_customer_profile!=false||$table_customer_profile_temp!=false) {
      $email_count = 1;
    }else{
      $email_count = 0;
    }
    
    return $email_count;
  }

	/**
	 * @param array $data
	 * @return array
	 */
	public function validateRegistrationData( $data ) {
		
		$error = array();

		
		if ( $this->config->get('prevent_email_as_login')) {
			//validate only if email login is not allowed
			$login_name_pattern = '/^[\w._-]+$/i';
    		if ( mb_strlen($data['loginname']) < 5
					|| mb_strlen($data['loginname']) > 64
					|| !preg_match($login_name_pattern, $data['loginname'])
			) {
      			$error['loginname'] = $this->language->get('error_loginname');
    		//validate uniqunes of login name
   		 	} else if ( !$this->is_unique_loginname($data['loginname']) ) {
   		   		$error['loginname'] = $this->language->get('error_loginname_notunique');
   		 	}			
		} 
	
    	if ((mb_strlen($data['firstname']) < 1) || (mb_strlen($data['firstname']) > 32)) {
      		$error['firstname'] = $this->language->get('error_firstname');
    	}

    	if ((mb_strlen($data['lastname']) < 1) || (mb_strlen($data['lastname']) > 32)) {
      		$error['lastname'] = $this->language->get('error_lastname');
    	}

		$pattern = '/^[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}$/i';

    	if ((mb_strlen($data['email']) > 96) || (!preg_match($pattern, $data['email']))) {
      		$error['email'] = $this->language->get('error_email');
    	}

    	if ($this->getTotalCustomersByEmail($data['email'])) {
      		$error['warning'] = $this->language->get('error_exists');
    	}

		if ((mb_strlen($data['telephone']) < 3) || (mb_strlen($data['telephone']) > 32)) {
			$error['telephone'] = $this->language->get('error_telephone');
		}

		// custom field begin

		if ((mb_strlen($data['id_number']) < 1)) {
      		$error['id_number'] = $this->language->get('error_id_number');
    	}
    	if ($data['id_type'] == FALSE) {
      		$error['id_type'] = $this->language->get('error_id_type');
    	}
    	if ((mb_strlen($data['cif']) < 1)) {
      		$error['cif'] = $this->language->get('error_cif');
    	}
    	if ((mb_strlen($data['bank_account_name']) < 1)) {
      		$error['bank_account_name'] = $this->language->get('error_bank_account_name');
    	}
    	if ((mb_strlen($data['bank_name']) == '')) {
      		$error['bank_name'] = $this->language->get('error_bank_name');
    	}
    	if ((mb_strlen($data['bank_account_no']) < 1)) {
      		$error['bank_account_no'] = $this->language->get('error_bank_account_no');
    	}

		// if ((mb_strlen($data['address_1']) < 3) || (mb_strlen($data['address_1']) > 128)) {
		// 	$error['address_1'] = $this->language->get('error_address_1');
		// }

		// if ((mb_strlen($data['city']) < 3) || (mb_strlen($data['city']) > 128)) {
		// 	$error['city'] = $this->language->get('error_city');
		// }
		// if ((mb_strlen($data['postcode']) < 3) || (mb_strlen($data['postcode']) > 128)) {
		// 	$error['postcode'] = $this->language->get('error_postcode');
		// }

		// if ($data['country_id'] == 'FALSE') {
		// 	$error['country'] = $this->language->get('error_country');
		// }

		// if ($data['zone_id'] == 'FALSE') {
		// 	$error['zone'] = $this->language->get('error_zone');
		// }

		if ((mb_strlen($data['password']) < 4) || (mb_strlen($data['password']) > 20)) {
			$error['password'] = $this->language->get('error_password');
		}

		if ($data['confirm'] != $data['password']) {
			$error['confirm'] = $this->language->get('error_confirm');
		}

		if ($this->config->get('config_account_id')) {
			$this->load->model('catalog/content');

			$content_info = $this->model_catalog_content->getContent($this->config->get('config_account_id'));

			if ($content_info) {
				if (!isset($data['agree'])) {
					$error['warning'] = sprintf($this->language->get('error_agree'), $content_info['title']);
				}
			}
		}
		if (!empty($data['cif'])) {
			if ($this->checkCIF($data['cif'])) {
				$error['warning'] = sprintf($this->language->get('error_duplicate_cif'));
			}
		}

		if (!empty($data['email'])) {
			if ($this->emailCheck($data['email'])=='1') {
				$error['warning'] = sprintf($this->language->get('errors_duplicte_email'));
			}
		}
    	return $error;
	}
	/**
	 * @param array $data
	 * @return array
	 */
	public function validateSubscribeData( $data ) {
		$error = array();

    	if ((mb_strlen($data['firstname']) < 1) || (mb_strlen($data['firstname']) > 32)) {
      		$error['firstname'] = $this->language->get('error_firstname');
    	}

    	if ((mb_strlen($data['lastname']) < 1) || (mb_strlen($data['lastname']) > 32)) {
      		$error['lastname'] = $this->language->get('error_lastname');
    	}

		$pattern = '/^[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}$/i';

    	if ((mb_strlen($data['email']) > 96) || (!preg_match($pattern, $data['email']))) {
      		$error['email'] = $this->language->get('error_email');
    	}

		if ( $this->getTotalCustomersByEmail($data['email'])) {
			$error['warning'] = $this->language->get('error_subscriber_exists');
		}

		if (!isset($this->session->data['captcha']) || ($this->session->data['captcha'] != $this->request->post['captcha'])) {
			$error['captcha'] = $this->language->get('error_captcha');
		}

    	return $error;
	}

	/**
	 * @param array $data
	 * @return array
	 */
	public function validateEditData( $data ) {
		$error = array();	
		
		//validate loginname only if cannot match email and if it is set. Edit of loginname not allowed
		if ( $this->config->get('prevent_email_as_login') && isset($data['loginname']) ) {
			//validate only if email login is not allowed
			$login_name_pattern = '/^[\w._-]+$/i';
    		if ((mb_strlen($data['loginname']) < 5) || (mb_strlen($data['loginname']) > 64)
    			|| (!preg_match($login_name_pattern, $data['loginname'])) ) {
      			$error['loginname'] = $this->language->get('error_loginname');
    		//validate uniqunes of login name
   		 	} else if ( !$this->is_unique_loginname($data['loginname']) ) {
   		   		$error['loginname'] = $this->language->get('error_loginname_notunique');
   		 	}			
		} 
		
		if ((mb_strlen($data['firstname']) < 1) || (mb_strlen($data['firstname']) > 32)) {
			$error['firstname'] = $this->language->get('error_firstname');
		}

		if ((mb_strlen($data['lastname']) < 1) || (mb_strlen($data['lastname']) > 32)) {
			$error['lastname'] = $this->language->get('error_lastname');
		}

		$pattern = '/^[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}$/i';

		if ((mb_strlen($data['email']) > 96) || (!preg_match($pattern, $data['email']))) {
			$error['email'] = $this->language->get('error_email');
		}
		
		if (($this->customer->getEmail() != $data['email']) && $this->getTotalCustomersByEmail($data['email'])) {
			$error['warning'] = $this->language->get('error_exists');
		}

		if ((mb_strlen($data['telephone']) < 3) || (mb_strlen($data['telephone']) > 32)) {
			$error['telephone'] = $this->language->get('error_telephone');
		}

		if ( count($error) && empty( $error['warning'] ) ) {
			$error['warning'] = $this->language->get('gen_data_entry_error');
		}
		
    	return $error;
	}

	public function getTotalTransactions() {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . $this->db->table("customer_transactions") . "` WHERE customer_id = '" . (int)$this->customer->getId() . "'" );
		
		return $query->row['total'];
	}
	
	public function getTransactions($start = 0, $limit = 20) {
		if ($start < 0) {
			$start = 0;
		}
		
		$query = $this->db->query("SELECT 
			t.customer_transaction_id, 
			t.order_id, 
			t.section, 
			t.credit, 
			t.debit, 
			t.transaction_type, 
			t.description, 
			t.create_date 
			FROM `" . $this->db->table("customer_transactions") . "` t 
			WHERE customer_id = '" . (int)$this->customer->getId() . "' 
			ORDER BY t.create_date DESC LIMIT " . (int)$start . "," . (int)$limit);
	
		return $query->rows;
	}

	public function getTotalTransactionHistory($cond = array()) {
		if(!$cond['date_start'] || !$cond['date_end']){
			return 0;
		}

		$sql = "SELECT 
				COUNT(*) as total 
			FROM `" . $this->db->table("transaction_history") . "` hist
			INNER JOIN `" . $this->db->table("customers") . "` cust
			ON hist.holder_id = cust.holder_id
			INNER JOIN `" . $this->db->table("products") . "` prod
			ON hist.product_code = prod.model
			LEFT JOIN `" . $this->db->table("product_descriptions") . "` proddesc
			ON prod.product_id = proddesc.product_id
			AND proddesc.language_id = '".$language_id."'
			INNER JOIN `" . $this->db->table("lookup") . "` trxtype
			ON hist.trx_type = trxtype.item_code
			AND trxtype.group_code = 'TRX_TYPE'
			LEFT JOIN `" . $this->db->table("lookup_descriptions") . "` trxtypedesc
			ON trxtype.lookup_id = trxtypedesc.lookup_id
			AND trxtypedesc.language_id = '".$language_id."'
			WHERE cust.customer_id = '" . (int)$this->customer->getId() . "'";

		if($cond['product_type']){
			$sql .= " AND hist.product_code = '".$cond['product_type']."'";
		}

		if($cond['date_start'] && $cond['date_end']){
			$sql .= " AND hist.trx_date > DATE_ADD('" . $this->db->escape($cond['date_start']) . "', INTERVAL -1 DAY)";
			$sql .= " AND hist.trx_date < DATE_ADD('" . $this->db->escape($cond['date_end']) . "', INTERVAL 1 DAY)";
		}

      	$query = $this->db->query($sql);
		
		return $query->row['total'];
	}

	public function getTransactionHistory($start = 0, $limit = 20, $cond = false){
		$language_id = (int)$this->config->get('storefront_language_id');
		if($start < 0){
			$start = 0;
		}

		$sql = "SELECT 
				hist.trx_id as frontend_trx_id,
				hist.backend_trx_id,
				hist.cif,
				hist.holder_id,
				hist.inv_account_no,
				hist.trx_date,
				hist.product_code,
				proddesc.name,
				proddesc.language_id,
				hist.trx_type,
				hist.trx_units as units,
				hist.trx_amount as amount,
				hist.fee_amount,
				hist.nav_date,
				hist.nav_value,
				hist.realized_gl,
				hist.movement_balance,
				hist.average_cost,
			  	trxtypedesc.item_name as trxtype_desc
			FROM `" . $this->db->table("transaction_history") . "` hist
			INNER JOIN `" . $this->db->table("customers") . "` cust
			ON hist.holder_id = cust.holder_id
			INNER JOIN `" . $this->db->table("products") . "` prod
			ON hist.product_code = prod.model
			LEFT JOIN `" . $this->db->table("product_descriptions") . "` proddesc
			ON prod.product_id = proddesc.product_id
			AND proddesc.language_id = '".$language_id."'
			INNER JOIN `" . $this->db->table("lookup") . "` trxtype
			ON hist.trx_type = trxtype.item_code
			AND trxtype.group_code = 'TRX_TYPE'
			LEFT JOIN `" . $this->db->table("lookup_descriptions") . "` trxtypedesc
			ON trxtype.lookup_id = trxtypedesc.lookup_id
			AND trxtypedesc.language_id = '".$language_id."'
			WHERE cust.customer_id = '" . (int)$this->customer->getId() . "'";

		if($cond['product_type']){
			$sql .= " AND hist.product_code = '".$cond['product_type']."'";
		}

		if($cond['date_start'] && $cond['date_end']){
			$sql .= " AND hist.trx_date > DATE_ADD('" . $this->db->escape($cond['date_start']) . "', INTERVAL -1 DAY)";
			$sql .= " AND hist.trx_date < DATE_ADD('" . $this->db->escape($cond['date_end']) . "', INTERVAL 1 DAY)";
		}

		$sql .= " ORDER BY hist.trx_date DESC";

		if($limit){
			$sql .= " LIMIT " . (int)$start . "," . (int)$limit;	
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getProductOptions(){
		$query = $this->db->query("SELECT 
			d.name as product_name,
			p.model as product_code 
			FROM `" . $this->db->table("products") . "` p 
			LEFT JOIN " . $this->db->table("product_descriptions") . " d ON (p.product_id = d.product_id) 
		");

		$products = array();

		foreach ($query->rows as $key => $product) {
			$products[$product['product_code']] = strtoupper($product['product_name']);
		}

		return $products;
	}

	public function getSubscribersCustomerGroupId() {
		$query = $this->db->query("SELECT customer_group_id	FROM `" . $this->db->table("customer_groups") . "` WHERE `name` = 'Newsletter Subscribers' LIMIT 0,1");
		$result = !$query->row['customer_group_id'] ? (int)$this->config->get('config_customer_group_id') :  $query->row['customer_group_id'];
		return $result;
	}

	public function keyReset($email, $key) {
			$sql = "UPDATE " . $this->db->table("customers") . " SET reset_key = '" . $this->db->escape($key) . "' WHERE email = '" . $this->db->escape($email) . "'";
      // echo $sql;exit;
      $this->db->query($sql);
	}
	public function isKeyValid($key) {
		
      	//$data = $this->db->query("SELECT COUNT(*)	FROM " . $this->db->table("customers") . " WHERE reset_key = '" . $this->db->escape($key) . "'");
      	
      	$query = $this->db->query("SELECT COUNT(*) as total	FROM " . $this->db->table("customers") . " WHERE reset_key = '" . $this->db->escape($key) . "'");
		$result_row = $this->dcrypt->decrypt_data($query->row, 'customers');
		return $query->row['total'];
	}

	public function validateResetPass( $data ) {
		$error = array();

		if ((mb_strlen($data['password']) < 4) || (mb_strlen($data['password']) > 20)) {
			$error['password'] = $this->language->get('error_password');
		}

		if ($data['confirm'] != $data['password']) {
			$error['confirm'] = $this->language->get('error_confirm');
		}
		return $error;
	}

	public function validateChangePass( $data ) {
		$error = array();

		//get current password by key
		$curr_pass = $this->getFieldCustom('password','reset_key',$data['key']);

		if ($curr_pass != $this->db->escape(AEncryption::getHash($data['old_password']))) {
			
			$this->session->data['error_old_password'] = $this->language->get('error_old_password');
			$error['old_password'] = $this->session->data['error_old_password'];
		}	

		if ((mb_strlen($data['password']) < 4) || (mb_strlen($data['password']) > 20)) {
			$this->session->data['password'] = $this->language->get('error_password');
			$error['password'] = $this->session->data['password'];
		}

		if ($data['confirm'] != $data['password']) {
			$this->session->data['confirm'] = $this->language->get('error_confirm');
			$error['confirm'] = $this->session->data['confirm'];
		}
		return $error;
	}

	public function resetPassByKey($password, $key) {
		$password = AEncryption::getHash($password);
      	$this->db->query("UPDATE " . $this->db->table("customers") . " SET password = '" . $this->db->escape($password) . "' WHERE reset_key = '" . $this->db->escape($key) . "'");
      	$this->db->query("UPDATE " . $this->db->table("customers") . " SET reset_key = null WHERE reset_key = '" . $this->db->escape($key) . "'");
	}

	public function getNavHistory($product_code){
		$language_id = (int)$this->config->get('storefront_language_id');
		$sql = "SELECT 
			nav.nav_date,
		    nav.product_code,
		    proddesc.name as product_name,
		    nav.nav_value as nav_per_unit
		FROM  `" . $this->db->table("navpu") . "` nav
		INNER JOIN `" . $this->db->table("products") . "` prod
		ON nav.product_code = prod.model
		LEFT JOIN `" . $this->db->table("product_descriptions") . "` proddesc
		ON prod.product_id = proddesc.product_id
		AND proddesc.language_id = '".$language_id."'
		WHERE nav.product_code = '".$product_code."'
		ORDER BY nav.nav_date desc
		LIMIT 30";

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getPortfolioBalance(){
		$language_id = (int)$this->config->get('storefront_language_id');

		$sql = "SELECT 
					cust.customer_id,
					CONCAT_WS(' ',cust.firstname,cust.lastname) as customer_name,
					custprofiles.risk_profile,
					riskprofiledesc.item_name as riskprofiledesc,
					custprofiles.risk_profile_last_update,
					custbal.balance_date,
				 	custbal.fund_type,
					lookupdesc.item_name as category,
				 	custbal.product_code,
				 	proddesc.product_id,
					proddesc.name as product_name,
					prod.status as product_status,
					custbal.nav_value as navpu,
					custbal.unrealized_gl as gain_loss,
					custbal.ending_balance as unit_holding,
					custbal.portfolio_pct as portfolio_percentage,
					custbal.initial_subs_date as initial_subscription,
					custbal.sync_datetime,
					custbal.ending_balance_amount as market_valuation
				FROM `" . $this->db->table("customers") . "` cust
				LEFT JOIN `" . $this->db->table("customer_profiles") . "` custprofiles
				ON cust.holder_id = custprofiles.holder_id
				LEFT JOIN (
					SELECT 
						bal.holder_id,
						bal.cif,
						bal.balance_date,
						bal.fund_type,
						bal.product_code,
						bal.nav_value,
						bal.unrealized_gl,
						bal.ending_balance,
						bal.portfolio_pct,
						bal.initial_subs_date,
						bal.sync_datetime,
						bal.ending_balance_amount
					FROM `" . $this->db->table("customer_balances") . "` bal
					INNER JOIN (
						SELECT product_code, max(nav_date) AS nav_date
						FROM `" . $this->db->table("navpu") . "` 
						GROUP BY product_code
					) latestnav
					ON bal.product_code = latestnav.product_code
					AND bal.balance_date = latestnav.nav_date
				) custbal
				ON cust.holder_id = custbal.holder_id
				LEFT JOIN `" . $this->db->table("products") . "` prod
				ON custbal.product_code = prod.model
				LEFT JOIN `" . $this->db->table("product_descriptions") . "` proddesc
				ON prod.product_id = proddesc.product_id
				AND proddesc.language_id = '".$language_id."'
				LEFT JOIN `" . $this->db->table("lookup") . "` lookup
				ON lookup.item_code LIKE CONCAT('%',custbal.fund_type,'%') 
				AND lookup.group_code = 'FUND_TYPE'
				LEFT JOIN `" . $this->db->table("lookup_descriptions") . "` lookupdesc
				ON lookup.lookup_id = lookupdesc.lookup_id
				AND lookupdesc.language_id = '".$language_id."'
				LEFT JOIN `" . $this->db->table("lookup") . "` riskprofile
				ON custprofiles.risk_profile = riskprofile.item_code
				AND riskprofile.group_code = 'RISK_PROFILE'
				LEFT JOIN `" . $this->db->table("lookup_descriptions") . "` riskprofiledesc
				ON riskprofile.lookup_id = riskprofiledesc.lookup_id
				AND riskprofiledesc.language_id = '".$language_id."'
				WHERE cust.customer_id = '" . (int)$this->customer->getId() . "'";

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getRiskprofile(){
		$portfolio_balance = $this->getPortfolioBalance();

		$results = array();
		foreach ($portfolio_balance as $key => $data) {
			$results['risk_profile'] = $data['risk_profile'];
			$results['riskprofiledesc'] = $data['riskprofiledesc'];
			$results['risk_profile_last_update'] = $data['risk_profile_last_update'];
		}

		return $results;
	}

	public function getRiskprofileAlloc($code){
		$sql = "SELECT 
					riskprofile_code,
					asset_class,
					allocation_pct 
				FROM `" . $this->db->table("riskprofile_portfolio_alloc") . "` 
				WHERE riskprofile_code = '".$code."'";

		$result = $this->db->query($sql);
		return $result->rows;
	}

	public function getInvestmentSummary($date_from, $date_to){
		$language_id = (int)$this->config->get('storefront_language_id');

		if(!$date_from || !$date_to){
			return array();
		}

		$sql = "SELECT value FROM `" . $this->db->table("settings") . "` WHERE `group` = 'general' AND `key` = 'trx_history_retention_in_days' GROUP BY `group`";
		$setting_day = $this->db->query($sql)->rows[0]['value'];

		$default_from = date("Y-m-d",strtotime(date("Y-m-d")." -".$setting_day." day"));
		$default_to = date("Y-m-d");

		$date_from = $date_from ? $date_from : $default_from;
		$date_to = $date_to ? $date_to : $default_to;

		$sql = "SELECT 
				    IFNULL(bal.cif,' ') as cif,
				    bal.holder_id,
				    bal.product_code,
				    bal.inv_account_no,
				    bal.beginning_balance,
				    endbal.ending_balance - bal.beginning_balance as movement,
				    endbal.ending_balance,
				    endbal.ending_balance_amount,
				    endbal.balance_date,
				    endbal.nav_value,
				    endbal.unrealized_gl,
				    total_realized.total_realized_gl,
				    proddesc.name as product_name,
				    CONCAT_WS(' ', cust.firstname, cust.lastname) as cust_name
			    FROM `" . $this->db->table("customer_balances") . "` bal
			    INNER JOIN `" . $this->db->table("customer_balances") . "` endbal
				    ON bal.holder_id = endbal.holder_id
				    AND bal.inv_account_no = endbal.inv_account_no
				    AND bal.product_code = endbal.product_code
			    INNER JOIN `" . $this->db->table("customers") . "` cust
			    	ON bal.holder_id = cust.holder_id
			    INNER JOIN (
				    SELECT hist.inv_account_no, hist.product_code, hist.holder_id, sum(IFNULL(hist.realized_gl,0)) as total_realized_gl 
				    FROM `" . $this->db->table("transaction_history") . "` hist
				    WHERE hist.nav_date > DATE_ADD('".$date_from."', INTERVAL -1 DAY)
				    AND hist.nav_date < DATE_ADD('".$date_to."', INTERVAL 1 DAY)
				    GROUP BY hist.inv_account_no, hist.product_code, hist.holder_id
			    ) total_realized
				    ON bal.holder_id = total_realized.holder_id
				    AND bal.inv_account_no = total_realized.inv_account_no
				    AND bal.product_code = total_realized.product_code
			    INNER JOIN `" . $this->db->table("products") . "` prod
			    	ON bal.product_code = prod.model
			    LEFT JOIN `" . $this->db->table("product_descriptions") . "` proddesc
				    ON prod.product_id = proddesc.product_id
				    AND proddesc.language_id = '".$language_id."'
			    WHERE cust.customer_id = '" . (int)$this->customer->getId() . "'";

		$sql .= " AND bal.balance_date = '".$date_from."'
				AND endbal.balance_date = '".$date_to."'";

		$sql .= " ORDER BY bal.inv_account_no, proddesc.name";

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTransactionDetails($start = 0, $limit = 20, $date_from = false, $date_to = false){
		$language_id = (int)$this->config->get('storefront_language_id');

		if(!$date_from || !$date_to){
			return array();
		}

		if ($start < 0) {
			$start = 0;
		}

		$sql = "SELECT value FROM `" . $this->db->table("settings") . "` WHERE `group` = 'general' AND `key` = 'trx_history_retention_in_days' GROUP BY `group`";
		$setting_day = $this->db->query($sql)->rows[0]['value'];

		$default_to = date("Y-m-d");
		$date_to = $date_to ? $date_to : $default_to;

		$default_from = date("Y-m-d",strtotime($date_to." -".$setting_day." day"));
		$date_from = $date_from ? $date_from : $default_from;

		$sql = "SELECT 
					hist.inv_account_no, 
					hist.nav_date as trx_date,
					hist.trx_amount,
					hist.trx_units,
					hist.net_trx_amount,
					hist.fee_amount,
					hist.realized_gl,
					hist.nav_value as nav_per_unit,
					hist.average_cost,
					proddesc.name as product_name,
					trxtypedesc.item_name as trx_type,
					bal.beginning_balance
				FROM `" . $this->db->table("transaction_history") . "` hist
				INNER JOIN `" . $this->db->table("customers") . "` cust
				ON hist.holder_id = cust.holder_id
				INNER JOIN `" . $this->db->table("products") . "` prod
				ON hist.product_code = prod.model
				LEFT JOIN `" . $this->db->table("product_descriptions") . "` proddesc
				ON prod.product_id = proddesc.product_id
				AND proddesc.language_id = '".$language_id."'
				INNER JOIN `" . $this->db->table("lookup") . "` trxtype
				ON trxtype.group_code = 'TRX_TYPE'
				AND trxtype.item_code = hist.trx_type
				LEFT JOIN `" . $this->db->table("lookup_descriptions") . "` trxtypedesc
				ON trxtype.lookup_id = trxtypedesc.lookup_id
				AND trxtypedesc.language_id = '".$language_id."'
				INNER JOIN `" . $this->db->table("customer_balances") . "` bal
				ON hist.inv_account_no = bal.inv_account_no
				AND hist.product_code = bal.product_code
				AND hist.holder_id = bal.holder_id ";

		$sql .= " AND bal.balance_date = '".$date_from."'";
		
		$sql .= " WHERE cust.customer_id = '" . (int)$this->customer->getId() . "'";
		
		$sql .= " AND hist.nav_date > DATE_ADD('".$date_from."', INTERVAL -1 DAY)
					AND hist.nav_date < DATE_ADD('".$date_to."', INTERVAL 1 DAY) ";

		$sql .= " ORDER BY hist.inv_account_no, proddesc.name, hist.nav_date";

		if($limit){
			$sql .= "  LIMIT " . (int)$start . "," . (int)$limit;	
		}

    	$query = $this->db->query($sql);

    	return $query->rows;
	}

	public function getTransactionDetailTotal($date_from = false, $date_to = false){
		$language_id = (int)$this->config->get('storefront_language_id');

		if(!$date_from || !$date_to){
			return 0;
		}

		$sql = "SELECT value FROM `" . $this->db->table("settings") . "` WHERE `group` = 'general' AND `key` = 'trx_history_retention_in_days' GROUP BY `group`";
		$setting_day = $this->db->query($sql)->rows[0]['value'];

		$default_from = date("Y-m-d",strtotime(date("Y-m-d")." -".$setting_day." day"));
		$default_to = date("Y-m-d");

		$date_from = $date_from ? $date_from : $default_from;
		$date_to = $date_to ? $date_to : $default_to;

		$sql = "SELECT 
					COUNT(*) as count
				FROM `" . $this->db->table("transaction_history") . "` hist
				INNER JOIN `" . $this->db->table("customers") . "` cust
				ON hist.holder_id = cust.holder_id
				INNER JOIN `" . $this->db->table("products") . "` prod
				ON hist.product_code = prod.model
				LEFT JOIN `" . $this->db->table("product_descriptions") . "` proddesc
				ON prod.product_id = proddesc.product_id
				AND proddesc.language_id = '".$language_id."'
				INNER JOIN `" . $this->db->table("lookup") . "` trxtype
				ON trxtype.group_code = 'TRX_TYPE'
				AND trxtype.item_code = hist.trx_type
				LEFT JOIN `" . $this->db->table("lookup_descriptions") . "` trxtypedesc
				ON trxtype.lookup_id = trxtypedesc.lookup_id
				AND trxtypedesc.language_id = '".$language_id."'
				INNER JOIN `" . $this->db->table("customer_balances") . "` bal
				ON hist.inv_account_no = bal.inv_account_no
				AND hist.product_code = bal.product_code
				AND hist.holder_id = bal.holder_id ";

		$sql .= " AND bal.balance_date = '".$date_from."'";
		
		$sql .= " WHERE cust.customer_id = '" . (int)$this->customer->getId() . "'";
		
		$sql .= " AND hist.nav_date > DATE_ADD('".$date_from."', INTERVAL -1 DAY)
					AND hist.nav_date < DATE_ADD('".$date_to."', INTERVAL 1 DAY) ";

		$sql .= " ORDER BY hist.inv_account_no, proddesc.name, hist.nav_date";

    	$query = $this->db->query($sql);

    	return $query->row['count'];	
	}

	public function getCountryByCode($country_code){
		$lang = (int)$this->config->get('storefront_language_id');
		$sql =  "SELECT cd.name
									FROM " . DB_PREFIX . "countries c LEFT JOIN "  . DB_PREFIX . "country_descriptions cd
									ON (c.country_id = cd.country_id and cd.language_id = $lang)
									WHERE c.iso_code_2 = '" . $country_code . "'";
		$query = $this->db->query($sql);
	
		return $query->row['name'];	
	}
}
