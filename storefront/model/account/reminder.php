<?php
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' )) {
	header ( 'Location: static_pages/' );
}
class ModelAccountReminder extends Model {
	public function addReminder($data){
		$sql = "INSERT INTO " . $this->db->table("reminder") . "
			  SET	customer_id = '".$this->db->escape($data['customer_id'])."',
			  		reminder_date = '".$this->db->escape($data['reminder_date'])."',
					descriptions = '".$this->db->escape($data['descriptions'])."',
					trx_id = '".$this->db->escape($data['trx_id'])."',
					created_datetime = NOW(),
					created_by = '".$this->db->escape($data['created_by'])."'
					";
		$this->db->query($sql);
	}
	public function getReminders($customer_id,$start) {
		$query = $this->db->query("SELECT *
								   FROM " . $this->db->table('reminder') . "
								   WHERE customer_id = ".$customer_id."
								   ORDER BY reminder_date ASC
								   LIMIT ".(int)$start.",10");

		$result_row = $query->rows;
		return $result_row;
	}
	public function getDetailReminder($id) {
		$query = $this->db->query("SELECT *
								   FROM " . $this->db->table('reminder') . "
								   WHERE reminder_id = ".$id."");

		$result_row = $query->row;
		return $result_row;
	}
	public function totalReminders($customer_id) {
		$query = $this->db->query("SELECT *
								   FROM " . $this->db->table('reminder') . "
								   WHERE customer_id = ".$customer_id."");

		$result_row = $query->rows;
		return count($result_row);
	}
	public function editReminder($data){
		$sql = "UPDATE " . $this->db->table("reminder") . "
			  SET	customer_id = '".$this->db->escape($data['customer_id'])."',
			  		reminder_date = '".$this->db->escape($data['reminder_date'])."',
					descriptions = '".$this->db->escape($data['descriptions'])."',
					trx_id = '".$this->db->escape($data['trx_id'])."',
					created_datetime = NOW(),
					created_by = '".$this->db->escape($data['created_by'])."'
					WHERE reminder_id = ".$data['reminder_id']."
					";
		$this->db->query($sql);
	}
	public function deleteReminder($reminder_id){
		$sql = "delete from " . $this->db->table("reminder") . "
					WHERE reminder_id = ".$reminder_id."";
		$this->db->query($sql);
	}

	public function reminderEmail(){
		$query = $this->db->query("select 
						rm.descriptions,
						rm.reminder_date,
						c.email ,c.firstname
				from " . $this->db->table("reminder") . " rm left join
				" . $this->db->table("customers") . " c 
						ON rm.customer_id = c.customer_id
				");

		$result_row = $query->rows;
		return $result_row;
	}

	public function addReminderSetup($product_id, $value, $status){

		$this->load->model('tool/lookup');
		$reminder_text = $this->model_tool_lookup->getLookupDescription('TEXT_REMINDER','REMINDER');
		if(!$reminder_text){
			$reminder_text = 'Reminder';
		}

		$customer_id = $this->session->data['customer_id'];
		$product = $this->model_catalog_product->getProduct($product_id);
		$productName = $product['name'];
		$description = $productName." ".$reminder_text;
		$customer = $this->model_account_customer->getCustomer($customer_id);
		$customerName = $customer['firstname']; 
		$data['status'] = 0;

		$data['transaction_id'] = 0;
		if($this->session->data['curr_product_key'] && $this->session->data['order_ids']){
			$curr_product_id = $this->session->data['curr_product_key'];
			$data['transaction_id'] = $this->session->data['order_ids'][$curr_product_id];
		}

		$sql = "INSERT INTO  ". $this->db->table("reminder_setup") ." 
						SET 
						customer_id = '".$this->db->escape($customer_id)."',
						description = '".$this->db->escape($description)."',
						day_of_occurence = '".$this->db->escape($value['reminder']['date'])."',
						number_of_occurences = '".$this->db->escape($value['reminder']['repeat'])."',
						created_datetime = '".date('Y-m-d h:i:s')."',
						created_by = '".$this->db->escape($customerName)."',
						transaction_id = '".$this->db->escape($data['transaction_id'])."',
						status = '".$this->db->escape($status)."'" ;

		$this->db->query($sql);
		// $reminder_id = $this->db->getLastId();

		//generate into table reminder based on number_of_occurences post data
		$current_date = date('Y-m-d');
		$current['year'] = date('Y');
		$current['month'] = date('m');
		$current['day'] = $value['reminder']['date'];

		$current_day = date('d');
		
		// $this->showDebug($value['reminder']['date'],false);

		$counter = 0;
		$i=1;
		while($counter<(int)$value['reminder']['repeat']){

			// $this->showDebug((int)$current_day,false);exit();

			if($current_day && $current_day<(int)$value['reminder']['date']){
				// $current['day'] = $value['reminder']['date'];
				$date = strtotime(implode('-',$current));
			}else{
				
				$date = strtotime(date("Y-m-d", strtotime(implode('-',$current))) . "+".$i." month");
				// $current['month'] +=1;
				$i++;
			}

			$current_date = date('Y-m-d',$date);
			$sqlSetup = "INSERT INTO ".$this->db->table("reminder")." 
										SET
										customer_id = '".$customer_id."',
										reminder_date = '".$this->db->escape($current_date) ."',
										descriptions = '".$this->db->escape($description) ."', 
										created_datetime = '".$this->db->escape(date('Y-m-d H:i:s'))."',
										trx_id = '".$this->db->escape($data['transaction_id'])."', 
										created_by = '".$this->db->escape($customerName) ."'";

			$current_day = FALSE;
			$this->db->query($sqlSetup);
			$counter++;
		}

	}
}
