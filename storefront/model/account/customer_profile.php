<?php
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' )) {
  header ( 'Location: static_pages/' );
}

/**
 * Class ModelSaleCustomerProfile
 */
class ModelAccountCustomerProfile extends Model 
{
	public function addCustomerProfile($data,$table = "customer_profiles") {
      	$sql = "INSERT INTO " . $this->db->table($table) . "
      	                SET cif = '" . $this->db->escape($data['cif']) . "',
      	                    holder_id = '" . $this->db->escape($data['holder_id']) . "',
      	                    first_name = '" . $this->db->escape($data['firstname']) . "',
      	                    last_name = '" . $this->db->escape($data['lastname']) . "',
      	                    full_name = '" . $this->db->escape($data['full_name']) . "',
      	                    domicile_address = '" . $this->db->escape($data['domicile_address']) . "',
      	                    domicile_city = '" . $this->db->escape($data['domicile_city']) . "',
      	                    domicile_postal_code = '" . $this->db->escape($data['domicile_postal_code']) . "',
      	                    domicile_country = '" . $this->db->escape($data['domicile_country']) . "',
      	                    tax_id_no = '" . $this->db->escape($data['tax_id_no']) . "',
      	                    tax_id_reg_date = '" . $this->db->escape($data['tax_id_reg_date']) . "',
      	                    phone_no = '" . $this->db->escape($data['phone_no']) . "',
      	                    mobile_phone_no = '" . $this->db->escape($data['mobile_phone_no']) . "',
      	                    email = '" . $this->db->escape($data['email']) . "',
      	                    fax_no = '" . $this->db->escape($data['fax_no']) . "',
      	                    place_of_birth = '" . $this->db->escape($data['place_of_birth']) . "',
      	                    date_of_birth = '" . $this->db->escape($data['date_of_birth']) . "',
      	                    gender = '" . $this->db->escape($data['gender']) . "',
      	                    marital_status = '" . $this->db->escape($data['marital_status']) . "',
                            religion = '" . $this->db->escape($data['religion']) . "',
      	                    id_type = '" . $this->db->escape($data['id_type']) . "',
      	                    id_number = '" . $this->db->escape($data['id_number']) . "',
      	                    id_expiry_date = '" . $this->db->escape($data['id_expiry_date']) . "',
      	                    nationality = '" . $this->db->escape($data['nationality']) . "',
      	                    residency_country = '" . $this->db->escape($data['residency_country']) . "',
      	                    residency_status = '" . $this->db->escape($data['residency_status']) . "',
      	                    residency_status_txt = '" . $this->db->escape($data['residency_status_txt']) . "',
      	                    occupation = '" . $this->db->escape($data['occupation']) . "',
      	                    occupation_txt = '" . $this->db->escape($data['occupation_txt']) . "',
      	                    company_name = '" . $this->db->escape($data['company_name']) . "',
      	                    company_type = '" . $this->db->escape($data['company_type']) . "',
      	                    education = '" . $this->db->escape($data['education']) . "',
      	                    spouse_name = '" . $this->db->escape($data['spouse_name']) . "',
      	                    spouse_occupation = '" . $this->db->escape($data['spouse_occupation']) . "',
      	                    mother_name = '" . $this->db->escape($data['mother_name']) . "',
      	                    father_name = '" . $this->db->escape($data['father_name']) . "',
      	                    inheritor = '" . $this->db->escape($data['inheritor']) . "',
      	                    inheritor_relationship = '" . $this->db->escape($data['inheritor_relationship']) . "',
      	                    gross_income = '" . $this->db->escape($data['gross_income']) . "',
      	                    source_of_income = '" . $this->db->escape($data['source_of_income']) . "',
      	                    source_of_income_txt = '" . $this->db->escape($data['source_of_income_txt']) . "',
      	                    additional_income = '" . $this->db->escape($data['additional_income']) . "',
      	                    add_source_of_income = '" . $this->db->escape($data['add_source_of_income']) . "',
      	                    source_of_fund = '" . $this->db->escape($data['source_of_fund']) . "',
      	                    source_of_fund_txt = '" . $this->db->escape($data['source_of_fund_txt']) . "',
      	                    inv_objective = '" . $this->db->escape($data['inv_objective']) . "',
      	                    inv_objective_txt = '" . $this->db->escape($data['inv_objective_txt']) . "',
      	                    bank_name = '" . $this->db->escape($data['bank_name']) . "',
      	                    bank_address = '" . $this->db->escape($data['bank_address']) . "',
      	                    bank_account_name = '" . $this->db->escape($data['bank_account_name']) . "',
      	                    bank_account_no = '" . $this->db->escape($data['bank_account_no']) . "',
      	                    correspondence_address = '" . $this->db->escape($data['correspondence_address']) . "',
                            correspondence_city = '" . $this->db->escape($data['correspondence_city']) . "',
                            correspondence_postal_code = '" . $this->db->escape($data['correspondence_postal_code']) . "',
                            correspondence_country = '" . $this->db->escape($data['correspondence_country']) . "',
                            approved = '" . $this->db->escape($data['approved']) . "',
                            updated_datetime = now(),
                            risk_profile = '" . $this->db->escape($data['risk_profile']) . "',
      	                    risk_profile_last_update = '" . $this->db->escape($data['risk_profile_last_update']) . "'";
      	
		//uncomment this to check error
      	// echo "sql:".$sql;exit;
                        // echo $sql;exit();    
      	$this->db->query($sql);
      	
      	$profile_id = $this->db->getLastId();
      	
		return $profile_id;
	}

  public function updateCustomerProfile($data,$table = "customer_profiles") {
        $sql = "UPDATE " . $this->db->table($table) . "
                        SET cif = '" . $this->db->escape($data['cif']) . "',
                            holder_id = '" . $this->db->escape($data['holder_id']) . "',
                            first_name = '" . $this->db->escape($data['firstname']) . "',
                            last_name = '" . $this->db->escape($data['lastname']) . "',
                            full_name = '" . $this->db->escape($data['full_name']) . "',
                            domicile_address = '" . $this->db->escape($data['domicile_address']) . "',
                            domicile_city = '" . $this->db->escape($data['domicile_city']) . "',
                            domicile_postal_code = '" . $this->db->escape($data['domicile_postal_code']) . "',
                            domicile_country = '" . $this->db->escape($data['domicile_country']) . "',
                            tax_id_no = '" . $this->db->escape($data['tax_id_no']) . "',
                            tax_id_reg_date = '" . $this->db->escape($data['tax_id_reg_date']) . "',
                            phone_no = '" . $this->db->escape($data['phone_no']) . "',
                            mobile_phone_no = '" . $this->db->escape($data['mobile_phone_no']) . "',
                            email = '" . $this->db->escape($data['email']) . "',
                            fax_no = '" . $this->db->escape($data['fax_no']) . "',
                            place_of_birth = '" . $this->db->escape($data['place_of_birth']) . "',
                            date_of_birth = '" . $this->db->escape($data['date_of_birth']) . "',
                            gender = '" . $this->db->escape($data['gender']) . "',
                            marital_status = '" . $this->db->escape($data['marital_status']) . "',
                            religion = '" . $this->db->escape($data['religion']) . "',
                            id_type = '" . $this->db->escape($data['id_type']) . "',
                            id_number = '" . $this->db->escape($data['id_number']) . "',
                            id_expiry_date = '" . $this->db->escape($data['id_expiry_date']) . "',
                            nationality = '" . $this->db->escape($data['nationality']) . "',
                            residency_country = '" . $this->db->escape($data['residency_country']) . "',
                            residency_status = '" . $this->db->escape($data['residency_status']) . "',
                            residency_status_txt = '" . $this->db->escape($data['residency_status_txt']) . "',
                            occupation = '" . $this->db->escape($data['occupation']) . "',
                            occupation_txt = '" . $this->db->escape($data['occupation_txt']) . "',
                            company_name = '" . $this->db->escape($data['company_name']) . "',
                            company_type = '" . $this->db->escape($data['company_type']) . "',
                            education = '" . $this->db->escape($data['education']) . "',
                            spouse_name = '" . $this->db->escape($data['spouse_name']) . "',
                            spouse_occupation = '" . $this->db->escape($data['spouse_occupation']) . "',
                            mother_name = '" . $this->db->escape($data['mother_name']) . "',
                            father_name = '" . $this->db->escape($data['father_name']) . "',
                            inheritor = '" . $this->db->escape($data['inheritor']) . "',
                            inheritor_relationship = '" . $this->db->escape($data['inheritor_relationship']) . "',
                            gross_income = '" . $this->db->escape($data['gross_income']) . "',
                            source_of_income = '" . $this->db->escape($data['source_of_income']) . "',
                            source_of_income_txt = '" . $this->db->escape($data['source_of_income_txt']) . "',
                            additional_income = '" . $this->db->escape($data['additional_income']) . "',
                            add_source_of_income = '" . $this->db->escape($data['add_source_of_income']) . "',
                            source_of_fund = '" . $this->db->escape($data['source_of_fund']) . "',
                            source_of_fund_txt = '" . $this->db->escape($data['source_of_fund_txt']) . "',
                            inv_objective = '" . $this->db->escape($data['inv_objective']) . "',
                            inv_objective_txt = '" . $this->db->escape($data['inv_objective_txt']) . "',
                            bank_name = '" . $this->db->escape($data['bank_name']) . "',
                            bank_address = '" . $this->db->escape($data['bank_address']) . "',
                            bank_account_name = '" . $this->db->escape($data['bank_account_name']) . "',
                            bank_account_no = '" . $this->db->escape($data['bank_account_no']) . "',
                            correspondence_address = '" . $this->db->escape($data['correspondence_address']) . "',
                            correspondence_city = '" . $this->db->escape($data['correspondence_city']) . "',
                            correspondence_postal_code = '" . $this->db->escape($data['correspondence_postal_code']) . "',
                            correspondence_country = '" . $this->db->escape($data['correspondence_country']) . "',
                            approved = '" . $this->db->escape($data['approved']) . "',
                            updated_datetime = now(),
                            risk_profile = '" . $this->db->escape($data['risk_profile']) . "',
                            risk_profile_last_update = '" . $this->db->escape($data['risk_profile_last_update']) . "' 
                            WHERE holder_id = '".$this->db->escape($data['holder_id'])."'";
        $this->db->query($sql);
        
        $profile_id = $this->db->getLastId();
        $this->loadModel('account/customers');
        //update juga table customers
        // 'firstname'
        // 'lastname'
        // 'email'
        // 'telephone'
        // 'fax'
        // 'bank_account_no'
        // 'bank_account_name'
        // 'id_type'

        
    return $profile_id;
  }

  public function updateFieldCustomer($field,$value,$holder_id){
    if(!is_null($value)){
      $sql = "UPDATE ".$this->db->table('customer_profiles')." SET $field = '". $this->db->escape($value) ."'";
    }else{
      $sql = "UPDATE ".$this->db->table('customer_profiles')." SET $field = NULL ";
    }
    $sql .= " WHERE holder_id = '". $this->db->escape($holder_id) ."'";
    $this->db->query($sql);
  }

  /**
   * @param int $holder_id
   * @return array
   */
  public function getWaitingCustomers($holder_id) {
    $sql = "SELECT * FROM ".$this->db->table('customer_profiles')." WHERE approved = 4";
    $query = $this->db->query($sql);
    $result_rows = $this->dcrypt->decrypt_data($query->rows, 'customer_profiles');
    // $this->showDebug($query);
    return $result_rows;
  }

  /**
   * @param int $holder_id
   * @return array
   */
  public function getCustomerId($holder_id) {
    $sql = "SELECT customer_id
                   FROM " . $this->db->table('customers') . " c 
                   INNER JOIN ".$this->db->table('customer_profiles')." cp
                   ON (c.holder_id = cp.holder_id)
                   WHERE cp.holder_id = '" . $holder_id . "'";
    $query = $this->db->query($sql);

    $result_row = $this->dcrypt->decrypt_data($query->row, 'customer_profiles');
    return $result_row['customer_id'];
  }

  /**
   * @param int $customer_id
   * @return array
   */
  public function getCustomerInfo($customer_id) {
    $sql = "SELECT *
                   FROM " . $this->db->table('customers') . " 
                   WHERE customer_id = '" . $customer_id . "'";
    $query = $this->db->query($sql);

    $result_row = $this->dcrypt->decrypt_data($query->row, 'customers');
    return $result_row;
  }

  public function updateFromTemporary($temp_data){
    $sql  = "UPDATE ".$this->db->table('customer_profiles')." SET ";
    foreach($temp_data as $key => $value){
      $sql .= $key." = '".$this->db->escape($value)."',";
    }
    $sql = substr($sql,0,-1);
    $sql .= " WHERE holder_id = '".$this->db->escape($temp_data['holder_id'])."'";
    $this->db->query($sql);

    //then delete temporary data
    $this->delTemporary($temp_data['holder_id']);
    $this->updateFieldCustomer('approved',NULL,$temp_data['holder_id']);
  }  

  private function delTemporary($holder_id){
    $delSql = "DELETE FROM ".$this->db->table('customer_profiles_temp')." WHERE holder_id = '".$this->db->escape($holder_id)."'";
    if($this->db->query($delSql)){
      return true;
    }else{
      return false;
    }

  }

	/**
	 * @param int $holder_id
	 * @return array
	 */
	public function getCustomer($holder_id,$table = "customer_profiles") {
		$sql = "SELECT DISTINCT *
                   FROM " . $this->db->table($table) . "
                   WHERE holder_id = '" . $holder_id . "'";
    $query = $this->db->query($sql);

		$result_row = $this->dcrypt->decrypt_data($query->row, 'customer_profiles');
		return $result_row;
	}

  public function getCustomerByCif($cif,$table = "customer_profiles") {
    $query = $this->db->query("SELECT count(*)
                   FROM " . $this->db->table($table) . "
                   WHERE cif = '" . $cif . "'");

    $result_row = $this->dcrypt->decrypt_data($query->row, 'customer_profiles');
    return count($result_row);
  }
   public function isPendingProfile($holder_id,$table = "customer_profiles_temp") {
    $query = $this->db->query("SELECT count(*) as total
                   FROM " . $this->db->table($table) . "
                   WHERE holder_id = '" . $holder_id . "'");
    $result = $query->row;
    return $result['total'];
  }
  public function getHolder(){
    $sql = "select holder_id from ".$this->db->table("customers")." where customer_id = ".$this->customer->getId()."";
    
    $query = $this->db->query($sql);
    return $query->row['holder_id'];
  }

  public function emailCheck($email) {
    
    $holder_id = self::getHolder();

    $curr_email_query = "select DISTINCT email from ".$this->db->table("customers")." WHERE holder_id = '".$holder_id."'";

    $current_email = $this->db->query($curr_email_query)->row['email'];
    
    $table_customer = $this->db->query("SELECT count(*) total from ".$this->db->table("customers")." c
                      where c.email NOT IN ('".$current_email."')
                      and c.email = '".$email."'")->row['total'];
    
    $table_customer_profile = $this->db->query("SELECT count(*) total from ".$this->db->table("customer_profiles")." c
                   where c.email NOT IN ('".$current_email."')
                   and c.email = '".$email."'")->row['total'];
    
    $table_customer_profile_temp = $this->db->query("SELECT count(*) total from ".$this->db->table("customer_profiles_temp")." c
                   where c.email NOT IN ('".$current_email."')
                   and c.email = '".$email."'")->row['total'];
    
    if ($table_customer!=false||$table_customer_profile!=false||$table_customer_profile_temp!=false) {
      $email_count = 1;
    }else{
      $email_count = 0;
    }
    
    return $email_count;
  }
}