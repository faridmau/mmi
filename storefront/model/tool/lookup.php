<?php
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' )) {
	header ( 'Location: static_pages/' );
}
class ModelToolLookup extends Model {
	public function addLookup($data) {

		if ($data['status']==1) {
			$status = 'ON';
		} else {
			$status = 'OFF';
		}
		
      	$this->db->query("INSERT INTO " . DB_PREFIX . "lookup SET 
      		group_code = '" . $this->db->escape($data['group_code']) . "',
      		item_code = '" . $this->db->escape($data['item_code']) . "',
      		item_name = '" . $this->db->escape($data['item_name']) . "',
      		status = '" . $this->db->escape($status) . "',
      		created_datetime = NOW()
      	");
		
		$lookup_id = $this->db->getLastId();

		return $lookup_id;
	}
	
	public function editLookup($lookup_id, $data) {
		
		$fields = array('group_code','item_name','item_code','status');
		$update = array();
		foreach ( $fields as $f ) {
			if ( isset($data[$f]) )
				
				if ($f=='status') {
					if ($data[$f]==1) {
						$update[] = "$f = 'ON'";
					}else{
						$update[] = "$f = 'OFF'";
					}
				}else{
					$update[] = "$f = '".$this->db->escape($data[$f])."'";
				}
				
		}
		
		if ( !empty($update) ) $this->db->query("UPDATE " . DB_PREFIX . "lookup SET ". implode(',', $update) ." WHERE lookup_id = '" . (int)$lookup_id . "'");

		// if (isset($data['manufacturer_store'])) {
		// 	$this->db->query("DELETE FROM " . DB_PREFIX . "manufacturers_to_stores WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");
		// 	foreach ($data['manufacturer_store'] as $store_id) {
		// 		$this->db->query("INSERT INTO " . DB_PREFIX . "manufacturers_to_stores SET manufacturer_id = '" . (int)$manufacturer_id . "', store_id = '" . (int)$store_id . "'");
		// 	}
		// }
		
		// if (isset($data['keyword'])) {
		// 	$data['keyword'] =  SEOEncode($data['keyword'],'manufacturer_id',$manufacturer_id);
		// 	if($data['keyword']){
		// 		$this->language->replaceDescriptions('url_aliases',
		// 												array('query' => "manufacturer_id=" . (int)$manufacturer_id),
		// 												array((int)$this->session->data['content_language_id'] => array('keyword' => $data['keyword'])));
		// 	}else{
		// 		$this->db->query("DELETE
		// 						FROM " . DB_PREFIX . "url_aliases
		// 						WHERE query = 'manufacturer_id=" . (int)$manufacturer_id . "'
		// 							AND language_id = '".(int)$this->session->data['content_language_id']."'");
		// 	}
		// }
		
		//$this->cache->delete('manufacturer');
	}
	
	public function deleteManufacturer($manufacturer_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "manufacturers WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "manufacturers_to_stores WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_aliases WHERE query = 'manufacturer_id=" . (int)$manufacturer_id . "'");

		$lm = new ALayoutManager();
		$lm->deletePageLayout('pages/product/manufacturer','manufacturer_id',(int)$manufacturer_id);
		$this->cache->delete('manufacturer');
	}	
	
	public function getLookup($lookup_id) {
		$query = $this->db->query("SELECT *
									FROM " . DB_PREFIX . "lookup
									WHERE lookup_id = '" . (int)$lookup_id . "'");
		
		return $query->row;
	}

	public function getLookupByGroupCode($group_code, $lang = false) {
		$query = $this->db->query("SELECT *
									FROM " . DB_PREFIX . "lookup
									WHERE group_code = '" . $group_code . "'");
		
		return $query->row;
	}
	
	public function getLookups($data = array(), $mode = 'default') {
		if ($data) {
			if ($mode == 'total_only') {
				$total_sql = 'count(*) as total';
			}
			else {
				$total_sql = '*';
			}
			$sql = "SELECT $total_sql FROM " . DB_PREFIX . "lookup";

			if ( !empty($data['subsql_filter']) )
				$sql .= " WHERE ".$data['subsql_filter'];

			//If for total, we done bulding the query
			if ($mode == 'total_only') {
			    $query = $this->db->query($sql);
			    return $query->row['total'];
			}
					
			$sort_data = array(
				'item_name',
				'item_code',
				'updated_datetime',
				'status'
			);	
			
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY item_name";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}					

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}				
			
			$query = $this->db->query($sql);
		
			return $query->rows;
		} 
	}

	public function getLookupData($group_code){
		$lang = $this->config->get('storefront_language_id');
		$sql = "SELECT l.lookup_id,l.item_code,l.item_name as item_optional,ld.item_name FROM " . $this->db->table("lookup") . " l
		left JOIN ".$this->db->table("lookup_descriptions")." ld ON (l.lookup_id = ld.lookup_id AND ld.language_id = '".$lang."') WHERE l.group_code = '".$group_code."'";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	// public function getLookupDescription($item_code){
	// 	$lang = $this->config->get('storefront_language_id');
	// 	$sql = "SELECT DISTINCT ld.item_name FROM " . $this->db->table("lookup") . " l 
	// 	LEFT JOIN ".$this->db->table("lookup_descriptions")."  ld ON (l.lookup_id = ld.lookup_id AND ld.language_id = '".$lang."') WHERE l.item_code = '".$item_code."'";
	// 	$query = $this->db->query($sql);
	// 	return $query->row;
	// }

	public function getTotalLookup($data = array()) {
		return $this->getLookups($data, 'total_only');
	}	

	public function getGroup() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "lookup GROUP BY group_code");

		return $query->rows;
	}

	public function getFundType(){
		$language_id = (int)$this->config->get('storefront_language_id');

		$sql = "SELECT 
					lookup.lookup_id, 
					lookup.item_code,
					lookupdesc.item_name 
				FROM `" . $this->db->table("lookup") . "` lookup 
				LEFT JOIN `" . $this->db->table("lookup_descriptions") . "` lookupdesc 
					ON lookup.lookup_id = lookupdesc.lookup_id
					AND lookupdesc.language_id = '".$language_id."' 
				WHERE lookup.group_code = 'FUND_TYPE' ";

		$query = $this->db->query($sql);

		$results = array();
		foreach ($query->rows as $key => $data) {
			$results[$data['item_code']] = $data['item_name'];
		}

		return $results;
	}

	public function getLookupDescription($item_code,$group_code=''){
		$lang = $this->config->get('storefront_language_id');
		$sql = "SELECT DISTINCT ld.item_name FROM " . $this->db->table("lookup") . " l 
		LEFT JOIN ".$this->db->table("lookup_descriptions")."  ld 
		ON (l.lookup_id = ld.lookup_id AND ld.language_id = '".$lang."') 
		WHERE l.item_code = '".$item_code."' AND l.group_code = '".$group_code."'";
		$query = $this->db->query($sql);
		return $query->row['item_name'];
	}
}	
?>