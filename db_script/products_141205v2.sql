-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.35-0ubuntu0.13.10.2 - (Ubuntu)
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table mmi.abmmi_products
CREATE TABLE IF NOT EXISTS `abmmi_products` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `model` varchar(64) COLLATE utf8_bin NOT NULL,
  `sku` varchar(64) COLLATE utf8_bin NOT NULL,
  `location` varchar(128) COLLATE utf8_bin NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `stock_status_id` int(11) NOT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `shipping` int(1) NOT NULL DEFAULT '1',
  `ship_individually` int(1) NOT NULL DEFAULT '0',
  `free_shipping` int(1) NOT NULL DEFAULT '0',
  `shipping_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `tax_class_id` int(11) NOT NULL,
  `date_available` date NOT NULL,
  `weight` decimal(5,2) NOT NULL DEFAULT '0.00',
  `weight_class_id` int(11) NOT NULL DEFAULT '0',
  `length` decimal(5,2) NOT NULL DEFAULT '0.00',
  `width` decimal(5,2) NOT NULL DEFAULT '0.00',
  `height` decimal(5,2) NOT NULL DEFAULT '0.00',
  `length_class_id` int(11) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `viewed` int(5) NOT NULL DEFAULT '0',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `subtract` int(1) NOT NULL DEFAULT '1',
  `minimum` int(11) NOT NULL DEFAULT '1',
  `maximum` int(11) NOT NULL DEFAULT '0',
  `cost` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `call_to_order` smallint(6) NOT NULL DEFAULT '0',
  `product_link` text COLLATE utf8_bin,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table mmi.abmmi_products: ~14 rows (approximately)
DELETE FROM `abmmi_products`;
/*!40000 ALTER TABLE `abmmi_products` DISABLE KEYS */;
INSERT INTO `abmmi_products` (`product_id`, `model`, `sku`, `location`, `quantity`, `stock_status_id`, `manufacturer_id`, `shipping`, `ship_individually`, `free_shipping`, `shipping_price`, `price`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `status`, `date_added`, `date_modified`, `viewed`, `sort_order`, `subtract`, `minimum`, `maximum`, `cost`, `call_to_order`, `product_link`) VALUES
	(111, 'RDMIA', 'MIA', '{ default_mandiri_ecash : reksadanamia, default_mandiri_clickpay: reksadanamiaclickpay }', 1, 1, 21, 0, 0, 0, 0.0000, 1.0000, 1, '2014-01-24', 0.00, 5, 0.00, 0.00, 0.00, 3, 1, '2014-01-25 07:19:38', '2014-01-25 09:28:24', 218, 0, 0, 1, 0, 0.0000, 0, 'http://mandiri-investasi.co.id/produk/produk-reksa-dana-2/campuran-2/mandiri-investa-aktif-mia/'),
	(112, 'RDMIDS', 'MIA', '{ default_mandiri_ecash : reksadanamitsya, default_mandiri_clickpay: reksadanamitsyaclickpay}', 1, 1, 21, 0, 0, 0, 0.0000, 1.0000, 1, '2014-01-24', 0.00, 5, 0.00, 0.00, 0.00, 3, 1, '2014-01-25 07:27:32', '2014-01-27 07:40:31', 52, 1, 0, 1, 0, 0.0000, 0, 'http://mandiri-investasi.co.id/produk/produk-reksa-dana-2/pendapatan/reksa-dana-mandiri-investa-dana-syariah-midsya/'),
	(113, 'RDMIDO2', 'MIDO2', '{ default_mandiri_ecash : reksadanamido2, default_mandiri_clickpay: reksadanamido2clickpay }', 1, 2, 21, 0, 0, 0, 0.0000, 1.0000, 1, '2014-01-24', 0.00, 5, 0.00, 0.00, 0.00, 3, 1, '2014-01-25 07:30:58', '2014-01-27 07:40:19', 16, 2, 0, 1, 0, 0.0000, 0, 'http://mandiri-investasi.co.id/produk/produk-reksa-dana-2/pendapatan/mandiri-investa-dana-obligasi-seri-ii-mido-ii/'),
	(114, 'RDMISB', 'MISB', '{ default_mandiri_ecash : reksadanamisb, default_mandiri_clickpay: reksadanamisbclickpay }', 1, 2, 21, 0, 0, 0, 0.0000, 1.0000, 1, '2014-01-24', 0.00, 5, 0.00, 0.00, 0.00, 3, 1, '2014-01-25 07:33:09', '2014-07-09 07:37:18', 20, 3, 0, 1, 0, 0.0000, 0, 'http://mandiri-investasi.co.id/produk/produk-reksa-dana-2/campuran-2/mandiri-investa-syariah-berimbang-misb/'),
	(116, 'RDMIDU', 'MIDU', '{ default_mandiri_ecash : reksadanamidu, default_mandiri_clickpay: reksadanamiduclickpay }', 1, 2, 21, 0, 0, 0, 0.0000, 1.0000, 1, '2014-01-24', 0.00, 5, 0.00, 0.00, 0.00, 3, 1, '2014-01-25 07:36:07', '2014-01-27 07:40:50', 34, 1, 0, 1, 0, 0.0000, 0, 'http://mandiri-investasi.co.id/produk/produk-reksa-dana-2/pendapatan/mandiri-investa-dana-utama-midu/'),
	(117, 'RDMITRAS', 'MITRAS', '{ default_mandiri_ecash : reksadanamitras, default_mandiri_clickpay: reksadanamitrasclickpay }', 1, 2, 21, 0, 0, 0, 0.0000, 1.0000, 1, '2014-01-24', 0.00, 5, 0.00, 0.00, 0.00, 3, 1, '2014-01-25 07:37:24', '2014-01-27 07:40:06', 18, 1, 0, 1, 0, 0.0000, 0, 'http://mandiri-investasi.co.id/produk/produk-reksa-dana-2/saham-2/mandiri-investa-atraktif-syariah-mitra-syariah/'),
	(118, 'RDMITRA', 'MITRA', '{ default_mandiri_ecash : reksadanamitra, default_mandiri_clickpay: reksadanamitraclickpay }', 1, 2, 21, 0, 0, 0, 0.0000, 1.0000, 1, '2014-01-24', 0.00, 5, 0.00, 0.00, 0.00, 3, 1, '2014-01-25 07:38:29', '2014-01-27 07:39:52', 30, 1, 0, 1, 0, 0.0000, 0, 'http://mandiri-investasi.co.id/produk/produk-reksa-dana-2/saham-2/mandiri-investa-atraktif-mitra/'),
	(119, 'RDMIPU', 'MPU', '{ default_mandiri_ecash : reksadanamipu, default_mandiri_clickpay: reksadanamipuclickpay}', 1, 2, 21, 0, 0, 0, 0.0000, 1.0000, 1, '2014-01-24', 0.00, 5, 0.00, 0.00, 0.00, 3, 1, '2014-01-25 07:39:59', '2014-01-27 07:41:06', 14, 1, 0, 1, 0, 0.0000, 0, 'http://mandiri-investasi.co.id/produk/produk-reksa-dana-2/pasar-uang-2/mandiri-investa-pasar-uang-mipu/'),
	(120, 'RDMIED', 'MIED', '{ default_mandiri_ecash : reksadanamied, default_mandiri_clickpay: reksadanamiedclickpay }', 1, 1, 21, 0, 0, 0, 0.0000, 1.0000, 1, '2014-01-24', 0.00, 5, 0.00, 0.00, 0.00, 3, 1, '2014-01-25 07:56:38', '2014-10-10 10:33:28', 21, 8, 0, 1, 0, 0.0000, 0, 'http://mandiri-investasi.co.id/produk/produk-reksa-dana-2/saham-2/mandiri-investa-ekuitas-dinamis-mied/'),
	(123, 'RDMIDB', 'MIDB', '{ default_mandiri_ecash : reksadanamidb, default_mandiri_clickpay: reksadanamidbclickpay }', 1, 1, 0, 1, 0, 0, 0.0000, 1.0000, 1, '2014-11-24', 0.00, 1, 0.00, 0.00, 0.00, 1, 1, '2014-11-25 15:37:18', '0000-00-00 00:00:00', 1, 1, 1, 1, 0, 0.0000, 0, NULL),
	(124, 'RDMIEA5P', 'MIESA5P', '{ default_mandiri_ecash : reksadanamiesa5p, default_mandiri_clickpay: reksadanamiesa5pclickpay }', 1, 1, 0, 1, 0, 0, 0.0000, 1.0000, 1, '2014-11-24', 0.00, 1, 0.00, 0.00, 0.00, 1, 1, '2014-11-25 15:40:37', '0000-00-00 00:00:00', 0, 1, 1, 1, 0, 0.0000, 0, NULL),
	(125, 'RDMIEDF', 'MIEDF', '{ default_mandiri_ecash : reksadanamiedf, default_mandiri_clickpay: reksadanamiedfclickpay }', 1, 1, 0, 1, 0, 0, 0.0000, 1.0000, 1, '2014-11-24', 0.00, 1, 0.00, 0.00, 0.00, 1, 1, '2014-11-25 15:48:08', '0000-00-00 00:00:00', 0, 1, 1, 1, 0, 0.0000, 0, NULL),
	(126, 'RDMIEM', 'MIEM', '{ default_mandiri_ecash : reksadanamiem, default_mandiri_clickpay: reksadanamiemclickpay }', 1, 1, 0, 1, 0, 0, 0.0000, 1.0000, 1, '2014-11-24', 0.00, 1, 0.00, 0.00, 0.00, 1, 1, '2014-11-25 15:49:50', '0000-00-00 00:00:00', 0, 1, 1, 1, 0, 0.0000, 0, 'http://mandiri-investasi.co.id/produk/produk-reksa-dana-2/saham-2/mandiri-investa-equity-movement-miem/'),
	(127, 'RDMIES', 'MIES', '{ default_mandiri_ecash : reksadanamies, default_mandiri_clickpay: reksadanamiesclickpay }', 1, 1, 0, 1, 0, 0, 0.0000, 1.0000, 1, '2014-11-24', 0.00, 1, 0.00, 0.00, 0.00, 1, 1, '2014-11-25 15:52:14', '0000-00-00 00:00:00', 0, 1, 1, 1, 0, 0.0000, 0, 'http://mandiri-investasi.co.id/produk/produk-reksa-dana-2/saham-2/mandiri-investa-ekuitas-syariah-mies/');
/*!40000 ALTER TABLE `abmmi_products` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
