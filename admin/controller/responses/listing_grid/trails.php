<?php
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if (!defined('DIR_CORE') || !IS_ADMIN) {
	header('Location: static_pages/');
}
class ControllerResponsesListingGridTrails extends AController {

	public function main() {

		//init controller data
		$this->extensions->hk_InitData($this, __FUNCTION__);

		$this->loadModel('tool/audit');
		$this->loadLanguage('tool/audit');

		$trails = $this->html->getSecureURL('tool/trails');
		//Prepare filter config
		$filter_params = array( 'start_date','end_date');
		$grid_filter_params = array( 'audited_datetime' );

		$filter_form = new AFilter(array( 'method' => 'get', 'filter_params' => $filter_params ));
		$filter_grid = new AFilter(array( 'method' => 'post', 'grid_filter_params' => $grid_filter_params ));
		// var_dump($filter_params);

		// $total = $this->model_catalog_review->getTotalReviews(array_merge($filter_form->getFilterData(), $filter_grid->getFilterData()));

		$total = $this->model_tool_audit->getTotalAuditTrails(array_merge($filter_form->getFilterData(), $filter_grid->getFilterData()));
		// var_dump($total);exit();
		$response = new stdClass();
		$response->page = $filter_grid->getParam('page');
		$response->total = $filter_grid->calcTotalPages($total);
		$response->records = $total;
		$results = $this->model_tool_audit->trailsByDate(array_merge($filter_form->getFilterData(), $filter_grid->getFilterData()));
		
		$resource = new AResource('image');
		$i = 0;
		$paging = json_decode(json_encode($response),TRUE);
		$nomor = (($paging['page']-1)*$filter_grid->request->post['rows'])+1;
		
		foreach ($results as $result) {
			$before = $after = "";
			$fields = explode('|',$result['field_name']);
			$data_before = explode('|',$result['data_before']);
			$data_after = explode('|',$result['data_after']);

			for($c=0;$c<count($fields);$c++){
				$before .= trim($fields[$c])." : ".trim($data_before[$c])."<br>";
				$after .= trim($fields[$c])." : ".trim($data_after[$c])."<br>";
			}

			$response->rows[ $i ][ 'id' ] = $result[ 'audit_id' ];
			$response->rows[ $i ][ 'cell' ] = array(
				$nomor,
				$result[ 'username' ],
				$result[ 'audited_ipaddress' ],
				$result[ 'login_datetime' ],
				$result[ 'logout_datetime' ],
				$result[ 'activity_name' ],
				// $result[ 'data_before' ],
				// $result[ 'data_after' ]
				$before,
				$after
			);
			$i++;
			$nomor++;
		}
		//update controller data
		$this->extensions->hk_UpdateData($this, __FUNCTION__);

		$this->load->library('json');
		$this->response->setOutput(AJson::encode($response));
	}
}

?>