<?php
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if (!defined('DIR_CORE') || !IS_ADMIN) {
	header('Location: static_pages/');
}
class ControllerResponsesListingGridAudit extends AController {

	public function main() {

		//init controller data
		$this->extensions->hk_InitData($this, __FUNCTION__);

		$this->loadModel('tool/audit');
		$this->loadLanguage('tool/audit');

		$trails = $this->html->getSecureURL('tool/trails');
		//Prepare filter config
		$filter_params = array( 'start_date','end_date');
		$grid_filter_params = array( 'audited_datetime' );

		$filter_form = new AFilter(array( 'method' => 'get', 'filter_params' => $filter_params ));
		$filter_grid = new AFilter(array( 'method' => 'post', 'grid_filter_params' => $grid_filter_params ));


		// $total = $this->model_catalog_review->getTotalReviews(array_merge($filter_form->getFilterData(), $filter_grid->getFilterData()));

		$total = $this->model_tool_audit->getTotalAuditDate(array_merge($filter_form->getFilterData(), $filter_grid->getFilterData()));
		// var_dump($total);exit();
		$response = new stdClass();
		$response->page = $filter_grid->getParam('page');
		$response->total = $filter_grid->calcTotalPages($total);
		$response->records = $total;
		$results = $this->model_tool_audit->getAllAuditDate(array_merge($filter_form->getFilterData(), $filter_grid->getFilterData()));
		// var_dump($results);exit();
		$resource = new AResource('image');
		$i = 0;
		// $nomor = 1;
		$paging = json_decode(json_encode($response),TRUE);
		$nomor = (($paging['page']-1)*$filter_grid->request->post['rows'])+1;
		foreach ($results as $result) {

			// $response->rows[ $i ][ 'id' ] = $result[ 'audit_id' ];
			$response->rows[ $i ][ 'cell' ] = array(
				$nomor,
				'<a href="'.$trails.'&date='.date("d-m-Y", strtotime($result[ 'audited_datetime' ])).'" style="display:block;padding:10px 0">'.dateISO2Display($result[ 'audited_datetime' ], $this->language->get('date_format_short')).'</a>'
				// dateISO2Display($result[ 'audited_datetime' ], $this->language->get('date_format_short'))
			);
			$i++;
			$nomor++;
		}

		//update controller data
		$this->extensions->hk_UpdateData($this, __FUNCTION__);

		$this->load->library('json');
		$this->response->setOutput(AJson::encode($response));
	}
}

?>