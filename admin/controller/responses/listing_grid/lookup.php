<?php
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' ) || !IS_ADMIN) {
	header ( 'Location: static_pages/' );
}
class ControllerResponsesListingGridLookup extends AController {
	public function main() {
		
		//init controller data
		$this->extensions->hk_InitData($this, __FUNCTION__);

		$this->loadLanguage('tool/lookup');
		$this->loadModel('tool/lookup');
		//---
		$this->loadModel('catalog/manufacturer');
		$this->loadModel('tool/image');


		//Prepare filter config
		$grid_filter_params = array( 'group_code', 'item_code');//,'item_name'
		$filter = new AFilter(array( 'method' => 'post', 'grid_filter_params' => $grid_filter_params ));
		$filter_data = $filter->getFilterData();
		// $this->showDebug($filter);
		// if($this->request->post['item_name']) 
		$filterData = html_entity_decode($this->request->post['filters']);
		
		// $this->showDebug($filterData);
		$total = $this->model_tool_lookup->getTotalLookup($filter_data);

		$response = new stdClass();
		$response->page = $filter->getParam('page');
		$response->total = $filter->calcTotalPages($total);
		$response->records = $total;
		$results = $this->model_tool_lookup->getLookups($filter_data);
		
		$i = 0;
		foreach ($results as $result) {
			$stat = $result['status'];
			if ($stat=='ON') {
				$statuses = 1;
			} else {
				$statuses = 0;
			}
			
			$response->rows[ $i ][ 'id' ] = $result[ 'lookup_id' ];
			$response->rows[ $i ][ 'cell' ] = array(
				$result[ 'group_code' ],
				$result[ 'item_code' ],//$result[ 'item_code' ],
				$this->html->buildInput(array(
					'name' => 'item_name[' . $result[ 'lookup_id' ] . ']',
					'value' => $result[ 'item_name_desc' ],
				)),
				$result[ 'updated_time' ],
				$this->html->buildCheckbox(array(
                    'name'  => 'status['.$result['lookup_id'].']',
                    'value' => $statuses,
                    'style'  => 'btn_switch',
                ))
			);
			$i++;
		}

		//update controller data
		$this->extensions->hk_UpdateData($this, __FUNCTION__);

		$this->load->library('json');
		$this->response->setOutput(AJson::encode($response));
	}

	public function update_field() {
		//var_dump($this->request->post);exit();
		//var_dump($this->request->post);exit();
		//init controller data
		$this->extensions->hk_InitData($this, __FUNCTION__);

		$this->loadLanguage('tool/lookup');
		$this->loadModel('tool/lookup');

		if (isset($this->request->get[ 'id' ])) {
			//request sent from edit form. ID in url
			foreach ($this->request->post as $field => $value) {
		
				if ($value=="") {
					$dd = new ADispatcher('responses/error/ajaxerror/validation',array('error_text'=>$err));
					return $dd->dispatch();
					// var_dump($key);
				}
					$this->model_tool_lookup->editLookup($this->request->get[ 'id' ], array( $field => $value ));
				
				
			}
			return null;
		}
		
		//request sent from jGrid. ID is key of array
		foreach ($this->request->post as $field => $value) {
			foreach ($value as $k => $v) {
				if ($v=="") {
					$dd = new ADispatcher('responses/error/ajaxerror/validation',array('error_text'=>$err));
					return $dd->dispatch();
				}
				$this->model_tool_lookup->editLookup($k, array( $field => $v ));
				
			}

		}

		//update controller data
		$this->extensions->hk_UpdateData($this, __FUNCTION__);
	}



	public function update() {
		//init controller data
		$this->extensions->hk_InitData($this, __FUNCTION__);

		if (!$this->user->canModify('listing_grid/lookup')) {
			$error = new AError('');
			return $error->toJSONResponse('NO_PERMISSIONS_402',
				array( 'error_text' => sprintf($this->language->get('error_permission_modify'), 'listing_grid/manufacturer'),
					'reset_value' => true
				));
		}
		
		$this->loadModel('tool/lookup');
		$this->loadLanguage('tool/lookup');
		$this->loadModel('catalog/product');
		
		$audit_log=array(
      "IP" => $this->request->server['REMOTE_ADDR'],
      "user_login" => $this->session->data['user_id'],
      "Activity" => "Delete Lookup",
      "login" => $this->session->data['user_last_login'],
      "logout" => ''
    );

		switch ($this->request->post[ 'oper' ]) {
			case 'del':
				$ids = array_unique(explode(',', $this->request->post[ 'id' ]));
				if (!empty($ids))
					foreach ($ids as $id) {
						$where = "lookup_id = '" . (int)$id . "'";
						$this->db->delete_with_trail($this->db->table("lookup"),$where,$audit_log);
					}
				break;
			case 'save':
				$allowedFields = array( 'status', 'item_name', );

				$ids = explode(',', $this->request->post[ 'id' ]);
				if (!empty($ids))
					//resort required. 
					if(  $this->request->post['resort'] == 'yes' ) {
						//get only ids we need
						foreach($ids as $id){
							$array[$id] = $this->request->post['sort_order'][$id];
						}
						$new_sort = build_sort_order($ids, min($array), max($array), $this->request->post['sort_direction']);
	 					$this->request->post['sort_order'] = $new_sort;
					}
					foreach ($ids as $id) {
						foreach ($allowedFields as $field) {
							$this->model_tool_lookup->editLookup($id, array( $field => $this->request->post[ $field ][ $id ] ));
						}
					}
				break;

			default:
				//print_r($this->request->post);
		}
		// exit;

		//update controller data
		$this->extensions->hk_UpdateData($this, __FUNCTION__);
	}
}