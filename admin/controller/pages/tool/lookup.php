<?php
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if (!defined('DIR_CORE') || !IS_ADMIN) {
    header('Location: static_pages/');
}
class ControllerPagesToolLookup extends AController {
    private $error = array();
    public $data = array();
    private $fields = array('group_code','item_code', 'item_name','language_id', 'status');
  
    public function main() {
        
        //init controller data
        $this->extensions->hk_InitData($this,__FUNCTION__);

        $this->document->setTitle( $this->language->get('heading_title') );

        $this->document->initBreadcrumb( array (
            'href'      => $this->html->getSecureURL('index/home'),
            'text'      => $this->language->get('text_home'),
            'separator' => FALSE
         ));
        $this->document->addBreadcrumb( array (
            'href'      => $this->html->getSecureURL('tool/lookup'),
            'text'      => $this->language->get('heading_title'),
            'separator' => ' :: '
         ));

        $this->view->assign('error_warning', $this->error['warning']);
        $this->view->assign('success', $this->session->data['success']);
        if (isset($this->session->data['success'])) {
            unset($this->session->data['success']);
        }

        $grid_settings = array( 
            'table_id' => 'lookup_grid',
            'url' => $this->html->getSecureURL('listing_grid/lookup'),
            'editurl' => $this->html->getSecureURL('listing_grid/lookup/update'),
            'update_field' => $this->html->getSecureURL('listing_grid/lookup/update_field'),
            'sortname' => 'sort_order',
            'sortorder' => 'asc',
            'actions' => array(
                'edit' => array(
                    'text' => $this->language->get('text_edit'),
                    'href' => $this->html->getSecureURL('tool/lookup/update', '&lookup_id=%ID%')
                ),
                'save' => array(
                    'text' => $this->language->get('button_save'),
                )
            ),
        );

        $grid_settings['colNames'] = array(
            $this->language->get('column_group'),
            $this->language->get('column_code'),
            $this->language->get('column_risk'),
            $this->language->get('column_last_update'),
            $this->language->get('column_status')
        );
        $grid_settings['colModel'] = array(
            array(
                'name' => 'group_code',
                'index' => 'group_code',
                'align' => 'center',
                'width' => 300,
                'sortable' => true,
                'search' => true,
            ),
            array(
                'name' => 'item_code',
                'index' => 'item_code',
                'width' => 300,
                'align' => 'center',
                'search' => true,
            ),
            array(
                'name' => 'item_name',
                'index' => 'item_name',
                'align' => 'center',
                'width' => 300,
                'sortable' => true,
                'search' => false,
            ),
            array(
                'name' => 'updated_datetime',
                'index' => 'updated_datetime',
                'width' => 200,
                'align' => 'center',
                'search' => false,
            ),
            array(
                'name' => 'status',
                'index' => 'status',
                'width' => 150,
                'align' => 'center',
                'search' => false,
            ),
        );

        $grid = $this->dispatch('common/listing_grid', array( $grid_settings ) );
        $this->view->assign('form_language_switch', $this->html->getContentLanguageSwitcher());
        $this->view->assign('language_id', $this->session->data['content_language_id']);

        $this->view->assign('listing_grid', $grid->dispatchGetOutput());

        $this->document->setTitle( $this->language->get('heading_title') );
        $this->view->assign( 'insert', $this->html->getSecureURL('tool/lookup/insert') );
        $this->view->assign('help_url', $this->gen_help_url('manufacturer_listing') );

        $this->processTemplate('pages/tool/lookup.tpl' );

        //update controller data
        $this->extensions->hk_UpdateData($this,__FUNCTION__);

    }
    public function insert() {

        //init controller data
        $this->extensions->hk_InitData($this,__FUNCTION__);
        $this->view->assign('form_language_switch', $this->html->getContentLanguageSwitcher());
        $this->document->setTitle( $this->language->get('heading_title') );
            
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->_validateForm()) {
            $lookup_id = $this->model_tool_lookup->addLookup($this->request->post);
            $this->session->data['success'] = $this->language->get('text_success');
            
            $this->redirect($this->html->getSecureURL('tool/lookup'));
        }
        $this->_getForm();

        //update controller data
        $this->extensions->hk_UpdateData($this,__FUNCTION__);
    }

    public function update() {
        
        //init controller data
        $this->extensions->hk_InitData($this,__FUNCTION__);

        $this->document->setTitle( $this->language->get('heading_title') );
        $this->view->assign('form_language_switch', $this->html->getContentLanguageSwitcher());
        $this->view->assign('error_warning', $this->error['warning']);
        $this->view->assign('success', $this->session->data['success']);
        if (isset($this->session->data['success'])) {
            unset($this->session->data['success']);
        }
        
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->_validateForm()) {
            /*    
            [group_code] => EXPIRE_LINK
            [item_code] => EXPIRE_LINK
            [item_name] => 7
            [status] => 1
            */

            $lookup_desc['item_name'] = $this->request->post['item_name'];
            // $this->showDebug($this->request->post);
            $this->model_tool_lookup->editLookup($this->request->get['lookup_id'], $this->request->post);
            $this->session->data['success'] = $this->language->get('text_success');
            $this->redirect($this->html->getSecureURL('tool/lookup/update', '&lookup_id=' . $this->request->get['lookup_id'] ));
        }


        $this->_getForm();

        //update controller data
        $this->extensions->hk_UpdateData($this,__FUNCTION__);
    }

    private function _getForm() {

        $this->view->assign('token', $this->session->data['token']);
        $this->view->assign('error_warning', $this->error['warning']);
        $this->view->assign('error_name', $this->error['name']);


        $this->document->initBreadcrumb( array (
            'href'      => $this->html->getSecureURL('index/home'),
            'text'      => $this->language->get('text_home'),
            'separator' => FALSE
         ));
        $this->document->addBreadcrumb( array ( 
            'href'      => $this->html->getSecureURL('tool/lookup'),
            'text'      => $this->language->get('heading_title'),
            'separator' => ' :: '
         ));
                            
        $this->view->assign('cancel', $this->html->getSecureURL('tool/lookup'));

        if (isset($this->request->get['lookup_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $lookup_info = $this->model_tool_lookup->getLookup($this->request->get['lookup_id']);
        }

        // $lookup_lang = $this->model_tool_lookup->getLookupIdByName('SIM');
        
        foreach ( $this->fields as $f ) {

            if (isset ( $this->request->post [$f] )) {
                $this->data [$f] = $this->request->post [$f];
            } elseif (isset($lookup_info) && isset($lookup_info[$f]) ) {
                $this->data[$f] = $lookup_info[$f];
            } else {
                $this->data[$f] = '';
            }
        }
        //var_dump($this->data);exit();
        if (!isset($this->request->get['lookup_id'])) {
            $this->data['action'] = $this->html->getSecureURL('tool/lookup/insert');

            //harus upte yaaaa.............XML

            $this->data['heading_title'] = $this->language->get('text_insert') . $this->language->get('text_lookup');
            $this->data['update'] = '';
            $form = new AForm('ST');
        } else {
            $this->data['action'] = $this->html->getSecureURL('tool/lookup/update', '&lookup_id=' . $this->request->get['lookup_id'] );
            $this->data['heading_title'] = $this->language->get('text_edit') . $this->language->get('text_lookup') . ' - ' . $this->data['item_code'];
            $this->data['update'] = $this->html->getSecureURL('listing_grid/lookup/update_field','&id='.$this->request->get['lookup_id']);
            $form = new AForm('HS');

            $this->data['lookup_edit'] = $this->html->getSecureURL('tool/lookup/update', '&lookup_id=' . $this->request->get['lookup_id'] );
            $this->data['tab_edit'] = $this->language->get('entry_edit');
            $this->data['tab_layout'] = $this->language->get('entry_layout');
            //$this->data['manufacturer_layout'] = $this->html->getSecureURL('catalog/manufacturer_layout', '&manufacturer_id=' . $this->request->get['manufacturer_id'] );
        }

        $this->document->addBreadcrumb( array (
            'href'      => $this->data['action'],
            'text'      => $this->data['heading_title'],
            'separator' => ' :: '
         ));

        $form->setForm(array(
            'form_name' => 'editFrm',
            'update' => $this->data['update'],
        ));

        $this->data['form']['id'] = 'editFrm';
        $this->data['form']['form_open'] = $form->getFieldHtml(array(
            'type' => 'form',
            'name' => 'editFrm',
            'attr' => 'confirm-exit="true"',
            'action' => $this->data['action'],
        ));
        $this->data['form']['submit'] = $form->getFieldHtml(array(
            'type' => 'button',
            'name' => 'submit',
            'text' => $this->language->get('button_save'),
            'style' => 'button1',
        ));
        $this->data['form']['cancel'] = $form->getFieldHtml(array(
            'type' => 'button',
            'name' => 'cancel',
            'text' => $this->language->get('button_cancel'),
            'style' => 'button2',
        ));
        
        // $group_code = $this->model_tool_lookup->getGroup();
        // //$options_group['0']='Select Type';
        // foreach ($group_code as $group) {
        //   $options_group[ $group['group_code'] ] = $group['group_code'];
        // }
        
        $this->data['form']['fields']['group_code'] = $form->getFieldHtml(array(
            'type' => 'input',
            'name' => 'group_code',
            'value' => $this->data['group_code'],
            'style' => 'large-field',
            'required' => true,
        ));
        $this->data['form']['fields']['item_code'] = $form->getFieldHtml(array(
            'type' => 'input',
            'name' => 'item_code',
            'value' => $this->data['item_code'],
            'required' => true,
            'style' => 'large-field'
        ));
        $this->data['form']['fields']['item_name'] = $form->getFieldHtml(array(
            'type' => 'input',
            'name' => 'item_name',
            'value' => $this->data['item_name'],
            'required' => true,
            'style' => 'large-field'
        ));
        
        // $langs = array();
        foreach($this->language->getActiveLanguages() as $key => $value){
            $langs[$value['language_id']] = $value['name'];
        }
        // $this->data['form']['fields']['language_id'] = $form->getFieldHtml(array(
        //     'type' => 'selectbox',
        //     'name' => 'language_id',
        //     'value' => $this->data['language_id']?$this->data['language_id']:$this->language->getContentLanguageID(),
        //     'options' => $langs,
        // ));

        $stat = $this->data['status'];
        if ($stat=='ON') {
            $status = 1;
        }else{
            $status = 0;
        }


        $this->data['form']['fields']['status'] = $form->getFieldHtml(array(
            'type' => 'checkbox',
            'name' => 'status',
            'value' => $status,
            'style' => 'btn_switch',
        ));
        //$this->view->assign('help_url', $this->gen_help_url('manufacturer_edit') );
        $this->view->batchAssign( $this->data );

        $this->addChild('responses/common/resource_library/get_resources_html', 'resources_html', 'responses/common/resource_library_scripts.tpl');
        $resources_scripts = $this->dispatch(
            'responses/common/resource_library/get_resources_scripts',
            array(
                'object_name' => 'lookup',
                'object_id' => $this->request->get['lookup_id'],
            )
        );
        $this->view->assign('resources_scripts', $resources_scripts->dispatchGetOutput());

        $this->processTemplate('pages/tool/lookup_form.tpl' );
    }  

    private function _validateForm() {
       if ((strlen(utf8_decode($this->request->post['group_code'])) < 1) || (strlen(utf8_decode($this->request->post['group_code'])) > 64)) {
            $this->error['warning'][] = $this->language->get('error_group');
        }
        
        if ((strlen(utf8_decode($this->request->post['item_code'])) < 1) || (strlen(utf8_decode($this->request->post['item_code'])) > 64)) {
            $this->error['warning'][] = $this->language->get('error_code');
        }
        if ((strlen(utf8_decode($this->request->post['item_name'])) < 1) || (strlen(utf8_decode($this->request->post['item_name'])) > 64)) {
            $this->error['warning'][] = $this->language->get('error_name');
        }

        $findCode = $this->model_tool_lookup->getLookupByGroupItem($this->request->post['group_code'],$this->request->post['item_code']);
        // var_dump($findCode);exit();
        if ($findCode!='0') {
            $this->error['warning'][] = $this->language->get('error_duplicate').$this->request->post['item_code'].$this->language->get('error_registered');
        }
        if (!$this->error) {
            return TRUE;
        } else {
            $this->error['warning'] = implode('<br>',$this->error['warning']);
            return FALSE;
        }
    }   
}