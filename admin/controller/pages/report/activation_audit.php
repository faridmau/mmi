<?php
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if (!defined('DIR_CORE') || !IS_ADMIN) {
    header('Location: static_pages/');
}
class ControllerPagesReportActivationAudit extends AController{
  public $data = array();
  public function main(){
    $this->extensions->hk_InitData($this,__FUNCTION__);
    $this->loadLanguage('report/activation_audit');
    //grid for searching
    $this->document->setTitle( $this->language->get('heading_title') );
    $this->document->initBreadcrumb(array(
        'href' => $this->html->getSecureURL('index/home'),
        'text' => $this->language->get('text_home'),
        'separator' => FALSE
    ));
    $this->document->addBreadcrumb(array(
        'href' => $this->html->getSecureURL('report/viewed'),
        'text' => $this->language->get('heading_title'),
        'separator' => ' :: '
    ));

    $form = new AForm();
    $form->setForm(array(
        'form_name' => 'customer_grid_search',
    ));

    $grid_settings = array(
        //id of grid
        'table_id' => 'customer_grid',
        // url to load data from
        'url' => $this->html->getSecureURL('listing_grid/customer/audit'),
        /*'editurl' => $this->html->getSecureURL('listing_grid/customer/update'),
        'update_field' => $this->html->getSecureURL ( 'listing_grid/customer/update_field' ),*/
        'sortname' => 'name',
        'sortorder' => 'asc',
        'multiselect' => 'false',
                // actions
        'actions' => array(
            'edit' => array(
              'text' => $this->language->get('text_edit'),
                'href' => $this->html->getSecureURL('report/activation_audit/detail','&customer_id=%ID%')
            ),
        ),
    );

    $grid_settings['colNames'] = array(
        $this->language->get('column_activation_date'),
        $this->language->get('column_approved_date'),
        $this->language->get('column_cif'),
        $this->language->get('column_customer_name'),
        $this->language->get('column_act_number'),
    );

    $grid_settings['colModel'] = array(
        array( 'name' => 'date_added',
                'index' => 'date_added',
                'width' => 160,
                'search' => false,
                'align' => 'center', ),
        array( 'name' => 'activation_date',
                'index' => 'activation_date',
                'width' => 160,
                'search' => false,
                'align' => 'center', ),
        array( 'name' => 'cif',
                'index' => 'cif',
                'width' => 140,
                'search' => false,
                'align' => 'center', ),
        array( 'name' => 'name',
                'index' => 'name',
                'width' => 80,
                'search' => false,
                'align' => 'center', ),
        array( 'name' => 'id_number',
                'index' => 'id_number',
                'width' => 120,
                'align' => 'center',
                'search' => false ),
    );

    //query
    $this->loadModel('sale/customer_group');
    $results = $this->model_sale_customer_group->getCustomerGroups();
    $groups = array( '' => $this->language->get('text_select_group'));
    foreach ( $results as $item) {
        $groups[ $item['customer_group_id'] ] = $item['name'];
        // echo $item['customer_group_id'].$item['name']."<br/>";
    }

    $grid_search_form = array();
    $grid_search_form['id'] = 'customer_grid_search';
    $grid_search_form['form_open'] = $form->getFieldHtml(array(
        'type' => 'form',
        'name' => 'customer_grid_search',
        'action' => '',
    ));

    $grid_search_form['submit'] = $form->getFieldHtml(array(
        'type' => 'button',
        'name' => 'submit',
        'text' => $this->language->get('button_go'),
        'style' => 'button1',
    ));

    $grid_search_form['reset'] = $form->getFieldHtml(array(
        'type' => 'button',
        'name' => 'reset',
        'text' => $this->language->get('button_reset'),
        'style' => 'button2',
    ));

    $grid_search_form['fields']['date1'] = $form->getFieldHtml(array(
      'type' => 'date',
      'name' => 'date1',
    ));

    $grid_search_form['fields']['date2'] = $form->getFieldHtml(array(
          'type' => 'date',
          'name' => 'date2',
    ));

    $grid_search_form['fields']['cif'] = $form->getFieldHtml(array(
          'type' => 'input',
          'name' => 'cif',
          'placeholder'=> 'CIF'
    ));

    $grid_settings['search_form'] = true;

    $grid = $this->dispatch('common/listing_grid', array( $grid_settings ) );
    $this->view->assign('listing_grid', $grid->dispatchGetOutput());
    $this->view->assign ( 'search_form', $grid_search_form );

    
    $this->processTemplate('pages/report/active_account_audit.tpl' );

    $this->extensions->hk_UpdateData($this,__FUNCTION__);
  }

  public function detail(){
    //init controller data
    $this->extensions->hk_InitData($this,__FUNCTION__);
    $this->document->setTitle( $this->language->get('heading_title') );

    $this->view->assign('success', $this->session->data['success']);
    if (isset($this->session->data['success'])) {
        unset($this->session->data['success']);
    }

    $this->_getAuditForm();

    //update controller data
    $this->extensions->hk_UpdateData($this,__FUNCTION__);
  }

  private function _getAuditForm(){


      $this->data['token'] = $this->session->data['token'];
      $this->data['error'] = $this->error;
      $this->loadCryptLib();
      
      $this->loadModel('tool/lookup');
      $this->loadModel('sale/customer');
      $this->loadLanguage('sale/customer');

      $this->document->initBreadcrumb( array (
          'href'      => $this->html->getSecureURL('index/home'),
          'text'      => $this->language->get('text_home'),
          'separator' => FALSE
       ));
      $this->document->addBreadcrumb( array ( 
          'href'      => $this->html->getSecureURL('sale/report'),
          'text'      => $this->language->get('heading_title'),
          'separator' => ' :: '
       ));

      $this->data['cancel'] = $this->html->getSecureURL('sale/report');


      if (isset($this->request->get['customer_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
          $customer_info = $this->model_sale_customer->getCustomerAudit($this->request->get['customer_id']);
      }

      foreach ($customer_info as $key => $value){
          $this->data[$key] = $value;
      }
      // $this->showDebug($this->data,false);

      if (!isset($this->request->get['customer_id'])) {
          $this->data['action'] = $this->html->getSecureURL('sale/customer/insert');
          $this->data['heading_title'] = $this->language->get('text_insert') . $this->language->get('text_customer');
          $this->data['update'] = '';
          $form = new AForm('ST');
      } else {
          $this->data['action'] = $this->html->getSecureURL('sale/customer/detail', '&customer_id=' . $this->request->get['customer_id'] );
          $this->data['heading_title'] = $this->language->get('tab_customer_details'). ' - ' . $this->data['first_name'] . ' ' . $this->data['last_name'] ;
          $this->data['update'] = $this->html->getSecureURL('listing_grid/customer/update_field','&id='.$this->request->get['customer_id']);
          $form = new AForm('HS');
      }

      $this->document->addBreadcrumb( array (
          'href'      => $this->data['action'],
          'text'      => $this->data['heading_title'],
          'separator' => ' :: '
       ));

      $form->setForm(array(
          'form_name' => 'cgFrm',
          'update' => $this->data['update'],
      ));

      $this->data['form']['id'] = 'cgFrm';
      $this->data['form']['form_open'] = $form->getFieldHtml(array(
          'type' => 'form',
          'name' => 'cgFrm',
          'attr' => 'confirm-exit="true"',
          'action' => $this->data['action'],
      ));
      $this->data['form']['submit'] = $form->getFieldHtml(array(
          'type' => 'button',
          'name' => 'submit',
          'text' => $this->language->get('button_save'),
          'style' => 'button1',
      ));
      $this->data['form']['approve'] = $form->getFieldHtml(array(
          'type' => 'button',
          'name' => 'submit',
          'text' => "Approve",
          'style' => 'button1',
      ));
      $this->data['form']['reject'] = $form->getFieldHtml(array(
          'type' => 'button',
          'name' => 'reject',
          'text' => 'Reject',//$this->language->get('button_cancel'),
          'style' => 'button1',
      ));
      $this->data['form']['cancel'] = $form->getFieldHtml(array(
          'type' => 'button',
          'name' => 'cancel',
          'text' => $this->language->get('button_cancel'),
          'style' => 'button2',
      ));

    $this->data['cancel'] = $this->html->getSecureURL('index/home');
    $this->data['reject'] = $this->html->getSecureURL('sale/customer/rejectCustomer&customer_id='.$this->request->get['customer_id']);
    $this->view->assign('retrieve_url',$this->html->getSecureURL('sale/customer/retrieveAccounts'));
  
    $required_input = array('first_name','last_name','email','telephone','id_number','id_type','cif');
    $bank_fields = array('bank_account_name','bank_name','bank_account_no');
    
    $required_input_backend = array('first_name_backend','last_name_backend','email_backend','telephone','id_number_backend','id_type_backend','cif_backend');
    $bank_fields_backend = array('bank_account_name_backend','bank_name_backend','bank_account_no_backend');
    $remarks = array('remark');

    foreach ( $required_input as $f ) {
        $this->data['form']['fields'][$f] = $form->getFieldHtml(array(
            'type' => 'input',
            'name' => $f,
            'value' => ($f=='id_type')?$this->model_tool_lookup->getLookupDescription($this->data[$f]):$this->data[$f],
            // 'placeholder' => "$f",
            'attr' => 'readonly',
        ));
    }

    foreach ( $required_input_backend as $f ) {
        //field untuk komparasi
        $this->data['form']['compare_fields'][$f] = $form->getFieldHtml(array(
            'type' => 'input',
            'name' => "c".$f,
            // 'value' => ($f=='id_type_backend')?$this->model_tool_lookup->getLookupDescription($this->data[$f]):$this->data[$f],
            'value' => $this->data[$f],
            // 'placeholder' => "$f",
            'attr' => 'readonly',
        ));
    }

    foreach ( $bank_fields as $f ) {
        $this->data['form']['bank'][$f] = $form->getFieldHtml(array(
            'type' => 'input',
            'name' => $f,
            // 'value' => ($f=='bank_name')?$this->model_tool_lookup->getLookupDescription($this->data[$f]):$this->data[$f],
            'value' => $this->data[$f],
            // 'placeholder' => "$f",
            'attr' => 'readonly',
        ));
    }

      //field untuk komparasi
      foreach ( $bank_fields_backend as $f ) {
          $this->data['form']['compare_bank'][$f] = $form->getFieldHtml(array(
              'type' => 'input',
              'name' => "c".$f,
              //'value' => ($f=='bank_name_backend')?$this->model_tool_lookup->getLookupDescription($this->data[$f]):$this->data[$f],
              'value' => $this->data[$f],
              // 'placeholder' => "$f",
              'attr' => 'readonly',
          ));
      }

      foreach ( $remarks as $f ) {
          $this->data['form']['remark'][$f] = $form->getFieldHtml(array(
              'type' => 'textarea',
              'name' => $f,
              'value' => $this->data[$f],
              'attr' => 'readonly',
              // 'placeholder' => "$f",
          ));
      }

      //approved date ~ di ambil dari table customer
      /*$customer_added = $this->model_sale_customer->getCustomer($this->request->get['customer_id'])['date_added'];
      $this->data['form']['date_added'] = $form->getFieldHtml(array(
          'type' => 'input',
          'name' => $f,
          'value' => $customer_added,
          'attr' => 'readonly',
          // 'placeholder' => "$f",
      ));*/


      //data untuk popup
      $this->data['popup_data'] = $this->model_sale_customer->getCustomers();

      $this->view->batchAssign( $this->data );
      $this->processTemplate('pages/report/active_account_audit_detail.tpl' );
  }
}

?>