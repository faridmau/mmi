<?php
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if (!defined('DIR_CORE') || !IS_ADMIN) {
    header('Location: static_pages/');
}
class ControllerPagesReportPotentialInstallment extends AController{
	public $data = array();
  public function main(){
    $this->extensions->hk_InitData($this,__FUNCTION__);
    // $this->loadLanguage('sale/customer');
    $this->document->setTitle($this->language->get('heading_potential_installment'));
      $this->document->initBreadcrumb(array(
          'href' => $this->html->getSecureURL('index/home'),
          'text' => $this->language->get('text_home'),
          'separator' => FALSE
      ));
      $this->document->addBreadcrumb(array(
          'href' => $this->html->getSecureURL('sale/customer/installment'),
          'text' => $this->language->get('heading_potential_installment'),
          'separator' => ' :: '
      ));

      //grid for searching

      $form = new AForm();
      $form->setForm(array(
        'form_name' => 'customer_grid_search',
      ));

      $grid_settings = array(
        //id of grid
        'table_id' => 'customer_grid',
        // url to load data from
        'url' => $this->html->getSecureURL('listing_grid/customer/installment'),
        'editurl' => $this->html->getSecureURL('listing_grid/customer/update'),
        'update_field' => $this->html->getSecureURL ( 'listing_grid/customer/update_field' ),
        'sortname' => 'name',
        'sortorder' => 'asc',
        'multiselect' => 'false',
      );

      $grid_settings['colNames'] = array(
        $this->language->get('column_registration_date'),
        $this->language->get('column_cif'),
        $this->language->get('column_customer_name'),
        $this->language->get('column_installment_date'),
        $this->language->get('column_amount'),
        $this->language->get('column_product_name'),
        /*'Registration Date',
        'CIF',
        'Customer Name',
        'Installment Date',
        'Amount',
        'Product Name'*/
      );

      $grid_settings['colModel'] = array(
        array( 'name' => 'reminder_date',
            'index' => 'reminder_date',
            'width' => 120,
            'align' => 'center', ),
        array( 'name' => 'cif',
            'index' => 'cif',
            'width' => 70,
            'align' => 'center', ),
        array( 'name' => 'name',
            'index' => 'name',
            'width' => 110,
            'align' => 'center', ),
        array( 'name' => 'topup_date',
            'index' => 'topup_date',
            'width' => 120,
            'align' => 'center',
            'search' => false ),
        array( 'name' => 'trx_amount',
            'index' => 'trx_amount',
            'width' => 50,
            'align' => 'center',
            'search' => false ),
        array( 'name' => 'product_name',
            'index' => 'product_name',
            'width' => 210,
            'align' => 'center',
            'search' => false ),
      );

      //query
      $this->loadModel('sale/customer_group');
      $results = $this->model_sale_customer_group->getCustomerGroups();
      $groups = array( '' => $this->language->get('text_select_group'), );
      foreach ( $results as $item) {
        $groups[ $item['customer_group_id'] ] = $item['name'];
        // echo $item['customer_group_id'].$item['name']."<br/>";
      }

      $grid_search_form = array();
          $grid_search_form['id'] = 'customer_grid_search';
          $grid_search_form['form_open'] = $form->getFieldHtml(array(
          'type' => 'form',
          'name' => 'customer_grid_search',
          'action' => '',
        ));
          $grid_search_form['submit'] = $form->getFieldHtml(array(
          'type' => 'button',
          'name' => 'submit',
          'text' => $this->language->get('button_go'),
          'style' => 'button1',
        ));
      $grid_search_form['reset'] = $form->getFieldHtml(array(
          'type' => 'button',
          'name' => 'reset',
          'text' => $this->language->get('button_reset'),
          'style' => 'button2',
        ));

      $grid_search_form['fields']['date1'] = $form->getFieldHtml(array(
          'type' => 'date',
          'name' => 'date1'
        ));
          $grid_search_form['fields']['date2'] = $form->getFieldHtml(array(
          'type' => 'date',
          'name' => 'date2'
        ));

      $grid_settings['search_form'] = true;

      $grid = $this->dispatch('common/listing_grid', array( $grid_settings ) );
      $this->view->assign('listing_grid', $grid->dispatchGetOutput());
      $this->view->assign ( 'search_form', $grid_search_form );

      $this->view->assign("heading_title","Registration Date");
      $this->processTemplate('pages/sale/potential_installment.tpl' );

      $this->extensions->hk_UpdateData($this,__FUNCTION__);
  }
}

?>