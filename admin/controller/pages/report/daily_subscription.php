<?php
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if (!defined('DIR_CORE') || !IS_ADMIN) {
    header('Location: static_pages/');
}
class ControllerPagesReportDailySubscription extends AController{
    public $data = array();
    public function main() {
      $this->loadLanguage('report/daily_subscription');
        //init controller data
        $this->extensions->hk_InitData($this, __FUNCTION__);
        $this->document->setTitle( $this->language->get('heading_title') );
        $this->document->initBreadcrumb(array(
            'href' => $this->html->getSecureURL('index/home'),
            'text' => $this->language->get('text_home'),
            'separator' => FALSE
        ));
        $this->document->addBreadcrumb(array(
            'href' => $this->html->getSecureURL('report/viewed'),
            'text' => $this->language->get('heading_title'),
            'separator' => ' :: '
        ));

        if (isset($this->session->data['error'])) {
            $this->data['error_warning'] = $this->session->data['error'];
            unset($this->session->data['error']);
        } elseif (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        //set content language to main language.
        if ($this->language->getContentLanguageID() != $this->language->getLanguageID()) {
            //reset content language
            $this->language->setCurrentContentLanguage($this->language->getLanguageID());
        }

        $grid_settings = array(
            //id of grid
            'table_id' => 'order_grid',
            // url to load data from
            'url' => $this->html->getSecureURL('listing_grid/daily_subscription', '&customer_id=' . $this->request->get['customer_id']),
            //'editurl' => $this->html->getSecureURL('listing_grid/daily_subscription/update'),
            //'update_field' => $this->html->getSecureURL('listing_grid/daily_subscription/update_field'),
            'sortname' => 'order_id',
            'sortorder' => 'desc',
            'multiselect' => 'false',
        );

        $grid_settings['colNames'] = array(
            $this->language->get('column_order'),
            $this->language->get('column_transaction_id'),
            $this->language->get('column_name'),
            $this->language->get('column_product_name'),
            $this->language->get('column_amount'),
            $this->language->get('column_fee_amount'),
            $this->language->get('column_total'),
            $this->language->get('column_payment_method')
        );
        $grid_settings['colModel'] = array(
            array('name' => 'order_id',
                'index' => 'order_id',
                'width' => 60,
                'align' => 'center',),
            array('name' => 'transaction_id',
                'index' => 'transaction_id',
                'width' => 140,
                'align' => 'center',
                'search' => false
            ),
            array('name' => 'name',
                'index' => 'name',
                'width' => 140,
                'align' => 'center',),
            array('name' => 'product_name',
                'index' => 'product_name',
                'width' => 140,
                'align' => 'center',
                'search' => false
            ),
            array('name' => 'amount',
                'index' => 'amount',
                'width' => 90,
                'align' => 'center',
                'search' => false
            ),
            array('name' => 'fee_amount',
                'index' => 'fee_amount',
                'width' => 90,
                'align' => 'center',
                'search' => false
            ),
            array('name' => 'total',
                'index' => 'total',
                'width' => 90,
                'align' => 'center',
                'search' => false
            ),
            array('name' => 'payment_method',
                'index' => 'payment_method',
                'width' => 90,
                'align' => 'center',
                'search' => false
                )
        );

        $this->loadModel('localisation/order_status');
        $results = $this->model_localisation_order_status->getOrderStatuses();
        // $statuses = array('' => $this->language->get('text_select_status'),);
        // foreach ($results as $item) {
        //     $statuses[$item['order_status_id']] = $item['name'];
        // }

        $results = $this->model_localisation_order_status->getPaymentMethods();
        $methods = array('' => $this->language->get('text_select_payment_method'));
        foreach ($results as $key => $item) {
            $methods[$item['payment_method']] = ucfirst($item['payment_method']);
        }

        $form = new AForm();
        $form->setForm(array(
            'form_name' => 'order_grid_search',
        ));

        $grid_search_form = array();
        $grid_search_form['id'] = 'order_grid_search';
        $grid_search_form['form_open'] = $form->getFieldHtml(array(
            'type' => 'form',
            'name' => 'order_grid_search',
            'action' => '',
        ));
        $grid_search_form['submit'] = $form->getFieldHtml(array(
            'type' => 'button',
            'name' => 'submit',
            'text' => $this->language->get('button_go'),
            'style' => 'button1',
        ));
        $grid_search_form['reset'] = $form->getFieldHtml(array(
            'type' => 'button',
            'name' => 'reset',
            'text' => $this->language->get('button_reset'),
            'style' => 'button2',
        ));
        $this->view->assign('js_date_format', format4Datepicker('Y-m-d'));
        $grid_search_form['fields']['payment_method'] = $form->getFieldHtml(array(
            'type' => 'selectbox',
            'name' => 'payment_method',
            'options' => $methods,
        ));
        $grid_search_form['fields']['date_added'] = $form->getFieldHtml(array(
            'type' => 'date',
            'name' => 'date_added',
      ));
        $grid_settings['search_form'] = true;


        $grid = $this->dispatch('common/listing_grid', array($grid_settings));
        $this->view->assign('listing_grid', $grid->dispatchGetOutput());
        $this->view->assign('search_form', $grid_search_form);
        $this->view->assign('help_url', $this->gen_help_url('order_listing'));

        $this->processTemplate('pages/report/daily_subscription.tpl');

        //update controller data
        $this->extensions->hk_UpdateData($this, __FUNCTION__);
    }
}

?>