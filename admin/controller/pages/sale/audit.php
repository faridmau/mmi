<?php
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' ) || !IS_ADMIN) {
	header ( 'Location: static_pages/' );
}
class ControllerPagesSaleAudit extends AController {
	public $data = array();
	private $error = array();
	
	private $profile_fields = array();
	
	private $fields = array('loginname','firstname','lastname', 'email', 'telephone', 'fax', 'newsletter', 
		'customer_group_id', 'status', 'approved', 'password','cart','address_id','ip','date_added','cif',
		'holder_id','bank_name','bank_account_name','bank_account_no','id_type','id_number','sync_datetime',
		'store_id','customer_id','first_name','last_name','domicile_address','domicile_city','domicile_postal_code',
		'domicile_country','tax_id_no','tax_id_reg_date','phone_no','mobile_phone_no','fax_no','place_of_birth',
		'date_of_birth','gender','marital_status','religion','id_expiry_date','nationality','residency_country',
		'residency_status','residency_status_txt','occupation','company_name','company_type','education',
		'spouse_name','spouse_occupation','mother_name','father_name','inheritor','inheritor_relationship',
		'gross_income','source_of_income','source_of_income_txt','additional_income','add_source_of_income',
		'source_of_fund','source_of_fund_txt','inv_objective','inv_objective_txt','bank_name','bank_address',
		'bank_account_name','bank_account_no','risk_profile','risk_profile_last_update','sync_datetime',
		'correspondence_address','correspondence_city','correspondence_code','correspondence_country');

	public function main()
	{
		//init controller data
		$this->extensions->hk_InitData($this, __FUNCTION__);

		$this->loadLanguage('sale/customer');
		$this->loadModel('sale/customer');
		$this->load->library('json');

		$approved = array(
			1 => $this->language->get('text_yes'),
			0 => $this->language->get('text_no'),
		);

		$page = $this->request->post[ 'page' ]; // get the requested page
		$limit = $this->request->post[ 'rows' ]; // get how many rows we want to have into the grid
		$sidx = $this->request->post[ 'sidx' ]; // get index row - i.e. user click to sort
		$sord = $this->request->post[ 'sord' ]; // get the direction

		$data = array(
			'sort' => $sidx,
			'order' => $sord,
			'start' => ($page - 1) * $limit,
			'limit' => $limit,
		);
		if ( has_value($this->request->get[ 'customer_group' ]) )
			$data['filter']['customer_group_id'] = $this->request->get[ 'customer_group' ];
		if ( has_value($this->request->get['status']) )
			$data['filter']['status'] = $this->request->get[ 'status' ];
		if ( has_value($this->request->get['approved']) )
			$data['filter']['approved'] = $this->request->get[ 'approved' ];
		$allowedFields = array( 'name', 'email' );
		if ( isset($this->request->post[ '_search' ]) && $this->request->post[ '_search' ] == 'true') {
			$searchData = AJson::decode(htmlspecialchars_decode($this->request->post[ 'filters' ]), true);

			foreach ($searchData[ 'rules' ] as $rule) {
				if (!in_array($rule[ 'field' ], $allowedFields)) continue;
				$data['filter'][ $rule[ 'field' ] ] = $rule[ 'data' ];
			}
		}

		$total = $this->model_sale_customer->getTotalCustomers($data);
		if ($total > 0) {
			$total_pages = ceil($total / $limit);
		} else {
			$total_pages = 0;
		}

		$response = new stdClass();
		$response->page = $page;
		$response->total = $total_pages;
		$response->records = $total;

		$results = $this->model_sale_customer->getCustomers($data);
		$i = 0;
		foreach ($results as $result) {

			$response->rows[ $i ][ 'id' ] = $result[ 'customer_id' ];
			$response->rows[ $i ][ 'cell' ] = array(
				$result[ 'name' ],
				'<a href="'.$this->html->getSecureURL('sale/contact','&email[]='.$result[ 'email' ]).'">'.$result[ 'email' ].'</a>',
				$result[ 'customer_group' ],
				$this->html->buildCheckbox(array(
					'name' => 'status[' . $result[ 'customer_id' ] . ']',
					'value' => $result[ 'status' ],
					'style' => 'btn_switch',
				)),
				$this->html->buildSelectbox(array(
					'name' => 'approved[' . $result[ 'customer_id' ] . ']',
					'value' => $result[ 'approved' ],
					'options' => $approved,
				)),
				($result[ 'orders_count' ]>0 ?
				$this->html->buildButton(array(
					'name' => 'view orders',
					'text' => $result[ 'orders_count' ],
					'style' => 'button2',
					'href'=> $this->html->getSecureURL('sale/order','&customer_id='.$result['customer_id']),
					'title' => $this->language->get('text_view').' '.$this->language->get('tab_history'),
					'target' => '_blank'
				))
				: 0),
			);
			$i++;
		}

		//update controller data
		$this->extensions->hk_UpdateData($this, __FUNCTION__);


		$this->response->setOutput(AJson::encode($response));
	}

	public function detail(){
  	//init controller data
    $this->extensions->hk_InitData($this,__FUNCTION__);
    // $this->loadLanguage('sale/customer');
    $this->document->setTitle( $this->language->get('heading_title') );

		$this->view->assign('success', $this->session->data['success']);
		if (isset($this->session->data['success'])) {
			unset($this->session->data['success']);
		}

		$customer_id = $this->request->get['customer_id'];
    	if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->_validateApproveForm($customer_id)) {
				//$this->_sendMail($customer_id);exit;
    		//1 = approve | 0 = reject | 2 = for sync
    		//insert status = 1 to array
    		$this->request->post['status'] = 1;
    		if($this->request->post['approved']==2){
					$this->showDebug($this->request->post,false);
					
					/* testing only */
					$this->request->post['approved'] = 1; //sementara di set ke 1, untuk keperluan testing
					/* testing only */

					if( (int)$this->request->post['approved'] ){
			  		$customer_info = $this->model_sale_customer->getCustomer($customer_id);
				  	$this->_sendMail($customer_id);
			  	}

					$this->model_sale_customer->editCustomer($this->request->get['customer_id'], $this->request->post);
					
					$this->session->data['success'] = $this->language->get('text_success');
					$this->redirect( $this->html->getSecureURL('index/home') );
    		}else{
					$this->_sendRejectMail($customer_id);

	    		$this->model_sale_customer->editCustomerField($customer_id, 'approved', 3);
					$this->model_sale_customer->editCustomerField($customer_id, 'remark', $this->request->post['remark']);
					$this->redirect($this->html->getSecureURL('index/home'));
    		}
		}

  	$this->_getForm();

    //update controller data
    $this->extensions->hk_UpdateData($this,__FUNCTION__);
	}

	private function _getForm(){
		$this->data['token'] = $this->session->data['token'];
		$this->data['error'] = $this->error;
		$this->loadCryptLib();

		$this->document->initBreadcrumb( array (
       		'href'      => $this->html->getSecureURL('index/home'),
       		'text'      => $this->language->get('text_home'),
      		'separator' => FALSE
   		 ));
   		$this->document->addBreadcrumb( array ( 
       		'href'      => $this->html->getSecureURL('sale/customer'),
       		'text'      => $this->language->get('heading_title'),
      		'separator' => ' :: '
   		 ));

		$this->data['cancel'] = $this->html->getSecureURL('sale/customer');

		if (isset($this->request->get['customer_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
      		$customer_info = $this->model_sale_customer->getCustomer($this->request->get['customer_id']);
    	}

		foreach ( $this->fields as $f ) {
			if (isset ( $this->request->post [$f] )) {
				$this->data[$f] = $this->request->post [$f];
			} elseif (isset($customer_info)) {
				$this->data[$f] = $customer_info[$f];
			} else {
				$this->data[$f] = '';
			}
		}

		if (!isset($this->request->get['customer_id'])) {
			$this->data['action'] = $this->html->getSecureURL('sale/customer/insert');
			$this->data['heading_title'] = $this->language->get('text_insert') . $this->language->get('text_customer');
			$this->data['update'] = '';
			$form = new AForm('ST');
		} else {
			$this->data['action'] = $this->html->getSecureURL('sale/customer/detail', '&customer_id=' . $this->request->get['customer_id'] );
			$this->data['heading_title'] = $this->language->get('text_edit') . $this->language->get('text_customer') . ' - ' . $this->data['firstname'] . ' ' . $this->data['lastname'] ;
			$this->data['update'] = $this->html->getSecureURL('listing_grid/customer/update_field','&id='.$this->request->get['customer_id']);
			$form = new AForm('HS');
		}

		$this->document->addBreadcrumb( array (
       		'href'      => $this->data['action'],
       		'text'      => $this->data['heading_title'],
      		'separator' => ' :: '
   		 ));

		$form->setForm(array(
		    'form_name' => 'cgFrm',
			'update' => $this->data['update'],
	    ));

        $this->data['form']['id'] = 'cgFrm';
        $this->data['form']['form_open'] = $form->getFieldHtml(array(
		    'type' => 'form',
		    'name' => 'cgFrm',
		    'attr' => 'confirm-exit="true"',
		    'action' => $this->data['action'],
	    ));
        $this->data['form']['submit'] = $form->getFieldHtml(array(
		    'type' => 'button',
		    'name' => 'submit',
		    'text' => $this->language->get('button_save'),
		    'style' => 'button1',
	    ));
	    $this->data['form']['approve'] = $form->getFieldHtml(array(
		    'type' => 'button',
		    'name' => 'submit',
		    'text' => "Approve",
		    'style' => 'button1',
	    ));
	    $this->data['form']['reject'] = $form->getFieldHtml(array(
		    'type' => 'button',
		    'name' => 'reject',
		    'text' => 'Reject',//$this->language->get('button_cancel'),
		    'style' => 'button1',
	    ));
		$this->data['form']['cancel'] = $form->getFieldHtml(array(
		    'type' => 'button',
		    'name' => 'cancel',
		    'text' => $this->language->get('button_cancel'),
		    'style' => 'button2',
	    ));

    $this->data['cancel'] = $this->html->getSecureURL('index/home');
    $this->data['reject'] = $this->html->getSecureURL('sale/customer/rejectCustomer&customer_id='.$this->request->get['customer_id']);
    $this->view->assign('retrieve_url',$this->html->getSecureURL('sale/customer/retrieveAccounts'));
    
		$required_input = array('firstname','lastname','email','telephone','id_number','id_type','cif');
		$bank_fields = array('bank_account_name','bank_name','bank_account_no');
		$remarks = array('remark');

		foreach ( $required_input as $f ) {
			$this->data['form']['fields'][$f] = $form->getFieldHtml(array(
				'type' => 'input',
				'name' => $f,
				'value' => $this->data[$f],
				// 'placeholder' => "$f",
				'attr' => 'readonly',
			));

			//field untuk komparasi
			$this->data['form']['compare_fields'][$f] = $form->getFieldHtml(array(
				'type' => 'input',
				'name' => "c".$f,
				'value' => $this->data[$f],
				// 'placeholder' => "$f",
				'attr' => 'readonly',
			));
		}

		foreach ( $bank_fields as $f ) {
			$this->data['form']['bank'][$f] = $form->getFieldHtml(array(
				'type' => 'input',
				'name' => $f,
				'value' => $this->data[$f],
				// 'placeholder' => "$f",
				'attr' => 'readonly',
			));

			//field untuk komparasi
			$this->data['form']['compare_bank'][$f] = $form->getFieldHtml(array(
				'type' => 'input',
				'name' => "c".$f,
				'value' => $this->data[$f],
				// 'placeholder' => "$f",
				'attr' => 'readonly',
			));
		}

		foreach ( $remarks as $f ) {
			$this->data['form']['remark'][$f] = $form->getFieldHtml(array(
				'type' => 'textarea',
				'name' => $f,
				'value' => $this->data[$f],
				// 'placeholder' => "$f",
			));
		}

		//data untuk popup
		$this->data['popup_data'] = $this->model_sale_customer->getCustomers();

		$this->view->batchAssign( $this->data );
		$this->processTemplate('pages/sale/active_account_audit_detail.tpl');
	}
}