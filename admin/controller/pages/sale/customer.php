<?php
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' ) || !IS_ADMIN) {
	header ( 'Location: static_pages/' );
}
class ControllerPagesSaleCustomer extends AController {
	public $data = array();
	public $data2 = array();
	private $error = array();
	
	private $profile_fields = array();
	
	private $fields = array('loginname','firstname','lastname', 'email', 'telephone', 'fax', 'newsletter', 
		'customer_group_id', 'status', 'approved', 'password','cart','address_id','ip','date_added','cif',
		'holder_id','bank_name','bank_account_name','bank_account_no','id_type','id_number','sync_datetime',
		'store_id','customer_id','first_name','last_name','domicile_address','domicile_city','domicile_postal_code',
		'domicile_country','tax_id_no','tax_id_reg_date','phone_no','mobile_phone_no','fax_no','place_of_birth',
		'date_of_birth','gender','marital_status','religion','id_expiry_date','nationality','residency_country',
		'residency_status','residency_status_txt','occupation','company_name','company_type','education',
		'spouse_name','spouse_occupation','mother_name','father_name','inheritor','inheritor_relationship',
		'gross_income','source_of_income','source_of_income_txt','additional_income','add_source_of_income',
		'source_of_fund','source_of_fund_txt','inv_objective','inv_objective_txt','bank_name','bank_address',
		'bank_account_name','bank_account_no','risk_profile','risk_profile_last_update','sync_datetime',
		'correspondence_address','correspondence_city','correspondence_postal_code','correspondence_country');

  
  public function main() {
    //init controller data

    $this->extensions->hk_InitData($this,__FUNCTION__);

		$this->document->setTitle( $this->language->get('heading_title') );
		$this->loadLanguage('sale/customer');
		$this->document->initBreadcrumb( array (
       		'href'      => $this->html->getSecureURL('index/home'),
       		'text'      => $this->language->get('text_home'),
      		'separator' => FALSE
   		 ));
   		$this->document->addBreadcrumb( array (
       		'href'      => $this->html->getSecureURL('sale/customer'),
       		'text'      => $this->language->get('heading_title'),
      		'separator' => ' :: '
   		 ));

		if (isset($this->session->data['error'])) {
			$this->data['error_warning'] = $this->session->data['error'];

			unset($this->session->data['error']);
		} elseif (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$grid_settings = array(
			//id of grid
            'table_id' => 'customer_grid',
            // url to load data from
			'url' => $this->html->getSecureURL('listing_grid/customer'),
			'editurl' => $this->html->getSecureURL('listing_grid/customer/update'),
			'update_field' => $this->html->getSecureURL ( 'listing_grid/customer/update_field' ),
            'sortname' => 'name',
            'sortorder' => 'asc',
			'multiselect' => 'true',
      // actions
      'actions' => array(
          'clone' => array(
              'text' => $this->language->get('button_actas'),
	    'href' => $this->html->getSecureURL('sale/customer/actonbehalf', '&customer_id=%ID%'),
	    'target' => 'new',
          ),
          'approve' => array(
              'text' => $this->language->get('button_approve'),
	    'href' => $this->html->getSecureURL('sale/customer/approve', '&customer_id=%ID%')
          ),
        'edit' => array(
              'text' => $this->language->get('text_edit'),
	    'href' => $this->html->getSecureURL('sale/customer/update', '&customer_id=%ID%')
          ),
        'save' => array(
              'text' => $this->language->get('button_save'),
          ),
          'delete' => array(
              'text' => $this->language->get('button_delete'),
          ),
      ),
		);

		$grid_settings['colNames'] = array(
			$this->language->get('column_name'),
			$this->language->get('column_email'),
			$this->language->get('column_group'),
			$this->language->get('column_status'),
			$this->language->get('column_approved'),
			$this->language->get('text_order'),
		);
		$grid_settings['colModel'] = array(
			array( 'name' => 'name',
					'index' => 'name',
					'width' => 160,
					'align' => 'center', ),
			array( 'name' => 'email',
					'index' => 'email',
					'width' => 140,
					'align' => 'center', ),
			array( 'name' => 'customer_group',
					'index' => 'customer_group',
					'width' => 80,
					'align' => 'center',
					'search' => false ),
			array( 'name' => 'status',
					'index' => 'status',
					'width' => 120,
					'align' => 'center',
					'search' => false ),
			array( 'name' => 'approved',
					'index' => 'approved',
					'width' => 110,
					'align' => 'center',
					'search' => false ),
			array( 'name' => 'orders',
					'index' => 'orders_count',
					'width' => 70,
					'align' => 'center',
					'search' => false ),
		);

		$this->loadModel('sale/customer_group');
		$results = $this->model_sale_customer_group->getCustomerGroups();
		$groups = array( '' => $this->language->get('text_select_group'), );
		foreach ( $results as $item) {
			$groups[ $item['customer_group_id'] ] = $item['name'];
		}

		$statuses = array(
			'' => $this->language->get('text_select_status'),
			1 => $this->language->get('text_enabled'),
			0 => $this->language->get('text_disabled'),
		);

		$approved = array(
			'' => $this->language->get('text_select_approved'),
			1 => $this->language->get('text_yes'),
			0 => $this->language->get('text_no'),
		);

		$form = new AForm();
    $form->setForm(array(
	    'form_name' => 'customer_grid_search',
    ));

	  $grid_search_form = array();
    $grid_search_form['id'] = 'customer_grid_search';
    $grid_search_form['form_open'] = $form->getFieldHtml(array(
	    'type' => 'form',
	    'name' => 'customer_grid_search',
	    'action' => '',
	  ));
    $grid_search_form['submit'] = $form->getFieldHtml(array(
	    'type' => 'button',
	    'name' => 'submit',
	    'text' => $this->language->get('button_go'),
	    'style' => 'button1',
	  ));
		$grid_search_form['reset'] = $form->getFieldHtml(array(
		    'type' => 'button',
		    'name' => 'reset',
		    'text' => $this->language->get('button_reset'),
		    'style' => 'button2',
	    ));

		$grid_search_form['fields']['customer_group'] = $form->getFieldHtml(array(
		    'type' => 'selectbox',
		    'name' => 'customer_group',
            'options' => $groups,
	    ));
        $grid_search_form['fields']['status'] = $form->getFieldHtml(array(
		    'type' => 'selectbox',
		    'name' => 'status',
            'options' => $statuses,
	    ));
		$grid_search_form['fields']['approved'] = $form->getFieldHtml(array(
		    'type' => 'selectbox',
		    'name' => 'approved',
            'options' => $approved,
	    ));

		$grid_settings['search_form'] = true;


    $grid = $this->dispatch('common/listing_grid', array( $grid_settings ) );
		$this->view->assign('listing_grid', $grid->dispatchGetOutput());
		$this->view->assign ( 'search_form', $grid_search_form );

		$this->document->setTitle( $this->language->get('heading_title') );
		$this->view->assign( 'insert', $this->html->getSecureURL('sale/customer/insert') );
		$this->view->assign('help_url', $this->gen_help_url('customer_listing') );

		$this->processTemplate('pages/sale/customer_list.tpl' );

    //update controller data
    $this->extensions->hk_UpdateData($this,__FUNCTION__);
	}

	public function retrieveAccounts(){
			$this->loadModel('tool/lookup');

			// print_r($_GET);exit;
	    $wsdlurl = 'http://mmimw.mercatocapitale.com/searchprofiles?wsdl'; 

	    $client = new nusoap_client($wsdlurl, true);
	    /*$client->timeout = 0;
			$client->response_timeout = 30;*/

	    $error = $client->getError();


	    if ($error) {
	      die("client construction error: {$error}\n");
	    }

	    $param = array(
	      'fullName' => $_GET['fullname'],
	      'email' => $_GET['email'],
	      'cif' => $_GET['cif'],
	    );

	    $result = $client->call("searchProfile", $param);
	    $error = $client->getError();

	    // if ($error) {
	    //   var_dump($client->response);
	    //   var_dump($client->getDebug());
	    //   die();
	    // }

	    if(!$error) { 
		    $customers = $result['searchResult']['customerProfiles'];
		    // if(count($customers)==1 && !is_null($customers)) $customers = array($customers); 
		    if($customers['email']){
		    	$idType = "'".$this->model_tool_lookup->getLookupDescription($customers['idType'])."'";
		    	$bankName = "'".$this->model_tool_lookup->getLookupDescription($customers['bankName'])."'";
		    	$customers['idType'] = $idType;
		    	$customers['bankName'] = $bankName;
		    	$customers = array($customers);
		    }

		    $counter = 0;
		    if (count($customers)>1){
		    	foreach($customers as $cs){
		    		foreach($cs as $key => $value){
		    			if($key=='idType') {
		    				$idType = "'".$this->model_tool_lookup->getLookupDescription($value)."'";
					    	$customers[$counter]['idType'] = $idType;
		    			}
		    			if($key=='bankName'){
					    	$bankName = "'".$this->model_tool_lookup->getLookupDescription($value)."'";
					    	$customers[$counter]['bankName'] = $bankName;
		    			}
		    		}
		    		$counter++;
		    	}
		    }
		    echo json_encode($customers);
	    }else{
	   	 echo 'error';
	    }

	    exit;
	}

  
  public function insert() {

    //init controller data
    $this->extensions->hk_InitData($this,__FUNCTION__);

  	$this->document->setTitle( $this->language->get('heading_title') );

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->_validateForm()) {
      $customer_id = $this->model_sale_customer->addCustomer($this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$this->redirect( $this->html->getSecureURL('sale/customer/update', '&customer_id=' . $customer_id ) );
		}
    	$this->_getForm();

        //update controller data
        $this->extensions->hk_UpdateData($this,__FUNCTION__);
  	} 
    
  	public function update() {
      //init controller data
      $this->extensions->hk_InitData($this,__FUNCTION__);
      $this->loadModel('sale/customer_profile');

    	$this->document->setTitle( $this->language->get('heading_title') );
		
			$this->view->assign('success', $this->session->data['success']);
			if (isset($this->session->data['success'])) {
				unset($this->session->data['success']);
			}
    	if (($this->request->server['REQUEST_METHOD'] == 'POST')) { // && $this->_validateEditForm($customer_id)
			    // $customer_id = $this->model_sale_customer_profile->getCustomer($this->request->post['holder_id']);
					$customer = $this->model_sale_customer->getCustomerByHolderId($this->request->post['holder_id']);
					//$this->model_sale_customer_profile->getCustomerEdit($this->request->get['customer_id']);
	    		/*$this->request->post["bank_account_name"] = $this->decrypt($this->privatekey,$request_data["bank_account_name"]);
	      	$this->request->post["bank_account_no"] = $this->decrypt($this->privatekey,$request_data["bank_account_no"]);*/
			    
			    //jika approved
			  	// $this->showDebug($this->request->post['approved']);
			    if( isset($this->request->post['approved']) && $this->request->post['approved']==0){
			    	
			    	$this->request->post['tax_id_reg_date'] = date("Y-m-d", strtotime($this->request->post['tax_id_reg_date']));
			    	$this->request->post["date_of_birth"] = date("Y-m-d", strtotime($this->request->post['date_of_birth']));
			    	$this->request->post["id_expiry_date"] = date("Y-m-d", strtotime($this->request->post['id_expiry_date']));
			    	// var_dump($this->request->post['holder_id']);exit();
			    	$holder_id = $this->request->post['holder_id'];

			    	// copy data customer profile temp to customer profile
			    	$this->model_sale_customer_profile->updateCustomerProfile($this->request->post);
			    	
			    	// update customer table
			    	$this->model_sale_customer->updateCustomerAudit($this->request->post);

			    	

			    	// update approved status to 2 on customer profile
			   		$this->model_sale_customer_profile->approve($holder_id);

			   		// hapus data di customer profile temp
			   		$this->model_sale_customer_profile->delCustomer($holder_id,'customer_profiles_temp');

			   		// menghapus remark jika sebelumnya ada text di customer tabel
			   		$this->model_sale_customer->fillRemark($customer['customer_id'],null);
			  
			  	}else{

			  		//jika reject
			  		// NOTE : buat fungsi untuk delete dan clone customer profile_temp saat reject
			  		// delete profile di customer_profile_temp
			  		$this->model_sale_customer_profile->updateFieldCustomer('approved',NULL,$this->request->post['holder_id']);
			  		$this->model_sale_customer_profile->delCustomer($this->request->post['holder_id'],"customer_profiles_temp");
			  		
			  		$customer_post = $this->model_sale_customer->getCustomer($customer['customer_id']);
		  			// $this->showDebug($customer_post);
		  			// send email rejected
			  		if ($customer_post) {

						$this->loadLanguage('mail/customer');
						$this->loadModel('setting/store');

						$store_info = $this->model_setting_store->getStore($customer_post['store_id']);

						if ($store_info) {
							$store_name = $store_info['store_name'];
							$store_url = $store_info['config_url'] . 'index.php?rt=account/login';
						} else {
							$store_name = $this->config->get('store_name');
							$store_url = $this->config->get('config_url') . 'index.php?rt=account/login';
						}

						//get remark by email
						$customer = $this->model_sale_customer->getCustomerByEmail($customer_post['email']);
						// $this->showDebug($customer);

						$message  = 'Hi '.$customer_post['firstname'].","."\n\n";
						$message .= sprintf($this->language->get('text_profile_reject_edit'), $store_name) . $this->request->post['remark'].".\n\n";
						$message .= sprintf($this->language->get('text_reject_try'))."\n\n";
						// $message .= $store_url . "\n\n";
						// $message .= $this->language->get('text_services_reject') . "\n\n";
						$message .= $this->language->get('text_thanks') . "\n";
						$message .= $store_name;
						

						$mail = new AMail( $this->config );
						$mail->setTo($customer_post['email']);
						$mail->setFrom($this->config->get('store_main_email'));
						$mail->setSender($store_name);
						$mail->setSubject($this->config->get('store_name').' - '.$this->language->get('text_profile_subject_reject'));
						$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
						
						$mail->send();
						//var_dump($customer['customer_id'],$this->request->post['remark']);exit();
						//$this->model_sale_customer->fillRemark($customer['customer_id'],$this->request->post['remark']);
						$this->model_sale_customer->fillRemark($customer['customer_id'],null);
					}

					// delete data di  customer profile temp 
			  		$this->model_sale_customer_profile->delCustomer($holder_id,'customer_profiles_temp');	
			  	}

				$this->session->data['success'] = $this->language->get('text_success');
				$this->redirect($this->html->getSecureURL('index/home').'#tabs-3');
			}

    	  $this->_getForm();
    	  
        //update controller data
        $this->extensions->hk_UpdateData($this,__FUNCTION__);
  	}

  public function subscribe(){
  		$this->extensions->hk_InitData($this,__FUNCTION__);
  		//grid for searching

  		$form = new AForm();
	    $form->setForm(array(
		    'form_name' => 'customer_grid_search',
	    ));

	    $grid_settings = array(
			//id of grid
            'table_id' => 'customer_transactio_grid',
            // url to load data from
			'url' => $this->html->getSecureURL('listing_grid/customer_transaction'),
            'sortname' => 'order_id',
            'sortorder' => 'asc',
			'multiselect' => 'false',
		);

		$grid_settings['colNames'] = array(
			/*$this->language->get('column_name'),
			$this->language->get('column_email'),
			$this->language->get('column_group'),
			$this->language->get('column_status'),
			$this->language->get('column_approved'),
			$this->language->get('text_order'),*/
			'Order ID',
			'Transaction ID',
			'Customer Name',
			'Product Name',
			'Amount',
			'Free Amount',
			'Total',
			'Payment Method'
		);
		$grid_settings['colModel'] = array(
			array( 'name' => 'order_id',
					'index' => 'order_id',
					'width' => 160,
					'align' => 'center', 
					'search' => true),
			array( 'name' => 'transaction_id',
					'index' => 'transaction_id',
					'width' => 160,
					'align' => 'center', 
					'search' => false),
			array( 'name' => 'customer_name',
					'index' => 'customer_name',
					'width' => 160,
					'align' => 'center', 
					'search' => true),
			array( 'name' => 'product_name',
					'index' => 'product_name',
					'width' => 160,
					'align' => 'center', 
					'search' => false),
			array( 'name' => 'amount',
					'index' => 'amount',
					'width' => 160,
					'align' => 'center', 
					'search' => false),
			array( 'name' => 'free_amount',
					'index' => 'free_amount',
					'width' => 160,
					'align' => 'center', 
					'search' => false),
			array( 'name' => 'total',
					'index' => 'total',
					'width' => 160,
					'align' => 'center', 
					'search' => false),
			array( 'name' => 'payment_method',
					'index' => 'payment_method',
					'width' => 160,
					'align' => 'center', 
					'search' => false),
			
		);

		//query
		$this->loadModel('sale/customer_group');

		$grid_search_form = array();
        $grid_search_form['id'] = 'customer_grid_search';
        $grid_search_form['form_open'] = $form->getFieldHtml(array(
		    'type' => 'form',
		    'name' => 'customer_grid_search',
		    'action' => '',
	    ));
        $grid_search_form['submit'] = $form->getFieldHtml(array(
		    'type' => 'button',
		    'name' => 'submit',
		    'text' => $this->language->get('button_go'),
		    'style' => 'button1',
	    ));
		$grid_search_form['reset'] = $form->getFieldHtml(array(
		    'type' => 'button',
		    'name' => 'reset',
		    'text' => $this->language->get('button_reset'),
		    'style' => 'button2',
	    ));

		$grid_settings['search_form'] = false;

		$grid = $this->dispatch('common/listing_grid', array( $grid_settings ) );
		$this->view->assign('listing_grid', $grid->dispatchGetOutput());
		$this->view->assign ( 'search_form', $grid_search_form );

		$this->view->assign("heading_title","Registration Date");
		$this->processTemplate('pages/sale/daily_subscribe.tpl' );

		$this->extensions->hk_UpdateData($this,__FUNCTION__);
  }

  public function detail(){
  	//init controller data
    $this->extensions->hk_InitData($this,__FUNCTION__);
    // $this->loadLanguage('sale/customer');
    $this->document->setTitle( $this->language->get('heading_title') );
    $this->loadModel('tool/lookup');

		$this->view->assign('success', $this->session->data['success']);
		if (isset($this->session->data['success'])) {
			unset($this->session->data['success']);
		}

		$customer_id = $this->request->get['customer_id'];

    	if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->_validateApproveForm($customer_id)) {
				//$this->_sendMail($customer_id);exit;
    		//0 = waiting |1 = approve | 2 = for sync | 3 = Edit Approve | 4 = Edit Reject |
    		//insert status = 1 to array
    		$this->request->post['status'] = 1;
    		$this->request->post['customer_id'] = $customer_id;
    		if($this->request->post['approved']==2){
					//$this->request->post['approved'] = 2; //sementara di set ke 1, untuk keperluan testing

    			//save juga ke table activation audit
    			// $this->showDebug($this->request->post);
    			$this->model_sale_customer->addCustomerAudit($this->request->post);

    			//update holder id
    			$this->model_sale_customer->editCustomerField($customer_id,'holder_id',$this->request->post['holder_id']);
    			$this->model_sale_customer->editCustomerField($customer_id,'approved',$this->request->post['approved']);

					if( (int)$this->request->post['approved'] ){
			  		$customer_info = $this->model_sale_customer->getCustomer($customer_id);
				  	//send mail after status change to 1
				  	// $this->_sendMail($customer_id);
			  	}

					//$this->model_sale_customer->editCustomer($this->request->get['customer_id'], $this->request->post);
					//just update approve status => update approve ReMoved after change password 
					// $this->model_sale_customer->editCustomerField($customer_id, 'approved', $this->request->post['approved']);
					
					//save data into table activation history
					//cek data by CIF
					/*if( $this->request->post['approved'] == 1){
						$this->request->post['cid_type'] = $this->model_tool_lookup->getLookupIdByName($this->request->post['cid_type'])['lookup_id'];
						$this->request->post['cbank_name'] = $this->model_tool_lookup->getLookupIdByName($this->request->post['cbank_name'])['lookup_id'];
						// $this->showDebug($this->request->post);
						$cif = $this->request->post['cif'];
						
						if($this->model_sale_customer->getCustomerHistory($cif)){
							$this->model_sale_customer->editCustomerHistory($cif,$this->request->post);
						}else{
							$this->model_sale_customer->addCustomerHistory($this->request->post);
						}
					}*/

					$this->session->data['success'] = $this->language->get('text_success');
					$this->redirect( $this->html->getSecureURL('index/home') );
    		}else{
    			//rejected
	    		// $this->model_sale_customer->editCustomerField($customer_id, 'approved', 3);
					$this->model_sale_customer->editCustomerField($customer_id, 'remark', $this->request->post['remark']);
					$this->_sendRejectMail($customer_id);
    			$this->model_sale_customer->deleteCustomer($customer_id);
					
					$this->redirect($this->html->getSecureURL('index/home'));
    		}
		}

  	$this->_getApproveForm();

    //update controller data
    $this->extensions->hk_UpdateData($this,__FUNCTION__);
	}

	private function _getApproveForm(){
		$this->data['token'] = $this->session->data['token'];
		$this->data['error'] = $this->error;
		$this->loadCryptLib();

		$this->document->initBreadcrumb( array (
       		'href'      => $this->html->getSecureURL('index/home'),
       		'text'      => $this->language->get('text_home'),
      		'separator' => FALSE
   		 ));
   		$this->document->addBreadcrumb( array ( 
       		'href'      => $this->html->getSecureURL('sale/customer'),
       		'text'      => $this->language->get('heading_title'),
      		'separator' => ' :: '
   		 ));

		$this->data['cancel'] = $this->html->getSecureURL('sale/customer');

		if (isset($this->request->get['customer_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
      		$customer_info = $this->model_sale_customer->getCustomer($this->request->get['customer_id']);
    	}

		foreach ( $this->fields as $f ) {
			if (isset ( $this->request->post [$f] )) {
				$this->data[$f] = $this->request->post [$f];
			} elseif (isset($customer_info)) {
				$this->data[$f] = $customer_info[$f];
			} else {
				$this->data[$f] = '';
			}
		}

		if (!isset($this->request->get['customer_id'])) {
			$this->data['action'] = $this->html->getSecureURL('sale/customer/insert');
			$this->data['heading_title'] = $this->language->get('text_insert') . $this->language->get('text_customer');
			$this->data['update'] = '';
			$form = new AForm('ST');
		} else {
			$this->data['action'] = $this->html->getSecureURL('sale/customer/detail', '&customer_id=' . $this->request->get['customer_id'] );
			$this->data['heading_title'] = $this->language->get('text_view') ." ". $this->language->get('text_customer') . ' - ' . $this->data['firstname'] . ' ' . $this->data['lastname'] ;
			$this->data['update'] = $this->html->getSecureURL('listing_grid/customer/update_field','&id='.$this->request->get['customer_id']);
			$form = new AForm('ST');
		}

		$this->document->addBreadcrumb( array (
     		'href'      => $this->data['action'],
     		'text'      => $this->data['heading_title'],
    		'separator' => ' :: '
 		 ));

		$form->setForm(array(
		    'form_name' => 'cgFrm',
			'update' => $this->data['update'],
	    ));

        $this->data['form']['id'] = 'cgFrm';
        $this->data['form']['form_open'] = $form->getFieldHtml(array(
		    'type' => 'form',
		    'name' => 'cgFrm',
		    'attr' => 'confirm-exit="true"',
		    'action' => $this->data['action'],
	    ));
        $this->data['form']['submit'] = $form->getFieldHtml(array(
		    'type' => 'button',
		    'name' => 'submit',
		    'text' => $this->language->get('button_save'),
		    'style' => 'button1',
	    ));
	    $this->data['form']['approve'] = $form->getFieldHtml(array(
		    'type' => 'button',
		    'name' => 'submit',
		    'text' => "Approve",
		    'style' => 'button1',
	    ));
	    $this->data['form']['reject'] = $form->getFieldHtml(array(
		    'type' => 'button',
		    'name' => 'reject',
		    'text' => 'Reject',//$this->language->get('button_cancel'),
		    'style' => 'button1',
	    ));
		$this->data['form']['cancel'] = $form->getFieldHtml(array(
		    'type' => 'button',
		    'name' => 'cancel',
		    'text' => $this->language->get('button_cancel'),
		    'style' => 'button2',
	    ));

		$this->view->assign('error_holder_id',$this->language->get('error_holder_id'));
		$this->view->assign('error_checklist',$this->language->get('error_checklist'));

    $this->data['cancel'] = $this->html->getSecureURL('index/home');
    $this->data['reject'] = $this->html->getSecureURL('sale/customer/rejectCustomer&customer_id='.$this->request->get['customer_id']);
    $this->view->assign('retrieve_url',$this->html->getSecureURL('sale/customer/retrieveAccounts'));
    $this->view->assign('check_holder_id',$this->html->getSecureURL('sale/customer/checkHolderID'));
    
		$required_input = array('firstname','lastname','email','telephone','id_number','id_type','cif');
		$bank_fields = array('bank_account_name','bank_name','bank_account_no');
		$remarks = array('remark');
		$this->loadModel('tool/lookup');
		foreach ( $required_input as $f ) {
			$this->data['form']['fields'][$f] = $form->getFieldHtml(array(
				'type' => 'input',
				'name' => $f,
				'value' => ($f=='id_type')?$this->model_tool_lookup->getLookupDescription($this->data[$f]):$this->data[$f],
				// 'placeholder' => "$f",
				'attr' => 'readonly',
			));

			//field untuk komparasi
			$this->data['form']['compare_fields'][$f] = $form->getFieldHtml(array(
				'type' => 'input',
				'name' => "c".$f,
				'value' => ($f=='id_type')?$this->model_tool_lookup->getLookupDescription($this->data[$f]):$this->data[$f],
				// 'placeholder' => "$f",
				'attr' => 'readonly',
			));
		}

		foreach ( $bank_fields as $f ) {
			$this->data['form']['bank'][$f] = $form->getFieldHtml(array(
				'type' => 'input',
				'name' => $f,
				'value' => ($f=='bank_name')?$this->model_tool_lookup->getLookupDescription($this->data[$f]):$this->data[$f],
				// 'placeholder' => "$f",
				'attr' => 'readonly',
			));

			//field untuk komparasi
			$this->data['form']['compare_bank'][$f] = $form->getFieldHtml(array(
				'type' => 'input',
				'name' => "c".$f,
				'value' => ($f=='bank_name')?$this->model_tool_lookup->getLookupDescription($this->data[$f]):$this->data[$f],
				// 'placeholder' => "$f",
				'attr' => 'readonly',
			));
		}

		foreach ( $remarks as $f ) {
			$this->data['form']['remark'][$f] = $form->getFieldHtml(array(
				'type' => 'textarea',
				'name' => $f,
				'value' => $this->data[$f],
				// 'placeholder' => "$f",
			));
		}

		//data untuk popup
		$this->data['popup_data'] = $this->model_sale_customer->getCustomers();

		$this->view->batchAssign( $this->data );
		$this->processTemplate('pages/sale/customer_approve_form.tpl' );
	}

	public function checkHolderID(){
		$this->loadModel('sale/customer');
		$holder_id = $this->request->get['holder_id'];
		//select ke table customer jika holder_id sudah ada
		$total_holder_id = $this->model_sale_customer->checkHolderID($holder_id);

		if($total_holder_id==0){
			echo $total_holder_id;
		}else{
			echo $total_holder_id;
		}
		exit();
	}

	public function getConfirmData(){
		$this->data['confirm'] = $this->model_sale_customer->getCustomer($_GET['customer_id']);
		echo json_encode($this->data['confirm']);
		exit;
	}

  private function _getForm() {
  	$this->loadCryptLib();
  	$this->data['token'] = $this->session->data['token'];
		$this->data['error'] = $this->error;

		$this->document->initBreadcrumb( array (
       		'href'      => $this->html->getSecureURL('index/home'),
       		'text'      => $this->language->get('text_home'),
      		'separator' => FALSE
   		 ));
 		$this->document->addBreadcrumb( array ( 
     		'href'      => $this->html->getSecureURL('sale/customer'),
     		'text'      => $this->language->get('heading_title'),
    		'separator' => ' :: '
 		 ));
		$this->data['cancel'] = $this->html->getSecureURL('sale/customer');
  	if (isset($this->request->get['customer_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
    		// $customer_info = $this->model_sale_customer->getCustomer($this->request->get['customer_id']);
  		$customer_info = $this->model_sale_customer_profile->getCustomerEdit($this->request->get['customer_id']);
  	}

		foreach ( $this->fields as $f ) {
			if (isset ( $this->request->post [$f] )) {
				$this->data [$f] = $this->request->post [$f];
			} elseif (isset($customer_info)) {
				$this->data[$f] = $customer_info[$f];
			} else {
				$this->data[$f] = '';
			}
		}

		if(has_value($customer_info['orders_count']) && $this->request->get['customer_id']){
			$this->data['button_orders_count'] = $this->html->buildButton(
				array(
					'name' => 'view orders',
					'text' => $this->language->get('text_order').': '.$customer_info['orders_count'],
					'style' => 'button2',
					'href'=> $this->html->getSecureURL('sale/order','&customer_id='.$this->request->get['customer_id']),
					'title' => $this->language->get('text_view').' '.$this->language->get('tab_history')
				)
			);
		}

			
  	if (!isset($this->data['customer_group_id'])) {
    		$this->data['customer_group_id'] = $this->config->get('config_customer_group_id');
  	}
  	if (!isset($this->data['status'])) {
    		$this->data['status'] = 1;
  	}
		if (!isset($this->data['password']) && isset($this->request->post['password'])) {
			$this->data['password'] = $this->request->post['password'];
		} else {
			$this->data['password'] = '';
		}		

		$this->loadModel('localisation/country');
		$this->loadModel('localisation/zone');
		$this->loadModel('tool/lookup');

		$this->data['countries'] = $this->model_localisation_country->getCountries();
			
		if (isset($this->request->post['addresses'])) { 
      $this->data['addresses'] = $this->request->post['addresses'];
		} elseif (isset($this->request->get['customer_id'])) {
			$this->data['addresses'] = $this->model_sale_customer->getAddressesByCustomerId($this->request->get['customer_id']);
		} else {
			$this->data['addresses'] = array();
    	}

        $this->data['category_products'] = $this->html->getSecureURL('product/product/category');
        $this->data['common_zone'] = $this->html->getSecureURL('common/zone');

    // $this->showDebug($this->data,true);
		if (!isset($this->request->get['holder_id'])) {
			$this->data['action'] = $this->html->getSecureURL('sale/customer/update');
			$this->data['heading_title'] = $this->language->get('text_edit') . $this->language->get('text_customer') . ' - ' . $this->data['first_name'] . ' ' . $this->data['last_name'] ;
			$this->data['update'] = '';
			$form = new AForm('ST');
		} else {
			$this->data['action'] = $this->html->getSecureURL('sale/customer/update', '&customer_id=' . $this->request->get['customer_id'] );
			$this->data['heading_title'] = $this->language->get('text_edit') . $this->language->get('text_customer') . ' - ' . $this->data['first_name'] . ' ' . $this->data['last_name'] ;
			$this->data['update'] = $this->html->getSecureURL('listing_grid/customer/update_field','&id='.$this->request->get['customer_id']);
			$form = new AForm('HS');
		}

		$this->document->addBreadcrumb( array (
       		'href'      => $this->data['action'],
       		'text'      => $this->data['heading_title'],
      		'separator' => ' :: '
   		 ));

		$form->setForm(array(
		    'form_name' => 'cgFrm',
			'update' => $this->data['update'],
	    ));

        $this->data['form']['id'] = 'cgFrm';
        $this->data['form']['form_open'] = $form->getFieldHtml(array(
		    'type' => 'form',
		    'name' => 'cgFrm',
		    'attr' => 'confirm-exit="true"',
		    'action' => $this->data['action'],
	    ));
        $this->data['form']['submit'] = $form->getFieldHtml(array(
		    'type' => 'button',
		    'name' => 'submit',
		    'text' => $this->language->get('button_save'),
		    'style' => 'button1',
	    ));
	    $this->data['form']['approve'] = $form->getFieldHtml(array(
		    'type' => 'button',
		    'name' => 'reject',
		    'text' => "Approve",
		    'style' => 'button1',
	    ));
	    $this->data['form']['reject'] = $form->getFieldHtml(array(
		    'type' => 'button',
		    'name' => 'reject',
		    'text' => 'Reject',//$this->language->get('button_cancel'),
		    'style' => 'button1',
	    ));
		$this->data['form']['cancel'] = $form->getFieldHtml(array(
		    'type' => 'button',
		    'name' => 'cancel',
		    'text' => $this->language->get('button_cancel'),
		    'style' => 'button2',
	    ));

		// $required_input = array('loginname', 'firstname', 'lastname', 'email', 'telephone', 'fax', 'password');
		$textArea = array('domicile_address','correspondence_address','bank_address');

		$required_input = array('holder_id','cif','first_name','last_name');
		$required_input_bottom = array('tax_id_no','tax_id_reg_date','phone_no','mobile_phone_no','email','fax_no','place_of_birth','date_of_birth');
		
		$domicile_address = array('domicile_address');
		$domicile = array('domicile_city','domicile_postal_code','domicile_country');

		$correspondence_address = array('correspondence_address');
		$correspondence = array('correspondence_city','correspondence_postal_code','correspondence_country');
		
		$required_input_right = array('gender','marital_status','religion','id_number','id_type','id_expiry_date','nationality',
			'residency_country','residency_status','occupation','company_name','company_type','education','spouse_name',
			'spouse_occupation','mother_name','father_name','inheritor','inheritor_relationship');

		//bank information
		$bank = array('bank_account_name','bank_name');
		$this->data['form']['bank_address'] = $form->getFieldHtml(array(
			'type' => 'textarea',
			'name' => 'bank_address',
			'value' => $this->data['bank_address'],
			// 'value' => $f,
			'attr' => 'readonly',
		));

		$this->data['form']['bank_account_no'] = $form->getFieldHtml(array(
			'type' => 'hidden',
			'name' => 'bank_account_no',
			'value' => $this->data['bank_account_no'],
			// 'value' => $f,
			'attr' => 'readonly',
		));
		//financial data
		$finacial = array('gross_income','source_of_income','additional_income','add_source_of_income','source_of_fund','inv_objective');
		$inLookup = array('id_type','gender','company_type','nationality','education','residency_status','inheritor_relationship','occupation','marital_status','religion','gross_income','source_of_income','additional_income','source_of_fund','add_source_of_income','inv_objective');
		//var_dump($this->data['gross_income']);
		foreach ( $required_input as $f ) {
			$this->data['form']['fields'][$f] = $form->getFieldHtml(array(
				'type' => ( in_array($f, $textArea) ? 'textarea' : 'hidden' ),
				'name' => $f,
				'value' => $this->data[$f],
				// 'placeholder' => "$f",
				'attr' => 'readonly',
			));
		}

		foreach ( $required_input_bottom as $f ) {
			$this->data['form']['fields_bottom'][$f] = $form->getFieldHtml(array(
				'type' => ( in_array($f, $textArea) ? 'textarea' : 'hidden' ),
				'name' => $f,
				'value' => ($f == 'date_of_birth'||$f == 'tax_id_reg_date' ? date("d-M-y", strtotime($this->data[$f])) : $this->data[$f]),
				// 'placeholder' => "$f",
				'attr' => 'readonly',
			));
		}

		//approved field
		$this->data['form']['domicile_address'] = $form->getFieldHtml(array(
			'type' => 'textarea',
			'name' => 'domicile_address',
			'value' => $this->data['domicile_address'],
			'attr' => 'readonly',
		));

		// foreach ( $domicile as $f ) {
		// 	$this->data['form']['domicile'][$f] = $form->getFieldHtml(array(
		// 		'type' => 'input',
		// 		'name' => $f,
		// 		'value' => $this->data[$f],
		// 		// 'placeholder' => "$f",
		// 		// 'required' => ( in_array($f, array('password', 'fax')) ? false: true), //exception
		// 		'attr' => 'readonly',
		// 	));
		// }


		$this->data['form']['domicile']['domicile_city'] = $form->getFieldHtml(array(
			'type' => 'hidden',
			'name' => 'domicile_city',
			'value' => $this->data['domicile_city'],
			'attr' => 'readonly',
		));
		$this->data['domicile_city'] = $this->model_localisation_zone->locationCity($this->data['domicile_country'],$this->data['domicile_city']);
		
		$this->data['form']['domicile']['domicile_postal_code'] = $form->getFieldHtml(array(
			'type' => 'hidden',
			'name' => 'domicile_postal_code',
			'value' => $this->data['domicile_postal_code'],
			'attr' => 'readonly',
		));

		$this->data['form']['domicile']['domicile_country'] = $form->getFieldHtml(array(
			'type' => 'hidden',
			'name' => 'domicile_country',
			'value' => $this->data['domicile_country'],
			'attr' => 'readonly',
		));
		$this->data['domicile_country'] = $this->model_localisation_country->getCountryByCode($this->data['domicile_country']);
		

		//correspondence address
		$this->data['form']['correspondence_address'] = $form->getFieldHtml(array(
			'type' => 'textarea',
			'name' => 'correspondence_address',
			'value' => $this->data['correspondence_address'],
			'attr' => 'readonly',
		));


		$this->data['form']['correspondence']['correspondence_city'] = $form->getFieldHtml(array(
			'type' => 'hidden',
			'name' => 'correspondence_city',
			'value' => $this->data['correspondence_city'],
			'attr' => 'readonly',
		));
		$this->data['correspondence_city'] = $this->model_localisation_zone->locationCity($this->data['correspondence_country'],$this->data['correspondence_city']);

		$this->data['form']['correspondence']['correspondence_postal_code'] = $form->getFieldHtml(array(
			'type' => 'hidden',
			'name' => 'correspondence_postal_code',
			'value' => $this->data['correspondence_postal_code'],
			'attr' => 'readonly',
		));
		
		$this->data['form']['correspondence']['correspondence_country'] = $form->getFieldHtml(array(
			'type' => 'hidden',
			'name' => 'correspondence_country',
			'value' => $this->data['correspondence_country'],
			'attr' => 'readonly',
		));
		$this->data['correspondence_country'] = $this->model_localisation_country->getCountryByCode($this->data['correspondence_country']);
		/*foreach ( $correspondence as $f ) {
			$this->data['form']['correspondence'][$f] = $form->getFieldHtml(array(
				'type' => 'input',
				'name' => $f,
				'value' => $this->data[$f],
				// 'placeholder' => "$f",
				// 'required' => ( in_array($f, array('password', 'fax')) ? false: true), //exception
				'attr' => 'readonly',
			));
		}*/

		foreach ( $required_input_right as $f ) {
			if (in_array($f, $inLookup)) {
				$value_input = $this->model_tool_lookup->getLookupDescription($this->data[$f]);
			} else {
				if ($f=='id_expiry_date') {
					$value_input = date("d-M-y", strtotime($this->data[$f]));
				} else {
					$value_input = $this->data[$f];
				}
				
			}
			
			$this->data['form']['fields2'][$f] = $form->getFieldHtml(array(
				'type' => ($f == 'password' ? 'passwordset' : 'hidden' ),
				'name' => $f,
				'value' => $this->data[$f],
				// 'value' => $f,
				// 'placeholder' => "$f",
				// 'required' => ( in_array($f, array('password', 'fax')) ? false: true), //exception
				'attr' => 'readonly',
			));
			$this->data[$f] = $value_input;
		}

		$this->data['form']['fields2']['residency_country'] = $form->getFieldHtml(array(
			'type' => 'hidden',
			'name' => 'residency_country',
			'value' => $this->data['residency_country'],
			'attr' => 'readonly',
		));
		$this->data['residency_country'] = $this->model_localisation_country->getCountryByCode($this->data['residency_country']);
		foreach ( $bank as $f ) {
			$this->data['form']['bank'][$f] = $form->getFieldHtml(array(
				'type' => ( in_array($f, $textArea) ? 'textarea' : 'hidden' ),
				'name' => $f,
				'value' => $this->data[$f],
				'value' => $this->data[$f],
				// 'value' => $f,
				// 'placeholder' => "$f",
				// 'required' => ( in_array($f, array('password', 'fax')) ? false: true), //exception
				'attr' => 'readonly',
			));
			$this->data[$f] = ($f=='bank_name')?$this->model_tool_lookup->getLookupDescription($this->data[$f]):$this->data[$f];
		}

		foreach ( $finacial as $f ) {
			
			$this->data['form']['financial'][$f] = $form->getFieldHtml(array(
				'type' => ($f == 'password' ? 'passwordset' : 'hidden' ),
				'name' => $f,
				'value' => $this->data[$f],
				// 'value' => $this->data[$f],
				// 'placeholder' => "$f",
				// 'required' => ( in_array($f, array('password', 'fax')) ? false: true), //exception
				'attr' => 'readonly',
			));
			$this->data[$f] = (in_array($f, $inLookup))?$this->model_tool_lookup->getLookupDescription($this->data[$f],$f):$this->data[$f];
		}
		// var_dump($this->data['customer']['gross_income']);
		//approved field
		$this->data['form']['fields_bottom']['approved'] = $form->getFieldHtml(array(
			'type' => 'hidden',
			'name' => 'approved',
			'value' => '',
			'attr' => 'readonly',
		));

		$this->data['form']['remark'] = $form->getFieldHtml(array(
			'type' => 'textarea',
			'name' => 'remark',
			'value' => '',
		));

		$this->loadModel('sale/customer_group');
		$results = $this->model_sale_customer_group->getCustomerGroups();
		$groups = array( '' => $this->language->get('text_select_group'), );
		foreach ( $results as $item) {
			$groups[ $item['customer_group_id'] ] = $item['name'];
		}

		$this->view->assign('help_url', $this->gen_help_url('customer_edit') );
        $this->loadModel('sale/customer_transaction');
		$balance = $this->model_sale_customer_transaction->getBalance($this->request->get['customer_id']);
		$currency = $this->currency->getCurrency($this->config->get('config_currency'));

		$this->data['balance'] = $this->language->get('text_balance').' '.$currency['symbol_left'].round($balance,2).$currency['symbol_right'];
		$this->view->batchAssign( $this->data );

		$this->processTemplate('pages/sale/customer_form.tpl' );
	}

	public function approve() {

		//init controller data
        $this->extensions->hk_InitData($this,__FUNCTION__);

		$this->loadLanguage('mail/customer');
    	
		if (!$this->user->canModify('sale/customer')) {
			$this->session->data['error'] = $this->language->get('error_permission');
			$this->redirect($this->html->getSecureURL('sale/customer'));
		}

		if (!isset($this->request->get['customer_id'])) {
			$this->redirect($this->html->getSecureURL('sale/customer'));
		}

		$this->model_sale_customer->editCustomerField($this->request->get['customer_id'], 'approved', true );
		$this->_sendMail($this->request->get['customer_id']);

		//update controller data
        $this->extensions->hk_UpdateData($this,__FUNCTION__);

		$this->redirect($this->html->getSecureURL('sale/customer'));
	}

	public function actonbehalf() {

        $this->extensions->hk_InitData($this,__FUNCTION__);
    	
		if (isset($this->request->get['customer_id'])) {
            startStorefrontSession($this->user->getId(), array('customer_id' => $this->request->get['customer_id']));
			$this->redirect($this->html->getCatalogURL('account/account'));
		}

        $this->extensions->hk_UpdateData($this,__FUNCTION__);

		$this->redirect($this->html->getSecureURL('sale/customer'));
	}

	/**
	 * @param null $customer_id
	 * @return bool
	 */
	private function _validateApproveForm($customer_id = null) {

    	if (!$this->user->canModify('sale/customer')) {
      		$this->error['warning'] = $this->language->get('error_permission');
    	}
    			
    	if ((strlen(utf8_decode($this->request->post['firstname'])) < 1) || (strlen(utf8_decode($this->request->post['firstname'])) > 32)) {
      		$this->error['firstname'] = $this->language->get('error_firstname');
    	}

    	if ((strlen(utf8_decode($this->request->post['lastname'])) < 1) || (strlen(utf8_decode($this->request->post['lastname'])) > 32)) {
      		$this->error['lastname'] = $this->language->get('error_lastname');
    	}

		$email_pattern = '/^[A-Z0-9._%-]+@[A-Z0-9][A-Z0-9.-]{0,61}\.[A-Z]{2,6}$/i';
    	
		if ((strlen(utf8_decode($this->request->post['email'])) > 96) || (!preg_match($email_pattern, $this->request->post['email']))) {
      		$this->error['email'] = $this->language->get('error_email');
    	}

    	if ((strlen(utf8_decode($this->request->post['telephone'])) < 3) || (strlen(utf8_decode($this->request->post['telephone'])) > 32)) {
      		$this->error['telephone'] = $this->language->get('error_telephone');
    	}

		if (!$this->error) {
	  		return TRUE;
		} else {
	  		return FALSE;
		}
  	}

  	/**
	 * @param null $customer_id
	 * @return bool
	 */
	private function _validateEditForm($customer_id = null) {
    	if (!$this->user->canModify('sale/customer_profile')) {
      		$this->error['warning'] = $this->language->get('error_permission');
    	}
    			
    	if ((strlen(utf8_decode($this->request->post['first_name'])) < 1) || (strlen(utf8_decode($this->request->post['first_name'])) > 32)) {
      		$this->error['firstname'] = $this->language->get('error_firstname');
    	}

    	if ((strlen(utf8_decode($this->request->post['last_name'])) < 1) || (strlen(utf8_decode($this->request->post['last_name'])) > 32)) {
      		$this->error['lastname'] = $this->language->get('error_lastname');
    	}

		$email_pattern = '/^[A-Z0-9._%-]+@[A-Z0-9][A-Z0-9.-]{0,61}\.[A-Z]{2,6}$/i';
    	
		if ((strlen(utf8_decode($this->request->post['email'])) > 96) || (!preg_match($email_pattern, $this->request->post['email']))) {
      		$this->error['email'] = $this->language->get('error_email');
    	}

    	if ((strlen(utf8_decode($this->request->post['phone_no'])) < 3) || (strlen(utf8_decode($this->request->post['phone_no'])) > 32)) {
      		$this->error['telephone'] = $this->language->get('error_telephone');
    	}

		if (!$this->error) {
	  		return TRUE;
		} else {
	  		return FALSE;
		}
  	}

	/**
	 * @param null $customer_id
	 * @return bool
	 */
	private function _validateForm($customer_id = null) {
    	if (!$this->user->canModify('sale/customer')) {
      		$this->error['warning'] = $this->language->get('error_permission');
    	}
    			
		$login_name_pattern = '/^[\w._-]+$/i';
    	if ( (strlen(utf8_decode($this->request->post['loginname'])) < 5) || (strlen(utf8_decode($this->request->post['loginname'])) > 64)
    		|| (!preg_match($login_name_pattern, $this->request->post['loginname']) && $this->config->get('prevent_email_as_login') ) ) {
      		$this->error['loginname'] = $this->language->get('error_loginname');
		//check uniqunes of login name
    	} else if ( !$this->model_sale_customer->is_unique_loginname($this->request->post['loginname'], $customer_id) ) {
      		$this->error['loginname'] = $this->language->get('error_loginname_notunique');
    	}
   	    	
    	if ((strlen(utf8_decode($this->request->post['firstname'])) < 1) || (strlen(utf8_decode($this->request->post['firstname'])) > 32)) {
      		$this->error['firstname'] = $this->language->get('error_firstname');
    	}

    	if ((strlen(utf8_decode($this->request->post['lastname'])) < 1) || (strlen(utf8_decode($this->request->post['lastname'])) > 32)) {
      		$this->error['lastname'] = $this->language->get('error_lastname');
    	}

		$email_pattern = '/^[A-Z0-9._%-]+@[A-Z0-9][A-Z0-9.-]{0,61}\.[A-Z]{2,6}$/i';
    	
		if ((strlen(utf8_decode($this->request->post['email'])) > 96) || (!preg_match($email_pattern, $this->request->post['email']))) {
      		$this->error['email'] = $this->language->get('error_email');
    	}

    	if ((strlen(utf8_decode($this->request->post['telephone'])) < 3) || (strlen(utf8_decode($this->request->post['telephone'])) > 32)) {
      		$this->error['telephone'] = $this->language->get('error_telephone');
    	}

    	if (($this->request->post['password']) || (!isset($this->request->get['customer_id']))) {
      		if ((strlen(utf8_decode($this->request->post['password'])) < 4)) {
        		$this->error['password'] = $this->language->get('error_password');
      		}

	  		if (!$this->error['password'] && $this->request->post['password'] != $this->request->post['password_confirm']) {
	    		$this->error['password'] = $this->language->get('error_confirm');
	  		}
    	}
		if($this->request->post['addresses']){
			foreach ($this->request->post['addresses'] as $key => $address) {
				if ((strlen(utf8_decode($address['firstname'])) < 1) || (strlen(utf8_decode($address['firstname'])) > 32)) {
					$this->error[$key]['firstname'] = $this->language->get('error_address_firstname');
				}
				if ((strlen(utf8_decode($address['lastname'])) < 1) || (strlen(utf8_decode($address['lastname'])) > 32)) {
					$this->error[$key]['lastname'] = $this->language->get('error_address_lastname');
				}
				if ((strlen(utf8_decode($address['address_1'])) < 1)) {
					$this->error[$key]['address_1'] = $this->language->get('error_address_1');
				}
				if ((strlen(utf8_decode($address['city'])) < 1)) {
					$this->error[$key]['city'] = $this->language->get('error_city');
				}
				if (empty($address['country_id']) || $address['country_id'] == 'FALSE') {
					$this->error[$key]['country_id'] = $this->language->get('error_country');
				}
				if (empty($address['zone_id']) || $address['zone_id'] == 'FALSE') {
					$this->error[$key]['zone_id'] = $this->language->get('error_zone');
				}
			}
		}
		if (!$this->error) {
	  		return TRUE;
		} else {
	  		return FALSE;
		}
  	}

	/**
	 * @param int $id  - customer_id
	 */
	private function _sendMail($id){

			// send email to customer
			$customer_info = $this->model_sale_customer->getCustomer($id);

			if ($customer_info) {

				$this->loadLanguage('mail/customer');
				$this->loadModel('setting/store');

				/* key for first login reset password */
				$password = substr(md5(rand()), 0, 7);
				$key = substr(md5(rand()), 0, 20);
				// $reset_link = $this->html->getURL('account/reset&key='.$key);

				// echo "key = $key reset link : ".$reset_link." | ".HTTP_SERVER." | ".REAL_HOST;exit;
				$reset_link = HTTP_SERVER."index.php?rt=account/reset&key=".$key;	

				$store_info = $this->model_setting_store->getStore($customer_info['store_id']);

				if ($store_info) {
					$store_name = $store_info['store_name'];
					$store_url = $store_info['config_url'] . 'index.php?rt=account/password';
				} else {
					$store_name = $this->config->get('store_name');
					$store_url = $this->config->get('config_url') . 'index.php?rt=account/login';
				}

				$message  = sprintf($this->language->get('text_welcome'), $store_name) . "\n\n";;
				$message .= sprintf($this->language->get('text_login'), $store_name) . $store_name."\n";
				$message .= $this->language->get('text_username_password') . "\n";
				// $message .= $this->language->get('text_services') . "\n\n";
				$message .= "URL= ".$reset_link."\n";
				$message .= "Email = ".$customer_info['email']."\n\n";
				$message .= $this->language->get('text_thanks') . "\n";
				$message .= $store_name;

				//insert key into database
				$this->model_sale_customer->keyReset($customer_info['email'], $key);

				$mail = new AMail( $this->config );
				$mail->setTo($customer_info['email']);
				$mail->setFrom($this->config->get('store_main_email'));
				$mail->setSender($store_name);
				$mail->setSubject(sprintf($this->language->get('text_subject'), $store_name));
				$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
				$mail->send();
			}
	}

	/**
	 * @param int $id  - customer_id
	 */
	private function _sendEditApproveMail($id){
			// send email to customer
			$customer_info = $this->model_sale_customer->getCustomer($id);
			// $this->showDebug($id,true);
			if ($customer_info) {

				$this->loadLanguage('mail/customer');
				$this->loadModel('setting/store');

				$store_info = $this->model_setting_store->getStore($customer_info['store_id']);

				if ($store_info) {
					$store_name = $store_info['store_name'];
					$store_url = $store_info['config_url'] . 'index.php?rt=account/password';
				} else {
					$store_name = $this->config->get('store_name');
					$store_url = $this->config->get('config_url') . 'index.php?rt=account/login';
				}

				$message  = sprintf($this->language->get('text_welcome_edit_approve'), $customer_info['firstname']) . "\n\n";;
				$message .= sprintf($this->language->get('text_edit_content'), $store_name) ."\n\n";
				$message .= $this->language->get('text_edit_thanks') . "\n";
				$message .= $store_name;

				$mail = new AMail( $this->config );
				$mail->setTo($customer_info['email']);
				$mail->setFrom($this->config->get('store_main_email'));
				$mail->setSender($store_name);
				$mail->setSubject(sprintf($this->language->get('text_edit_subject'), $store_name));
				$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
				$mail->send();
			}
	}

	/**
	 * @param int $id  - customer_id
	 */
	private function _sendEditRejectMail($id){
			// send email to customer
			$customer_info = $this->model_sale_customer->getCustomer($id);
			
			// $this->showDebug($id,true);
			if ($customer_info) {

				$this->loadLanguage('mail/customer');
				$this->loadModel('setting/store');

				$store_info = $this->model_setting_store->getStore($customer_info['store_id']);

				if ($store_info) {
					$store_name = $store_info['store_name'];
					$store_url = $store_info['config_url'] . 'index.php?rt=account/password';
				} else {
					$store_name = $this->config->get('store_name');
					$store_url = $this->config->get('config_url') . 'index.php?rt=account/login';
				}

				if($this->request->post['remark']){
					$remark = $this->request->post['remark'];
				}

				$message  = sprintf($this->language->get('text_welcome_edit_approve'), $customer_info['firstname']) . "\n\n";;
				$message .= $this->language->get('text_content_reject_edit') .$remark."\n\n";
				$message .= sprintf($this->language->get('text_reject_try')). "\n\n";
				$message .= $this->language->get('text_thanks') . "\n";
				$message .= $store_name;
				
				$mail = new AMail( $this->config );
				$mail->setTo($customer_info['email']);
				$mail->setFrom($this->config->get('store_main_email'));
				$mail->setSender($store_name);
				$mail->setSubject(sprintf($this->language->get('text_edit_subject'), $store_name));
				$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
				$mail->send();
			}
	}

	/**
	 * @param int $id  - customer_id
	 */
	private function _sendRejectMail($id){
			// send email to customer
			$customer_info = $this->model_sale_customer->getCustomer($id);


			if ($customer_info) {

				$this->loadLanguage('mail/customer');
				$this->loadModel('setting/store');

				$store_info = $this->model_setting_store->getStore($customer_info['store_id']);

				if ($store_info) {
					$store_name = $store_info['store_name'];
					$store_url = $store_info['config_url'] . 'index.php?rt=account/login';
				} else {
					$store_name = $this->config->get('store_name');
					$store_url = $this->config->get('config_url') . 'index.php?rt=account/login';
				}

				//get remark by email
				$customer = $this->model_sale_customer->getCustomerByEmail($customer_info['email']);
				// $this->showDebug($customer);

				$message  = sprintf($this->language->get('text_welcome_reject'), $store_name) . "\n\n";;
				$message .= sprintf($this->language->get('text_content_reject'), $store_name) . $customer['remark']."\n\n";
				$message .= sprintf($this->language->get('text_reject_try'), "(021) 527 3110/cs@mandiri-investasi.co.id")."\n\n";
				// $message .= $store_url . "\n\n";
				// $message .= $this->language->get('text_services_reject') . "\n\n";
				$message .= $this->language->get('text_thanks') . "\n";
				$message .= $store_name;
				
				$mail = new AMail( $this->config );
				$mail->setTo($customer_info['email']);
				$mail->setFrom($this->config->get('store_main_email'));
				$mail->setSender($store_name);
				$mail->setSubject(sprintf($this->language->get('text_subject_reject'), $store_name));
				$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
				$mail->send();
			}
	}

}