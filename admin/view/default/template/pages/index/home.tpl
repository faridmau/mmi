<div class="contentBox">
  <div class="cbox_tl"><div class="cbox_tr"><div class="cbox_tc">
	<div class="heading"><?php echo $shortcut_heading; ?></div>
  </div></div></div>
  <div class="cbox_cl"><div class="cbox_cr"><div class="cbox_cc">
    <div id="cpanel">
      <ul class="quick_icon">
        <?php foreach( $shortcut as $item ) { ?>
            <li>
				<div class="iconbox_l"><div class="iconbox_r"><div class="iconbox_c">
				<a href="<?php echo $item['href'] ?>">
					<img src="<?php echo RDIR_TEMPLATE . 'image/icons/' . $item['icon'] ?>" alt="<?php echo  $item['text'] ?>" />
					<span><?php echo $item['text'] ?></span>
				</a>
				</div></div></div>
            </li>
        <?php } ?>
      </ul>
      <div class="clr_both"></div>
    </div>
  </div></div></div>
  <div class="cbox_bl"><div class="cbox_br"><div class="cbox_bc"></div></div></div>
</div>

<div class="contentBox">
  <div id="tabsAdmin">
    <ul>
      <li><a href="#tabs-1"><?php echo $text_latest_10_orders; ?></a></li>
      <?php if ($this->user->user_group_id==1 || $this->user->canAccess('approval/activation_approval')) { ?>
      <li><a href="#tabs-2"><?php echo $text_activation_account_approval; ?></a></li>
      <?php } ?>
      <?php if ($this->user->user_group_id==1 || $this->user->canAccess('approval/edit_approval') || $this->user->user_group_id==1) { ?>
      <li id="edit_account_tab"><a href="#tabs-3"><?php echo $text_edit_account_approval; ?></a></li>
      <?php } ?>
      <!-- <li id="edit_account_tab"><a href="#tabs-3"><?php //echo $text_edit_account_approval; ?></a></li> -->
    </ul>
    <div id="tabs-1">
      <!-- <div class="cbox_tl">
        <div class="cbox_tr">
          <div class="cbox_tc">
            <div class="heading"><?php echo $text_latest_10_orders; ?></div>
            <div class="toolbar flt_right">
            <div style="margin:14px 10px 0 0;"><a href="<?php echo $orders_url; ?>"><?php echo $orders_text; ?></a></div>
            </div>
          </div>
        </div>
      </div> -->
      <div class="cbox_cl"><div class="cbox_cr"><div class="cbox_cc">
        <table class="list">
          <thead>
            <tr>
              <td class="center"><b><?php echo $column_order; ?></b></td>
              <td class="left"><b><?php echo $column_name; ?></b></td>
              <td class="left"><b><?php echo $column_status; ?></b></td>
              <td class="left"><b><?php echo $column_date_added; ?></b></td>
              <td class="right"><b><?php echo $column_total; ?></b></td>
              <td class="center"><b><?php echo $column_action; ?></b></td>
            </tr>
          </thead>
          <tbody>
          <?php if ($orders) { ?>
            <?php foreach ($orders as $order) { ?>
            <tr>
              <td class="center"><?php echo $order['order_id']; ?></td>
              <td class="left"><?php echo $order['name']; ?></td>
              <td class="left"><?php echo $order['status']; ?></td>
              <td class="left"><?php echo $order['date_added']; ?></td>
              <td class="right"><?php echo $order['total']; ?></td>
              <td class="center"><?php foreach ($order['action'] as $action) { ?>
              <a class="btn_action" href="<?php echo $action['href']; ?>"><img src="<?php echo RDIR_TEMPLATE; ?>image/icons/icon_grid_edit.png" alt="<?php echo $action['text']; ?>" /></a>
              <?php } ?></td>
            </tr>
            <?php } ?>
          <?php } else { ?>
            <tr>
              <td class="center" colspan="6"><?php echo $text_no_results; ?></td>
            </tr>
          <?php } ?>
          </tbody>
        </table>
      </div></div></div>
      <div class="cbox_bl"><div class="cbox_br"><div class="cbox_bc"></div></div></div>
    </div>
    <?php if ($this->user->user_group_id==1 || $this->user->canAccess('approval/activation_approval')) { ?>
    <div id="tabs-2">
      <div class="cbox_cl"><div class="cbox_cr"><div class="cbox_cc">
        <table class="list">
          <thead>
            <tr>
              <td class="center"><b><?php echo $column_date_added; ?></b></td>
              <td class="left"><b><?php echo $column_cif; ?></b></td>
              <td class="left"><b><?php echo $column_name; ?></b></td>
              <td class="center"><b><?php echo $column_id_number; ?></b></td>
              <td class="center"><b><?php echo $column_action; ?></b></td>
            </tr>
          </thead>
          <tbody>
          <?php if ($waiting) { ?>
            <?php foreach ($waiting as $wait) { ?>
            <tr>
              <td class="center"><?php echo $wait['date_added']; ?></td>
              <td class="left"><?php echo $wait['cif']; ?></td>
              <td class="left"><?php echo $wait['name']; ?></td>
              <td class="center"><?php echo $wait['id_card'] ?></td>
              <td class="center"><?php foreach ($wait['action'] as $action) { ?>
              <a class="btn_action" href="<?php echo $action['href']; ?>"><img src="<?php echo RDIR_TEMPLATE; ?>image/icons/icon_grid_view.png" alt="<?php echo $action['text']; ?>" /></a>
              <?php } ?></td>
            </tr>
            <?php } ?>
          <?php } else { ?>
            <tr>
              <td class="center" colspan="6"><?php echo $text_no_results; ?></td>
            </tr>
          <?php } ?>
          </tbody>
        </table>
      </div></div></div>
      <div class="cbox_bl"><div class="cbox_br"><div class="cbox_bc"></div></div></div>
    </div>
    <?php } ?>
    <?php if ($this->user->user_group_id==1 || $this->user->canAccess('approval/edit_approval')) { ?>
    <div id="tabs-3">
      <div class="cbox_cl"><div class="cbox_cr"><div class="cbox_cc">
        <table class="list">
          <thead>
            <tr>
              <td class="center"><b><?php echo $column_date_added; ?></b></td>
              <td class="center"><b><?php echo $column_holder_id; ?></b></td>
              <td class="left"><b><?php echo $column_cif; ?></b></td>
              <td class="left"><b><?php echo $column_name; ?></b></td>
              <td class="center"><b><?php echo $column_action; ?></b></td>
            </tr>
          </thead>
          <tbody>
          <?php if ($customers) { ?>
            <?php foreach ($customers as $cust) { ?>
            <tr>
              <td class="center"><?php echo $cust['updated_datetime']; ?></td>
              <td class="center"><?php echo $cust['id_holder'] ?></td>
              <td class="left"><?php echo $cust['cif']; ?></td>
              <td class="left"><?php echo $cust['name']; ?></td>
              <td class="center"><?php foreach ($cust['action'] as $action) { ?>
              <a class="btn_action" href="<?php echo $action['href']; ?>"><img src="<?php echo RDIR_TEMPLATE; ?>image/icons/icon_grid_edit.png" alt="<?php echo $action['text']; ?>" /></a>
              <?php } ?></td>
            </tr>
            <?php } ?>
          <?php } else { ?>
            <tr>
              <td class="center" colspan="6"><?php echo $text_no_results; ?></td>
            </tr>
          <?php } ?>
          </tbody>
        </table>
      </div></div></div>
      <div class="cbox_bl"><div class="cbox_br"><div class="cbox_bc"></div></div></div>
    </div>
    <?php } ?>
  </div>
</div>

<!--[if IE]>
<script type="text/javascript" src="<?php echo RDIR_TEMPLATE; ?>javascript/jquery/flot/excanvas.js"></script>
<![endif]-->
<script type="text/javascript" src="<?php echo RDIR_TEMPLATE; ?>javascript/jquery/flot/jquery.flot.js"></script>
<script type="text/javascript"><!--
$(function() {
  // console.log(<?php echo "$tab_approve";?>);
  var page = "<?php echo "$tab_approve"; ?>";
  $( "#tabsAdmin" ).tabs();
  if(page==1){
    console.log('page == 1');
    $( "#tabsAdmin" ).tabs({selected: "#tabs-2"});
    $('html, body').animate({
        scrollTop: $("#tabs-2").offset().top
    }, 1000);
  }
});
//--></script>