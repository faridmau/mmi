<?php if (!empty($error['warning'])) { ?>
	<div class="warning alert alert-error"><?php echo $error['warning']; ?></div>
<?php } else if (count(array_keys($error))) { ?>
	<div class="warning alert alert-error">
		<?php
		foreach ($error as $key => $error_text) {
			if (is_array($error_text)) {
				foreach ($error_text as $error_text2) {
					echo $error_text2 . '<br />';
				}
			} else {
				echo $error_text . '<br />';
			}
		} ?>
	</div>
<?php } ?>

<?php if ($success) { ?>
	<div class="success alert alert-success"><?php echo $success; ?></div>
<?php } ?>

<div id="customer-approve" class="contentBox">
	<div class="cbox_tl">
		<div class="cbox_tr">
			<div class="cbox_tc">
				<div class="heading icon_title_customer" style="width:45%;"><?php echo $heading_title; ?></div>
				<div id="retrieved-info" class="heading icon_title_customer"><?php echo $retrieved_cust_information; ?></div>
				<?php echo $this->getHookVar('extension_tabs'); ?>
			</div>
		</div>
	</div>
	<div class="cbox_cl">
		<div class="cbox_cr">
			<div class="cbox_cc">
				<?php echo $form['form_open']; ?>
				<div class="fieldset">
					<div class="heading"><?php echo $form_title; ?></div>
					<div class="top_left">
						<div class="top_right">
							<div class="top_mid"></div>
						</div>
					</div>
					<div class="cont_left percent-50">
						<div class="cont_right">
							<div class="cont_mid">
								<div style="display: inline-block; width: 100%;">
									<div id="form">
										<div id="tab_general">
											<div class="customer-general-detail">
												<table class="form customer-detail">
													<tr><td><h3 class="form-title">Personal Details</h3></td></tr>
													<?php foreach ($form['fields'] as $name => $field) { ?>
														<tr>
															<td><?php echo ${'entry_' . $name}; ?></td>
															<td id="payment_<?php echo $name; ?>">
																<?php echo $field; ?>
																<?php if (!empty($error[$name])) { ?>
																	<div class="field_err"><?php echo $error[$name]; ?></div>
																<?php } ?>
															</td>
															<?php if($name == 'cif') {?>
															<td>
																<a class="btn_standard"><span id="cgFrm_retrieve" class="button1" title="Retrieve"><span>Retrieve</span></span></a>
																<input type="hidden" id="token" value="<?php echo $token; ?>" />
																<input type="hidden" name="holder_id" id="cgFrm_holder_id" value="" />
															</td>
															<?php } ?>
														</tr>
													<?php }  ?>
												</table>
												<input id="cgFrm_approved" name="approved" type="hidden" readonly="readonly" value="2" />
											</div>
											<div class="customer-general-detail">
												<table class="form customer-detail">
													<tr><td colspan=2><h3 class="form-title">Bank Account Information</h3></td></tr>
													<?php foreach ($form['bank'] as $name => $field) { ?>
														<tr>
															<td><?php echo ${'entry_' . $name}; ?></td>
															<td id="payment_<?php echo $name; ?>">
																<?php echo $field; ?>
																<?php if (!empty($error[$name])) { ?>
																	<div class="field_err"><?php echo $error[$name]; ?></div>
																<?php } ?>
															</td>
														</tr>
													<?php }  ?>
												</table>
											</div>
											<div class="customer-general-detail">
												<table class="form customer-detail">
													<tr><td><h3 class="form-title">Remarks</h3></td></tr>
													<?php foreach ($form['remark'] as $name => $field) { ?>
														<tr>
															<td colspan='2' id="payment_<?php echo $name; ?>">
																<?php echo $field; ?>
																<?php if (!empty($error[$name])) { ?>
																	<div class="field_err"><?php echo $error[$name]; ?></div>
																<?php } ?>
															</td>
														</tr>
													<?php }  ?>
												</table>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
					<!-- content right start -->
					<div id="compareDiv" class="cont_left percent-50">
						<div class="cont_right">
							<div class="cont_mid">
								<div style="display: inline-block; width: 100%;">
									<div id="form">
										<div id="tab_general">
											<div class="customer-general-detail">
												<table class="form customer-detail">
													<tr><td><h3 class="form-title">Personal Details</h3></td></tr>
													<?php foreach ($form['compare_fields'] as $name => $field) { ?>
														<tr>
															<td><?php echo ${'entry_' . $name}; ?></td>
															<td id="payment_<?php echo $name; ?>">
																<?php echo $field; ?>
																<?php if (!empty($error[$name])) { ?>
																	<div class="field_err"><?php echo $error[$name]; ?></div>
																<?php } ?>
															</td>
															<td><input type="checkbox" class="chkConfirm" name="chkConfirm" value="<?php echo $name; ?>"></td>
														</tr>
													<?php }  ?>
												</table>
											</div>
											<div class="customer-general-detail">
												<table class="form customer-detail">
													<tr><td colspan=2><h3 class="form-title">Bank Account Information</h3></td></tr>
													<?php foreach ($form['compare_bank'] as $name => $field) { ?>
														<tr>
															<td><?php echo ${'entry_' . $name}; ?></td>
															<td id="payment_<?php echo $name; ?>">
																<?php echo $field; ?>
																<?php if (!empty($error[$name])) { ?>
																	<div class="field_err"><?php echo $error[$name]; ?></div>
																<?php } ?>
															</td>														
															<td><input type="checkbox" class="chkConfirm" name="chkConfirm" value="<?php echo $name; ?>"></td>
														</tr>
													<?php }  ?>
												</table>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
					<!-- content right end -->
					<div class="bottom_left">
						<div class="bottom_right">
							<div class="bottom_mid"></div>
						</div>
					</div>
				</div>
				<div class="buttons align_center percent-100">
					<button id="approveBtn" type="submit" class="btn_standard"><?php echo $form['approve']; ?></button>
					<button id="rejectBtn" type="submit" class="btn_standard"><?php echo $form['reject']; ?></button>
					<a class="btn_standard" href="<?php echo $cancel; ?>"><?php echo $form['cancel']; ?></a>
				</div>
				</form>
			</div>
		</div>
	</div>

</div>

<div id="confirm_account_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<form id="confirm-form">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><span class="title">Please choose one of this customer</span></h3>
  </div>
  <div class="modal-body">
		<!-- data dari ajax -->
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <span id="confirm-account" class="btn btn-primary">Save changes</span>
  </div>
</form>
</div>

<div id="reject_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-body"></div>
	<div class="modal-footer"><button class="btn" data-dismiss="modal" aria-hidden="true">Close</button></div>
</div>

<div class="loading-animation"></div>

<script type="text/javascript">
	$(document).ready(function(){
		$('.loading-animation').hide();
		var totalChk = $( "input:checkbox").length;
		var token = $("#token").val();	
		//disable button
		// $('#approveBtn span').addClass('disable');
		// $('#approveBtn').attr('disabled','disabled');
		$('#retrieved-info').hide();

		$('#approveBtn').click(function(e){
			var totalSelected = $( "input:checkbox:checked").length;
			// console.log(totalChk,totalSelected);

			if(totalChk!=totalSelected) { 
				customModal('#reject_modal',"<?php echo $error_checklist; ?>");
				e.preventDefault();
			}else{
				//send ajax request to check holder_id if already exists
				var holder_id = $('#cgFrm_holder_id').val();
				// console.log("holder_ID: ",holder_id);
				$.ajax({
					url: "<?php echo $check_holder_id; ?>",
					method: 'GET',
					data: {
						holder_id : holder_id,
					},
					success: function(data){
						if(data>0){
							customModal('#reject_modal',"<?php echo $error_holder_id; ?>");
							e.preventDefault();
						}
					},
					error: function(data){
						console.log("error");
					}
				});
			}
			e.preventDefault();
		});

		// var compareDiv = $("#compareDiv").hide();
		var compareDiv = $("#compareDiv .cont_right").hide();

		/*var sendCustID = function(CustID){
			$.ajax({
			  url: "index.php?rt=sale/customer/getConfirmData",
			  type: 'GET',
			  cache: false,
			  data: {
			  	token: token,
			  	customer_id : CustID,
			  	s : 'mmi'
			  },
			  success : function(result,status){
			  	var obj = jQuery.parseJSON(result);
			  	// console.log(obj);
			  	$.each(obj,function(key, val){
			  		$("#compareDiv #cgFrm_c"+key).val(val);
			  	});
			  },
			  error : function(error){
			  	console.log(error);
			  }
			});
		}*/

		var insertToInput = function(idx){
			var firstname = $("#fn_"+idx).val();
			var lastname = $("#ln_"+idx).val();
			var email = $("#em_"+idx).val();
			var phone = $("#pn_"+idx).val();
			var idNumber = $("#in_"+idx).val();
			var idType = $("#it_"+idx).val();
			var cif = $("#cif_"+idx).val();
			var bankAccountName = $("#ban_"+idx).val();
			var bankName = $("#bn_"+idx).val();
			var bankAccountNo = $("#bano_"+idx).val();
			var holderId = $("#hid_"+idx).val();
			

			$("#cgFrm_cfirstname").val(firstname);
			$("#cgFrm_clastname").val(lastname);
			$("#cgFrm_cemail").val(email);
			$("#cgFrm_ctelephone").val(phone);
			$("#cgFrm_cid_number").val(idNumber);
			$("#cgFrm_cid_type").val(idType);
			$("#cgFrm_ccif").val(cif);
			$("#cgFrm_cbank_account_name").val(bankAccountName);
			$("#cgFrm_cbank_name").val(bankName);
			$("#cgFrm_cbank_account_no").val(bankAccountNo);
			$("#cgFrm_holder_id").val(holderId);

			// console.log(firstname, lastname, email, pn);return;
		}

		/*$( "input:checkbox").change(function(){
			console.log();
			var totalSelected = $( "input:checkbox:checked").length;
			if(totalChk==totalSelected) { 
				$('#approveBtn').removeAttr('disabled','disabled'); 
				// $('#approveBtn span').removeClass('disable');
			}else{ 
				$('#approveBtn').attr('disabled','disabled');				
				// $('#approveBtn span').addClass('disable');
			}
		});*/

		$('#confirm-account').click(function(){
			var form = $("#confirm-form");
			var result  = $( "input:radio[name=confirmAccount]:checked" ).val();
			//reset all checkbox value to false
			$('.chkConfirm').prop('checked',false);
			if(!result) return;
			insertToInput(result);
			$('#confirm_account_modal').modal('hide')
			// var compareDiv = $("#compareDiv").show();
			$("#compareDiv .cont_right").show();
			$('#retrieved-info').show();
			// $("#cgFrm_retrieve").hide();
		});

		$('#cgFrm_retrieve').click(function(e){

			// $('#confirm_account_modal').modal();
			// totalChk = $( ".chkConfirm" ).length;
			// alert('send ajax function');
			//show loading animation while request being send

			$('.loading-animation').show();
			var fullname = $('#cgFrm_firstname').val()+" "+$('#cgFrm_lastname').val(); 
			var cif = $('#cgFrm_cif').val(); 
			var email = $('#cgFrm_email').val(); 

			$.ajax({
				url : "<?php echo $retrieve_url; ?>",
				method : 'GET',
				data : {
					fullname : fullname,
					cif : cif,
					email : email
					/*fullname:'Inna Racgma',
					cif:'12345789',
					email:'innarachma+2@gmail.com'*/
				},
				success : function(data){
					if(data=='error') {
						$('.loading-animation').hide();
						alert('gagal tersambung ke web service');
						return;
					} 

					if(data=="null" || data==null) {
						$('.loading-animation').hide();
						customModal('#reject_modal','There\'s no data on server with <BR/>fullname : '+fullname+' <BR/>cif : '+cif + ' <BR/>email : '+email); 
						return;
					}
					var obj = jQuery.parseJSON(data);
					var table = '<table>';
					table += '<thead>'
												+'<th>&nbsp;</th>'
												+'<th>First Name</th>'
												+'<th>Last Name</th>'
												+'<th>Email</th>'
												+'<th>Telephone</th>'
												+'<th>ID Number</th>'
												+'<th>ID Type</th>'
												+'<th>CIF</th>'
												+'<th>Account Name</th>'
												+'<th>Bank Name</th>'
												+'<th>Account No</th>'
								 			'</thead>';
					table += '<tbody>';
					$.each(obj,function(key, val){
						table += '<tr>';
						table += '<input id= hid_'+key+' type=hidden readonly value='+val.holderId+' />';
						table += '<td><input type=radio name=confirmAccount value='+key+'></td>';
						table += '<td><input style="width:80px" id= fn_'+key+' type=text readonly value='+val.firstName+' /></td>';
						table += '<td><input style="width:80px" id= ln_'+key+' type=text readonly value='+val.lastName+' /></td>';
						table += '<td><input id= em_'+key+' type=text readonly value='+val.email+' /></td>';
						table += '<td><input style="width:80px" id= pn_'+key+' type=text readonly value='+val.phoneNo+' /></td>';
						table += '<td><input style="width:80px" id= in_'+key+' type=text readonly value='+val.idNumber+' /></td>';
						table += '<td><input style="width:80px" id= it_'+key+' type=text readonly value='+val.idType+' /></td>';
						table += '<td><input style="width:50px" id= cif_'+key+' type=text readonly value='+val.cif+' ></td>';
						table += '<td><input style="width:120px" id= ban_'+key+' type=text readonly value='+val.bankAccountName+' /></td>';
						table += '<td><input style="width:120px" id= bn_'+key+' type=text readonly value='+val.bankName+' /></td>';
						table += '<td><input style="width:80px" id= bano_'+key+' type=text readonly value='+val.bankAccountNo+' /></td>';
						table += '</tr>';
					});

					table += "</tbody></table>";
					$('.loading-animation').hide();

					//isi data dengan hasil return dari ajax
					var ele  = $('#confirm_account_modal'); 
					ele.modal();
					$('#confirm_account_modal .modal-body').html(table);
				},
				error : function(data,status){
					alert('gagal tersambungke web service');
					return;
				} 
			});
		});

		// $("#confirm-form").submit(function(e){
		// 	var data = this.serialize();
		// 	console.log(data);
		// 	e.preventDefault();
		// });

		//validasi untuk reject
		$('#rejectBtn').click(function(e){
			console.log($('#cgFrm_remark').val());
			var textArea = $('#cgFrm_remark').val().trim();
			if(textArea==null || textArea==''){
				//$('#reject_modal').modal();
				customModal('#reject_modal','Remark must be filled if you reject this account');
				e.preventDefault();
			}else{
				$('#cgFrm_approved').val(0);
			}
		});

		function customModal(id,content){
			$(id+" .modal-body").html(content);
			$(id).modal();
		}


		/*var publickey = "<?php echo publicKeyToHex(PRIVATE_KEY)?>";
    var rsakey = new RSAKey();
    rsakey.setPublic(publickey, "10001");
    var enc_ac = rsakey.encrypt($('#cgFrm_bank_account_name').val());
    var enc_an = rsakey.encrypt($('#cgFrm_bank_account_no').val());

		//encrypt
    $('#cgFrm_bank_account_name').val(enc_ac);
    $('#cgFrm_bank_account_no').val(enc_an);*/

	});
</script>