<?php if (!empty($error['warning'])) { ?>
	<div class="warning alert alert-error"><?php echo $error['warning']; ?></div>
<?php } else if (count(array_keys($error))) { ?>
	<div class="warning alert alert-error">
		<?php
		foreach ($error as $key => $error_text) {
			if (is_array($error_text)) {
				foreach ($error_text as $error_text2) {
					echo $error_text2 . '<br />';
				}
			} else {
				echo $error_text . '<br />';
			}
		} ?>
	</div>
<?php } ?>

<?php if ($success) { ?>
	<div class="success alert alert-success"><?php echo $success; ?></div>
<?php } ?>

<div class="contentBox edit-customer-approve">
	<div class="cbox_tl">
		<div class="cbox_tr">
			<div class="cbox_tc">
				<div class="heading icon_title_customer"><?php echo $heading_title; ?></div>
				<?php echo $this->getHookVar('extension_tabs'); ?>
			</div>
		</div>
	</div>
	<div class="cbox_cl">
		<div class="cbox_cr">
			<div class="cbox_cc">

				<?php echo $form['form_open']; ?>
				<div class="fieldset">
					<div class="heading"><?php echo $form_title; ?></div>
					<div class="top_left">
						<div class="top_right">
							<div class="top_mid"></div>
						</div>
					</div>
					<div class="cont_left">
						<div class="cont_right">
							<div class="cont_mid">
								<div style="display: inline-block; width: 100%;">
									<div id="form">
										<div id="tab_general" class="">
											<div class="customer-general-detail">
												<div class="left">
													<table class="form customer-detail">
														<?php foreach ($form['fields'] as $name => $field) { ?>
															<tr>
																<td><?php if($name!="approved") echo ${'entry_' . $name}; ?></td>
																<td id="payment_<?php echo $name; ?>">
																	<?php echo $field; ?><?php echo ${$name} ?>
																	<?php if (!empty($error[$name])) { ?>
																		<div class="field_err"><?php echo $error[$name]; ?></div>
																	<?php } ?>
																</td>
															</tr>
														<?php }  ?>
														<tr>
															<td><?php echo $title_domicile_address; ?></td>
														</tr>
														<tr>
															<td rowspawn=3 colspan=2><?php echo $form['domicile_address']; ?></td>
															<td>
																<?php foreach ($form['domicile'] as $name => $field) { ?>
																<div class="col-md-12">
																	<div class="col-md-6"><?php echo ${'entry_'.$name}; ?></div>
																	<div class="col-md-6"><?php echo $field?><?php echo ${$name} ?></div>
																</div>
																<?php } ?>
															</td>
														</tr>
														<tr>
															<td><?php echo $title_correspondence_address; ?></td>
														</tr>
														<tr>
															<td rowspawn=3 colspan=2><?php echo $form['correspondence_address']; ?></td>
															<td>
																<?php foreach ($form['correspondence'] as $name => $field) { ?>
																<div class="col-md-12">
																	<div class="col-md-6"><?php echo ${'entry_'.$name}; ?></div>
																	<div class="col-md-6"><?php echo $field?><?php echo ${$name} ?></div>
																</div>
																<?php } ?>		
															</td>
														</tr>
														<?php foreach ($form['fields_bottom'] as $name => $field) { ?>
															<tr>
																<td><?php if($name!="approved") echo ${'entry_' . $name}; ?></td>
																<td id="payment_<?php echo $name; ?>">
																	<?php echo $field; ?>
																	<?php if ($name =='tax_id_reg_date'||$name =='date_of_birth'): ?>
																		<?php echo date("d-M-y", strtotime(${$name})); ?>
																	<?php else: ?>
																		<?php echo ${$name} ?>	
																	<?php endif ?>
																	
																	<?php if (!empty($error[$name])) { ?>
																		<div class="field_err"><?php echo $error[$name]; ?></div>
																	<?php } ?>
																</td>
															</tr>
														<?php }  ?>
													</table>
												</div>
												<div class="left">
													<table class="form customer-detail">
														<?php foreach ($form['fields2'] as $name => $field) { ?>
															<tr>
																<td><?php echo ${'entry_' . $name}; ?></td>
																<td id="payment_<?php echo $name; ?>">
																	<?php echo $field; ?><?php echo ${$name}; ?>
																	<?php if (!empty($error[$name])) { ?>
																		<div class="field_err"><?php echo $error[$name]; ?></div>
																	<?php } ?>
																</td>
															</tr>
														<?php }  ?>
													</table>
												</div>
											</div>
											<div class="customer-general-detail">
												<div class="left">
													<span class="title"><?php echo $title_bank_accout; ?></span>
													<table class="form customer-detail">
														<?php foreach ($form['bank'] as $name => $field) { ?>
															<tr>
																<td><?php echo ${'entry_' . $name}; ?></td>
																<td id="payment_<?php echo $name; ?>">
																	<?php echo $field; ?><?php echo ${$name}; ?>
																	<?php if (!empty($error[$name])) { ?>
																		<div class="field_err"><?php echo $error[$name]; ?></div>
																	<?php } ?>
																</td>
															</tr>
														<?php }  ?>
														<tr><td><?php echo ${'entry_bank_address' }; ?></td></tr>
														<tr>
															<td colspan=2 id="bank_address; ?>">
																<?php echo $form['bank_address']; ?>
															</td>
														</tr>
														<tr>
															<td><?php echo ${'entry_bank_account_no'};?></td>
															<td id="bank_account_no; ?>"><?php echo $form['bank_account_no']; ?><?php echo $bank_account_no ?></td>
														</tr>
													</table>
												</div>
												<div class="left">
													<span class="title"><?php echo $title_financial; ?></span>
													<table class="form customer-detail">
														<?php foreach ($form['financial'] as $name => $field) { ?>
															<tr>
																<td><?php echo ${'entry_' . $name}; ?></td>
																<td id="payment_<?php echo $name; ?>">
																	<?php echo $field; ?><?php echo ${$name}; ?>
																	<?php if (!empty($error[$name])) { ?>
																		<div class="field_err"><?php echo $error[$name]; ?></div>
																	<?php } ?>
																</td>
															</tr>
														<?php }  ?>
													</table>
												</div>
											</div>
												
											<div class="col-md-12 remark-textarea">
												<span class="title"><?php echo $title_remark; ?></span>
												<?php echo $form['remark']; ?>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
					<div class="bottom_left">
						<div class="bottom_right">
							<div class="bottom_mid"></div>
						</div>
					</div>
				</div>
				<div class="buttons align_center">
					<button id="approveBtn" type="submit" class="btn_standard"><?php echo $form['approve']; ?></button>
					<button id="rejectBtn" type="submit" class="btn_standard"><?php echo $form['reject']; ?></button>
					<a class="btn_standard" href="<?php echo $cancel; ?>"><?php echo $form['cancel']; ?></a>
				</div>
				</form>

			</div>
		</div>
	</div>
	<div class="cbox_bl">
		<div class="cbox_br">
			<div class="cbox_bc"></div>
		</div>
	</div>
</div>

<div id="reject_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-body"></div>
	<div class="modal-footer"><button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo $entry_close; ?></button></div>
</div>

<script type="text/javascript"><!--
	$(document).ready(function(){
		$('#cgFrm_education').css('width','100%');
		$('#payment_approved').css('display','none');
		
		var ele = $("#cgFrm_approved");
		$("#approveBtn").click(function(e){
			ele.val(0); // 3 == wait edit approve
			// alert('approve '+ ele.val());
			// e.preventDefault();
		});
		$("#rejectBtn").click(function(e){
			ele.val(3);
			var remark = $("#cgFrm_remark").val().trim();
			if(remark.trim==null || remark==''){
				customModal('#reject_modal','<?php echo $text_remark; ?>');
				e.preventDefault();
				return;
			}
		});

		function customModal(id,content){
			$(id+" .modal-body").html(content);
			$(id).modal();
		}

		var publickey = "<?php echo publicKeyToHex(PRIVATE_KEY)?>";
    var rsakey = new RSAKey();
    rsakey.setPublic(publickey, "10001");
    var enc_ac = rsakey.encrypt($('#cgFrm_bank_account_name').val());
    var enc_an = rsakey.encrypt($('#cgFrm_bank_account_no').val());

		//encrypt
    /*$('#cgFrm_bank_account_name').val(enc_ac);
    $('#cgFrm_bank_account_no').val(enc_an);*/
	});	
//--></script>