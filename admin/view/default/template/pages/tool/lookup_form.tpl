<?php if ($error_warning) { ?>
<div class="warning alert alert-error"><?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($success) { ?>
<div class="success alert alert-success"><?php echo $success; ?></div>
<?php } ?>

<div class="contentBox">
  <div class="cbox_tl"><div class="cbox_tr"><div class="cbox_tc">
    <div class="heading icon_title_brand"><?php echo $heading_title; ?></div>
<?php if ($update) { ?>
  
<?php } ?>
    <div class="toolbar">
    <?php if ( !empty ($help_url) ) : ?>
          <div class="help_element"><a href="<?php echo $help_url; ?>" target="new"><img src="<?php echo $template_dir; ?>image/icons/help.png"/></a></div>
      <?php endif; ?>
    </div>
    <?php echo $form_language_switch; ?>
  </div></div></div>
  <div class="cbox_cl"><div class="cbox_cr"><div class="cbox_cc">

  <?php echo $form['form_open']; ?>
  <div class="fieldset">
    <div class="heading"><?php echo $form_title; ?></div>
    <div class="top_left"><div class="top_right"><div class="top_mid"></div></div></div>
    <div class="cont_left"><div class="cont_right"><div class="cont_mid">
    <table class="form">
      <tr>
        <td><?php echo $entry_group_code; ?></td>
        <td><?php echo $form['fields']['group_code']; ?>
        <?php if ( !empty($error_group_code) ) { ?>
          <div class="error"><?php echo($error_group) ?></div>
        <?php } ?>
        </td>
      </tr>
      <tr>
        <td><?php echo $entry_item_code; ?></td>
        <td><?php echo $form['fields']['item_code']; ?>
        <?php if ( !empty($error_item_code) ) { ?>
          <div class="error"><?php echo($error_code) ?></div>
        <?php } ?>
        </td>
      </tr>
      <tr>
        <td><?php echo $entry_item_name; ?></td>
        <td><?php echo $form['fields']['item_name']; ?>
        <?php if ( !empty($error_item_name) ) { ?>
          <div class="error"><?php echo($error_name) ?></div>
        <?php } ?>
        </td>
      </tr>
     <!--  <tr>
        <td><?php echo $entry_lang; ?></td>
        <td><?php echo $form['fields']['language_id']; ?>
        <?php if ( !empty($error_languages) ) { ?>
          <div class="error"><?php echo($error_name) ?></div>
        <?php } ?>
        </td>
      </tr> -->
      <tr>
        <td><?php echo $entry_status; ?></td>
        <td><?php echo $form['fields']['status']; ?>
        <?php if ( !empty($error_status) ) { ?>
          <div class="error"><?php echo($error_status) ?></div>
        <?php } ?>
        </td>
      </tr>
    </table>
    </div></div></div>
      <div class="bottom_left"><div class="bottom_right"><div class="bottom_mid"></div></div></div>
  </div><!-- <div class="fieldset"> -->


    <?php if ( !empty($update) ) { echo $resources_html; } ?>

  <div class="buttons align_center">
    <button type="submit" class="btn_standard"><?php echo $form['submit']; ?></button>
    <a class="btn_standard" href="<?php echo $cancel; ?>" ><?php echo $form['cancel']; ?></a>
    </div>
  </form>

  </div></div></div>
  <div class="cbox_bl"><div class="cbox_br"><div class="cbox_bc"></div></div></div>
</div>