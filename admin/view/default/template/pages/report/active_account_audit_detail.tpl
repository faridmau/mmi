<?php if (!empty($error['warning'])) { ?>
	<div class="warning alert alert-error"><?php echo $error['warning']; ?></div>
<?php } else if (count(array_keys($error))) { ?>
	<div class="warning alert alert-error">
		<?php
		foreach ($error as $key => $error_text) {
			if (is_array($error_text)) {
				foreach ($error_text as $error_text2) {
					echo $error_text2 . '<br />';
				}
			} else {
				echo $error_text . '<br />';
			}
		} ?>
	</div>
<?php } ?>

<?php if ($success) { ?>
	<div class="success alert alert-success"><?php echo $success; ?></div>
<?php } ?>

<div id="customer-approve" class="contentBox">
	<div class="cbox_tl">
		<div class="cbox_tr">
			<div class="cbox_tc">
				<div class="heading icon_title_customer" style="width:45%;"><?php echo $heading_title; ?></div>
				<div id="retrieved-info" class="heading icon_title_customer"><?php echo $retrieved_cust_information; ?></div>
				<?php echo $this->getHookVar('extension_tabs'); ?>
			</div>
		</div>
	</div>
	<div class="cbox_cl">
		<div class="cbox_cr">
			<div class="cbox_cc">
				<?php echo $form['form_open']; ?>
				<div class="fieldset">
					<div class="heading"><?php echo $form_title; ?></div>
					<div class="top_left">
						<div class="top_right">
							<div class="top_mid"></div>
						</div>
					</div>
					<div class="cont_left percent-50">
						<div class="cont_right">
							<div class="cont_mid">
								<div style="display: inline-block; width: 100%;">
									<div id="form">
										<div id="tab_general">
											<div class="customer-general-detail">
												<table class="form customer-detail">
													<tr><td><h3 class="form-title">Personal Details</h3></td></tr>
													<?php foreach ($form['fields'] as $name => $field) { ?>
														<tr>
															<td><?php echo ${'entry_' . $name}; ?></td>
															<td id="payment_<?php echo $name; ?>">
																<?php echo $field; ?>
																<?php if (!empty($error[$name])) { ?>
																	<div class="field_err"><?php echo $error[$name]; ?></div>
																<?php } ?>
															</td>
														</tr>
													<?php }  ?>
												</table>
												<input id="cgFrm_approved" name="approved" type="hidden" readonly="readonly" value="2" />
											</div>
											<div class="customer-general-detail">
												<table class="form customer-detail">
													<tr><td colspan=2><h3 class="form-title">Bank Account Information</h3></td></tr>
													<?php foreach ($form['bank'] as $name => $field) { ?>
														<tr>
															<td><?php echo ${'entry_' . $name}; ?></td>
															<td id="payment_<?php echo $name; ?>">
																<?php echo $field; ?>
																<?php if (!empty($error[$name])) { ?>
																	<div class="field_err"><?php echo $error[$name]; ?></div>
																<?php } ?>
															</td>
														</tr>
													<?php }  ?>
												</table>
											</div>
											<div class="customer-general-detail">
												<table class="form customer-detail">
													<tr><td><h3 class="form-title">Remarks</h3></td></tr>
													<?php foreach ($form['remark'] as $name => $field) { ?>
														<tr>
															<td colspan='2' id="payment_<?php echo $name; ?>">
																<?php echo $field; ?>
																<?php if (!empty($error[$name])) { ?>
																	<div class="field_err"><?php echo $error[$name]; ?></div>
																<?php } ?>
															</td>
														</tr>
													<?php }  ?>
												</table>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
					<!-- content right start -->
					<div id="compareDiv" class="cont_left percent-50">
						<div class="cont_right">
							<div class="cont_mid">
								<div style="display: inline-block; width: 100%;">
									<div id="form">
										<div id="tab_general">
											<div class="customer-general-detail">
												<table class="form customer-detail customer-detail-right">
													<tr><td><h3 class="form-title">Personal Details</h3></td></tr>
													<?php foreach ($form['compare_fields'] as $name => $field) { ?>
														<tr>
															<td><?php echo ${'entry_' . $name}; ?></td>
															<td id="payment_<?php echo $name; ?>">
																<?php echo $field; ?>
																<?php if (!empty($error[$name])) { ?>
																	<div class="field_err"><?php echo $error[$name]; ?></div>
																<?php } ?>
															</td>
														</tr>
													<?php }  ?>
												</table>
											</div>
											<div class="customer-general-detail">
												<table class="form customer-detail">
													<tr><td colspan=2><h3 class="form-title">Bank Account Information</h3></td></tr>
													<?php foreach ($form['compare_bank'] as $name => $field) { ?>
														<tr>
															<td><?php echo ${'entry_' . $name}; ?></td>
															<td id="payment_<?php echo $name; ?>">
																<?php echo $field; ?>
																<?php if (!empty($error[$name])) { ?>
																	<div class="field_err"><?php echo $error[$name]; ?></div>
																<?php } ?>
															</td>														
														</tr>
													<?php }  ?>
												</table>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
					<!-- content right end -->
					<div class="bottom_left">
						<div class="bottom_right">
							<div class="bottom_mid"></div>
						</div>
					</div>
				</div>
				</form>
			</div>
		</div>
	</div>

</div>

<div id="confirm_account_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<form id="confirm-form">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><span class="title">Please choose one of this customer</span></h3>
  </div>
  <div class="modal-body">
		<!-- data dari ajax -->
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <span id="confirm-account" class="btn btn-primary">Save changes</span>
  </div>
</form>
</div>