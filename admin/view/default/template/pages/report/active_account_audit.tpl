<?php if ($error_warning) { ?>
<div class="warning alert alert-error"><?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($success) { ?>
<div class="success alert alert-success"><?php echo $success; ?></div>
<?php } ?>

<div class="contentBox">
  <div class="cbox_tl"><div class="cbox_tr"><div class="cbox_tc">
    <div class="heading icon_title_customer"><?php echo $column_activation_date; ?></div>
	<?php
	if ( !empty($search_form) ) {
	    echo '<div class="filter">';
	    echo $search_form['form_open'];
	    foreach ($search_form['fields'] as $f) echo $f;
	  	echo '<button type="submit" class="btn_standard">'.$search_form['submit'].'</button>';
	  	echo '<button type="reset" class="btn_standard">'.$search_form['reset'].'</button>';
	    echo '</form>';
	    echo '</div>';
	}
	?>
  </div></div></div>
  <div class="cbox_cl"><div class="cbox_cr"><div class="cbox_cc">
    <?php echo $listing_grid; ?>
  </div></div></div>
  <div class="cbox_bl"><div class="cbox_br"><div class="cbox_bc"></div></div></div>
</div>