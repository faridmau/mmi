<?php
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' ) || !IS_ADMIN) {
	header ( 'Location: static_pages/' );
}
class ModelUserUser extends Model {
	public function addUser($data) {
		
		$params['username'] = "'".$this->db->escape($data['username'])."'";
		$params['password'] = "'".$this->db->escape(AEncryption::getHash($data['password']))."'";
		$params['firstname'] = "'".$this->db->escape($data['firstname'])."'";
		$params['lastname'] = "'".$this->db->escape($data['lastname'])."'";
		$params['email'] = "'".$this->db->escape($data['email'])."'";
		$params['user_group_id'] = "'".$this->db->escape((int)$data['user_group_id'])."'";
		$params['status'] = "'".$this->db->escape((int)$data['status'])."'";
		$date = date('Y-m-d h:i:s');
		$params['date_added'] = "'".$date."'";
		
		$audit_log=array(
      "IP" => $this->request->server['REMOTE_ADDR'],
      "user_login" => $this->session->data['user_id'],
      "Activity" => "Add User",
      "login" => $this->session->data['user_last_login'],
      "logout" => ''
    );
    	
    $this->db->insert_with_trail($this->db->table("users"),$params,$audit_log);

		return $this->db->getLastId();
	}
	
	public function editUser($user_id, $data) {

		$fields = array('username', 'firstname', 'lastname', 'email', 'user_group_id', 'status',);
		$update = array();
		
		$audit_log=array(
      "IP" => $this->request->server['REMOTE_ADDR'],
      "user_login" => $this->session->data['user_id'],
      "Activity" => "Edit User",
      "login" => $this->session->data['user_last_login'],
      "logout" => ''
    );

		foreach ( $fields as $f ) {
			if ( isset($data[$f]) )
				$params[$f] = "'".$this->db->escape($data[$f])."'";
				// $update[] = "$f = '".$this->db->escape($data[$f])."'";
		}

		if ( !empty($data['password']) )
				$params['password'] = "'".$this->db->escape(AEncryption::getHash($data['password']))."'";

			$where = "user_id = '" . (int)$user_id . "'";
				//$update[] = "password = '". $this->db->escape(AEncryption::getHash($data['password'])) ."'";

		if ( !empty($params) ){
			$this->db->update_with_trail($this->db->table("users"),$params,$where,$audit_log);
			/*$sql = "UPDATE `" . DB_PREFIX . "users` SET ". implode(',', $update) ." WHERE user_id = '" . (int)$user_id . "'";
			$this->db->query( $sql );*/
		}
	}
	
	public function deleteUser($user_id) {
		$audit_log=array(
      "IP" => $this->request->server['REMOTE_ADDR'],
      "user_login" => $this->session->data['user_id'],
      "Activity" => "Delete User",
      "login" => $this->session->data['user_last_login'],
      "logout" => ''
    );

    $where = "user_id = '" . (int)$user_id . "'";
		$this->db->delete_with_trail($this->db->table("users"), $where, $audit_log);
		// $this->db->query("DELETE FROM `" . DB_PREFIX . "users` WHERE user_id = '" . (int)$user_id . "'");
	}
	
	public function getUser($user_id) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "users` WHERE user_id = '" . (int)$user_id . "'");
	
		return $query->row;
	}
	
	public function getUsers($data = array(), $mode = 'default') {
		if ($mode == 'total_only') {
			$sql = "SELECT count(*) as total FROM `" . DB_PREFIX . "users`";
		} else {
			$sql = "SELECT * FROM `" . DB_PREFIX . "users`";		
		}
		if ( !empty($data['subsql_filter']) )
			$sql .= " WHERE ".$data['subsql_filter'];

	    //If for total, we done bulding the query
		if ($mode == 'total_only') {
	    	$query = $this->db->query($sql);
	    	return $query->row['total'];
	    }

		$sort_data = array(
			'username',
			'user_group_id',
			'status',
			'date_added'
		);	
			
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY username";	
		}
			
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}			
			
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
			
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
			
		$query = $this->db->query($sql);
	
		return $query->rows;
	}

	public function getTotalUsers($data = array()) {
 		return $this->getUsers($data, 'total_only');
	}

	public function getTotalUsersByGroupId($user_group_id) {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "users` WHERE user_group_id = '" . (int)$user_group_id . "'");
		
		return $query->row['total'];
	}
}
?>