<?php
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' ) || !IS_ADMIN) {
	header ( 'Location: static_pages/' );
}

/**
 * Class ModelSaleCustomerProfile
 */
class ModelSaleCustomerProfile extends Model 
{
	public function addCustomerProfile($data,$table="customer_profiles") {
      	$sql = "INSERT INTO " . $this->db->table($table) . "
      	                SET cust_profile_id = '" . $this->db->escape($data['cust_profile_id']) . "',
      	                	  cif = '" . $this->db->escape($data['cif']) . "',
      	                    holder_id = '" . $this->db->escape($data['holder_id']) . "',
      	                    first_name = '" . $this->db->escape($data['first_name']) . "',
      	                    last_name = '" . $this->db->escape($data['last_name']) . "',
      	                    full_name = '" . $this->db->escape($data['full_name']) . "',
      	                    domicile_address = '" . $this->db->escape($data['domicile_address']) . "',
      	                    domicile_city = '" . $this->db->escape($data['domicile_city']) . "',
      	                    domicile_postal_code = '" . $this->db->escape($data['domicile_postal_code']) . "',
      	                    domicile_country = '" . $this->db->escape($data['domicile_country']) . "',
      	                    tax_id_no = '" . $this->db->escape($data['tax_id_no']) . "',
      	                    tax_id_reg_date = '" . $this->db->escape($data['tax_id_reg_date']) . "',
      	                    phone_no = '" . $this->db->escape($data['phone_no']) . "',
      	                    mobile_phone_no = '" . $this->db->escape($data['mobile_phone_no']) . "',
      	                    email = '" . $this->db->escape($data['email']) . "',
      	                    fax_no = '" . $this->db->escape($data['fax_no']) . "',
      	                    place_of_birth = '" . $this->db->escape($data['place_of_birth']) . "',
      	                    date_of_birth = '" . $this->db->escape($data['date_of_birth']) . "',
      	                    gender = '" . $this->db->escape($data['gender']) . "',
      	                    marital_status = '" . $this->db->escape($data['marital_status']) . "',
      	                    id_type = '" . $this->db->escape($data['id_type']) . "',
      	                    id_number = '" . $this->db->escape($data['id_number']) . "',
      	                    id_expiry_date = '" . $this->db->escape($data['id_expiry_date']) . "',
      	                    nationality = '" . $this->db->escape($data['nationality']) . "',
      	                    residency_country = '" . $this->db->escape($data['residency_country']) . "',
      	                    residency_status = '" . $this->db->escape($data['residency_status']) . "',
      	                    residency_status_txt = '" . $this->db->escape($data['residency_status_txt']) . "',
      	                    occupation = '" . $this->db->escape($data['occupation']) . "',
      	                    occupation_txt = '" . $this->db->escape($data['occupation_txt']) . "',
      	                    company_name = '" . $this->db->escape($data['company_name']) . "',
      	                    company_type = '" . $this->db->escape($data['company_type']) . "',
      	                    education = '" . $this->db->escape($data['education']) . "',
      	                    spouse_name = '" . $this->db->escape($data['spouse_name']) . "',
      	                    spouse_occupation = '" . $this->db->escape($data['spouse_occupation']) . "',
      	                    mother_name = '" . $this->db->escape($data['mother_name']) . "',
      	                    father_name = '" . $this->db->escape($data['father_name']) . "',
      	                    inheritor = '" . $this->db->escape($data['inheritor']) . "',
      	                    inheritor_relationship = '" . $this->db->escape($data['inheritor_relationship']) . "',
      	                    gross_income = '" . $this->db->escape($data['gross_income']) . "',
      	                    source_of_income = '" . $this->db->escape($data['source_of_income']) . "',
      	                    source_of_income_txt = '" . $this->db->escape($data['source_of_income_txt']) . "',
      	                    additional_income = '" . $this->db->escape($data['additional_income']) . "',
      	                    add_source_of_income = '" . $this->db->escape($data['add_source_of_income']) . "',
      	                    source_of_fund = '" . $this->db->escape($data['source_of_fund']) . "',
      	                    source_of_fund_txt = '" . $this->db->escape($data['source_of_fund_txt']) . "',
      	                    inv_objective = '" . $this->db->escape($data['inv_objective']) . "',
      	                    inv_objective_txt = '" . $this->db->escape($data['inv_objective_txt']) . "',
      	                    bank_name = '" . $this->db->escape($data['bank_name']) . "',
                            approved = '" . $this->db->escape($data['approved']) . "',
      	                    bank_address = '" . $this->db->escape($data['bank_address']) . "',
      	                    bank_account_name = '" . $this->db->escape($data['bank_account_name']) . "',
      	                    bank_account_no = '" . $this->db->escape($data['bank_account_no']) . "',
      	                    risk_profile = '" . $this->db->escape($data['risk_profile']) . "',
      	                    risk_profile_last_update = '" . $this->db->escape($data['risk_profile_last_update']) . "'";
      	
		//uncomment this to check error
      	// echo "sql:".$sql;exit;

      	$this->db->query($sql);
      	
      	$profile_id = $this->db->getLastId();
      	
		return $profile_id;
	}
  public function updateCustomerProfile($data) {

    $params['cif'] = "'".$this->db->escape($data['cif'])."'";
    $params['holder_id'] = "'".$this->db->escape($data['holder_id'])."'";
    $params['first_name'] = "'".$this->db->escape($data['first_name'])."'";
    $params['last_name'] = "'".$this->db->escape($data['last_name'])."'";
    $params['full_name'] = "'".$this->db->escape($data['first_name']).' '.$this->db->escape($data['last_name'])."'";
    $params['domicile_address'] = "'".$this->db->escape($data['domicile_address'])."'";
    $params['domicile_city'] = "'".$this->db->escape($data['domicile_city'])."'";
    $params['domicile_postal_code'] = "'".$this->db->escape($data['domicile_postal_code'])."'";
    $params['domicile_country'] = "'".$this->db->escape($data['domicile_country'])."'";
    $params['correspondence_address'] = "'".$this->db->escape($data['correspondence_address'])."'";
    $params['correspondence_city'] = "'".$this->db->escape($data['correspondence_city'])."'";
    $params['correspondence_postal_code'] = "'".$this->db->escape($data['correspondence_postal_code'])."'";
    $params['correspondence_country'] = "'".$this->db->escape($data['correspondence_country'])."'";
    $params['tax_id_no'] = "'".$this->db->escape($data['tax_id_no'])."'";
    $params['tax_id_reg_date'] = "'".$this->db->escape($data['tax_id_reg_date'])."'";
    $params['phone_no'] = "'".$this->db->escape($data['phone_no'])."'";
    $params['mobile_phone_no'] = "'".$this->db->escape($data['mobile_phone_no'])."'";
    $params['email'] = "'".$this->db->escape($data['email'])."'";
    $params['fax_no'] = "'".$this->db->escape($data['fax_no'])."'";
    $params['place_of_birth'] = "'".$this->db->escape($data['place_of_birth'])."'";
    $params['date_of_birth'] = "'".$this->db->escape($data['date_of_birth'])."'";
    $params['gender'] = "'".$this->db->escape($data['gender'])."'";
    $params['marital_status'] = "'".$this->db->escape($data['marital_status'])."'";
    $params['religion'] = "'".$this->db->escape($data['religion'])."'";
    $params['id_number'] = "'".$this->db->escape($data['id_number'])."'";
    $params['id_type'] = "'".$this->db->escape($data['id_type'])."'";
    $params['id_expiry_date'] = "'".$this->db->escape($data['id_expiry_date'])."'";
    $params['nationality'] = "'".$this->db->escape($data['nationality'])."'";
    $params['residency_status'] = "'".$this->db->escape($data['residency_status'])."'";
    $params['residency_country'] = "'".$this->db->escape($data['residency_country'])."'";
    $params['occupation'] = "'".$this->db->escape($data['occupation'])."'";
    $params['company_name'] = "'".$this->db->escape($data['company_name'])."'";
    $params['company_type'] = "'".$this->db->escape($data['company_type'])."'";
    $params['education'] = "'".$this->db->escape($data['education'])."'";
    $params['spouse_name'] = "'".$this->db->escape($data['spouse_name'])."'";
    $params['spouse_occupation'] = "'".$this->db->escape($data['spouse_occupation'])."'";
    $params['mother_name'] = "'".$this->db->escape($data['mother_name'])."'";
    $params['father_name'] = "'".$this->db->escape($data['father_name'])."'";
    $params['inheritor'] = "'".$this->db->escape($data['inheritor'])."'";
    $params['inheritor_relationship'] = "'".$this->db->escape($data['inheritor_relationship'])."'";
    $params['bank_account_name'] = "'".$this->db->escape($data['bank_account_name'])."'";
    $params['bank_account_no'] = "'".$this->db->escape($data['bank_account_no'])."'";
    $params['bank_name'] = "'".$this->db->escape($data['bank_name'])."'";
    $params['bank_address'] = "'".$this->db->escape($data['bank_address'])."'";
    $params['gross_income'] = "'".$this->db->escape($data['gross_income'])."'";
    $params['source_of_income'] = "'".$this->db->escape($data['source_of_income'])."'";
    $params['additional_income'] = "'".$this->db->escape($data['additional_income'])."'";
    $params['add_source_of_income'] = "'".$this->db->escape($data['add_source_of_income'])."'";
    $params['source_of_fund'] = "'".$this->db->escape($data['source_of_fund'])."'";
    $params['inv_objective'] = "'".$this->db->escape($data['inv_objective'])."'";
    $params['approved_by'] = "'".$this->session->data['user_id']."'";
    $params['approved_datetime'] = "'".date("Y-m-d H:i:s")."'";
    

    $audit_log=array(
      "IP" => $this->request->server['REMOTE_ADDR'],
      "user_login" => $this->session->data['user_id'],
      "Activity" => "Edit Account Approval",
      "login" => $this->session->data['user_last_login'],
      "logout" => $this->session->data['logout_time']
    );

    //uncomment this to check error
        // echo "sql:".$sql;exit;
    $where = "holder_id = '" . $this->db->escape($data['holder_id']) . "'";
    $this->db->update_with_trail($this->db->table('customer_profiles'),$params,$where,$audit_log);
    $this->db->query("UPDATE " . $this->db->table("customer_profiles") . "
                    SET updated_datetime = now()
                    WHERE holder_id = '" . $this->db->escape($data['holder_id']) . "'");
  }

  public function updateFieldCustomer($field,$value,$holder_id){
    if(!is_null($value)){
      $sql = "UPDATE ".$this->db->table('customer_profiles')." SET $field = '". $this->db->escape($value) ."'";
    }else{
      $sql = "UPDATE ".$this->db->table('customer_profiles')." SET $field = NULL ";
    }
    $sql .= " WHERE holder_id = '". $this->db->escape($holder_id) ."'";
    $this->db->query($sql);
  }

	/**
	 * @param int $holder_id
	 * @return array
	 */
	public function getCustomer($holder_id, $table = "customer_profiles") {
		$sql = "SELECT DISTINCT *
                   FROM " . $this->db->table($table) . "
                   WHERE holder_id = '" . $holder_id . "'";

    $query = $this->db->query($sql);
		$result_row = $this->dcrypt->decrypt_data($query->row, $table);
		return $result_row;
	}

  /**
   * @return array
   */
  public function getCustomersEdit() {
    $sql = "SELECT DISTINCT cp.updated_datetime, c.holder_id, c.customer_id, c.cif, CONCAT(c.firstname,' ',c.lastname) as name
                   FROM " . $this->db->table('customers') . " c
                   INNER JOIN ". $this->db->table('customer_profiles_temp') ." cp
                   ON (c.holder_id = cp.holder_id)
                   WHERE c.approved = 1 ORDER BY updated_datetime ASC";
    // echo $sql;exit;
    $query = $this->db->query($sql);
    $result_rows = $this->dcrypt->decrypt_data($query->rows, "customer");
    return $result_rows;
  }

  /**
   * @return array
   */
  public function getCustomerEdit($customer_id) {
    $sql = "SELECT DISTINCT cpt.*
                   FROM " . $this->db->table('customers') . " c
                   INNER JOIN ". $this->db->table('customer_profiles_temp') ." cpt
                   ON (c.holder_id = cpt.holder_id)
                   WHERE c.approved = 1 AND cpt.approved = '0' AND c.customer_id = '".$customer_id."'";
    
    $query = $this->db->query($sql);
    $result_row = $this->dcrypt->decrypt_data($query->row, "customer_profiles");
    // $this->showDebug($query);
    // $result_rows = $query->rows();
    return $result_row;
  }

   /**
    * @param int $holder_id
    * @return array
    */
   public function delCustomer($holder_id, $table = "customer_profiles") {
      $sql = "DELETE
               FROM " . $this->db->table($table) . "
               WHERE holder_id = '" . $holder_id . "'";
      $query = $this->db->query($sql);
   }

  public function approve($holder_id) {
    $this->db->query("UPDATE " . $this->db->table("customer_profiles") . "
              SET approved = '2'
                    WHERE holder_id = '" . $holder_id . "'");
  }
  public function cloneCustomerProfile($holder_id) {
    $sql = "UPDATE " . $this->db->table("customer_profiles") . " p,
      " . $this->db->table("customer_profiles_temp") . " temp 
      SET p.cif=temp.cif,p.holder_id=temp.holder_id,p.first_name=temp.first_name,p.last_name=temp.last_name,p.full_name=temp.full_name,p.domicile_address=temp.domicile_address,p.domicile_city=temp.domicile_city,p.domicile_postal_code=temp.domicile_postal_code,p.domicile_country=temp.domicile_country,p.tax_id_no=temp.tax_id_no,p.tax_id_reg_date=temp.tax_id_reg_date,p.phone_no=temp.phone_no,p.mobile_phone_no=temp.mobile_phone_no,p.email=temp.email,p.fax_no=temp.fax_no,p.place_of_birth=temp.place_of_birth,p.date_of_birth=temp.date_of_birth,p.gender=temp.gender,p.marital_status=temp.marital_status,p.id_type=temp.id_type,p.id_number=temp.id_number,p.id_expiry_date=temp.id_expiry_date,p.nationality=temp.nationality,p.residency_country=temp.residency_country,p.residency_status=temp.residency_status,p.residency_status_txt=temp.residency_status_txt,p.occupation=temp.occupation,p.occupation_txt=temp.occupation_txt,p.company_name=temp.company_name,p.company_type=temp.company_type,p.education=temp.education,p.spouse_name=temp.spouse_name,p.spouse_occupation=temp.spouse_occupation,p.mother_name=temp.mother_name,p.father_name=temp.father_name,p.inheritor=temp.inheritor,p.inheritor_relationship=temp.inheritor_relationship,p.gross_income=temp.gross_income,p.source_of_income=temp.source_of_income,p.source_of_income_txt=temp.source_of_income_txt,p.additional_income=temp.additional_income,p.add_source_of_income=temp.add_source_of_income,p.source_of_fund=temp.source_of_fund,p.source_of_fund_txt=temp.source_of_fund_txt,p.inv_objective=temp.inv_objective,p.inv_objective_txt=temp.inv_objective_txt,p.bank_name=temp.bank_name,p.bank_address=temp.bank_address,p.bank_account_name=temp.bank_account_name,p.bank_account_no=temp.bank_account_no,p.risk_profile=temp.risk_profile,p.risk_profile_last_update=temp.risk_profile_last_update,p.correspondence_address=temp.correspondence_address,p.correspondence_postal_code=temp.correspondence_postal_code,p.correspondence_country=temp.correspondence_country,p.correspondence_city=temp.correspondence_city,p.religion=temp.religion,p.updated_datetime=temp.updated_datetime
    WHERE p.holder_id = '".$holder_id."'";
    // echo $sql;exit;
    $this->db->query($sql);
  }
}