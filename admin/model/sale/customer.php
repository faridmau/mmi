<?php
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' ) || !IS_ADMIN) {
	header ( 'Location: static_pages/' );
}
/**
 * Class ModelSaleCustomer
 */
class ModelSaleCustomer extends Model {
	/**
	 * @param $data
	 * @return int
	 */
	public function addCustomer($data) {
		//encrypt customer data
		$key_sql = '';
		if ( $this->dcrypt->active ) {
			$data = $this->dcrypt->encrypt_data($data, 'customers');
			$key_sql = ", key_id = '" . (int)$data['key_id'] . "'";
		}
      	$this->db->query("INSERT INTO " . $this->db->table("customers") . "
      	                SET loginname = '" . $this->db->escape($data['loginname']) . "',
      	                	firstname = '" . $this->db->escape($data['firstname']) . "',
      	                    lastname = '" . $this->db->escape($data['lastname']) . "',
      	                    email = '" . $this->db->escape($data['email']) . "',
      	                    telephone = '" . $this->db->escape($data['telephone']) . "',
      	                    fax = '" . $this->db->escape($data['fax']) . "',
      	                    newsletter = '" . (int)$data['newsletter'] . "',
      	                    customer_group_id = '" . (int)$data['customer_group_id'] . "',
      	                    password = '" . $this->db->escape(AEncryption::getHash($data['password'])) . "',
      	                    status = '" . (int)$data['status'] . "',
      	                    approved = '" . (int)$data['approved'] . "'"
      	                    .$key_sql . ",
      	                    date_added = NOW()");
      	
      	$customer_id = $this->db->getLastId();
      	
      	if (isset($data['addresses'])) {		
      		foreach ($data['addresses'] as $address) {
      			//encrypt address data
				$key_sql = '';
				if ( $this->dcrypt->active ) {
					$address = $this->dcrypt->encrypt_data($address, 'addresses');
					$key_sql = ", key_id = '" . (int)$address['key_id'] . "'";
				}
      			$this->db->query("INSERT INTO " . $this->db->table("addresses") . "
      			                  SET customer_id = '" . (int)$customer_id . "',
      			                        firstname = '" . $this->db->escape($address['firstname']) . "',
      			                        lastname = '" . $this->db->escape($address['lastname']) . "',
      			                        company = '" . $this->db->escape($address['company']) . "',
      			                        address_1 = '" . $this->db->escape($address['address_1']) . "',
      			                        address_2 = '" . $this->db->escape($address['address_2']) . "',
      			                        city = '" . $this->db->escape($address['city']) . "',
      			                        postcode = '" . $this->db->escape($address['postcode']) . "',
      			                        country_id = '" . (int)$address['country_id'] . "'"
      			                        .$key_sql . ", 
      			                        zone_id = '" . (int)$address['zone_id'] . "'");
			}
		}

		return $customer_id;
	}

	/**
	 * @param $data
	 * @return int
	 */
	public function addCustomerAudit($data) {

		$params['customer_id'] = "'".$this->db->escape($data['customer_id'])."'";
		$params['activation_datetime'] = "'".$this->db->escape(date('Y-m-d h:i:s'))."'";
		$params['activated_by'] = "'".$this->db->escape($this->session->data['user_id'])."'";
		$params['status'] = 5;

    $params['first_name'] = "'".$this->db->escape($data['firstname'])."'";
    $params['first_name_backend'] = "'".$this->db->escape($data['cfirstname'])."'";
    $params['last_name'] = "'".$this->db->escape($data['lastname'])."'";
    $params['last_name_backend'] = "'".$this->db->escape($data['clastname'])."'";

    $params['email'] = "'".$this->db->escape($data['email'])."'";
    $params['email_backend'] = "'".$this->db->escape($data['cemail'])."'";
    $params['id_number'] ="'".$this->db->escape($data['id_number'])."'";
    $params['id_number_backend'] ="'".$this->db->escape($data['cid_number'])."'";

    $params['telephone'] = "'".$this->db->escape($data['telephone']) ."'";
    $params['telephone_backend'] = "'".$this->db->escape($data['ctelephone']) ."'";

    $params['id_type'] = "'".$this->db->escape($data['id_type']) ."'";
    $params['id_type_backend'] = "'".$this->db->escape($data['cid_type']) ."'";
    $params['cif'] = "'".$this->db->escape($data['cif']) . "'";
    $params['cif_backend'] = "'".$this->db->escape($data['ccif']) . "'";

    $params['bank_account_name'] = "'" . $this->db->escape($data['bank_account_name'])."'";
    $params['bank_account_name_backend'] = "'" . $this->db->escape($data['cbank_account_name'])."'";
    $params['bank_name'] = "'".$this->db->escape($data['bank_name'])."'";
    $params['bank_name_backend'] = "'".$this->db->escape($data['cbank_name'])."'";

    $params['bank_account_no'] = "'".$this->db->escape($data['cbank_account_no'])."'";
    $params['bank_account_no_backend'] = "'".$this->db->escape($data['cbank_account_no'])."'";

    /*$params['telephone'] = "'".$this->db->escape($data['ctelephone'])."'";
    $params['approved'] = "'".(int)$data['approved']."'";
    $params['status'] = "'".(int)$data['status']."'".$key_sql."";
    $params['holder_id'] = "'".$this->db->escape($data['ccif']) . "'";
    $params['ip'] = "'".$this->db->escape($data['ip'])."'";
    $params['date_added'] = "NOW()";*/
     // user_login | IP | Login date | Logout date | Activity | Data Before | Data After
    
    $audit_log=array(
      "IP" => $this->request->server['REMOTE_ADDR'],
      "user_login" => $this->session->data['user_id'],
      "Activity" => "Account Approval Audit",
      "login" => $this->session->data['user_last_login'],
      "logout" => $this->session->data['logout_time']
    );

    $this->db->insert_with_trail($this->db->table("activation_audit"),$params,$audit_log);
  	
  	$customer_id = $this->db->getLastId();
      	
		return $customer_id;
	}

	public function updateCustomerAudit($data) {
	
    $params['firstname'] = "'".$this->db->escape($data['first_name'])."'";
    $params['lastname'] = "'".$this->db->escape($data['last_name'])."'";
    $params['loginname'] = "'".$this->db->escape($data['email'])."'";
    $params['email'] = "'".$this->db->escape($data['email'])."'";
    $params['bank_account_name'] = "'".$this->db->escape($data['bank_account_name'])."'";
    $params['bank_account_no'] = "'".$this->db->escape($data['bank_account_no'])."'";
    $params['bank_name'] = "'".$this->db->escape($data['bank_name'])."'";
    $params['id_number'] = "'".$this->db->escape($data['id_number'])."'";
    $params['id_type'] = "'".$this->db->escape($data['id_type'])."'";
    $params['fax'] = "'".$this->db->escape($data['fax_no'])."'";
    $params['telephone'] = "'".$this->db->escape($data['phone_no'])."'";

    $where = "holder_id = '" . $this->db->escape($data['holder_id']) . "'";
    $audit_log=array(
      "IP" => $this->request->server['REMOTE_ADDR'],
      "user_login" => $this->session->data['user_id'],
      "Activity" => "Edit Account Approval",
      "login" => $this->session->data['user_last_login'],
      "logout" => $this->session->data['logout_time']
    );

    $this->db->update_with_trail($this->db->table('customers'),$params,$where,$audit_log);
	}


	public function updateFieldCustomer($field,$value,$holder_id){
    if(!is_null($value)){
      $sql = "UPDATE ".$this->db->table('customer_profiles')." SET $field = '". $this->db->escape($value) ."'";
    }else{
      $sql = "UPDATE ".$this->db->table('customer_profiles')." SET $field = NULL ";
    }
    $sql .= " WHERE holder_id = '". $this->db->escape($holder_id) ."'";
    $this->db->query($sql);
  }

	/**
	 * @param $data
	 * @return int
	 */
	public function addCustomerHistory($data) {
		//encrypt customer data
		$key_sql = '';
		if ( $this->dcrypt->active ) {
			$data = $this->dcrypt->encrypt_data($data, 'customers');
			$key_sql = ", key_id = '" . (int)$data['key_id'] . "'";
		}

  	$params['loginname'] = "'".$this->db->escape($data['cemail'])."'";
    $params['firstname'] = "'".$this->db->escape($data['cfirstname'])."'";
    $params['lastname'] = "'".$this->db->escape($data['clastname'])."'";
    $params['email'] = "'".$this->db->escape($data['cemail'])."'";
    $params['telephone'] = "'".$this->db->escape($data['ctelephone'])."'";
    // $params['fax'] = "'".$this->db->escape($data['fax'])."'";
    // $params['password'] = "'".$this->db->escape(AEncryption::getHash($data['password']))."'";
    // $params['newsletter'] = (int)$data['newsletter'];
    // $params['customer_group_id'] = "'".(int)$data['customer_group_id']."'";
    $params['approved'] = "'".(int)$data['approved']."'";
    $params['status'] = "'".(int)$data['status']."'".$key_sql."";
    $params['id_number'] ="'".$this->db->escape($data['cid_number'])."'";
    $params['id_type'] = "'".$this->db->escape($data['cid_type']) ."'";
    $params['cif'] = "'".$this->db->escape($data['ccif']) . "'";
    $params['holder_id'] = "'".$this->db->escape($data['ccif']) . "'";
    $params['bank_account_name'] = "'" . $this->db->escape($data['cbank_account_name'])."'";
    $params['bank_name'] = "'".$this->db->escape($data['cbank_name'])."'";
    $params['bank_account_no'] = "'".$this->db->escape($data['cbank_account_no'])."'";
    $params['ip'] = "'".$this->db->escape($data['ip'])."'";
    $params['date_added'] = "NOW()";
     // user_login | IP | Login date | Logout date | Activity | Data Before | Data After
    
    $audit_log=array(
      "IP" => $this->request->server['REMOTE_ADDR'],
      "user_login" => $this->session->data['user_id'],
      "Activity" => "Account Approval",
      "login" => $this->session->data['user_last_login'],
      "logout" => $this->session->data['logout_time']
    );

    $this->db->insert_with_trail($this->db->table("customers_history"),$params,$audit_log);
  	
  	$customer_id = $this->db->getLastId();
      	
		return $customer_id;
	}

	/* added by fazrin, untuk email approve*/
	public function keyReset($email, $key) {	
      	$this->db->query("UPDATE " . $this->db->table("customers") . " SET reset_key = '" . $this->db->escape($key) . "' WHERE email = '" . $this->db->escape($email) . "'");
	}

	/**
	 * @param int $customer_id
	 * @param array $data
	 */
	public function editCustomer($customer_id, $data) {
		//encrypt address data
		$key_sql = '';
		if ( $this->dcrypt->active ) {
			$data = $this->dcrypt->encrypt_data($data, 'customers');
			$key_sql = ", key_id = '" . (int)$data['key_id'] . "'";
		}

		$sql = "UPDATE " . $this->db->table("customers") . "
						SET firstname = '" . $this->db->escape($data['cfirstname']) . "',
							lastname = '" . $this->db->escape($data['clastname']) . "',
							email = '" . $this->db->escape($data['cemail']) . "',
							telephone = '" . $this->db->escape($data['ctelephone']) . "',
							id_number = '" . $data['cid_number'] . "',
							id_type = '" . $data['cid_type'] . "',
							cif = '" . $data['ccif'] . "',
							bank_account_name = '" . $data['cbank_account_name'] . "',
							bank_name = '" . $data['cbank_name'] . "',
							bank_account_no = '" . $data['cbank_account_no'] . "',
							status = '" . (int)$data['status'] . "'" .$key_sql . ", 
							approved = '" . (int)$data['approved'] . "'
						WHERE customer_id = '" . (int)$customer_id . "'";
		
		// echo $sql;exit;

		$this->db->query($sql);
	
      	if ($data['password']) {
        	$this->db->query("UPDATE " . $this->db->table("customers") . "
        	                  SET password = '" . $this->db->escape(AEncryption::getHash($data['password'])) . "'
        	                  WHERE customer_id = '" . (int)$customer_id . "'");
      	}
      	
      	$this->db->query("DELETE FROM " . $this->db->table("addresses") . " WHERE customer_id = '" . (int)$customer_id . "'");
      	
      	if (isset($data['addresses'])) {
      		foreach ($data['addresses'] as $address) {	
      			//encrypt address data
				$key_sql = '';
				if ( $this->dcrypt->active ) {
					$address = $this->dcrypt->encrypt_data($address, 'addresses');
					$key_sql = ", key_id = '" . (int)$address['key_id'] . "'";
				}
				$this->db->query("INSERT INTO " . $this->db->table("addresses"). "
								  SET customer_id = '" . (int)$customer_id . "',
								        firstname = '" . $this->db->escape($address['firstname']) . "',
								        lastname = '" . $this->db->escape($address['lastname']) . "',
								        company = '" . $this->db->escape($address['company']) . "',
								        address_1 = '" . $this->db->escape($address['address_1']) . "',
								        address_2 = '" . $this->db->escape($address['address_2']) . "',
								        city = '" . $this->db->escape($address['city']) . "',
								        postcode = '" . $this->db->escape($address['postcode']) . "',
								        country_id = '" . (int)$address['country_id'] . "'"
								        .$key_sql . ",
								        zone_id = '" . (int)$address['zone_id'] . "'");
			}
		}
	}

	/**
	 * @param int $customer_id
	 * @param array $data
	 */
	public function editCustomerHistory($cif, $data) {
		//encrypt address data
		$key_sql = '';
		if ( $this->dcrypt->active ) {
			$data = $this->dcrypt->encrypt_data($data, 'customers_history');
			$key_sql = ", key_id = '" . (int)$data['key_id'] . "'";
		}

		$sql = "UPDATE " . $this->db->table("customers_history") . "
						SET firstname = '" . $this->db->escape($data['cfirstname']) . "',
							lastname = '" . $this->db->escape($data['clastname']) . "',
							email = '" . $this->db->escape($data['cemail']) . "',
							telephone = '" . $this->db->escape($data['ctelephone']) . "',
							id_number = '" . $data['cid_number'] . "',
							id_type = '" . $data['cid_type'] . "',
							cif = '" . $data['ccif'] . "',
							bank_account_name = '" . $data['cbank_account_name'] . "',
							bank_name = '" . $data['cbank_name'] . "',
							bank_account_no = '" . $data['cbank_account_no'] . "',
							status = '" . (int)$data['status'] . "'" .$key_sql . ", 
							approved = '" . (int)$data['approved'] . "'
						WHERE cif = '" . $cif . "'";
		
		// echo $sql;exit;

		$this->db->query($sql);
	
      	if ($data['password']) {
        	$this->db->query("UPDATE " . $this->db->table("customers") . "
        	                  SET password = '" . $this->db->escape(AEncryption::getHash($data['password'])) . "'
        	                  WHERE customer_id = '" . (int)$customer_id . "'");
      	}
      	
      	$this->db->query("DELETE FROM " . $this->db->table("addresses") . " WHERE customer_id = '" . (int)$customer_id . "'");
      	
      	if (isset($data['addresses'])) {
      		foreach ($data['addresses'] as $address) {	
      			//encrypt address data
				$key_sql = '';
				if ( $this->dcrypt->active ) {
					$address = $this->dcrypt->encrypt_data($address, 'addresses');
					$key_sql = ", key_id = '" . (int)$address['key_id'] . "'";
				}
				$this->db->query("INSERT INTO " . $this->db->table("addresses"). "
								  SET customer_id = '" . (int)$customer_id . "',
								        firstname = '" . $this->db->escape($address['firstname']) . "',
								        lastname = '" . $this->db->escape($address['lastname']) . "',
								        company = '" . $this->db->escape($address['company']) . "',
								        address_1 = '" . $this->db->escape($address['address_1']) . "',
								        address_2 = '" . $this->db->escape($address['address_2']) . "',
								        city = '" . $this->db->escape($address['city']) . "',
								        postcode = '" . $this->db->escape($address['postcode']) . "',
								        country_id = '" . (int)$address['country_id'] . "'"
								        .$key_sql . ",
								        zone_id = '" . (int)$address['zone_id'] . "'");
			}
		}
	}

	/**
	 * @param int $customer_id
	 * @param string $field
	 * @param mixed $value
	 */
	public function editCustomerField($customer_id, $field, $value) {

		$data = array('loginname', 'firstname', 'lastname', 'email', 'telephone', 'fax', 'newsletter', 'customer_group_id', 'status', 'approved' );
		if ( in_array($field, $data) )

			if ( $this->dcrypt->active && in_array($field, $this->dcrypt->getEcryptedFields("customers")) ) {
				//check key_id to use 
				$query_key = $this->db->query("select key_id from " . $this->db->table("customers") . "
							  WHERE customer_id = '" . (int)$customer_id . "'");
				$key_id = $query_key->rows[0]['key_id'];		
				$value = $this->dcrypt->encrypt_field($value, $key_id);
			}
			$this->db->query("UPDATE " . $this->db->table("customers") . "
							  SET $field = '" . $this->db->escape($value) . "'
							  WHERE customer_id = '" . (int)$customer_id . "'");

      	if ($field == 'password') {
        	$this->db->query("UPDATE " . $this->db->table("customers") . "
        	                  SET password = '" . $this->db->escape(AEncryption::getHash($value)) . "'
        	                  WHERE customer_id = '" . (int)$customer_id . "'");
      	}
	}

	/**
	 * @param int $customer_id
	 * @return array
	 */
	public function getAddressesByCustomerId($customer_id) {
		$address_data = array();
		
		$query = $this->db->query("SELECT *
									FROM " . $this->db->table("addresses") . "
									WHERE customer_id = '" . (int)$customer_id . "'");
	
		foreach ($query->rows as $result) {
			$result = $this->dcrypt->decrypt_data($result, 'addresses');
			
			$this->load->model('localisation/country');
			$this->load->model('localisation/zone');
			
			$country_row = $this->model_localisation_country->getCountry($result['country_id']);						
			if ( $country_row ) {
				$country = $country_row['name'];
				$iso_code_2 = $country_row['iso_code_2'];
				$iso_code_3 = $country_row['iso_code_3'];
				$address_format = $country_row['address_format'];
			} else {
				$country = '';
				$iso_code_2 = '';
				$iso_code_3 = '';	
				$address_format = '';
			}
			
			$zone_row = $this->model_localisation_zone->getZone($result['zone_id']);			
			if ( $zone_row ) {
				$zone = $zone_row['name'];
				$code = $zone_row['code'];
			} else {
				$zone = '';
				$code = '';
			}		
		
			$address_data[] = array(
				'address_id'     => $result['address_id'],
				'firstname'      => $result['firstname'],
				'lastname'       => $result['lastname'],
				'company'        => $result['company'],
				'address_1'      => $result['address_1'],
				'address_2'      => $result['address_2'],
				'postcode'       => $result['postcode'],
				'city'           => $result['city'],
				'zone_id'        => $result['zone_id'],
				'zone'           => $zone,
				'zone_code'      => $code,
				'country_id'     => $result['country_id'],
				'country'        => $country,	
				'iso_code_2'     => $iso_code_2,
				'iso_code_3'     => $iso_code_3,
				'address_format' => $address_format
			);
		}		
		
		return $address_data;
	}

	/**
	 * @param int $customer_id
	 */
	public function deleteCustomer($customer_id) {
		$this->db->query("DELETE FROM " . $this->db->table("customers") . " WHERE customer_id = '" . (int)$customer_id . "'");
		$this->db->query("DELETE FROM " . $this->db->table("addresses") . " WHERE customer_id = '" . (int)$customer_id . "'");
	}

	/**
	 * @param int $customer_id
	 * @return array
	 */
	public function getCustomer($customer_id) {
		$sql = "SELECT DISTINCT *,
									(SELECT COUNT(order_id)
										FROM " . $this->db->table("orders") . "
										WHERE customer_id = '" . (int)$customer_id . "'
												AND order_status_id>0) as orders_count
								   FROM " . $this->db->table("customers") . "
								   WHERE customer_id = '" . (int)$customer_id . "'";
		$query = $this->db->query($sql);

		$result_row = $this->dcrypt->decrypt_data($query->row, 'customers');
		return $result_row;
	}


	/**
	 * @param int $customer_id
	 * @return array
	 */
	public function getCustomersAudit($data = array()) {
    $where = "";
    //filter
    if(!empty($data['filter'])){
      //date filter
      if($data['filter']['date1'] && $data['filter']['date2']){
        $where .= " AND DATE_FORMAT(activation_datetime,'%Y-%m-%d') BETWEEN DATE_FORMAT('".$data['filter']['date1']."','%Y-%m-%d') AND DATE_FORMAT('".$data['filter']['date2']."','%Y-%m-%d')";
      }
      
      //cif filter
      if($data['filter']['cif']){
        $where .= " AND cif like '%".$data['filter']['cif']."%'";
      }

    }

    //limit & offset
    if($data['limit']){
      $where .= " LIMIT ".$data['limit']." OFFSET ".$data['start'];
    }

    $sql = "SELECT * FROM ". $this->db->table("activation_audit") . 
                     " WHERE status = 1".$where;
    // var_dump($data);
    // echo $sql;exit();

    $query = $this->db->query($sql);
    return $query->rows;
  }

  /**
   * @param int $customer_id
   * @return array
   */
  public function getCustomerAudit($customer_id) {
    $sql = "SELECT DISTINCT * FROM " . $this->db->table("activation_audit") . "
								   WHERE customer_id = '" . (int)$customer_id . "'";
		$query = $this->db->query($sql);

		return $query->row;
	}

	public function getTotalCustomersAudit($data = array()) {
    $where = "";
    //filter
    if(!empty($data['filter'])){
      //date filter
      if($data['filter']['date1'] && $data['filter']['date2']){
        $where .= " AND DATE_FORMAT(activation_datetime,'%Y-%m-%d') BETWEEN DATE_FORMAT('".$data['filter']['date1']."','%Y-%m-%d') AND DATE_FORMAT('".$data['filter']['date2']."','%Y-%m-%d')";
      }
      
      //cif filter
      if($data['filter']['cif']){
        $where .= " AND cif like '%".$data['filter']['cif']."%'";
      }

    }

		$sql = "SELECT count(*) as total FROM " . $this->db->table("activation_audit") . 
									 " WHERE status = 1".$where;
		$query = $this->db->query($sql);
		return $query->row['total'];
	}

	/**
	 * @param int $customer_id
	 * @return array
	 */
	public function getCustomerHistory($cif) {
		$sql = "SELECT DISTINCT * FROM " . $this->db->table("customers_history") . "
								   WHERE cif = '" . $cif . "'";
								   // echo $sql;exit;
		$query = $this->db->query($sql);
		$result_row = $this->dcrypt->decrypt_data($query->row, 'customers_history');
		return $result_row;
	}

	public function getCustomerByCif($cif) {
		$sql = "SELECT DISTINCT * FROM " . $this->db->table("customers") . "
								   WHERE cif = '" . $cif . "'";
								   // echo $sql;exit;
		$query = $this->db->query($sql);
		$result_row = $this->dcrypt->decrypt_data($query->row, 'customers');
		return $result_row;
	}

	public function getCustomerByHolderId($holder_id) {
		$sql = "SELECT DISTINCT * FROM " . $this->db->table("customers") . "
								   WHERE holder_id = '" . $holder_id . "'";
								   // echo $sql;exit;
		$query = $this->db->query($sql);
		$result_row = $this->dcrypt->decrypt_data($query->row, 'customers');
		return $result_row;
	}

  public function checkHolderID($holder_id){
    $sql = "SELECT COUNT(*) total FROM " . $this->db->table("customers") . " WHERE holder_id = '".$holder_id."'";
    $query = $this->db->query($sql);
    return $query->row['total'];
  }

	/**
	 * @param string $email
	 * @return array
	 */
	public function getCustomerByEmail($email) {
		$sql = "SELECT * FROM " . $this->db->table("customers") . " WHERE LOWER(`email`) = LOWER('" . $this->db->escape($email) . "')";
		//assuming that data is not encrypted. Can not call these otherwise
		$query = $this->db->query($sql);
		return $query->row;
	}

	/**
	 * @param array $data
	 * @return array|int
	 */
	public function getTotalCustomers($data = array()) {
		return $this->getCustomers($data, 'total_only');
	}

	/**
	 * @param array $data
	 * @param string $mode
	 * @return array|int
	 */
	public function getCustomers($data = array(), $mode = 'default') {
		if ( $mode == 'total_only' && !$this->dcrypt->active ) {
			$sql = "SELECT COUNT(*) as total ";
		} else {
			$sql = "SELECT c.customer_id,
			  	c.firstname,
  				c.lastname,
  				c.loginname,
  				c.email,
  				c.status,
  				c.approved,
  				c.sync_datetime,
  				c.customer_group_id,
					c.date_added,
					c.cif,
					c.holder_id,
					c.id_number,
					CONCAT(c.firstname, ' ', c.lastname) AS name,
					cg.name AS customer_group
				";
		}
		if ( $mode != 'total_only'){
			$sql .= ", COUNT(o.order_id) as orders_count  ";
		}

		if ( $this->dcrypt->active ) {
			$sql .= ", c.key_id ";
		}

		$sql .= " FROM " . $this->db->table("customers") . " c	LEFT JOIN " . $this->db->table("customer_groups") . " cg ON (c.customer_group_id = cg.customer_group_id) ";
		if ( $mode != 'total_only'){
			$sql .= " LEFT JOIN " . $this->db->table("orders") . " o ON (c.customer_id = o.customer_id AND o.order_status_id>0) ";
		}

		/*
			Added by fazrin
			join ke table customer_profile_temp untuk mencari customer yg membutuhkan edit approval
		*/
		if($mode == "edit_approval" ){
			$sql .= " INNER JOIN " . $this->db->table("customer_profiles") . " cpt ON (c.cif = cpt.cif) ";
		} 

		// echo "$sql ";exit;

		$implode = array();
		$filter = (isset($data['filter']) ? $data['filter'] : array());

		if (has_value($filter['name'])) {
			$implode[] = "CONCAT(c.firstname, ' ', c.lastname) LIKE '%" . $this->db->escape($filter['name']) . "%' collate utf8_general_ci";
		}
	    //more specific login, last and first name search
		if (has_value($filter['loginname'])) {
			$implode[] = "LOWER(c.loginname) = LOWER('" .$this->db->escape($filter['loginname']) . "') collate utf8_general_ci";
		}
		if (has_value($filter['firstname'])) {
			$implode[] = "LOWER(c.firstname) LIKE LOWER('" .$this->db->escape($filter['firstname']) . "%') collate utf8_general_ci";
		}
		if (has_value($filter['lastname'])) {
			$implode[] = "LOWER(c.lastname) LIKE LOWER('" .$this->db->escape($filter['lastname']) . "%') collate utf8_general_ci";
		}
		//select differently if encrypted
		if ( !$this->dcrypt->active ) {
			if (has_value($filter['email'])) {
				$implode[] = "c.email LIKE '%" . $this->db->escape($filter['email']) . "%' collate utf8_general_ci";
			}
			if (has_value($filter['telephone'])) {
				$implode[] = "c.telephone LIKE '%" . $this->db->escape($filter['telephone']) . "%' collate utf8_general_ci";
			}
		}

		if (has_value($filter['cif'])) {
			$implode[] = "c.cif LIKE '%" .$this->db->escape($filter['cif']) . "%' collate utf8_general_ci";
		}
		
		if (has_value($filter['customer_group_id'])) {
			$implode[] = "cg.customer_group_id = '" . $this->db->escape($filter['customer_group_id']) . "'";
		}
		// select only subscribers (group + customers with subscription)
		if (has_value($filter['all_subscribers'])) {
			$implode[] = "( (c.newsletter=1 AND c.status = 1 AND c.approved = 1) OR
						(c.newsletter=1 AND cg.customer_group_id = '".(int)$this->getSubscribersCustomerGroupId()."'))";
		}

		// select only customers without newsletter subscribers
		if (has_value($filter['only_customers'])) {
			$implode[] = "cg.customer_group_id NOT IN (".(int)$this->getSubscribersCustomerGroupId().") ";
		}
		
		if (has_value($filter['status'])) {
			$implode[] = "c.status = '" . (int)$filter['status'] . "'";
		}	
		
		if (has_value($filter['approved'])) {
			$implode[] = "c.approved = '" . (int)$filter['approved'] . "'";
		}

    if (has_value($filter['customer_group_id'])) {
      $implode[] = "c.customer_group_id = '" . (int)$filter['customer_group_id'] . "'";
    }		
		
		if (has_value($filter['date_added'])) {
			$implode[] = "DATE(c.date_added) = DATE('" . $this->db->escape($filter['date_added']) . "')";
		}

		if (has_value($filter['date1']) && has_value($filter['date2'])) {
			$implode[] = "c.sync_datetime between DATE('" . $this->db->escape($filter['date1']) . "') AND DATE('" . $this->db->escape($filter['date2']) . "')";
		}
		// menampilkan data customer profile yang pending (approved=0)
		if($mode == "edit_approval" ){
			$implode[] = "cpt.approved = 0";
		}
		if ($implode) {
			$sql .= " WHERE " . implode(" AND ", $implode);
		}

		//If for total, we done bulding the query
		if ($mode == 'total_only' && !$this->dcrypt->active) {
			$query = $this->db->query($sql);
			return $query->row['total'];
		}
					
		$sort_data = array(
			'name',
			'c.date_added',
			'c.loginname',
			'c.lastname',
			'c.email',
			'customer_group',
			'c.status',
			'c.approved',
			'orders_count'
		);	

		if ( $mode != 'total_only'){
			$sql .= " GROUP BY c.customer_id,
						  	c.firstname,
			  				c.lastname,
			  				c.loginname,
			  				c.email,
			  				c.status,
			  				c.approved,
			  				c.customer_group_id,
							CONCAT(c.firstname, ' ', c.lastname),
							cg.name ";
		}



		//Total culculation for encrypted mode 
		// NOTE: Performance slowdown might be noticed or larger search results	
		if ( $mode != 'total_only' ) {
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY name";	
			}
				
			if (isset($data['order']) && (strtoupper($data['order']) == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
			
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}			
	
				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
				
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}			
		}	

		$query = $this->db->query($sql);
		$result_rows = $query->rows;
		if ( $this->dcrypt->active ) {
			if (has_value($filter['email'])) {
				$result_rows = $this->_filter_by_encrypted_field($result_rows, 'email', $filter['email']);
			}
			if (has_value($filter['telephone'])) {
				$result_rows = $this->_filter_by_encrypted_field($result_rows, 'telephone', $filter['telephone']);
			}			
		}		

		if ($mode == 'total_only') {
			//we get here only if in data encryption mode
			return count($result_rows);
		}
		//finaly decrypt data and return result
		for ($i = 0; $i < count($result_rows); $i++) {
			$result_rows[$i] = $this->dcrypt->decrypt_data($result_rows[$i], 'customers');	
		}
		
		return $result_rows;	
	}

  public function getPotentialInstallment($data = array(), $mode = 'default'){
    $language_id = $this->language->getLanguageID();
    $sql = "SELECT DISTINCT
              rmd.reminder_date,
              CONCAT(cust.firstname, ' ', cust.lastname) AS name,
              cust.cif,
              trx.trx_amount,
              trx.topup_date,
              proddesc.name as product_name
            FROM " . $this->db->table("reminder") . " rmd
            INNER JOIN " . $this->db->table("customers") . " cust
              ON rmd.customer_id = cust.customer_id
            INNER JOIN " . $this->db->table("order_products") . " trx
              ON rmd.trx_id = trx.order_product_id
            INNER JOIN " . $this->db->table("product_descriptions") . " proddesc
              ON trx.product_id = proddesc.product_id
              AND proddesc.language_id = ". (int)$language_id ;

      $filter = (isset($data['filter']) ? $data['filter'] : array());
      if (has_value($filter['date1']) && has_value($filter['date2'])) {
        $sql .= " WHERE rmd.reminder_date > DATE_ADD('" . $this->db->escape($filter['date1']) . "', INTERVAL -1 DAY)
              AND rmd.reminder_date < DATE_ADD('" . $this->db->escape($filter['date2']) . "', INTERVAL +1 DAY)";
      }
      
      if(has_value($filter['cif'])){
        $sql .= " AND cust.cif like '%".$filter['cif']."%'";
      }

      if(has_value($filter['name'])){
        $sql .= " AND CONCAT(cust.firstname, ' ', cust.lastname) like '%".$filter['name']."%'";
      }

      //Total culculation for encrypted mode 
      // NOTE: Performance slowdown might be noticed or larger search results 
      if ( $mode != 'total_only' ) {
        if (isset($data['sort'])) {  //&& in_array($data['sort'], $sort_data)
          $sql .= " ORDER BY " . $data['sort']; 
        } else {
          $sql .= " ORDER BY rmd.reminder_date desc, cust.firstname, proddesc.name"; 
        }
          
        if (isset($data['order']) && (strtoupper($data['order']) == 'DESC')) {
          $sql .= " DESC";
        } else {
          $sql .= " ASC";
        }
        
        if (isset($data['start']) || isset($data['limit'])) {
          if ($data['start'] < 0) {
            $data['start'] = 0;
          }     
    
          if ($data['limit'] < 1) {
            $data['limit'] = 20;
          } 
          
          $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }     
      } 

      // if(!$mode=='total_only'){
        // var_dump($sql, $data, $mode);exit();
      // }

      $query = $this->db->query($sql);
      $result_rows = $query->rows;

      if ($mode == 'total_only') {
        //we get here only if in data encryption mode
        return count($result_rows);
      }


      //finaly decrypt data and return result
      for ($i = 0; $i < count($result_rows); $i++) {
        $result_rows[$i] = $this->dcrypt->decrypt_data($result_rows[$i], 'customers');  
      }

      return $result_rows;  

  }

	/**
	 * @param array $data
	 * @param string $field
	 * @param mixed $value
	 * @return array
	 */
	private function _filter_by_encrypted_field($data, $field, $value) {
		if ( !count($data) ) {
			return array();
		}
		if ( !has_value($field) || !has_value($value) ) {
			return $data;
		}
		$result_rows = array(); 
		foreach ($data as $result) {
			if ( $this->dcrypt->active ) {
				$fvalue = $this->dcrypt->decrypt_field($result[$field], $result['key_id']);
			} else {
				$fvalue = $result[$field];
			}
			if ( !(strpos (strtolower($fvalue), strtolower($value)) === false) ) {
				$result_rows[] = $result;
			}
		}	
		return $result_rows;
	}

	/**
	 * @param int $customer_id
	 */
	public function approve($customer_id) {
		$this->db->query("UPDATE " . $this->db->table("customers") . "
						  SET approved = '1'
						        WHERE customer_id = '" . (int)$customer_id . "'");
	}
	public function fillRemark($customer_id,$remark) {
		$sql = "UPDATE " . $this->db->table("customers") . "
						  SET remark = '".$remark."'
						        WHERE customer_id = '" . (int)$customer_id . "'";
		
		$this->db->query($sql);
	}
	
	public function getCustomersByNewsletter() {
		$query = $this->db->query("SELECT *
									FROM " . $this->db->table("customers") . "
									WHERE newsletter = '1'
									ORDER BY firstname, lastname, email");
	
		$result_rows = array();
		foreach ($query->rows as $row) {
			$result_rows[] = $this->dcrypt->decrypt_data($row, 'customers');	
		}		
		return $result_rows;
	}

	/**
	 * @param string $keyword
	 * @return array
	 */
	public function getCustomersByKeyword($keyword) {
		if ($keyword) {
			$query = $this->db->query("SELECT *
									   FROM " . $this->db->table("customers") . "
									   WHERE LCASE(CONCAT(firstname, ' ', lastname)) LIKE '%" . $this->db->escape(strtolower($keyword)) . "%'
									        OR LCASE(email) LIKE '%" . $this->db->escape(strtolower($keyword)) . "%'
									   ORDER BY firstname, lastname, email");
	
			$result_rows = array();
			foreach ($query->rows as $row) {
				$result_rows[] = $this->dcrypt->decrypt_data($row, 'customers');	
			}		
			return $result_rows;
		} else {
			return array();	
		}
	}
	/**
	 * @param array $emails
	 * @return array
	 */
	public function getCustomersByEmails($emails) {
		$emails = (array)$emails;
		if ($emails) {
			$sql = "SELECT *
				   FROM " . $this->db->table("customers") . "
				   WHERE ";
			foreach($emails as $email){
				$where[] = "LCASE(email) LIKE '%" . $this->db->escape(strtolower($email)) . "%'";
			}
			$sql .= implode(' OR ', $where);
			$sql .= "ORDER BY firstname, lastname, email";
			
			// echo $sql;exit();

			$query = $this->db->query($sql);
			$result_rows = array();
			foreach ($query->rows as $row) {
				$result_rows[] = $this->dcrypt->decrypt_data($row, 'customers');
			}
			return $result_rows;
		} else {
			return array();
		}
	}

	/**
	 * @param int $product_id
	 * @return array
	 */
	public function getCustomersByProduct($product_id) {
		if ($product_id) {
			$query = $this->db->query("SELECT DISTINCT `email`
										FROM `" . $this->db->table("orders") . "` o
										LEFT JOIN " . $this->db->table("order_products") . " op ON (o.order_id = op.order_id)
										WHERE op.product_id = '" . (int)$product_id . "' AND o.order_status_id <> '0'");
	
			$result_rows = array();
			foreach ($query->rows as $row) {
				$result_rows[] = $this->dcrypt->decrypt_data($row, 'orders');	
			}		
			return $result_rows;
		} else {
			return array();	
		}
	}

	/**
	 * @param int $customer_id
	 * @return array
	 */
	public function getAddresses($customer_id) {
		$query = $this->db->query("SELECT *
									FROM " . $this->db->table("addresses") . "
									WHERE customer_id = '" . (int)$customer_id . "'");
	
		$result_rows = array();
		foreach ($query->rows as $row) {
			$result_rows[] = $this->dcrypt->decrypt_data($row, 'addresses');	
		}		
		return $result_rows;
	}

	/**
	 * @param string $loginname
	 * @param string $customer_id
	 * @return bool
	 */
	public function is_unique_loginname( $loginname, $customer_id = '' ) {
		if( empty($loginname) ) {
			return false;
		}
		//exclude diven customer from checking
		$not_current_customer = '';
		if ( has_value($customer_id) ) {
			$not_current_customer = "AND customer_id <> '$customer_id'";
		}
      	$query = $this->db->query("SELECT COUNT(*) AS total
      	                           FROM " . $this->db->table("customers") . "
      	                           WHERE LOWER(`loginname`) = LOWER('" . $loginname . "') " . $not_current_customer);
      	if ($query->row['total'] > 0) {
      		return false;
      	} else {
      		return true;
      	}                           
	}
		
	public function getTotalCustomersAwaitingApproval() {
      	$query = $this->db->query("SELECT COUNT(*) AS total
      	                           FROM " . $this->db->table("customers") . "
      	                           WHERE status = '0' OR approved = '0'");

		return $query->row['total'];
	}

	/**
	 * @param int $customer_id
	 * @return mixed
	 */
	public function getTotalAddressesByCustomerId($customer_id) {
      	$query = $this->db->query("SELECT COUNT(*) AS total
      	                            FROM " . $this->db->table("addresses") . "
      	                            WHERE customer_id = '" . (int)$customer_id . "'");
		
		return $query->row['total'];
	}

	/**
	 * @param int $country_id
	 * @return int
	 */
	public function getTotalAddressesByCountryId($country_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total
									FROM " . $this->db->table("addresses") . "
									WHERE country_id = '" . (int)$country_id . "'");
		
		return (int)$query->row['total'];
	}

	/**
	 * @param int $zone_id
	 * @return int
	 */
	public function getTotalAddressesByZoneId($zone_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total
									FROM " . $this->db->table("addresses") . "
									WHERE zone_id = '" . (int)$zone_id . "'");
		
		return (int)$query->row['total'];
	}

	/**
	 * @param int $customer_group_id
	 * @return int
	 */
	public function getTotalCustomersByCustomerGroupId($customer_group_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total
								   FROM " . $this->db->table("customers") . "
								   WHERE customer_group_id = '" . (int)$customer_group_id . "'");
		
		return (int)$query->row['total'];
	}

	public function getAllSubscribers($data=array(), $mode = 'default'){
		$data['filter']['all_subscribers'] = 1;
		return $this->getCustomers($data, $mode);
	}

	public function getOnlyNewsletterSubscribers($data=array(), $mode = 'default'){
		$data['filter']['customer_group_id'] = $this->getSubscribersCustomerGroupId();
		return $this->getCustomers($data, $mode);
	}

	public function getOnlyCustomers($data=array(), $mode = 'default'){
		$data['filter']['only_customers'] = 1;
		return $this->getCustomers($data, $mode);
	}


	public function getSubscribersCustomerGroupId() {
		$query = $this->db->query("SELECT customer_group_id	FROM `" . $this->db->table("customer_groups") . "` WHERE `name` = 'Newsletter Subscribers' LIMIT 0,1");
		$result = !$query->row['customer_group_id'] ? (int)$this->config->get('config_customer_group_id') :  $query->row['customer_group_id'];
		return $result;
	}
  public function getCustomersTempCount() {
      $query = $this->db->query("SELECT COUNT(*) AS total
                     FROM " . $this->db->table("customer_profiles_temp") . "");
      
      return $query->row['total'];
  }    
}