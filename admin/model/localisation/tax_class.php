<?php
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' ) || !IS_ADMIN) {
	header ( 'Location: static_pages/' );
}
class ModelLocalisationTaxClass extends Model {
	public function addTaxClass($data) {
		$this->db->query( "INSERT INTO " . DB_PREFIX . "tax_classes
				SET date_added = NOW() ");
				
		$tax_class_id = $this->db->getLastId();

		$audit_log=array(
      "IP" => $this->request->server['REMOTE_ADDR'],
      "user_login" => $this->session->data['user_id'],
      "Activity" => "Add Tax Class",
      "login" => $this->session->data['user_last_login'],
      "logout" => ''
    );
    	
		foreach ($data['tax_class'] as $language_id => $value) {
			$this->language->replaceDescriptions(
											'tax_class_descriptions',
											 array('tax_class_id' => (int)$tax_class_id),
											 array($language_id => array(
												 'title' => $value['title'],
												 'description' => $value['description'],
											 )),
											false,
											$audit_log);

			$params['tax_class_id'] = (int)$tax_class_id;
			$params['title'] = $value['title'];
			$params['description'] = $value['description'];
			
		}
				
		$this->cache->delete('tax_class');
		return $tax_class_id;
	}

	public function addTaxRate($tax_class_id, $data) {
		$audit_log=array(
	      "IP" => $this->request->server['REMOTE_ADDR'],
	      "user_login" => $this->session->data['user_id'],
	      "Activity" => "Add Tax Rate",
	      "login" => $this->session->data['user_last_login'],
	      "logout" => ''
	    );

		$params['location_id'] = "'".$this->db->escape((int)$data['location_id'])."'";
		$params['zone_id'] = "'".$this->db->escape((int)$data['zone_id'])."'";
		$params['priority'] = "'".$this->db->escape((int)$data['priority'])."'";
		$params['rate'] = "'".$this->db->escape((float)$data['rate'])."'";
		$params['rate_prefix'] = "'".$this->db->escape($data['rate_prefix'])."'";
		$params['threshold_condition'] = "'".$this->db->escape($data['threshold_condition'])."'";
		$params['threshold'] = "'".$this->db->escape((float)$data['threshold'])."'";
		$params['tax_class_id'] = "'".$this->db->escape((int)$tax_class_id)."'";
		$params['date_added'] = "'".$this->db->escape(date("Y-m-d h:i:s"))."'";

		$this->db->insert_with_trail( DB_PREFIX . "tax_rates",$params,$audit_log);

		$tax_rate_id = $this->db->getLastId();
				
		foreach ($data['tax_rate'] as $language_id => $value) {
			$this->language->replaceDescriptions('tax_rate_descriptions',
											 array('tax_rate_id' => (int)$tax_rate_id),
											 array($language_id => array(
												 'description' => $value['description'],
											 )),
											 false,
											 $audit_log );
			$params['tax_rate_id'] = "'".$this->db->escape($data['tax_rate_id'])."'";
			$params['description'] = "'".$this->db->escape($data['description'])."'";
		}

		$this->cache->delete('tax_class');
		return $tax_rate_id;
	}
	
	public function editTaxClass($tax_class_id, $data) {

		if ( count($data['tax_class']) ) {
			$audit_log=array(
	      "IP" => $this->request->server['REMOTE_ADDR'],
	      "user_login" => $this->session->data['user_id'],
	      "Activity" => "Edit Tax Class",
	      "login" => $this->session->data['user_last_login'],
	      "logout" => ''
	    );

	    $data_before = $this->getTaxClass((int)$tax_class_id);
	    
	    $before['tax_class_id'] = $data_before['tax_class_id'];
	    $before['title'] = $data_before['title'];
	    $before['description'] = $data_before['description'];

	    $after['tax_class_id'] = $tax_class_id;
	    foreach($data['tax_class'] as $dt){
		    $after['title'] = $dt['title'];
		    $after['description'] = $dt['description'];
	    }

			$update = array('date_modified = NOW()');
			$this->db->query("UPDATE `" . DB_PREFIX . "tax_classes`
							  SET ". implode(',', $update) ."
							  WHERE tax_class_id = '" . (int)$tax_class_id . "'");
			// $where = "tax_class_id = '" . (int)$tax_class_id . "'";

			// $this->db->update_with_trail(. DB_PREFIX . "tax_classes",$after,$where,$audit_log);

			foreach ($data['tax_class'] as $language_id => $value) {
				//save only if value defined
				if (isset($value['title'])) {
					$this->language->replaceDescriptions('tax_class_descriptions',
											 array('tax_class_id' => (int)$tax_class_id),
											 array($language_id => array(
												 'title' => $value['title'],
											 )),
											 false,
											 $audit_log );
				}
				if (isset($value['description'])) {
					$this->language->replaceDescriptions('tax_class_descriptions',
											 array('tax_class_id' => (int)$tax_class_id),
											 array($language_id => array(
												 'description' => $value['description'],
											 )),
											 false,
											 $audit_log );
				}
				// $this->db->audit_log_only($before,$after,$audit_log);
			}
							  						  
			$this->cache->delete('tax_class');
		}
	}

	public function editTaxRate($tax_rate_id, $data) {
		$audit_log=array(
      "IP" => $this->request->server['REMOTE_ADDR'],
      "user_login" => $this->session->data['user_id'],
      "Activity" => "Edit Tax Rate",
      "login" => $this->session->data['user_last_login'],
      "logout" => ''
    );

    $data_before = $this->getTaxRate($tax_rate_id);
    $data_after = $data;
    unset($data_before['tax_rate'], $data_before['language_id'], $data_before['date_added'], $data_before['date_modified']);
    unset($data_after['tax_rate']);
    
		$fields = array('location_id', 'zone_id', 'priority','rate_prefix', 'threshold_condition' );
		$update = array('date_modified = NOW()');
		$data_after['date_modified'] = "'".$this->db->escape(date("Y-m-d h:i:s"))."'";

		foreach ( $fields as $f ) {
			if ( isset($data[$f]) )
				$update[] = "$f = '".$this->db->escape($data[$f])."'";
				$data_after[$f] = "'".$this->db->escape($data[$f])."'";
		}

		$update[] = "rate = '" . preformatFloat($data['rate'], $this->language->get('decimal_point'))."'";
		$update[] = "threshold = '" . preformatFloat($data['threshold'], $this->language->get('decimal_point'))."'";
		
		$data_after['rate'] = "'" . preformatFloat($data['rate'], $this->language->get('decimal_point'))."'";
		$data_after['threshold'] = "'" . preformatFloat($data['threshold'], $this->language->get('decimal_point'))."'";
		unset($data_after['all_zones']);
		$where = "tax_rate_id = '" . (int)$tax_rate_id . "'";
		
		if ( !empty($update) ) {
			// $this->db->query("UPDATE `" . DB_PREFIX . "tax_rates`
			// 					SET ". implode(',', $update) ."
			// 					WHERE tax_rate_id = '" . (int)$tax_rate_id . "'");
			$this->db->update_with_trail(DB_PREFIX . "tax_rates",$data_after,$where,$audit_log);
			
			$this->cache->delete('tax_class');
			$this->cache->delete('location');
		} 
		if (count($data['tax_rate'])) {
			foreach ($data['tax_rate'] as $language_id => $value) {
				$this->language->replaceDescriptions('tax_rate_descriptions',
												 array('tax_rate_id' => (int)$tax_rate_id),
												 array($language_id => array(
													 'description' => $value['description'],
												 )),
												 false,
												 $audit_log );
				$data_after['tax_rate_id'] = (int)$tax_rate_id;
				$data_after['description'] = $value['description'];
			}		
			$this->cache->delete('tax_class');
			$this->cache->delete('location');
		}
		// $this->db->audit_log_only($data_before,$data_after,$audit_log);
	}
	
	public function deleteTaxClass($tax_class_id) {
		$data_before = $this->getTaxClass((int)$tax_class_id);
    $before['tax_class_id'] = $data_before['tax_class_id'];
    $before['title'] = $data_before['title'];
    $before['description'] = $data_before['description'];
		
		// print_r($before);exit;
		$this->db->query("DELETE FROM " . DB_PREFIX . "tax_classes
							WHERE tax_class_id = '" . (int)$tax_class_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "tax_class_descriptions
							WHERE tax_class_id = '" . (int)$tax_class_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "tax_rates
							WHERE tax_class_id = '" . (int)$tax_class_id . "'");
		$this->cache->delete('tax_class');

		$audit_log=array(
      "IP" => $this->request->server['REMOTE_ADDR'],
      "user_login" => $this->session->data['user_id'],
      "Activity" => "Delete Tax Class",
      "login" => $this->session->data['user_last_login'],
      "logout" => ''
    );

		// $this->db->audit_log_only($before,"",$audit_log);
	}

	public function deleteTaxRate($tax_rate_id) {
		
		$data_before = $this->getTaxRate((int)$tax_rate_id);
		// echo "<pre>";print_r($data_before);exit;
    foreach($data_before['tax_rate'] as $dt){
    	$data_before['description'] = $dt['description'];
    }

		$this->db->query("DELETE FROM " . DB_PREFIX . "tax_rates
							WHERE tax_rate_id = '" . (int)$tax_rate_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "tax_rate_descriptions
							WHERE tax_rate_id = '" . (int)$tax_rate_id . "'");
		$this->cache->delete('tax_class');

		$audit_log=array(
      "IP" => $this->request->server['REMOTE_ADDR'],
      "user_login" => $this->session->data['user_id'],
      "Activity" => "Delete Tax Rate",
      "login" => $this->session->data['user_last_login'],
      "logout" => ''
    );

		// $this->db->audit_log_only($data_before,"",$audit_log);
	}
	
	public function getTaxClass($tax_class_id) {
		$language_id = $this->session->data['content_language_id'];

		$query = $this->db->query("SELECT t.tax_class_id, td1.title, td1.description
									FROM " . DB_PREFIX . "tax_classes t
									LEFT JOIN " . DB_PREFIX . "tax_class_descriptions td1 ON 
									(t.tax_class_id = td1.tax_class_id AND td1.language_id = '" . (int)$language_id . "')
									WHERE t.tax_class_id = '" . (int)$tax_class_id . "'");
		$ret_data = $query->row;
		$ret_data['tax_class'] = $this->getTaxClassDescriptions($tax_class_id); 
		return $ret_data;
	}

	public function getTaxClassDescriptions($tax_class_id) {
		$tax_data = array();
		$query = $this->db->query( "SELECT *
									FROM " . DB_PREFIX . "tax_class_descriptions
									WHERE tax_class_id = '" . (int)$tax_class_id . "'");
		foreach ($query->rows as $result) {
			$tax_data[$result['language_id']] = array('title' => $result['title'], 'description' => $result['description'] );
		}	
		return $tax_data;
	}


	public function getTaxRate($tax_rate_id) {
		$language_id = $this->session->data['content_language_id'];

		$query = $this->db->query("SELECT td1.*, t.*
									FROM " . DB_PREFIX . "tax_rates t
									LEFT JOIN " . DB_PREFIX . "tax_rate_descriptions td1 ON 
									(t.tax_rate_id = td1.tax_rate_id AND td1.language_id = '" . (int)$language_id . "')
									WHERE t.tax_rate_id = '" . (int)$tax_rate_id . "'");
		$ret_data = $query->row;
		$ret_data['tax_rate'] = $this->getTaxRateDescriptions($tax_rate_id); 
		return $ret_data;
	}

	public function getTaxRateDescriptions($tax_rate_id) {
		$tax_data = array();
		$query = $this->db->query( "SELECT *
									FROM " . DB_PREFIX . "tax_rate_descriptions
									WHERE tax_rate_id = '" . (int)$tax_rate_id . "'");
		foreach ($query->rows as $result) {
			$tax_data[$result['language_id']] = array('description' => $result['description']);
		}	
		return $tax_data;
	}

	public function getTaxClasses($data = array(), $mode = 'default') {
		$language_id = $this->session->data['content_language_id'];
		$default_language_id = $this->language->getDefaultLanguageID();

    	if ($data || $mode == 'total_only') {
			if ($mode == 'total_only') {
				$sql = "SELECT count(*) as total FROM " . DB_PREFIX . "tax_classes t ";
			}
			else {
				$sql = "SELECT t.tax_class_id, 
							   td.title,
							   td.description  
						FROM " . DB_PREFIX . "tax_classes t ";
			}
			$sql .= "LEFT JOIN " . DB_PREFIX . "tax_class_descriptions td ON (t.tax_class_id = td.tax_class_id AND td.language_id = '" . (int)$language_id . "') ";
				
		    if ( !empty($data['subsql_filter']) )
				$sql .= " WHERE ".$data['subsql_filter'];

			//If for total, we done bulding the query
			if ($mode == 'total_only') {
		 	   $query = $this->db->query($sql);
		 	   return $query->row['total'];
			}

			$sql .= " ORDER BY td.title";	
			
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
			
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}					

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}
			
	  		$query = $this->db->query($sql);
		
			return $query->rows;		
		} else {
			$tax_class_data = $this->cache->get('tax_class.all', $language_id);

			if (is_null($tax_class_data)) {
				if ($language_id == $default_language_id) {
					$query = $this->db->query( "SELECT *
											FROM " . DB_PREFIX . "tax_classes t
											LEFT JOIN " . DB_PREFIX . "tax_class_descriptions td 
												ON (t.tax_class_id = td.tax_class_id AND td.language_id = '" . (int)$language_id . "') 
											");
							
				} else {
					//merge text for missing country translations. 
					$query = $this->db->query("SELECT t.tax_class_id, 
												COALESCE( td1.title,td2.title) as title, 
												COALESCE( td1.description,td2.description) as description
									FROM " . DB_PREFIX . "tax_classes t
									LEFT JOIN " . DB_PREFIX . "tax_class_descriptions td1 ON 
									(t.tax_class_id = td1.tax_class_id AND td1.language_id = '" . (int)$language_id . "')
									LEFT JOIN " . DB_PREFIX . "tax_class_descriptions td2 ON 
									(t.tax_class_id = td2.tax_class_id AND td2.language_id = '" . (int)$default_lang_id . "')
								");	
				}								
				$tax_class_data = $query->rows;
				$this->cache->set('tax_class.all', $tax_class_data, $language_id);
			}
			
			return $tax_class_data;			
		}
	}
	
	public function getTaxRates($tax_class_id) {
		$language_id = $this->session->data['content_language_id'];
      	$query = $this->db->query("SELECT td.*, t.*
      	                            FROM " . DB_PREFIX . "tax_rates t
									LEFT JOIN " . DB_PREFIX . "tax_rate_descriptions td 
										ON (t.tax_rate_id = td.tax_rate_id AND td.language_id = '" . (int)$language_id . "') 
      	                            WHERE tax_class_id = '" . (int)$tax_class_id . "'");
		
		return $query->rows;
	}

	public function getTotalTaxClasses($data = array()) {
		return $this->getTaxClasses($data, 'total_only');
	}	
	
	public function getTotalTaxRatesByLocationID($location_id) {
      	$query = $this->db->query("SELECT COUNT(*) AS total
      	                           FROM " . DB_PREFIX . "tax_rates
      	                           WHERE location_id = '" . (int)$location_id . "'");
		
		return $query->row['total'];
	}		
}
?>