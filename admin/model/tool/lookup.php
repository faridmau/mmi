<?php
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' ) || !IS_ADMIN) {
	header ( 'Location: static_pages/' );
}
class ModelToolLookup extends Model {
	public function addLookup($data) {

		if ($data['status']==1) {
			$status = 'ON';
		} else {
			$status = 'OFF';
		}

		$params['group_code'] = "'".$this->db->escape($data['group_code'])."'";
		$params['item_code'] = "'".$this->db->escape($data['item_code'])."'";
		$params['item_name'] = "'".$this->db->escape($data['item_name'])."'";
		$params['status'] = "'".$this->db->escape($status)."'";
		$params['created_datetime'] = "'".date('Y-m-d h:i:s')."'";
		
		// print_r($data);exit;

		$audit_log=array(
      "IP" => $this->request->server['REMOTE_ADDR'],
      "user_login" => $this->session->data['user_id'],
      "Activity" => "Add Lookup",
      "login" => $this->session->data['user_last_login'],
      "logout" => ''
    );
    	
    $this->db->insert_with_trail($this->db->table("lookup"),$params,$audit_log);

		$lookup_id = $this->db->getLastId();
		//insert ke table lookup desc
		$sql = "INSERT INTO ".DB_PREFIX."lookup_descriptions (lookup_id, language_id, item_name, created_datetime) 
						VALUES ($lookup_id, ".$this->language->getContentLanguageID().",'".$this->db->escape($data['item_name'])."',NOW())";
		
		$this->db->query($sql);

		return $lookup_id;
	}
	
	public function editLookup($lookup_id, $data) {
			
		$fields = array('group_code','item_code','item_name','status');
		$update = array();
		foreach ( $fields as $f ) {
			if ( isset($data[$f]) )
				// echo $f." | ".$data[$f];
				if ($f=='status') {
					if ($data[$f]==1) {
						// $update[] = "$f = 'ON'";
						$stat_ajx = "'ON'";
						$status[$f] = "'".$this->db->escape('ON')."'";
						$params[$f] = "'".$this->db->escape('ON')."'";
					}else{
						// $update[] = "$f = 'OFF'";
						$stat_ajx = "'OFF'";
						$status[$f] = "'".$this->db->escape('OFF')."'";
						$params[$f] = "'".$this->db->escape('OFF')."'";
					}
				}else{
					$status[$f] = "'".$this->db->escape($data[$f])."'";
					$params[$f] = "'".$this->db->escape($data[$f])."'";
				}	
		}
		//var_dump($status);exit();
		if ( !empty($params) || !empty($status) ){
			$audit_log=array(
	      "IP" => $this->request->server['REMOTE_ADDR'],
	      "user_login" => $this->session->data['user_id'],
	      "Activity" => "Edit Lookup",
	      "login" => $this->session->data['user_last_login'],
	      "logout" => $this->session->data['logout_time']
	    );

			$lang = $this->language->getContentLanguageID();
			$desc['lookup_id'] = $lookup_id;
			$desc['created_by'] = "'".$this->db->escape("system")."'";
			//cek lookup id di lookup description
			// print_r($desc);exit;
			if($this->getLookupDataByID($lookup_id,$lang)){
				$q = $this->db->query("SELECT username FROM " . DB_PREFIX . "users WHERE user_id = '" . (int)$this->session->data['user_id'] . "'");
				$desc['item_name'] = $params['item_name'];
				$desc['updated_by'] = "'".$this->db->escape($q->row['username'])."'";
				$curr_date = date('Y-m-d h:i:s');
	    	$desc['updated_datetime'] = "'".$curr_date."'";

	    	$where_lookup = "lookup_id = '" . (int)$lookup_id . "'"; 
	    	$where = "lookup_id = '" . (int)$lookup_id . "' AND language_id = '".$lang."'";
	    	// $status['status'] = $params['status'];
				if(count($data)==1 && ($data['group_code'] || $data['item_code']||$status['status'])) {
					if ($data["group_code"]){
						$data['group_code'] = "'".$this->db->escape($data["group_code"])."'";
					}elseif($data['item_code']){
						$data['item_code'] = "'".$this->db->escape($data["item_code"])."'";
					}else{
						$data['status'] = $stat_ajx;
					}
					$this->db->update_with_trail($this->db->table("lookup"),$data,$where_lookup,$audit_log); 
					
					return;
					
				}

				if(!empty($status)) $this->db->update_with_trail($this->db->table("lookup"),$status,$where_lookup,$audit_log);
				// var_dump();
				// exit;
				if(!empty($desc)) {
					$this->db->update_with_trail($this->db->table("lookup_descriptions"),$desc,$where,$audit_log);
				}
					
			}else{
				
				$audit_log['Activity'] = "Add Lookup Description";
				$params['lookup_id'] = $lookup_id;
				$params['language_id'] = $lang;
				$curr_date = date('Y-m-d h:i:s');
				$params['created_datetime'] = "'".$curr_date."'";
				$where_lookup = "lookup_id = '" . (int)$lookup_id . "'"; 
				if(count($data)==1 && ($data['group_code'] || $data['item_code']||$status['status'])) {
					$audit_log['Activity'] = "Update Lookup";
					if ($data["group_code"]){
						$data['group_code'] = "'".$this->db->escape($data["group_code"])."'";
					}elseif($data['item_code']){
						$data['item_code'] = "'".$this->db->escape($data["item_code"])."'";
					}else{
						$data['status'] = $stat_ajx;
					}
					//var_dump('$data');exit();
					$this->db->update_with_trail($this->db->table("lookup"),$data,$where_lookup,$audit_log); 
					
					return;
					
				}
				if(!empty($status)) $this->db->update_with_trail($this->db->table("lookup"),$status,$where_lookup,$audit_log);
				unset($params['group_code'],$params['item_code'],$params['status']);
				// var_dump($params);exit();
				$this->db->insert_with_trail($this->db->table("lookup_descriptions"),$params,$audit_log);
				
			}
			
		}
	}
	
	public function deleteManufacturer($manufacturer_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "manufacturers WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "manufacturers_to_stores WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_aliases WHERE query = 'manufacturer_id=" . (int)$manufacturer_id . "'");

		$lm = new ALayoutManager();
		$lm->deletePageLayout('pages/product/manufacturer','manufacturer_id',(int)$manufacturer_id);
		$this->cache->delete('manufacturer');
	}	
	
	public function getLookup($lookup_id, $lang = false) {
		if(!$lang) $lang = $this->language->getContentLanguageID();
		
		$sql = "SELECT l.group_code, l.item_code, l.status, ld.item_name,ld.language_id
						FROM " . DB_PREFIX . "lookup l
						LEFT JOIN ". DB_PREFIX . "lookup_descriptions ld
						ON (l.lookup_id = ld.lookup_id AND language_id = '".$lang."')";
		$sql .= " WHERE l.lookup_id = '" . (int)$lookup_id . "'";
		$query = $this->db->query($sql);
		
		return $query->row;
	}

	public function getLookupIdByName($item_name) {
		$sql = "SELECT lookup_id FROM " . DB_PREFIX . "lookup	WHERE item_name  = '" . $item_name . "'";
		$query = $this->db->query($sql);
		
		return $query->row;
	}

	public function getLookupData($group_code){
		$sql = "SELECT lookup_id,item_name FROM " . $this->db->table("lookup") . " WHERE group_code = '".$group_code."'";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getLookupDescription($item_code,$group_code){
		$lang = $this->language->getContentLanguageID();
		$sql = "SELECT DISTINCT ld.item_name FROM " . $this->db->table("lookup") . " l 
		LEFT JOIN ".$this->db->table("lookup_descriptions")."  ld ON (l.lookup_id = ld.lookup_id AND ld.language_id = '".$lang."') WHERE l.item_code = '".$item_code."'";
		if (!empty($group_code)) {
			$sql .= " AND group_code ='".$group_code."' ";
		}
		$query = $this->db->query($sql);
		return $query->row['item_name'];
	}

	public function getLookupDataByID($ID, $lang = false){
		$lang = (int)$this->language->getContentLanguageID();
		$sql = "SELECT lookup_id,item_name FROM " . $this->db->table("lookup_descriptions") . " ld WHERE lookup_id = '".$ID."'";
		if($lang) $sql .= " AND ld.language_id = '".$lang."' ";
		$query = $this->db->query($sql);
		// echo "<pre>";print_r($query->row);exit;
		return $query->row;
	}
	
	public function getLookups($data = array(), $mode = 'default') {
		if (!empty($data[ 'content_language_id' ])) {
			$language_id = ( int )$data[ 'content_language_id' ];
		} else {
			$language_id = (int)$this->language->getContentLanguageID();
		}

		if ($data) {
			// $curr_lang = $this->language->getContentLanguageID();
			if ($mode == 'total_only') {
				$total_sql = 'count(l.lookup_id) as total';
			}
			else {
				$total_sql = 'l.lookup_id as lookup_id, l.item_code as item_code, l.group_code as group_code, l.status, ld.updated_datetime as updated_time, 
				ld.lookup_desc_id as lookup_desc_id, ld.lookup_id as ldlid, ld.item_name as item_name_desc';
			}
			$sql = "SELECT $total_sql FROM " . DB_PREFIX . "lookup l LEFT JOIN ".DB_PREFIX."lookup_descriptions ld 
			on (l.lookup_id = ld.lookup_id AND ld.language_id = '" . $language_id . "')";

			if ( !empty($data['subsql_filter']) )	$sql .= " WHERE ".$data['subsql_filter'];
			// if(strpos($sql,'item_name')) $sql = str_replace('LOWER(`item_name`)','ld.item_name',$sql);
			// echo $sql."\n";
			//If for total, we done bulding the query
			if ($mode == 'total_only') {
			    $query = $this->db->query($sql);
			    return $query->row['total'];
			}

			// echo $sql; exit;
			// print_r($sql);exit;
					
			$sort_data = array(
				'group_code',
				'item_name',
				'item_code',
				'updated_datetime',
				'status'
			);	

			
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY ld.item_name";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}					

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}				
			
			// echo $sql;exit;

			$query = $this->db->query($sql);
		
			return $query->rows;
		} 
	}

	public function getTotalLookup($data = array()) {
		return $this->getLookups($data, 'total_only');
	}

	public function getGroup() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "lookup GROUP BY group_code");

		return $query->rows;
	}
	
	public function getLookupByGroupItem($group_code,$item_code) {
		$sql = "SELECT count(*) as total FROM " . DB_PREFIX . "lookup WHERE item_code  = '" . $item_code . "' AND group_code = '" . $group_code . "'";
		
		$get_lookup_id = $this->request->get['lookup_id'];

		if (!is_null($get_lookup_id)) {
			$sql .= "AND lookup_id NOT IN ('" . $get_lookup_id . "')";
		}
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}	
?>