<?php
/*------------------------------------------------------------------------------
  $Id$

  AbanteCart, Ideal OpenSource Ecommerce Solution
  http://www.AbanteCart.com

  Copyright © 2011-2014 Belavier Commerce LLC

  This source file is subject to Open Software License (OSL 3.0)
  License details is bundled with this package in the file LICENSE.txt.
  It is also available at this URL:
  <http://www.opensource.org/licenses/OSL-3.0>

 UPGRADE NOTE:
   Do not edit or add to this file if you wish to upgrade AbanteCart to newer
   versions in the future. If you wish to customize AbanteCart for your
   needs please refer to http://www.AbanteCart.com for more information.
------------------------------------------------------------------------------*/
if (! defined ( 'DIR_CORE' ) || !IS_ADMIN) {
	header ( 'Location: static_pages/' );
}
class ModelToolAudit extends Model {
	
	public function getAllAuditDate($data = array(), $mode = 'default') {

		if ($mode == 'total_only') {
			$total_sql = 'count(*) as total';
		}
		else {
			$total_sql = 'audited_datetime';
		}
			
		$sql = "SELECT
		            $total_sql
		        FROM " . DB_PREFIX . "audittrails";
		
		$filter = (isset($data['filter']) ? $data['filter'] : array());
		if (isset($filter['start_date']) && !is_null($filter['end_date'])) {
	    	$sql .= " WHERE audited_datetime BETWEEN '" .date("Y-m-d", strtotime($filter['start_date'])) . "' AND  '".date("Y-m-d", strtotime($filter['end_date']))."'";
	    }
	    // if (isset($filter['status']) && !is_null($filter['status'])) {
	    // 	$sql .= " AND r.status = '" . (int)$filter['status'] . "'";
	    // }
	    
		if ( !empty($data['subsql_filter']) ) {
			$sql .= " AND ".$data['subsql_filter'];
		}

		//If for total, we done bulding the query
		if ($mode == 'total_only') {
			$sql .= " GROUP BY DATE_FORMAT(audited_datetime,'%Y%m%d')";
			// var_dump($sql);exit();
		    $query = $this->db->query($sql);

		    return count($query->rows);
		}
		
		$sort_data = array(
			'audited_datetime' => 'audited_datetime'
		);	
		$sql .= " GROUP BY DATE_FORMAT(audited_datetime,'%Y%m%d')";
		
		if (isset($data['sort']) && in_array($data['sort'], array_keys($sort_data)) ) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY r.date_added";	
		}
			
		if (isset($data['order']) && (strtoupper($data['order']) == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}			

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
			
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}																																							  
		
		$query = $this->db->query($sql);																																				
		
		return $query->rows;	
	}

	public function getTotalAuditDate($data = array()) {

		return $this->getAllAuditDate($data, 'total_only');
	}

	public function trailsByDate($data = array(), $mode = 'default') {
		// $sql = "SELECT * from " . DB_PREFIX . "audittrails a 
		// 		INNER JOIN " . DB_PREFIX . "audittrail_dtl dtl 
		// 		ON a.audit_id = dtl.audit_id";
		
		if ($mode == 'total_only') {
			$total_sql = '*';
		}
		else {
			$total_sql = '*';
		}
			
		$sql = "SELECT
		            $total_sql
		        FROM " . DB_PREFIX . "users u 
		        INNER JOIN " . DB_PREFIX . "audittrails a
		        ON u.user_id = a.audited_user
				INNER JOIN " . DB_PREFIX . "audittrail_dtl dtl 
				ON a.audit_id = dtl.audit_id";
		
		$filter = (isset($data['filter']) ? $data['filter'] : array());
		// if (isset($filter['start_date']) && !is_null($filter['end_date'])) {
	 //    	$sql .= " WHERE audited_datetime BETWEEN '" . $filter['start_date'] . "' AND  '".$filter['end_date']."'";
	 //    }
	    // if (isset($filter['status']) && !is_null($filter['status'])) {
	    // 	$sql .= " AND r.status = '" . (int)$filter['status'] . "'";
	    // }
	    
		if ( !empty($data['subsql_filter']) ) {
			$sql .= " AND ".$data['subsql_filter'];
		}

		//If for total, we done bulding the query
		if ($mode == 'total_only') {
			$sql .= " WHERE a.audited_datetime like '".date("Y-m-d", strtotime($this->request->get['date']))."%'";
		    $query = $this->db->query($sql);
		    return count($query->rows);
		}
		
		$sort_data = array(
			'a.audited_datetime' => 'a.audited_datetime'
		);	
		$sql .= " WHERE a.audited_datetime like '".date("Y-m-d", strtotime($this->request->get['date']))."%'";
		
		if (isset($data['sort']) && in_array($data['sort'], array_keys($sort_data)) ) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY a.audited_datetime";	
		}
			
		if (isset($data['order']) && (strtoupper($data['order']) == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}			

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
			
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}																																							  
		
		$query = $this->db->query($sql);																																				
			
		return $query->rows;
	}
	
	public function getTotalAuditTrails($data = array()) {

		return $this->trailsByDate($data, 'total_only');
	}

	public function editReview($review_id, $data) {

		$allowFields = array( 'product_id','customer_id','author','text','rating','status','date_added');
		$update_data = array(' date_modified = NOW() ');
		foreach ( $data as $key => $val ) {
			if(in_array($key,$allowFields)){
				$update_data[] = "`$key` = '" . $this->db->escape($val) . "' ";
			}
		}
		$this->db->query("UPDATE " . DB_PREFIX . "reviews
						  SET ".implode(',', $update_data)."
						  WHERE review_id = '" . (int)$review_id . "'");
        $this->cache->delete('product.rating.'.(int)$data['product_id']);
        $this->cache->delete('product.reviews.totals');
	}
	
	public function deleteReview($review_id) {
        $review = $this->getReview($review_id);
		$this->db->query("DELETE FROM " . DB_PREFIX . "reviews WHERE review_id = '" . (int)$review_id . "'");
        $this->cache->delete('product.rating.'.(int)$review['product_id']);
        $this->cache->delete('product.reviews.totals');
	}
	
	public function getReview($review_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "reviews WHERE review_id = '" . (int)$review_id . "'");
		
		return $query->row;
	}

	
	
	public function getTotalReviews( $data = array() ) {
		return $this->getReviews($data, 'total_only');
	}
	
	public function getTotalReviewsAwaitingApproval() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "reviews WHERE status = '0'");
		
		return $query->row['total'];
	}


	public function getReviewProducts() {
		$sql = "SELECT DISTINCT r.product_id, pd.name
		FROM " . DB_PREFIX . "reviews r LEFT JOIN " . DB_PREFIX . "product_descriptions pd ON (r.product_id = pd.product_id)
		WHERE pd.language_id = '" . (int)$this->config->get('storefront_language_id') . "'";
		$query = $this->db->query($sql);

		$result =  array();
		foreach ( $query->rows as $row ) {
			$result[ $row['product_id'] ] = $row['name'];
		}

		return $result;
	}
}
?>