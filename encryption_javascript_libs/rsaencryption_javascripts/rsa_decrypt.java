/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package nl.strohalm.cyclos.utils;

import java.io.FileInputStream;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.Security;
import java.security.spec.PKCS8EncodedKeySpec;
import javax.crypto.Cipher;
import org.apache.commons.codec.binary.Base64;

import au.com.safenet.crypto.provider.SAFENETProvider;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import ptdam.emoney.EmoneyConfiguration;

/**
 * Helper class used to hash passwords and manage salts
 * @author luis
 */
public class RSAHandler {
	
	public static String decodePassword(String encoded) throws Exception{
		String result = "";
		result = decodePasswordBC(encoded);
		
		return result;
	}

	public static String decodePasswordBC(String encoded)
		    throws Exception
		  {
		    byte[] encrypedPwd = encoded.getBytes("UTF-8");
		    byte[] decrypedPwd = Base64.decodeBase64(encrypedPwd);
		    
		    String privateKeyLocation = EmoneyConfiguration.getEmoneyProperties().getProperty("rsa.privatekey.der.location");
		    Security.addProvider(new BouncyCastleProvider());
		    PrivateKey privatekey = readPrivateKey(privateKeyLocation);
		    Cipher decrypt_cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		    decrypt_cipher.init(2, privatekey);
		    byte[] plaintext = decrypt_cipher.doFinal(decrypedPwd);
		    String decrypted = new String(plaintext, "UTF-8");
		    if (decrypted.length() == 8) {
		      return decrypted.substring(2);
		    }
		    return decrypted;
		  }

	  public static PrivateKey readPrivateKey(String filename) throws Exception
	  {
	    FileInputStream file = new FileInputStream(filename);
	    byte[] bytes = new byte[file.available()];
	    file.read(bytes);
	    file.close();
	    PKCS8EncodedKeySpec privspec = new PKCS8EncodedKeySpec(bytes);
	    KeyFactory factory = KeyFactory.getInstance("RSA");
	    PrivateKey privkey = factory.generatePrivate(privspec);
	    return privkey;
	  }
}
